The MIT License (MIT)

Copyright (c) 2019 RuoYi-Vue-Plus

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This project includes:
  Apache Commons BeanUtils under Apache License, Version 2.0
  Apache Commons Codec under Apache License, Version 2.0
  Apache Commons Collections under Apache License, Version 2.0
  Apache Commons Compress under Apache License, Version 2.0
  Apache Commons CSV under Apache License, Version 2.0
  Apache Commons IO under Apache License, Version 2.0
  Apache Commons Lang under Apache License, Version 2.0
  Apache Commons Math under Apache License, Version 2.0
  Apache Commons Text under Apache License, Version 2.0
  Apache Commons Validator under Apache License, Version 2.0
  Apache HttpClient under Apache License, Version 2.0
  Apache HttpClient Mime under Apache License, Version 2.0
  Apache HttpCore under Apache License, Version 2.0
  Apache Log4j API under Apache License, Version 2.0
  Apache Log4j to SLF4J Adapter under Apache License, Version 2.0
  Apache POI under Apache License, Version 2.0
  Apache POI - API based on OPC and OOXML schemas under Apache License, Version 2.0
  Apache POI - Common under Apache License, Version 2.0
  AspectJ Weaver under Eclipse Public License - v 2.0
  AWS Java SDK for Amazon S3 under Apache License, Version 2.0
  AWS Java SDK for AWS KMS under Apache License, Version 2.0
  AWS SDK for Java - Core under Apache License, Version 2.0
  bihua-common under The MIT License (MIT)
  bihua-framework under The MIT License (MIT)
  bihua-oss under The MIT License (MIT)
  bihua-sms under The MIT License (MIT)
  bihua-system under The MIT License (MIT)
  bihua-wx under The MIT License (MIT)
  Bouncy Castle Provider under Bouncy Castle Licence
  Byte Buddy (without dependencies) under Apache License, Version 2.0
  Checker Qual under The MIT License
  ClassMate under Apache License, Version 2.0
  com.github.therapi:therapi-runtime-javadoc under The Apache License, Version 2.0
  Commons CLI under The Apache Software License, Version 2.0
  Commons Digester under The Apache Software License, Version 2.0
  Commons Logging under The Apache Software License, Version 2.0
  curvesapi under BSD License
  dom4j under BSD 3-clause New License
  dynamic-datasource-spring-boot-starter under Apache License, Version 2.0
  easyexcel under Apache 2
  easyexcel-core under Apache 2
  easyexcel-support under Apache 2
  Ehcache under The Apache Software License, Version 2.0
  error-prone annotations under Apache 2.0
  fastjson under Apache 2
  FindBugs-jsr305 under The Apache Software License, Version 2.0
  Gson under Apache-2.0
  Guava InternalFutureFailureAccess and InternalFutures under The Apache Software License, Version 2.0
  Guava ListenableFuture only under The Apache Software License, Version 2.0
  Guava: Google Core Libraries for Java under Apache License, Version 2.0
  HdrHistogram under Public Domain, per Creative Commons CC0 or BSD-2-Clause
  Hibernate Validator Engine under Apache License 2.0
  HikariCP under The Apache Software License, Version 2.0
  hutool-captcha under Mulan Permissive Software License，Version 2
  hutool-core under Mulan Permissive Software License，Version 2
  hutool-crypto under Mulan Permissive Software License，Version 2
  hutool-extra under Mulan Permissive Software License，Version 2
  hutool-http under Mulan Permissive Software License，Version 2
  hutool-json under Mulan Permissive Software License，Version 2
  hutool-jwt under Mulan Permissive Software License，Version 2
  hutool-log under Mulan Permissive Software License，Version 2
  hutool-script under Mulan Permissive Software License，Version 2
  hutool-setting under Mulan Permissive Software License，Version 2
  istack common utility code runtime under Eclipse Distribution License - v 1.0
  J2ObjC Annotations under The Apache Software License, Version 2.0
  Jackson dataformat: CBOR under The Apache Software License, Version 2.0
  Jackson datatype: jdk8 under The Apache Software License, Version 2.0
  Jackson datatype: JSR310 under The Apache Software License, Version 2.0
  Jackson-annotations under The Apache Software License, Version 2.0
  Jackson-core under The Apache Software License, Version 2.0
  jackson-databind under The Apache Software License, Version 2.0
  Jackson-dataformat-YAML under The Apache Software License, Version 2.0
  Jackson-module-parameter-names under The Apache Software License, Version 2.0
  Jakarta Activation under EDL 1.0
  Jakarta Activation API jar under EDL 1.0
  Jakarta Annotations API under EPL 2.0 or GPL2 w/ CPE
  Jakarta Bean Validation API under Apache License 2.0
  Jakarta Mail API under EPL 2.0 or GPL2 w/ CPE or EDL 1.0
  Jakarta Servlet under EPL 2.0 or GPL2 w/ CPE
  Jakarta WebSocket - Server API under Eclipse Public License 2.0 or GNU General Public License, version 2 with the GNU Classpath Exception
  Jakarta XML Binding API under Eclipse Distribution License - v 1.0
  Java Servlet API under CDDL + GPLv2 with classpath exception
  JAXB Runtime under Eclipse Distribution License - v 1.0
  JBoss Logging 3 under Apache License, version 2.0
  JBoss Marshalling API under Apache License 2.0
  JBoss Marshalling River under Apache License 2.0
  JBoss Threads under Apache License 2.0
  JCL 1.2 implemented over SLF4J under Apache License, Version 2.0
  JMES Path Query library under Apache License, Version 2.0
  Joda-Time under Apache 2
  Jodd BeanUtil under BSD-2-Clause
  Jodd Core under BSD-2-Clause
  JSQLParser library under GNU Library or Lesser General Public License (LGPL) V2.1 or The Apache Software License, Version 2.0
  JSR107 API and SPI under Apache License, Version 2.0
  JUL to SLF4J bridge under MIT License
  Kryo under 3-Clause BSD License
  LatencyUtils under Public Domain, per Creative Commons CC0
  lock4j-core under Apache License, Version 2.0
  lock4j-redisson-spring-boot-starter under Apache License, Version 2.0
  Logback Classic Module under Eclipse Public License - v 1.0 or GNU Lesser General Public License
  Logback Core Module under Eclipse Public License - v 1.0 or GNU Lesser General Public License
  micrometer-core under The Apache Software License, Version 2.0
  MinLog under 3-Clause BSD License
  MXParser under Indiana University Extreme! Lab Software License
  mybatis under The Apache Software License, Version 2.0
  mybatis-plus under The Apache License, Version 2.0
  mybatis-spring under The Apache Software License, Version 2.0
  Netty/All-in-One under Apache License, Version 2.0
  Netty/Buffer under Apache License, Version 2.0
  Netty/Codec under Apache License, Version 2.0
  Netty/Codec/DNS under Apache License, Version 2.0
  Netty/Codec/HAProxy under Apache License, Version 2.0
  Netty/Codec/HTTP under Apache License, Version 2.0
  Netty/Codec/HTTP2 under Apache License, Version 2.0
  Netty/Codec/Memcache under Apache License, Version 2.0
  Netty/Codec/MQTT under Apache License, Version 2.0
  Netty/Codec/Redis under Apache License, Version 2.0
  Netty/Codec/SMTP under Apache License, Version 2.0
  Netty/Codec/Socks under Apache License, Version 2.0
  Netty/Codec/Stomp under Apache License, Version 2.0
  Netty/Codec/XML under Apache License, Version 2.0
  Netty/Common under Apache License, Version 2.0
  Netty/Handler under Apache License, Version 2.0
  Netty/Handler/Proxy under Apache License, Version 2.0
  Netty/Handler/Ssl/Ocsp under Apache License, Version 2.0
  Netty/Resolver under Apache License, Version 2.0
  Netty/Resolver/DNS under Apache License, Version 2.0
  Netty/Resolver/DNS/Classes/MacOS under Apache License, Version 2.0
  Netty/Resolver/DNS/Native/MacOS under Apache License, Version 2.0
  Netty/Transport under Apache License, Version 2.0
  Netty/Transport/Classes/Epoll under Apache License, Version 2.0
  Netty/Transport/Classes/KQueue under Apache License, Version 2.0
  Netty/Transport/Native/Epoll under Apache License, Version 2.0
  Netty/Transport/Native/KQueue under Apache License, Version 2.0
  Netty/Transport/Native/Unix/Common under Apache License, Version 2.0
  Netty/Transport/RXTX under Apache License, Version 2.0
  Netty/Transport/SCTP under Apache License, Version 2.0
  Netty/Transport/UDT under Apache License, Version 2.0
  Non-Blocking Reactive Foundation for the JVM under Apache License, Version 2.0
  Objenesis under Apache License, Version 2.0
  p6spy under The Apache Software License, Version 2.0
  Project Lombok under The MIT License
  reactive-streams under MIT-0
  Redisson under Apache v2
  Redisson/Spring Boot Starter under Apache v2
  Redisson/Spring Data Redis v2.7.x integration under Apache v2
  ReflectASM under 3-Clause BSD License
  RocketMQ Spring Boot AutoConfigure under Apache License, Version 2.0
  RocketMQ Spring Boot Starter under Apache License, Version 2.0
  rocketmq-acl 4.9.1 under Apache License, Version 2.0
  rocketmq-client 4.9.1 under Apache License, Version 2.0
  rocketmq-common 4.9.1 under Apache License, Version 2.0
  rocketmq-logging 4.9.1 under Apache License, Version 2.0
  rocketmq-remoting 4.9.1 under Apache License, Version 2.0
  rocketmq-srvutil 4.9.1 under Apache License, Version 2.0
  RxJava under The Apache Software License, Version 2.0
  sa-token-core under Apache 2
  sa-token-jwt under Apache 2
  sa-token-servlet under Apache 2
  sa-token-spring-boot-autoconfig under Apache 2
  sa-token-spring-boot-starter under Apache 2
  SLF4J API Module under MIT License
  SnakeYAML under Apache License, Version 2.0
  software.amazon.ion:ion-java under The Apache License, Version 2.0
  SparseBitSet under The Apache Software License, Version 2.0
  Spring AOP under Apache License, Version 2.0
  Spring Beans under Apache License, Version 2.0
  Spring Boot Admin Client under Apache License, Version 2.0
  Spring Boot Admin Client Starter under Apache License, Version 2.0
  Spring Commons Logging Bridge under Apache License, Version 2.0
  Spring Context under Apache License, Version 2.0
  Spring Context Support under Apache License, Version 2.0
  Spring Core under Apache License, Version 2.0
  Spring Data Core under Apache License, Version 2.0
  Spring Data KeyValue under Apache License, Version 2.0
  Spring Data Redis under Apache License, Version 2.0
  Spring Expression Language (SpEL) under Apache License, Version 2.0
  Spring JDBC under Apache License, Version 2.0
  Spring Messaging under Apache License, Version 2.0
  Spring Object/XML Marshalling under Apache License, Version 2.0
  Spring Transaction under Apache License, Version 2.0
  Spring Web under Apache License, Version 2.0
  Spring Web MVC under Apache License, Version 2.0
  spring-boot under Apache License, Version 2.0
  spring-boot-actuator under Apache License, Version 2.0
  spring-boot-actuator-autoconfigure under Apache License, Version 2.0
  spring-boot-autoconfigure under Apache License, Version 2.0
  spring-boot-configuration-processor under Apache License, Version 2.0
  spring-boot-starter under Apache License, Version 2.0
  spring-boot-starter-actuator under Apache License, Version 2.0
  spring-boot-starter-aop under Apache License, Version 2.0
  spring-boot-starter-data-redis under Apache License, Version 2.0
  spring-boot-starter-jdbc under Apache License, Version 2.0
  spring-boot-starter-json under Apache License, Version 2.0
  spring-boot-starter-logging under Apache License, Version 2.0
  spring-boot-starter-undertow under Apache License, Version 2.0
  spring-boot-starter-validation under Apache License, Version 2.0
  spring-boot-starter-web under Apache License, Version 2.0
  springdoc-openapi-common under The Apache License, Version 2.0
  springdoc-openapi-javadoc under The Apache License, Version 2.0
  springdoc-openapi-webmvc-core under The Apache License, Version 2.0
  swagger-annotations under Apache License 2.0
  swagger-core under Apache License 2.0
  swagger-models under Apache License 2.0
  tomcat-embed-el under Apache License, Version 2.0
  TransmittableThreadLocal(TTL) under Apache 2
  TXW2 Runtime under Eclipse Distribution License - v 1.0
  Undertow Core under Apache License Version 2.0
  Undertow Servlet under Apache License Version 2.0
  Undertow WebSockets JSR356 implementations under Apache License Version 2.0
  WildFly Client Configuration under Apache License 2.0
  wildfly-common under Apache License 2.0
  WxJava - Common Java SDK under The Apache License, Version 2.0
  WxJava - MP Java SDK under The Apache License, Version 2.0
  XML Commons External Components XML APIs under The Apache Software License, Version 2.0 or The SAX License or The W3C License
  XML Pull Parsing API under Public Domain
  XmlBeans under The Apache Software License, Version 2.0
  XNIO API under Apache License 2.0
  XNIO NIO Implementation under Apache License 2.0
  XStream Core under BSD-3-Clause


This project also includes code under copywrite of the following entities:
  http://code.google.com/p/maven-license-plugin/
  https://gitee.com/dromara/RuoYi-Vue-Plus.git
  https://gitee.com/Ning310975876/ruo-yi-vue-blog.git
  https://gitee.com/mqttsnet/thinglinks.git
  https://github.com/niefy/wx-api.git
  https://github.com/niefy/wx-manage.git
