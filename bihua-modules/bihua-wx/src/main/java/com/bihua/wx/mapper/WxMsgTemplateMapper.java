package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxMsgTemplate;
import com.bihua.wx.domain.vo.WxMsgTemplateVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 消息模板Mapper接口
 *
 * @author bihua
 * @date 2023-05-22
 */
public interface WxMsgTemplateMapper extends BaseMapperPlus<WxMsgTemplateMapper, WxMsgTemplate, WxMsgTemplateVo> {

}
