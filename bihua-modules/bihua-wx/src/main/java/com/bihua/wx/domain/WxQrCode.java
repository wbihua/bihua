package com.bihua.wx.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 公众号带参二维码对象 wx_qr_code
 *
 * @author bihua
 * @date 2023-05-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_qr_code")
public class WxQrCode extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * appid
     */
    private String appid;
    /**
     * 是否为临时二维码
     */
    private Boolean isTemp;
    /**
     * 场景值ID
     */
    private String sceneStr;
    /**
     * 二维码ticket
     */
    private String ticket;
    /**
     * 二维码图片解析后的地址
     */
    private String url;
    /**
     * 该二维码失效时间
     */
    private Date expireTime;

}
