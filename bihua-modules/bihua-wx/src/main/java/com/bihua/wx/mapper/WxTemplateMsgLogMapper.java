package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxTemplateMsgLog;
import com.bihua.wx.domain.vo.WxTemplateMsgLogVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 微信模版消息发送记录Mapper接口
 *
 * @author bihua
 * @date 2023-05-22
 */
public interface WxTemplateMsgLogMapper extends BaseMapperPlus<WxTemplateMsgLogMapper, WxTemplateMsgLog, WxTemplateMsgLogVo> {

}
