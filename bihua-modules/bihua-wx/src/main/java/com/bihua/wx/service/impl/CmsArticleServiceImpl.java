package com.bihua.wx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.wx.domain.bo.CmsArticleBo;
import com.bihua.wx.domain.vo.CmsArticleVo;
import com.bihua.wx.domain.CmsArticle;
import com.bihua.wx.mapper.CmsArticleMapper;
import com.bihua.wx.service.ICmsArticleService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * CMS文章中心Service业务层处理
 *
 * @author bihua
 * @date 2023-05-24
 */
@RequiredArgsConstructor
@Service
public class CmsArticleServiceImpl implements ICmsArticleService {

    private final CmsArticleMapper baseMapper;

    /**
     * 查询CMS文章中心
     */
    @Override
    public CmsArticleVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询CMS文章中心列表
     */
    @Override
    public TableDataInfo<CmsArticleVo> queryPageList(CmsArticleBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsArticle> lqw = buildQueryWrapper(bo);
        Page<CmsArticleVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询CMS文章中心列表
     */
    @Override
    public List<CmsArticleVo> queryList(CmsArticleBo bo) {
        LambdaQueryWrapper<CmsArticle> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsArticle> buildQueryWrapper(CmsArticleBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsArticle> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getType() != null, CmsArticle::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getTitle()), CmsArticle::getTitle, bo.getTitle());
        lqw.eq(StringUtils.isNotBlank(bo.getSummary()), CmsArticle::getSummary, bo.getSummary());
        lqw.eq(StringUtils.isNotBlank(bo.getTags()), CmsArticle::getTags, bo.getTags());
        lqw.eq(StringUtils.isNotBlank(bo.getContent()), CmsArticle::getContent, bo.getContent());
        lqw.eq(StringUtils.isNotBlank(bo.getCategory()), CmsArticle::getCategory, bo.getCategory());
        lqw.eq(StringUtils.isNotBlank(bo.getSubCategory()), CmsArticle::getSubCategory, bo.getSubCategory());
        lqw.eq(bo.getOpenCount() != null, CmsArticle::getOpenCount, bo.getOpenCount());
        lqw.eq(bo.getStartTime() != null, CmsArticle::getStartTime, bo.getStartTime());
        lqw.eq(bo.getEndTime() != null, CmsArticle::getEndTime, bo.getEndTime());
        lqw.eq(StringUtils.isNotBlank(bo.getTargetLink()), CmsArticle::getTargetLink, bo.getTargetLink());
        lqw.eq(StringUtils.isNotBlank(bo.getImage()), CmsArticle::getImage, bo.getImage());
        return lqw;
    }

    /**
     * 新增CMS文章中心
     */
    @Override
    public Boolean insertByBo(CmsArticleBo bo) {
        CmsArticle add = BeanUtil.toBean(bo, CmsArticle.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改CMS文章中心
     */
    @Override
    public Boolean updateByBo(CmsArticleBo bo) {
        CmsArticle update = BeanUtil.toBean(bo, CmsArticle.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsArticle entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除CMS文章中心
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
