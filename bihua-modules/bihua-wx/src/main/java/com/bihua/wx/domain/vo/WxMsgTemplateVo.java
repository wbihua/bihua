package com.bihua.wx.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import lombok.Data;


/**
 * 消息模板视图对象 wx_msg_template
 *
 * @author bihua
 * @date 2023-05-22
 */
@Data
@ExcelIgnoreUnannotated
public class WxMsgTemplateVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 公众号模板ID
     */
    @ExcelProperty(value = "公众号模板ID")
    private String templateId;

    /**
     * 模版名称
     */
    @ExcelProperty(value = "模版名称")
    private String name;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 模板内容
     */
    @ExcelProperty(value = "模板内容")
    private String content;

    /**
     * 消息内容
     */
    @ExcelProperty(value = "消息内容")
    private JSONArray data;

    /**
     * 链接
     */
    @ExcelProperty(value = "链接")
    private String url;

    /**
     * 小程序信息
     */
    @ExcelProperty(value = "小程序信息")
    private JSONObject miniprogram;

    /**
     * 是否有效
     */
    @ExcelProperty(value = "是否有效")
    private Boolean status;


}
