package com.bihua.wx.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * CMS文章中心视图对象 cms_article
 *
 * @author bihua
 * @date 2023-05-24
 */
@Data
@ExcelIgnoreUnannotated
public class CmsArticleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 文章类型[1:普通文章,5:帮助中心]
     */
    @ExcelProperty(value = "文章类型[1:普通文章,5:帮助中心]")
    private Integer type;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 文章摘要
     */
    @ExcelProperty(value = "文章摘要")
    private String summary;

    /**
     * 文章标签
     */
    @ExcelProperty(value = "文章标签")
    private String tags;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String content;

    /**
     * 分类
     */
    @ExcelProperty(value = "分类")
    private String category;

    /**
     * 二级目录
     */
    @ExcelProperty(value = "二级目录")
    private String subCategory;

    /**
     * 点击次数
     */
    @ExcelProperty(value = "点击次数")
    private Integer openCount;

    /**
     * 生效时间
     */
    @ExcelProperty(value = "生效时间")
    private Date startTime;

    /**
     * 失效时间
     */
    @ExcelProperty(value = "失效时间")
    private Date endTime;

    /**
     * 指向外链
     */
    @ExcelProperty(value = "指向外链")
    private String targetLink;

    /**
     * 文章首图OSSID
     */
    @ExcelProperty(value = "文章首图OSSID")
    private String ossId;

    /**
     * 文章首图
     */
    @ExcelProperty(value = "文章首图")
    private String image;


}
