package com.bihua.wx.domain.bo;

import java.sql.Time;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自动回复规则业务对象 wx_msg_reply_rule
 *
 * @author ruoyi
 * @date 2023-05-22
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxMsgReplyRuleBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long ruleId;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 规则名称
     */
    @NotBlank(message = "规则名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ruleName;

    /**
     * 匹配的关键词、事件等
     */
    @NotBlank(message = "匹配的关键词、事件等不能为空", groups = { AddGroup.class, EditGroup.class })
    private String matchValue;

    /**
     * 是否精确匹配
     */
    @NotNull(message = "是否精确匹配不能为空", groups = { AddGroup.class, EditGroup.class })
    private boolean exactMatch;

    /**
     * 回复消息类型
     */
    @NotBlank(message = "回复消息类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String replyType;

    /**
     * 回复消息内容
     */
    @NotBlank(message = "回复消息内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String replyContent;

    /**
     * 规则是否有效
     */
    @NotNull(message = "规则是否有效不能为空", groups = { AddGroup.class, EditGroup.class })
    private boolean status;

    /**
     * 备注说明
     */
    private String desc;

    /**
     * 生效起始时间
     */
    @NotNull(message = "生效起始时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Time effectTimeStart;

    /**
     * 生效结束时间
     */
    @NotNull(message = "生效结束时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Time effectTimeEnd;

    /**
     * 规则优先级
     */
    @NotNull(message = "规则优先级不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer priority;


}
