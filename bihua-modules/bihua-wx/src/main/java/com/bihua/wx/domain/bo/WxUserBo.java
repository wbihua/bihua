package com.bihua.wx.domain.bo;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户业务对象 wx_user
 *
 * @author ruoyi
 * @date 2023-05-21
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxUserBo extends BaseEntity {

    /**
     * 微信openid
     */
    @NotBlank(message = "微信openid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String openid;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String phone;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nickname;

    /**
     * 性别(0-未知、1-男、2-女)
     */
    @NotNull(message = "性别(0-未知、1-男、2-女)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer sex;

    /**
     * 城市
     */
    @NotBlank(message = "城市不能为空", groups = { AddGroup.class, EditGroup.class })
    private String city;

    /**
     * 省份
     */
    @NotBlank(message = "省份不能为空", groups = { AddGroup.class, EditGroup.class })
    private String province;

    /**
     * 头像
     */
    @NotBlank(message = "头像不能为空", groups = { AddGroup.class, EditGroup.class })
    private String headimgurl;

    /**
     * 订阅时间
     */
    @NotNull(message = "订阅时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date subscribeTime;

    /**
     * 是否关注
     */
    @NotNull(message = "是否关注不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean subscribe;

    /**
     * unionid
     */
    @NotBlank(message = "unionid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String unionid;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;

    /**
     * 标签ID列表
     */
    @NotBlank(message = "标签ID列表不能为空", groups = { AddGroup.class, EditGroup.class })
    private List<Long> tagidList;

    /**
     * 关注场景
     */
    @NotBlank(message = "关注场景不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subscribeScene;

    /**
     * 扫码场景值
     */
    @NotBlank(message = "扫码场景值不能为空", groups = { AddGroup.class, EditGroup.class })
    private String qrSceneStr;


}
