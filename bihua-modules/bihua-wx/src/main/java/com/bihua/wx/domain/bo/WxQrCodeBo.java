package com.bihua.wx.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 公众号带参二维码业务对象 wx_qr_code
 *
 * @author bihua
 * @date 2023-05-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxQrCodeBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 是否为临时二维码
     */
    @NotNull(message = "是否为临时二维码不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean isTemp;

    /**
     * 场景值ID
     */
    @NotBlank(message = "场景值ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String sceneStr;

    /**
     * 二维码ticket
     */
    @NotBlank(message = "二维码ticket不能为空", groups = { EditGroup.class })
    private String ticket;

    /**
     * 二维码图片解析后的地址
     */
    @NotBlank(message = "二维码图片解析后的地址不能为空", groups = { EditGroup.class })
    private String url;

    /**
     * 该二维码失效时间
     */
    @NotNull(message = "该二维码失效时间不能为空", groups = { EditGroup.class })
    private Date expireTime;

    @Max(value = 2592000, message = "过期时间不得超过30天")
    private Integer expireSeconds;


}
