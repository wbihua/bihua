package com.bihua.wx.domain;

import java.util.Date;

import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;
import com.bihua.framework.handler.JSONArrayTypeHandler;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import me.chanjar.weixin.mp.bean.result.WxMpUser;

/**
 * 用户对象 wx_user
 *
 * @author ruoyi
 * @date 2023-05-21
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_user")
@NoArgsConstructor
public class WxUser extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 微信openid
     */
    @TableId(value = "openid", type = IdType.INPUT)
    private String openid;
    /**
     * appid
     */
    private String appid;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别(0-未知、1-男、2-女)
     */
    private Integer sex;
    /**
     * 城市
     */
    private String city;
    /**
     * 省份
     */
    private String province;
    /**
     * 头像
     */
    private String headimgurl;
    /**
     * 订阅时间
     */
    private Date subscribeTime;
    /**
     * 是否关注
     */
    private Boolean subscribe;
    /**
     * unionid
     */
    private String unionid;
    /**
     * 备注
     */
    private String remark;
    /**
     * 标签ID列表
     */
    @TableField(typeHandler = JSONArrayTypeHandler.class)
    private JSONArray tagidList;
    /**
     * 关注场景
     */
    private String subscribeScene;
    /**
     * 扫码场景值
     */
    private String qrSceneStr;

    public WxUser(WxMpUser wxMpUser, String appid) {
        this.openid = wxMpUser.getOpenId();
        this.appid = appid;
        this.subscribe=wxMpUser.getSubscribe();
        if(wxMpUser.getSubscribe()){
            this.nickname = wxMpUser.getNickname();
            this.headimgurl = wxMpUser.getHeadImgUrl();
            this.subscribeTime = new Date(wxMpUser.getSubscribeTime()*1000);
            this.unionid=wxMpUser.getUnionId();
            this.remark=wxMpUser.getRemark();
            this.tagidList= JSONArray.parseArray(JSONObject.toJSONString(wxMpUser.getTagIds()));
            this.subscribeScene=wxMpUser.getSubscribeScene();
            String qrScene =  wxMpUser.getQrScene();
            this.qrSceneStr= !StringUtils.hasText(qrScene) ? wxMpUser.getQrSceneStr() : qrScene;
        }
    }
}
