package com.bihua.wx.service;

import com.bihua.wx.domain.WxMsg;
import com.bihua.wx.domain.bo.WxMsgQueryBo;
import com.bihua.wx.domain.vo.WxMsgVo;
import com.bihua.wx.domain.bo.WxMsgBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 微信消息Service接口
 *
 * @author bihua
 * @date 2023-05-23
 */
public interface IWxMsgService {

    /**
     * 查询微信消息
     */
    WxMsgVo queryById(Long id);

    /**
     * 查询微信消息列表
     */
    TableDataInfo<WxMsgVo> queryPageList(WxMsgQueryBo bo, PageQuery pageQuery);

    /**
     * 查询微信消息列表
     */
    List<WxMsgVo> queryList(WxMsgQueryBo bo);

    /**
     * 新增微信消息
     */
    Boolean insertByBo(WxMsgBo bo);

    /**
     * 修改微信消息
     */
    Boolean updateByBo(WxMsgBo bo);

    /**
     * 校验并批量删除微信消息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
