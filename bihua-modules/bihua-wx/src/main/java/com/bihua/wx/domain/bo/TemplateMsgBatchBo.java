package com.bihua.wx.domain.bo;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Data;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

/**
 * @author bihua
 * @date 2023年05月23日 10:06
 */
@Data
public class TemplateMsgBatchBo {
    @NotNull(message = "需用户筛选条件参数")
    WxUserBo wxUserFilterParams;
    @NotEmpty(message = "模板ID不得为空")
    private String templateId;
    private String url;
    private WxMpTemplateMessage.MiniProgram miniprogram;
    @NotEmpty(message = "消息模板数据不得为空")
    private List<WxMpTemplateData> data;
}
