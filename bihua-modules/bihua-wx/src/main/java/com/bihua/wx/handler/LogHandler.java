package com.bihua.wx.handler;

import java.util.Map;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.wx.domain.WxMsg;
import com.bihua.wx.domain.bo.WxMsgBo;
import com.bihua.wx.service.IWxMsgService;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

/**
 * @author Binary Wang
 */
@RequiredArgsConstructor
@Component
public class LogHandler extends AbstractHandler {

    private final IWxMsgService wxMsgService;

    @Override
    public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage,
                                    Map<String, Object> context, WxMpService wxMpService,
                                    WxSessionManager sessionManager) {
        try {
            this.logger.debug("\n接收到请求消息，内容：{}", JSON.toJSONString(wxMessage));
            wxMsgService.insertByBo(BeanCopyUtils.copy(new WxMsg(wxMessage), WxMsgBo.class));
        } catch (Exception e) {
            this.logger.error("记录消息异常",e);
        }

        return null;
    }

}
