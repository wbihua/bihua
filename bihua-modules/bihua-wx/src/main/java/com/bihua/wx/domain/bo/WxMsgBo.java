package com.bihua.wx.domain.bo;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.framework.handler.JSONObjectTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 微信消息业务对象 wx_msg
 *
 * @author bihua
 * @date 2023-05-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxMsgBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 微信用户ID
     */
    @NotBlank(message = "微信用户ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String openid;

    /**
     * 消息方向
     */
    private byte inOut;

    /**
     * 消息类型
     */
    @NotBlank(message = "消息类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String msgType;

    /**
     * 消息详情
     */
    private JSONObject detail;


}
