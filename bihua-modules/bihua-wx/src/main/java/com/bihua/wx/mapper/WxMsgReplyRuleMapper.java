package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxMsgReplyRule;
import com.bihua.wx.domain.vo.WxMsgReplyRuleVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 自动回复规则Mapper接口
 *
 * @author ruoyi
 * @date 2023-05-22
 */
public interface WxMsgReplyRuleMapper extends BaseMapperPlus<WxMsgReplyRuleMapper, WxMsgReplyRule, WxMsgReplyRuleVo> {

}
