package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxUser;
import com.bihua.wx.domain.vo.WxUserVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 用户Mapper接口
 *
 * @author ruoyi
 * @date 2023-05-21
 */
public interface WxUserMapper extends BaseMapperPlus<WxUserMapper, WxUser, WxUserVo> {

    void unsubscribe(String openid);
}
