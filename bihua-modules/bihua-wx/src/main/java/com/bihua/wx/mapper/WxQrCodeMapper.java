package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxQrCode;
import com.bihua.wx.domain.vo.WxQrCodeVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 公众号带参二维码Mapper接口
 *
 * @author bihua
 * @date 2023-05-19
 */
public interface WxQrCodeMapper extends BaseMapperPlus<WxQrCodeMapper, WxQrCode, WxQrCodeVo> {

}
