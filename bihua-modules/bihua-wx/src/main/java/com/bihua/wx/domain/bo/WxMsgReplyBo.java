package com.bihua.wx.domain.bo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

/**
 * @author bihua
 * @date 2023年05月23日 22:12
 */
@Data
public class WxMsgReplyBo {
    @NotEmpty(message = "用户信息不得为空")
    private String openid;
    @NotEmpty(message = "回复类型不得为空")
    private String replyType;
    @NotEmpty(message = "回复内容不得为空")
    private String replyContent;
}
