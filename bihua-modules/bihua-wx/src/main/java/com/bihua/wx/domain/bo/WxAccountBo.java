package com.bihua.wx.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 公众号账号业务对象 wx_account
 *
 * @author ruoyi
 * @date 2023-05-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxAccountBo extends BaseEntity {

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { EditGroup.class })
    private String appid;

    /**
     * 公众号名称
     */
    @NotBlank(message = "公众号名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 账号类型
     */
    private Integer type;

    /**
     * 认证状态
     */
    private boolean verified;

    /**
     * appsecret
     */
    @NotBlank(message = "appsecret不能为空", groups = { AddGroup.class, EditGroup.class })
    private String secret;

    /**
     * token
     */
    private String token;

    /**
     * aesKey
     */
    private String aesKey;


}
