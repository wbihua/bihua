package com.bihua.wx.domain.bo;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;
import java.util.List;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 消息模板业务对象 wx_msg_template
 *
 * @author bihua
 * @date 2023-05-22
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxMsgTemplateBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 公众号模板ID
     */
    @NotBlank(message = "公众号模板ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String templateId;

    /**
     * 模版名称
     */
    @NotBlank(message = "模版名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;

    /**
     * 模板内容
     */
    @NotBlank(message = "模板内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 消息内容
     */
    private JSONArray data;

    /**
     * 链接
     */
    private String url;

    /**
     * 小程序信息
     */
    private JSONObject miniprogram;

    /**
     * 是否有效
     */
    @NotNull(message = "是否有效不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean status;


}
