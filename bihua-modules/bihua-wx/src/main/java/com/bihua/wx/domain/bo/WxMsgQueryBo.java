package com.bihua.wx.domain.bo;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

/**
 * @author bihua
 * @date 2023年05月23日 20:41
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxMsgQueryBo extends BaseEntity {
    /**
     * appid
     */
    private String appid;

    /**
     * 微信用户ID
     */
    private String openid;

    /**
     * 消息类型
     */
    private String msgTypes;

    /**
     * 开始时间
     */
    private String startTime;


}
