package com.bihua.wx.config;

import com.bihua.common.utils.spring.SpringUtils;
import com.bihua.system.service.ISysConfigService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;

@RequiredArgsConstructor
@Configuration
public class WxMpServiceConfiguration {

    @Bean
    public WxMpService wxMpService() {
        WxMpService wxMpService =  new WxMpServiceImpl();
        String maxRetryTimes = SpringUtils.getBean(ISysConfigService.class).selectConfigByKey("wx.maxRetryTimes");
        wxMpService.setMaxRetryTimes(Integer.valueOf(maxRetryTimes));
        return wxMpService;
    }
}
