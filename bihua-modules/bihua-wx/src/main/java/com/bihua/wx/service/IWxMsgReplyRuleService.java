package com.bihua.wx.service;

import com.bihua.wx.domain.WxMsgReplyRule;
import com.bihua.wx.domain.vo.WxMsgReplyRuleVo;
import com.bihua.wx.domain.bo.WxMsgReplyRuleBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 自动回复规则Service接口
 *
 * @author ruoyi
 * @date 2023-05-22
 */
public interface IWxMsgReplyRuleService {

    /**
     * 查询自动回复规则
     */
    WxMsgReplyRuleVo queryById(Long ruleId);

    /**
     * 查询自动回复规则列表
     */
    TableDataInfo<WxMsgReplyRuleVo> queryPageList(WxMsgReplyRuleBo bo, PageQuery pageQuery);

    /**
     * 查询自动回复规则列表
     */
    List<WxMsgReplyRuleVo> queryList(WxMsgReplyRuleBo bo);

    /**
     * 获取当前时段内所有有效的回复规则
     *
     * @return 有效的规则列表
     */
    List<WxMsgReplyRuleVo> getValidRules();
    /**
     * 新增自动回复规则
     */
    Boolean insertByBo(WxMsgReplyRuleBo bo);

    /**
     * 修改自动回复规则
     */
    Boolean updateByBo(WxMsgReplyRuleBo bo);

    /**
     * 校验并批量删除自动回复规则信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
