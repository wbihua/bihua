package com.bihua.wx.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.wx.domain.bo.WxTemplateMsgLogBo;
import com.bihua.wx.domain.vo.WxTemplateMsgLogVo;

/**
 * 微信模版消息发送记录Service接口
 *
 * @author bihua
 * @date 2023-05-22
 */
public interface IWxTemplateMsgLogService {

    /**
     * 查询微信模版消息发送记录
     */
    WxTemplateMsgLogVo queryById(Long logId);

    /**
     * 查询微信模版消息发送记录列表
     */
    TableDataInfo<WxTemplateMsgLogVo> queryPageList(WxTemplateMsgLogBo bo, PageQuery pageQuery);

    /**
     * 查询微信模版消息发送记录列表
     */
    List<WxTemplateMsgLogVo> queryList(WxTemplateMsgLogBo bo);

    /**
     * 校验并批量删除微信模版消息发送记录信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
