package com.bihua.wx.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 微信模版消息发送记录业务对象 wx_template_msg_log
 *
 * @author bihua
 * @date 2023-05-22
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class WxTemplateMsgLogBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long logId;

    /**
     * appid
     */
    @NotBlank(message = "appid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appid;

    /**
     * 用户openid
     */
    @NotBlank(message = "用户openid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String touser;

    /**
     * templateid
     */
    @NotBlank(message = "templateid不能为空", groups = { AddGroup.class, EditGroup.class })
    private String templateId;

    /**
     * 消息数据
     */
    @NotBlank(message = "消息数据不能为空", groups = { AddGroup.class, EditGroup.class })
    private List<String> data;

    /**
     * 消息链接
     */
    @NotBlank(message = "消息链接不能为空", groups = { AddGroup.class, EditGroup.class })
    private String url;

    /**
     * 小程序信息
     */
    @NotBlank(message = "小程序信息不能为空", groups = { AddGroup.class, EditGroup.class })
    private String miniprogram;

    /**
     * 发送时间
     */
    @NotNull(message = "发送时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date sendTime;

    /**
     * 发送结果
     */
    @NotBlank(message = "发送结果不能为空", groups = { AddGroup.class, EditGroup.class })
    private String sendResult;


}
