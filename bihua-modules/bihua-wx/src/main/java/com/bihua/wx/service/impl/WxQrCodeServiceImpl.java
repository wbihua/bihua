package com.bihua.wx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;
import org.springframework.stereotype.Service;
import com.bihua.wx.domain.bo.WxQrCodeBo;
import com.bihua.wx.domain.vo.WxQrCodeVo;
import com.bihua.wx.domain.WxQrCode;
import com.bihua.wx.mapper.WxQrCodeMapper;
import com.bihua.wx.service.IWxQrCodeService;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 公众号带参二维码Service业务层处理
 *
 * @author bihua
 * @date 2023-05-19
 */
@RequiredArgsConstructor
@Service
public class WxQrCodeServiceImpl implements IWxQrCodeService {

    private final WxQrCodeMapper baseMapper;

    private final WxMpService wxService;

    /**
     * 查询公众号带参二维码
     */
    @Override
    public WxQrCodeVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询公众号带参二维码列表
     */
    @Override
    public TableDataInfo<WxQrCodeVo> queryPageList(WxQrCodeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WxQrCode> lqw = buildQueryWrapper(bo);
        Page<WxQrCodeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询公众号带参二维码列表
     */
    @Override
    public List<WxQrCodeVo> queryList(WxQrCodeBo bo) {
        LambdaQueryWrapper<WxQrCode> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WxQrCode> buildQueryWrapper(WxQrCodeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WxQrCode> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAppid()), WxQrCode::getAppid, bo.getAppid());
        lqw.eq(bo.getIsTemp() != null, WxQrCode::getIsTemp, bo.getIsTemp());
        lqw.eq(StringUtils.isNotBlank(bo.getSceneStr()), WxQrCode::getSceneStr, bo.getSceneStr());
        lqw.eq(StringUtils.isNotBlank(bo.getTicket()), WxQrCode::getTicket, bo.getTicket());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), WxQrCode::getUrl, bo.getUrl());
        lqw.eq(bo.getExpireTime() != null, WxQrCode::getExpireTime, bo.getExpireTime());
        return lqw;
    }

    /**
     * 新增公众号带参二维码
     */
    @Override
    public Boolean insertByBo(WxQrCodeBo bo) {
        WxQrCode add = BeanUtil.toBean(bo, WxQrCode.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改公众号带参二维码
     */
    @Override
    public Boolean updateByBo(WxQrCodeBo bo) {
        WxQrCode update = BeanUtil.toBean(bo, WxQrCode.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WxQrCode entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除公众号带参二维码
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 创建公众号带参二维码
     *
     *
     * @param appid
     * @param bo
     * @return
     */
    @Override
    public WxMpQrCodeTicket createQrCode(String appid, WxQrCodeBo bo) throws WxErrorException {
        WxMpQrCodeTicket ticket;
        //创建临时二维码
        if (bo.getIsTemp()) {
            ticket = wxService.getQrcodeService().qrCodeCreateTmpTicket(bo.getSceneStr(), bo.getExpireSeconds());
        } else {//创建永久二维码
            ticket = wxService.getQrcodeService().qrCodeCreateLastTicket(bo.getSceneStr());
        }
        WxQrCode wxQrCode = new WxQrCode();
        wxQrCode.setAppid(appid);
        wxQrCode.setIsTemp(bo.getIsTemp());
        wxQrCode.setSceneStr(bo.getSceneStr());
        wxQrCode.setTicket(ticket.getTicket());
        wxQrCode.setUrl(ticket.getUrl());
        if (bo.getIsTemp()) {
            wxQrCode.setExpireTime(new Date(System.currentTimeMillis() + ticket.getExpireSeconds() * 1000L));
        }
        this.baseMapper.insert(wxQrCode);
        return ticket;
    }
}
