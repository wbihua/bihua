package com.bihua.wx.domain;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.bihua.framework.handler.JSONArrayTypeHandler;
import com.bihua.framework.handler.JSONObjectTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;
import lombok.NoArgsConstructor;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

/**
 * 微信模版消息发送记录对象 wx_template_msg_log
 *
 * @author bihua
 * @date 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_template_msg_log")
@NoArgsConstructor
public class WxTemplateMsgLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "log_id", type = IdType.AUTO)
    private Long logId;
    /**
     * appid
     */
    private String appid;
    /**
     * 用户openid
     */
    private String touser;
    /**
     * templateid
     */
    private String templateId;
    /**
     * 消息数据
     */
    @TableField(typeHandler = JSONArrayTypeHandler.class)
    private JSONArray data;
    /**
     * 消息链接
     */
    private String url;
    /**
     * 小程序信息
     */
    @TableField(typeHandler = JSONObjectTypeHandler.class)
    private JSONObject miniprogram;
    /**
     * 发送时间
     */
    private Date sendTime;
    /**
     * 发送结果
     */
    private String sendResult;

    public WxTemplateMsgLog(WxMpTemplateMessage msg, String appid, String sendResult) {
        this.appid = appid;
        this.touser = msg.getToUser();
        this.templateId = msg.getTemplateId();
        this.url = msg.getUrl();
        this.miniprogram = JSONObject.parseObject(JSON.toJSONString(msg.getMiniProgram()));
        this.data = JSONArray.parseArray(JSON.toJSONString(msg.getData()));
        this.sendTime = new Date();
        this.sendResult = sendResult;
    }
}
