package com.bihua.wx.domain;

import java.sql.Time;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 自动回复规则对象 wx_msg_reply_rule
 *
 * @author ruoyi
 * @date 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_msg_reply_rule")
public class WxMsgReplyRule extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "rule_id", type = IdType.AUTO)
    private Long ruleId;
    /**
     * appid
     */
    private String appid;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 匹配的关键词、事件等
     */
    private String matchValue;
    /**
     * 是否精确匹配
     */
    private boolean exactMatch;
    /**
     * 回复消息类型
     */
    private String replyType;
    /**
     * 回复消息内容
     */
    private String replyContent;
    /**
     * 规则是否有效
     */
    @TableField(value = "`status`")
    private boolean status;
    /**
     * 备注说明
     */
    @TableField(value = "`desc`")
    private String desc;
    /**
     * 生效起始时间
     */
    private Time effectTimeStart;
    /**
     * 生效结束时间
     */
    private Time effectTimeEnd;
    /**
     * 规则优先级
     */
    private Integer priority;

}
