package com.bihua.wx.domain.bo;

import lombok.Data;

/**
 * @author bihua
 * @date 2023年05月17日 14:57
 */
@Data
public class MaterialFileBo {
    String mediaId;
}
