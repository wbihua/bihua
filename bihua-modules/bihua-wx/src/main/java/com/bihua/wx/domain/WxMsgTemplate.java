package com.bihua.wx.domain;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.bihua.framework.handler.JSONArrayTypeHandler;
import com.bihua.framework.handler.JSONObjectTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;
import lombok.NoArgsConstructor;
import me.chanjar.weixin.mp.bean.template.WxMpTemplate;

/**
 * 消息模板对象 wx_msg_template
 *
 * @author bihua
 * @date 2023-05-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_msg_template")
@NoArgsConstructor
public class WxMsgTemplate extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * appid
     */
    private String appid;
    /**
     * 公众号模板ID
     */
    private String templateId;
    /**
     * 模版名称
     */
    @TableField(value = "`name`")
    private String name;
    /**
     * 标题
     */
    private String title;
    /**
     * 模板内容
     */
    private String content;
    /**
     * 消息内容
     */
    @TableField(typeHandler = JSONArrayTypeHandler.class)
    private JSONArray data;
    /**
     * 链接
     */
    private String url;
    /**
     * 小程序信息
     */
    @TableField(typeHandler = JSONObjectTypeHandler.class)
    private JSONObject miniprogram;
    /**
     * 是否有效
     */
    @TableField(value = "`status`")
    private Boolean status;

    public WxMsgTemplate(WxMpTemplate mpTemplate, String appid) {
        this.appid = appid;
        this.templateId=mpTemplate.getTemplateId();
        this.title=mpTemplate.getTitle();
        this.name=mpTemplate.getTemplateId();
        this.content = mpTemplate.getContent();
        this.status=true;
    }
}
