package com.bihua.wx.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;
import java.util.List;


/**
 * 用户视图对象 wx_user
 *
 * @author ruoyi
 * @date 2023-05-21
 */
@Data
@ExcelIgnoreUnannotated
public class WxUserVo {

    private static final long serialVersionUID = 1L;

    /**
     * 微信openid
     */
    @ExcelProperty(value = "微信openid")
    private String openid;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 手机号
     */
    @ExcelProperty(value = "手机号")
    private String phone;

    /**
     * 昵称
     */
    @ExcelProperty(value = "昵称")
    private String nickname;

    /**
     * 性别(0-未知、1-男、2-女)
     */
    @ExcelProperty(value = "性别(0-未知、1-男、2-女)")
    private Integer sex;

    /**
     * 城市
     */
    @ExcelProperty(value = "城市")
    private String city;

    /**
     * 省份
     */
    @ExcelProperty(value = "省份")
    private String province;

    /**
     * 头像
     */
    @ExcelProperty(value = "头像")
    private String headimgurl;

    /**
     * 订阅时间
     */
    @ExcelProperty(value = "订阅时间")
    private Date subscribeTime;

    /**
     * 是否关注
     */
    @ExcelProperty(value = "是否关注")
    private Boolean subscribe;

    /**
     * unionid
     */
    @ExcelProperty(value = "unionid")
    private String unionid;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 标签ID列表
     */
    @ExcelProperty(value = "标签ID列表")
    private List<Long> tagidList;

    /**
     * 关注场景
     */
    @ExcelProperty(value = "关注场景")
    private String subscribeScene;

    /**
     * 扫码场景值
     */
    @ExcelProperty(value = "扫码场景值")
    private String qrSceneStr;


}
