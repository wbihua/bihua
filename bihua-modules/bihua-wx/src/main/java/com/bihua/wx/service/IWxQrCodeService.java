package com.bihua.wx.service;

import com.bihua.wx.domain.WxQrCode;
import com.bihua.wx.domain.vo.WxQrCodeVo;
import com.bihua.wx.domain.bo.WxQrCodeBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;

import java.util.Collection;
import java.util.List;

/**
 * 公众号带参二维码Service接口
 *
 * @author bihua
 * @date 2023-05-19
 */
public interface IWxQrCodeService {

    /**
     * 查询公众号带参二维码
     */
    WxQrCodeVo queryById(Long id);

    /**
     * 查询公众号带参二维码列表
     */
    TableDataInfo<WxQrCodeVo> queryPageList(WxQrCodeBo bo, PageQuery pageQuery);

    /**
     * 查询公众号带参二维码列表
     */
    List<WxQrCodeVo> queryList(WxQrCodeBo bo);

    /**
     * 新增公众号带参二维码
     */
    Boolean insertByBo(WxQrCodeBo bo);

    /**
     * 修改公众号带参二维码
     */
    Boolean updateByBo(WxQrCodeBo bo);

    /**
     * 校验并批量删除公众号带参二维码信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 创建公众号带参二维码
     *
     *
     * @param appid
     * @param bo
     * @return
     */
    WxMpQrCodeTicket createQrCode(String appid, WxQrCodeBo bo) throws WxErrorException;
}
