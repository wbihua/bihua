package com.bihua.wx.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * CMS文章中心对象 cms_article
 *
 * @author bihua
 * @date 2023-05-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_article")
public class CmsArticle extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 文章类型[1:普通文章,5:帮助中心]
     */
    private Integer type;
    /**
     * 标题
     */
    private String title;
    /**
     * 文章摘要
     */
    private String summary;
    /**
     * 文章标签
     */
    private String tags;
    /**
     * 内容
     */
    private String content;
    /**
     * 分类
     */
    private String category;
    /**
     * 二级目录
     */
    private String subCategory;
    /**
     * 点击次数
     */
    private Integer openCount;
    /**
     * 生效时间
     */
    private Date startTime;
    /**
     * 失效时间
     */
    private Date endTime;
    /**
     * 指向外链
     */
    private String targetLink;
    /**
     * 文章首图OSSID
     */
    private String ossId;
    /**
     * 文章首图
     */
    private String image;

}
