package com.bihua.wx.domain.vo;

import java.sql.Time;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 自动回复规则视图对象 wx_msg_reply_rule
 *
 * @author ruoyi
 * @date 2023-05-22
 */
@Data
@ExcelIgnoreUnannotated
public class WxMsgReplyRuleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long ruleId;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 规则名称
     */
    @ExcelProperty(value = "规则名称")
    private String ruleName;

    /**
     * 匹配的关键词、事件等
     */
    @ExcelProperty(value = "匹配的关键词、事件等")
    private String matchValue;

    /**
     * 是否精确匹配
     */
    @ExcelProperty(value = "是否精确匹配")
    private boolean exactMatch;

    /**
     * 回复消息类型
     */
    @ExcelProperty(value = "回复消息类型")
    private String replyType;

    /**
     * 回复消息内容
     */
    @ExcelProperty(value = "回复消息内容")
    private String replyContent;

    /**
     * 规则是否有效
     */
    @ExcelProperty(value = "规则是否有效")
    private boolean status;

    /**
     * 备注说明
     */
    @ExcelProperty(value = "备注说明")
    private String desc;

    /**
     * 生效起始时间
     */
    @ExcelProperty(value = "生效起始时间")
    private Time effectTimeStart;

    /**
     * 生效结束时间
     */
    @ExcelProperty(value = "生效结束时间")
    private Time effectTimeEnd;

    /**
     * 规则优先级
     */
    @ExcelProperty(value = "规则优先级")
    private Integer priority;


}
