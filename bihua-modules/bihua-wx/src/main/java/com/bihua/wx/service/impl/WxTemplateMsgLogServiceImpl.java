package com.bihua.wx.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;
import com.bihua.wx.domain.WxTemplateMsgLog;
import com.bihua.wx.domain.bo.WxTemplateMsgLogBo;
import com.bihua.wx.domain.vo.WxTemplateMsgLogVo;
import com.bihua.wx.mapper.WxTemplateMsgLogMapper;
import com.bihua.wx.service.IWxTemplateMsgLogService;

import lombok.RequiredArgsConstructor;

/**
 * 微信模版消息发送记录Service业务层处理
 *
 * @author bihua
 * @date 2023-05-22
 */
@RequiredArgsConstructor
@Service
public class WxTemplateMsgLogServiceImpl implements IWxTemplateMsgLogService {

    private final WxTemplateMsgLogMapper baseMapper;

    /**
     * 查询微信模版消息发送记录
     */
    @Override
    public WxTemplateMsgLogVo queryById(Long logId){
        return baseMapper.selectVoById(logId);
    }

    /**
     * 查询微信模版消息发送记录列表
     */
    @Override
    public TableDataInfo<WxTemplateMsgLogVo> queryPageList(WxTemplateMsgLogBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WxTemplateMsgLog> lqw = buildQueryWrapper(bo);
        Page<WxTemplateMsgLogVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询微信模版消息发送记录列表
     */
    @Override
    public List<WxTemplateMsgLogVo> queryList(WxTemplateMsgLogBo bo) {
        LambdaQueryWrapper<WxTemplateMsgLog> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WxTemplateMsgLog> buildQueryWrapper(WxTemplateMsgLogBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<WxTemplateMsgLog> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAppid()), WxTemplateMsgLog::getAppid, bo.getAppid());
        lqw.eq(StringUtils.isNotBlank(bo.getTouser()), WxTemplateMsgLog::getTouser, bo.getTouser());
        lqw.eq(StringUtils.isNotBlank(bo.getTemplateId()), WxTemplateMsgLog::getTemplateId, bo.getTemplateId());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), WxTemplateMsgLog::getUrl, bo.getUrl());
        lqw.eq(StringUtils.isNotBlank(bo.getMiniprogram()), WxTemplateMsgLog::getMiniprogram, bo.getMiniprogram());
        lqw.eq(bo.getSendTime() != null, WxTemplateMsgLog::getSendTime, bo.getSendTime());
        lqw.eq(StringUtils.isNotBlank(bo.getSendResult()), WxTemplateMsgLog::getSendResult, bo.getSendResult());
        return lqw;
    }

    /**
     * 批量删除微信模版消息发送记录
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
