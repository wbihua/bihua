package com.bihua.wx.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.bihua.common.utils.spring.SpringUtils;
import com.bihua.system.service.ISysConfigService;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;
import me.chanjar.weixin.common.util.http.apache.DefaultApacheHttpClientBuilder;
import me.chanjar.weixin.mp.config.WxMpHostConfig;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;

/**
 * 公众号账号对象 wx_account
 *
 * @author ruoyi
 * @date 2023-05-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_account")
public class WxAccount extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * appid
     */
    @TableId(value = "appid", type = IdType.INPUT)
    private String appid;
    /**
     * 公众号名称
     */
    private String name;
    /**
     * 账号类型
     */
    private Integer type;
    /**
     * 认证状态
     */
    private boolean verified;
    /**
     * appsecret
     */
    private String secret;
    /**
     * token
     */
    private String token;
    /**
     * aesKey
     */
    private String aesKey;

    public WxMpDefaultConfigImpl toWxMpConfigStorage(){
        WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
        configStorage.setAppId(appid);
        configStorage.setSecret(secret);
        configStorage.setToken(token);
        configStorage.setAesKey(aesKey);
        WxMpHostConfig hostConfig = new WxMpHostConfig();
        hostConfig.setApiHost("http://www.wbihua.com/weixin-api");
        hostConfig.setMpHost("http://www.wbihua.com/mp-api");
        hostConfig.setOpenHost("http://www.wbihua.com/open-api");
        configStorage.setHostConfig(hostConfig);
        DefaultApacheHttpClientBuilder builder = DefaultApacheHttpClientBuilder.get();
        String soTimeout = SpringUtils.getBean(ISysConfigService.class).selectConfigByKey("wx.soTimeout");
        builder.setSoTimeout(Integer.valueOf(soTimeout));
        configStorage.setApacheHttpClientBuilder(builder);
        return configStorage;
    }

}
