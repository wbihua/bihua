package com.bihua.wx.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * CMS文章中心业务对象 cms_article
 *
 * @author bihua
 * @date 2023-05-24
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsArticleBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 文章类型[1:普通文章,5:帮助中心]
     */
    @NotNull(message = "文章类型[1:普通文章,5:帮助中心]不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer type;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;

    /**
     * 文章摘要
     */
    @NotBlank(message = "文章摘要不能为空", groups = { AddGroup.class, EditGroup.class })
    private String summary;

    /**
     * 文章标签
     */
    @NotBlank(message = "文章标签不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tags;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 分类
     */
    @NotBlank(message = "分类不能为空", groups = { AddGroup.class, EditGroup.class })
    private String category;

    /**
     * 二级目录
     */
    @NotBlank(message = "二级目录不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subCategory;

    /**
     * 点击次数
     */
    @NotNull(message = "点击次数不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer openCount;

    /**
     * 生效时间
     */
    @NotNull(message = "生效时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date startTime;

    /**
     * 失效时间
     */
    private Date endTime;

    /**
     * 指向外链
     */
    @NotBlank(message = "指向外链不能为空", groups = { AddGroup.class, EditGroup.class })
    private String targetLink;

    /**
     * 文章首图OSSID
     */
    @NotBlank(message = "文章首图OSSID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ossId;

    /**
     * 文章首图
     */
    @NotBlank(message = "文章首图不能为空", groups = { AddGroup.class, EditGroup.class })
    private String image;


}
