package com.bihua.wx.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;
import java.util.List;


/**
 * 微信模版消息发送记录视图对象 wx_template_msg_log
 *
 * @author bihua
 * @date 2023-05-22
 */
@Data
@ExcelIgnoreUnannotated
public class WxTemplateMsgLogVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long logId;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 用户openid
     */
    @ExcelProperty(value = "用户openid")
    private String touser;

    /**
     * templateid
     */
    @ExcelProperty(value = "templateid")
    private String templateId;

    /**
     * 消息数据
     */
    @ExcelProperty(value = "消息数据")
    private List<String> data;

    /**
     * 消息链接
     */
    @ExcelProperty(value = "消息链接")
    private String url;

    /**
     * 小程序信息
     */
    @ExcelProperty(value = "小程序信息")
    private String miniprogram;

    /**
     * 发送时间
     */
    @ExcelProperty(value = "发送时间")
    private Date sendTime;

    /**
     * 发送结果
     */
    @ExcelProperty(value = "发送结果")
    private String sendResult;


}
