package com.bihua.wx.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.fastjson.JSONObject;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 微信消息视图对象 wx_msg
 *
 * @author bihua
 * @date 2023-05-23
 */
@Data
@ExcelIgnoreUnannotated
public class WxMsgVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 微信用户ID
     */
    @ExcelProperty(value = "微信用户ID")
    private String openid;

    /**
     * 消息方向
     */
    @ExcelProperty(value = "消息方向")
    private byte inOut;

    /**
     * 消息类型
     */
    @ExcelProperty(value = "消息类型")
    private String msgType;

    /**
     * 消息详情
     */
    @ExcelProperty(value = "消息详情")
    private JSONObject detail;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;


}
