package com.bihua.wx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.wx.domain.bo.WxMsgReplyRuleBo;
import com.bihua.wx.domain.vo.WxMsgReplyRuleVo;
import com.bihua.wx.domain.WxMsgReplyRule;
import com.bihua.wx.mapper.WxMsgReplyRuleMapper;
import com.bihua.wx.service.IWxMsgReplyRuleService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 自动回复规则Service业务层处理
 *
 * @author ruoyi
 * @date 2023-05-22
 */
@RequiredArgsConstructor
@Service
public class WxMsgReplyRuleServiceImpl implements IWxMsgReplyRuleService {

    private final WxMsgReplyRuleMapper baseMapper;

    /**
     * 查询自动回复规则
     */
    @Override
    public WxMsgReplyRuleVo queryById(Long ruleId){
        return baseMapper.selectVoById(ruleId);
    }

    /**
     * 查询自动回复规则列表
     */
    @Override
    public TableDataInfo<WxMsgReplyRuleVo> queryPageList(WxMsgReplyRuleBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WxMsgReplyRule> lqw = buildQueryWrapper(bo);
        Page<WxMsgReplyRuleVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询自动回复规则列表
     */
    @Override
    public List<WxMsgReplyRuleVo> queryList(WxMsgReplyRuleBo bo) {
        LambdaQueryWrapper<WxMsgReplyRule> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WxMsgReplyRule> buildQueryWrapper(WxMsgReplyRuleBo bo) {
        LambdaQueryWrapper<WxMsgReplyRule> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAppid()), WxMsgReplyRule::getAppid, bo.getAppid());
        lqw.like(StringUtils.isNotBlank(bo.getRuleName()), WxMsgReplyRule::getRuleName, bo.getRuleName());
        lqw.eq(StringUtils.isNotBlank(bo.getMatchValue()), WxMsgReplyRule::getMatchValue, bo.getMatchValue());
        lqw.eq(StringUtils.isNotBlank(bo.getReplyType()), WxMsgReplyRule::getReplyType, bo.getReplyType());
        lqw.eq(StringUtils.isNotBlank(bo.getReplyContent()), WxMsgReplyRule::getReplyContent, bo.getReplyContent());
        lqw.eq(StringUtils.isNotBlank(bo.getDesc()), WxMsgReplyRule::getDesc, bo.getDesc());
        lqw.eq(bo.getEffectTimeStart() != null, WxMsgReplyRule::getEffectTimeStart, bo.getEffectTimeStart());
        lqw.eq(bo.getEffectTimeEnd() != null, WxMsgReplyRule::getEffectTimeEnd, bo.getEffectTimeEnd());
        lqw.eq(bo.getPriority() != null, WxMsgReplyRule::getPriority, bo.getPriority());
        return lqw;
    }

    /**
     * 获取当前时段内所有有效的回复规则
     *
     * @return
     */
    @Override
    public List<WxMsgReplyRuleVo> getValidRules() {
        return baseMapper.selectVoList(
            new QueryWrapper<WxMsgReplyRule>()
                .eq("status", 1)
                .isNotNull("match_value")
                .ne("match_value", "")
                .orderByDesc("priority"));
    }
    /**
     * 新增自动回复规则
     */
    @Override
    public Boolean insertByBo(WxMsgReplyRuleBo bo) {
        WxMsgReplyRule add = BeanUtil.toBean(bo, WxMsgReplyRule.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setRuleId(add.getRuleId());
        }
        return flag;
    }

    /**
     * 修改自动回复规则
     */
    @Override
    public Boolean updateByBo(WxMsgReplyRuleBo bo) {
        WxMsgReplyRule update = BeanUtil.toBean(bo, WxMsgReplyRule.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WxMsgReplyRule entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除自动回复规则
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
