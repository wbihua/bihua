package com.bihua.wx.domain;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.*;
import com.bihua.framework.handler.JSONObjectTypeHandler;
import com.bihua.wx.domain.bo.WxMsgBo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;
import lombok.NoArgsConstructor;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.util.WxMpConfigStorageHolder;

/**
 * 微信消息对象 wx_msg
 *
 * @author bihua
 * @date 2023-05-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("wx_msg")
@NoArgsConstructor
public class WxMsg extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * appid
     */
    private String appid;
    /**
     * 微信用户ID
     */
    private String openid;
    /**
     * 消息方向
     */
    private byte inOut;
    /**
     * 消息类型
     */
    private String msgType;
    /**
     * 消息详情
     */
    @TableField(typeHandler = JSONObjectTypeHandler.class)
    private JSONObject detail;

    public static class WxMsgInOut{
        static final byte IN=0;
        static final byte OUT=1;
    }
    public WxMsg(WxMpXmlMessage wxMessage) {
        this.openid=wxMessage.getFromUser();
        this.appid= WxMpConfigStorageHolder.get();
        this.inOut = WxMsgInOut.IN;
        this.msgType = wxMessage.getMsgType();
        this.detail = new JSONObject();
        Long createTime = wxMessage.getCreateTime();
        this.setCreateTime(createTime==null?new Date():new Date(createTime*1000));
        if(WxConsts.XmlMsgType.TEXT.equals(this.msgType)){
            this.detail.put("content",wxMessage.getContent());
        }else if(WxConsts.XmlMsgType.IMAGE.equals(this.msgType)){
            this.detail.put("picUrl",wxMessage.getPicUrl());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.VOICE.equals(this.msgType)){
            this.detail.put("format",wxMessage.getFormat());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.VIDEO.equals(this.msgType) ||
            WxConsts.XmlMsgType.SHORTVIDEO.equals(this.msgType)){
            this.detail.put("thumbMediaId",wxMessage.getThumbMediaId());
            this.detail.put("mediaId",wxMessage.getMediaId());
        }else if(WxConsts.XmlMsgType.LOCATION.equals(this.msgType)){
            this.detail.put("locationX",wxMessage.getLocationX());
            this.detail.put("locationY",wxMessage.getLocationY());
            this.detail.put("scale",wxMessage.getScale());
            this.detail.put("label",wxMessage.getLabel());
        }else if(WxConsts.XmlMsgType.LINK.equals(this.msgType)){
            this.detail.put("title",wxMessage.getTitle());
            this.detail.put("description",wxMessage.getDescription());
            this.detail.put("url",wxMessage.getUrl());
        }else if(WxConsts.XmlMsgType.EVENT.equals(this.msgType)){
            this.detail.put("event",wxMessage.getEvent());
            this.detail.put("eventKey",wxMessage.getEventKey());
        }
    }
    public static WxMsgBo buildOutMsg(String msgType, String openid, JSONObject detail){
        WxMsgBo bo = new WxMsgBo();
        bo.setAppid(WxMpConfigStorageHolder.get());
        bo.setMsgType(msgType);
        bo.setOpenid(openid);
        bo.setDetail(detail);
        bo.setCreateTime(new Date());
        bo.setInOut(WxMsgInOut.OUT);
        return bo;
    }
}
