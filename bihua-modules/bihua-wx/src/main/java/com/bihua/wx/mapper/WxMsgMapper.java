package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxMsg;
import com.bihua.wx.domain.vo.WxMsgVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 微信消息Mapper接口
 *
 * @author bihua
 * @date 2023-05-23
 */
public interface WxMsgMapper extends BaseMapperPlus<WxMsgMapper, WxMsg, WxMsgVo> {

}
