package com.bihua.wx.domain.bo;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author bihua
 * @date 2023年05月23日 12:11
 */
public class StringToListJsonDeserializer extends JsonDeserializer<List<Long>> {
    @Override
    public List<Long> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {
        if (p == null || p.getText() == null) {
            return null;
        }
        String s = p.getText();
        List<Long> result = new ArrayList<>();
        result.add(Long.valueOf(s));
        return result;
    }
}
