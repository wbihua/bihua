package com.bihua.wx.service;

import com.bihua.wx.domain.CmsArticle;
import com.bihua.wx.domain.vo.CmsArticleVo;
import com.bihua.wx.domain.bo.CmsArticleBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * CMS文章中心Service接口
 *
 * @author bihua
 * @date 2023-05-24
 */
public interface ICmsArticleService {

    /**
     * 查询CMS文章中心
     */
    CmsArticleVo queryById(Long id);

    /**
     * 查询CMS文章中心列表
     */
    TableDataInfo<CmsArticleVo> queryPageList(CmsArticleBo bo, PageQuery pageQuery);

    /**
     * 查询CMS文章中心列表
     */
    List<CmsArticleVo> queryList(CmsArticleBo bo);

    /**
     * 新增CMS文章中心
     */
    Boolean insertByBo(CmsArticleBo bo);

    /**
     * 修改CMS文章中心
     */
    Boolean updateByBo(CmsArticleBo bo);

    /**
     * 校验并批量删除CMS文章中心信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
