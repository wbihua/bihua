package com.bihua.wx.mapper;

import com.bihua.wx.domain.WxAccount;
import com.bihua.wx.domain.vo.WxAccountVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 公众号账号Mapper接口
 *
 * @author ruoyi
 * @date 2023-05-16
 */
public interface WxAccountMapper extends BaseMapperPlus<WxAccountMapper, WxAccount, WxAccountVo> {

}
