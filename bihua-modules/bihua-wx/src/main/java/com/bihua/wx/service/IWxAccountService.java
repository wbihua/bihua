package com.bihua.wx.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.wx.domain.bo.WxAccountBo;
import com.bihua.wx.domain.vo.WxAccountVo;

/**
 * 公众号账号Service接口
 *
 * @author ruoyi
 * @date 2023-05-16
 */
public interface IWxAccountService {

    /**
     * 查询公众号账号
     */
    WxAccountVo queryById(String appid);

    /**
     * 查询公众号账号列表
     */
    TableDataInfo<WxAccountVo> queryPageList(WxAccountBo bo, PageQuery pageQuery);

    /**
     * 查询公众号账号列表
     */
    List<WxAccountVo> queryList(WxAccountBo bo);

    /**
     * 新增公众号账号
     */
    Boolean insertByBo(WxAccountBo bo);

    /**
     * 修改公众号账号
     */
    Boolean updateByBo(WxAccountBo bo);

    /**
     * 校验并批量删除公众号账号信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
