package com.bihua.wx.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 公众号账号视图对象 wx_account
 *
 * @author ruoyi
 * @date 2023-05-16
 */
@Data
@ExcelIgnoreUnannotated
public class WxAccountVo {

    private static final long serialVersionUID = 1L;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 公众号名称
     */
    @ExcelProperty(value = "公众号名称")
    private String name;

    /**
     * 账号类型
     */
    @ExcelProperty(value = "账号类型")
    private Integer type;

    /**
     * 认证状态
     */
    @ExcelProperty(value = "认证状态")
    private boolean verified;

    /**
     * appsecret
     */
    @ExcelProperty(value = "appsecret")
    private String secret;

    /**
     * token
     */
    @ExcelProperty(value = "token")
    private String token;

    /**
     * aesKey
     */
    @ExcelProperty(value = "aesKey")
    private String aesKey;


}
