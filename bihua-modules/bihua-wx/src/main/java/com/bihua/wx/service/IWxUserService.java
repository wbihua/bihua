package com.bihua.wx.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.wx.domain.WxUser;
import com.bihua.wx.domain.bo.WxUserBo;
import com.bihua.wx.domain.vo.WxUserVo;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;

/**
 * 用户Service接口
 *
 * @author ruoyi
 * @date 2023-05-21
 */
public interface IWxUserService {

    /**
     * 查询用户
     */
    WxUserVo queryById(String openid);

    /**
     * 查询用户
     */
    List<WxUserVo> listByIds(List<String> openids);

    /**
     * 查询用户列表
     */
    TableDataInfo<WxUserVo> queryPageList(WxUserBo bo, PageQuery pageQuery);

    /**
     * 查询用户列表
     */
    List<WxUserVo> queryList(WxUserBo bo);

    /**
     * 校验并批量删除用户信息
     */
    Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);

    /**
     * 同步用户列表
     */
    void syncWxUsers(String appid);

    /**
     * 通过传入的openid列表，同步用户列表
     * @param openids
     */
    void syncWxUsers(List<String> openids, String appid);

    /**
     * 根据openid更新用户信息
     *
     * @param openid
     * @return
     */
    WxUser refreshUserInfo(String openid, String appid);

    /**
     * 异步批量更新用户信息
     * @param openidList
     */
    void refreshUserInfoAsync(String[] openidList, String appid);

    /**
     * 取消关注，更新关注状态
     *
     * @param openid
     */
    void unsubscribe(String openid);

    /**
     * 获取公众号用户标签
     * @return
     * @throws WxErrorException
     * @param appid
     */
    List<WxUserTag> getWxTags(String appid) throws WxErrorException;

    /**
     * 创建标签
     * @param appid
     * @param name 标签名称
     */
    void creatTag(String appid, String name) throws WxErrorException;

    /**
     * 修改标签
     * @param appid
     * @param tagid 标签ID
     * @param name 标签名称
     */
    void updateTag(String appid, Long tagid, String name) throws WxErrorException;

    /**
     * 删除标签
     *
     * @param appid
     * @param tagid 标签ID
     * @throws WxErrorException
     */
    void deleteTag(String appid, Long tagid) throws WxErrorException;

    /**
     * 批量给用户打标签
     *
     * @param appid
     * @param tagid 标签ID
     * @param openidList 用户openid列表
     * @throws WxErrorException
     */
    void batchTagging(String appid, Long tagid, String[] openidList) throws WxErrorException;

    /**
     * 批量取消用户标签
     *
     * @param appid
     * @param tagid 标签ID
     * @param openidList 用户openid列表
     * @throws WxErrorException
     */
    void batchUnTagging(String appid, Long tagid, String[] openidList) throws WxErrorException;

}
