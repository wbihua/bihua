package com.bihua.wx.mapper;

import com.bihua.wx.domain.CmsArticle;
import com.bihua.wx.domain.vo.CmsArticleVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * CMS文章中心Mapper接口
 *
 * @author bihua
 * @date 2023-05-24
 */
public interface CmsArticleMapper extends BaseMapperPlus<CmsArticleMapper, CmsArticle, CmsArticleVo> {

}
