package com.bihua.wx.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bihua.wx.domain.bo.WxMsgQueryBo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.wx.domain.bo.WxMsgBo;
import com.bihua.wx.domain.vo.WxMsgVo;
import com.bihua.wx.domain.WxMsg;
import com.bihua.wx.mapper.WxMsgMapper;
import com.bihua.wx.service.IWxMsgService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 微信消息Service业务层处理
 *
 * @author bihua
 * @date 2023-05-23
 */
@RequiredArgsConstructor
@Service
public class WxMsgServiceImpl implements IWxMsgService {

    private final WxMsgMapper baseMapper;

    /**
     * 查询微信消息
     */
    @Override
    public WxMsgVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询微信消息列表
     */
    @Override
    public TableDataInfo<WxMsgVo> queryPageList(WxMsgQueryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<WxMsg> lqw = buildQueryWrapper(bo);
        Page<WxMsgVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询微信消息列表
     */
    @Override
    public List<WxMsgVo> queryList(WxMsgQueryBo bo) {
        LambdaQueryWrapper<WxMsg> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<WxMsg> buildQueryWrapper(WxMsgQueryBo bo) {
        LambdaQueryWrapper<WxMsg> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAppid()), WxMsg::getAppid, bo.getAppid());
        lqw.eq(StringUtils.isNotBlank(bo.getOpenid()), WxMsg::getOpenid, bo.getOpenid());
        lqw.in(StringUtils.isNotBlank(bo.getMsgTypes()), WxMsg::getMsgType, Arrays.asList(bo.getMsgTypes().split(",")));
        lqw.ge(StringUtils.isNotBlank(bo.getStartTime()), WxMsg::getCreateTime, bo.getStartTime());
        return lqw;
    }

    /**
     * 新增微信消息
     */
    @Override
    public Boolean insertByBo(WxMsgBo bo) {
        WxMsg add = BeanUtil.toBean(bo, WxMsg.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改微信消息
     */
    @Override
    public Boolean updateByBo(WxMsgBo bo) {
        WxMsg update = BeanUtil.toBean(bo, WxMsg.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(WxMsg entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除微信消息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
