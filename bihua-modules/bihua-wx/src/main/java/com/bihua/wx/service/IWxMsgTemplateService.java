package com.bihua.wx.service;

import com.bihua.wx.domain.WxMsgTemplate;
import com.bihua.wx.domain.bo.TemplateMsgBatchBo;
import com.bihua.wx.domain.vo.WxMsgTemplateVo;
import com.bihua.wx.domain.bo.WxMsgTemplateBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;

import java.util.Collection;
import java.util.List;

/**
 * 消息模板Service接口
 *
 * @author bihua
 * @date 2023-05-22
 */
public interface IWxMsgTemplateService {

    /**
     * 查询消息模板
     */
    WxMsgTemplateVo queryById(Long id);

    /**
     * 查询消息模板列表
     */
    TableDataInfo<WxMsgTemplateVo> queryPageList(WxMsgTemplateBo bo, PageQuery pageQuery);

    /**
     * 查询消息模板列表
     */
    List<WxMsgTemplateVo> queryList(WxMsgTemplateBo bo);

    /**
     * 新增消息模板
     */
    Boolean insertByBo(WxMsgTemplateBo bo);

    /**
     * 修改消息模板
     */
    Boolean updateByBo(WxMsgTemplateBo bo);

    /**
     * 校验并批量删除消息模板信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 同步公众号已添加的消息模板
     * @throws WxErrorException
     */
    void syncWxTemplate(String appid) throws WxErrorException;

    /**
     * 批量消息发送
     * @param bo
     * @param appid
     */
    void sendMsgBatch(TemplateMsgBatchBo bo, String appid);

    /**
     * 发送微信模版消息
     */
    void sendTemplateMsg(WxMpTemplateMessage msg, String appid);
}
