package com.bihua.wx.domain.vo;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 公众号带参二维码视图对象 wx_qr_code
 *
 * @author bihua
 * @date 2023-05-19
 */
@Data
@ExcelIgnoreUnannotated
public class WxQrCodeVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * appid
     */
    @ExcelProperty(value = "appid")
    private String appid;

    /**
     * 是否为临时二维码
     */
    @ExcelProperty(value = "是否为临时二维码")
    private Boolean isTemp;

    /**
     * 场景值ID
     */
    @ExcelProperty(value = "场景值ID")
    private String sceneStr;

    /**
     * 二维码ticket
     */
    @ExcelProperty(value = "二维码ticket")
    private String ticket;

    /**
     * 二维码图片解析后的地址
     */
    @ExcelProperty(value = "二维码图片解析后的地址")
    private String url;

    /**
     * 该二维码失效时间
     */
    @ExcelProperty(value = "该二维码失效时间")
    private Date expireTime;


}
