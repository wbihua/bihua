## 1.错误代码：45009
```
一.问题详细描述
【请求地址】: http://www.wbihua.com/weixin-api/cgi-bin/material/batchget_material?access_token=68_tRahegvmeeK_mSMWWXBnHuzrkmv9Wwj8ojxxF_-gU6agHiN5cWTFDwmpVG8CGCoZ9upx7r2E_hNnoxe5YKYE9Ms9BjWwP2dVs6n-FAy4rvhxhEBIxDvgChT3ndoOXWdADARAZ
【请求参数】：{"type":"image","offset":0,"count":20}
【错误信息】：错误代码：45009, 错误信息：接口调用超过限制，微信原始报文：{"errcode":45009,"errmsg":"reach max api daily quota limit rid: 646617ce-5bb3f6d4-73e0ad5e"}
二.相关资料
https://developers.weixin.qq.com/doc/offiaccount/openApi/clear_quota.html
注意事项
1、如果要清空公众号的接口的quota，则需要用公众号的access_token；如果要清空小程序的接口的quota，则需要用小程序的access_token；如果要清空第三方平台的接口的quota，则需要用第三方平台的component_access_token
2、如果是第三方服务商代公众号或者小程序清除quota，则需要用authorizer_access_token
3、每个帐号每月共10次清零操作机会，清零生效一次即用掉一次机会；第三方帮助公众号/小程序调用时，实际上是在消耗公众号/小程序自身的quota
4、由于指标计算方法或统计时间差异，实时调用量数据可能会出现误差，一般在1%以内
请求地址
POST https://api.weixin.qq.com/cgi-bin/clear_quota?access_token=ACCESS_TOKEN
{
"appid":"wx448f04719cd48f69"
}
```
![清空api的调用quota](45009.png)

## 2.status code: 413, reason phrase: HTTP/1.1 413 Request Entity Too Large
```
一.问题详细描述
2023-05-18 22:52:34 [XNIO-1 task-3] ERROR m.c.w.m.api.impl.BaseWxMpServiceImpl - 
【请求地址】: http://www.wbihua.com/weixin-api/cgi-bin/material/add_material?type=video&access_token=68_NUhWv3C3cOaTfS-23byWkes4KjD_tS9NucKh4tYDjIiEo7WwRyfdWoVSnghoU-vz8XgHe2JyngI2xmeeW8dc7oRxC621Rw8xML9ZpWMTTUSBIJGRyw2bdSHU3hEFMZiAJAJQD
【请求参数】：WxMpMaterial(name=202305182213, file=C:\Users\wuzhong\AppData\Local\Temp\202305182213--459245907818092421.mp4, videoTitle=202305182213, videoIntroduction=null)
【异常信息】：status code: 413, reason phrase: HTTP/1.1 413 Request Entity Too Large
2023-05-18 22:52:34 [XNIO-1 task-3] ERROR c.r.f.w.e.GlobalExceptionHandler - 请求地址'/wx/wxAssets/materialFileUpload',发生系统异常.
me.chanjar.weixin.common.error.WxErrorException: status code: 413, reason phrase: HTTP/1.1 413 Request Entity Too Large
	at me.chanjar.weixin.mp.api.impl.BaseWxMpServiceImpl.executeInternal(BaseWxMpServiceImpl.java:457)
	at me.chanjar.weixin.mp.api.impl.BaseWxMpServiceImpl.execute(BaseWxMpServiceImpl.java:384)
	at me.chanjar.weixin.mp.api.impl.WxMpMaterialServiceImpl.materialFileUpload(WxMpMaterialServiceImpl.java:83)
	at com.ruoyi.wx.service.impl.WxAssetsServiceImpl.materialFileUpload(WxAssetsServiceImpl.java:116)
二、解决方法
增加client_max_body_size  20m;

location /weixin-api/{
    proxy_ignore_client_abort on;
    proxy_read_timeout 600s;
    proxy_pass      https://api.weixin.qq.com/;
    client_max_body_size  20m;
}
```
## 3.Read timed out
```
一.问题详细描述
【请求地址】: http://www.wbihua.com/weixin-api/cgi-bin/material/add_material?type=video&access_token=68_lYzOG_X03F-8ucIMfCnJHgaoL0-ghWQe18lJqLahwUAund1Z9Hdzk-JtCgzNRDoVY3qfD98iYErotnNzhJIo4p9hLL01kj-cv6moj6C19ErAWDcBe05--YhHGkIUUXcAHAFFI
【请求参数】：WxMpMaterial(name=202305182213, file=C:\Users\wuzhong\AppData\Local\Temp\202305182213--109357984358147147.mp4, videoTitle=202305182213, videoIntroduction=null)
【异常信息】：Read timed out
2023-05-19 14:46:32 [XNIO-1 task-1] ERROR c.r.f.w.e.GlobalExceptionHandler - 请求地址'/wx/wxAssets/materialFileUpload',发生系统异常.
me.chanjar.weixin.common.error.WxErrorException: Read timed out
	at me.chanjar.weixin.mp.api.impl.BaseWxMpServiceImpl.executeInternal(BaseWxMpServiceImpl.java:444)
	at me.chanjar.weixin.mp.api.impl.BaseWxMpServiceImpl.execute(BaseWxMpServiceImpl.java:371)
	at me.chanjar.weixin.mp.api.impl.WxMpMaterialServiceImpl.materialFileUpload(WxMpMaterialServiceImpl.java:83)
	at com.ruoyi.wx.service.impl.WxAssetsServiceImpl.materialFileUpload(WxAssetsServiceImpl.java:116)
二、解决方法
DefaultApacheHttpClientBuilder builder = DefaultApacheHttpClientBuilder.get();
builder.setSoTimeout(600000);
configStorage.setApacheHttpClientBuilder(builder);

WxMpDefaultConfigImpl configStorage = new WxMpDefaultConfigImpl();
configStorage.setAppId(appid);
configStorage.setSecret(secret);
configStorage.setToken(token);
configStorage.setAesKey(aesKey);
WxMpHostConfig hostConfig = new WxMpHostConfig();
hostConfig.setApiHost("http://www.wbihua.com/weixin-api");
hostConfig.setMpHost("http://www.wbihua.com/mp-api");
hostConfig.setOpenHost("http://www.wbihua.com/open-api");
configStorage.setHostConfig(hostConfig);
DefaultApacheHttpClientBuilder builder = DefaultApacheHttpClientBuilder.get();
builder.setSoTimeout(600000);
configStorage.setApacheHttpClientBuilder(builder);

```
## 4.客户端超时设置
```
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 100000
})
```