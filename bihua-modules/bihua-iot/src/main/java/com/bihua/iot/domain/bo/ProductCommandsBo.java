package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品设备服务命令业务对象 product_commands
 *
 * @author bihua
 * @date 2023-06-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductCommandsBo extends BaseEntity {

    /**
     * 命令id
     */
    @NotNull(message = "命令id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 服务ID
     */
    @NotNull(message = "服务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long serviceId;

    /**
     * 指示命令的名字，如门磁的LOCK命令、摄像头的VIDEO_RECORD命令，命令名与参数共同构成一个完整的命令。
支持英文大小写、数字及下划线，长度[2,50]。

     */
    @NotBlank(message = "指示命令的名字不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 命令描述。
     */
    private String description;


}
