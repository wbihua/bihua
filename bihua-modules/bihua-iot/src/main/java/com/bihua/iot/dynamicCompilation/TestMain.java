package com.bihua.iot.dynamicCompilation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import com.bihua.iot.dynamicCompilation.bytecode.InjectionSystem;
import org.apache.commons.text.StringEscapeUtils;

public class TestMain {

    public static void main(String[] args) {
        String protocolContent =
            "package com.bihua.iot.dynamicCompilation;\n" +
                "\n" +
                "import java.util.Date;\n" +
                "\n" +
                "import cn.hutool.json.JSONArray;\n" +
                "import cn.hutool.json.JSONObject;\n" +
                "import cn.hutool.json.JSONUtil;\n" +
                "\n" +
                "\n" +
                "public class BihuaProtocol {\n" +
                "\n" +
                "    public static void main(String[] args) throws Exception {\n" +
                "        String injson = \"\";\n" +
                "        if (args != null && args.length != 0) {\n" +
                "            injson = args[0];\n" +
                "        }\n" +
                "        JSONObject jsonObject = JSONUtil.parseObj(injson);\n" +
                "        String data = ((JSONObject) jsonObject.get(\"msg\")).get(\"data\").toString();\n" +
                "        JSONArray services = new JSONArray();\n" +
                "        JSONObject server = new JSONObject();\n" +
                "        server.put(\"serviceId\", ((JSONObject) jsonObject.get(\"msg\")).get(\"serviceId\"));\n" +
                "        server.put(\"data\", data);\n" +
                "        server.put(\"eventTime\", new Date());\n" +
                "        services.add(server);\n" +
                "        JSONObject device = new JSONObject();\n" +
                "        device.put(\"deviceId\", ((JSONObject) jsonObject.get(\"msg\")).get(\"devEui\"));\n" +
                "        device.put(\"services\", services);\n" +
                "        JSONArray devices = new JSONArray();\n" +
                "        devices.add(device);\n" +
                "        JSONObject root = new JSONObject();\n" +
                "        root.put(\"devices\", devices);\n" +
                "        System.out.println(root.toString());\n" +
                "    }\n" +
                "}\n";
        String body = "{\n" +
            "\"msg\":{\n" +
            "\"devEui\":\"deviceid1\",\n" +
            "\"data\":{ \"wendu\":11,\"time\":79},\n" +
            "\"serviceId\":\"service1\"\n" +
            "}\n" +
            "}";
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        PrintWriter out = new PrintWriter(buffer, true);
        byte[] classBytes = DynamicLoaderEngine.compile(protocolContent, out, null);//传入要执行的代码
        byte[] injectedClass = ClassInjector.injectSystem(classBytes);
        InjectionSystem.inject(null, new PrintStream(buffer, true), null);
        DynamicClassLoader classLoader = new DynamicClassLoader(TestMain.class.getClassLoader());
        DynamicLoaderEngine.executeMain(classLoader, injectedClass, out, body);
        body = buffer.toString().trim();
        System.out.println(body);
    }
}
