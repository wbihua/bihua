package com.bihua.iot.service;

import com.alibaba.fastjson.JSONObject;
import com.bihua.iot.domain.DeviceAction;
import com.bihua.iot.domain.vo.DeviceActionVo;
import com.bihua.iot.domain.bo.DeviceActionBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 设备动作数据Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceActionService {

    /**
     * 查询设备动作数据
     */
    DeviceActionVo queryById(Long id);

    /**
     * 查询设备动作数据列表
     */
    TableDataInfo<DeviceActionVo> queryPageList(DeviceActionBo bo, PageQuery pageQuery);

    /**
     * 查询设备动作数据列表
     */
    List<DeviceActionVo> queryList(DeviceActionBo bo);

    /**
     * 新增设备动作数据
     */
    Boolean insertByBo(DeviceActionBo bo);

    /**
     * 修改设备动作数据
     */
    Boolean updateByBo(DeviceActionBo bo);

    /**
     * 校验并批量删除设备动作数据信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 保存事件动作
     *
     * @param thinglinksMessage
     */
    void insertEvent(JSONObject thinglinksMessage);

    /**
     * 设备连接事件
     *
     * @param thinglinksMessage
     */
    void connectEvent(JSONObject thinglinksMessage);

    /**
     * 设备断开事件
     *
     * @param thinglinksMessage
     */
    void closeEvent(JSONObject thinglinksMessage);

    /**
     * 刷新设备缓存
     * @param thinglinksMessage
     */
    void refreshDeviceCache(JSONObject thinglinksMessage);
}
