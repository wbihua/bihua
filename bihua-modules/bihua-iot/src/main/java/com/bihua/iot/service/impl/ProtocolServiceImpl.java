package com.bihua.iot.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.utils.redis.RedisUtils;
import com.bihua.iot.constant.Constants;
import com.bihua.iot.domain.Protocol;
import com.bihua.iot.domain.bo.DeviceBo;
import com.bihua.iot.domain.bo.ProtocolBo;
import com.bihua.iot.domain.vo.DeviceVo;
import com.bihua.iot.domain.vo.ProtocolVo;
import com.bihua.iot.mapper.ProtocolMapper;
import com.bihua.iot.models.Protocols;
import com.bihua.iot.service.IDeviceService;
import com.bihua.iot.service.IProtocolService;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 协议信息Service业务层处理
 *
 * @author bihua
 * @date 2023-06-19
 */
@RequiredArgsConstructor
@Service
public class ProtocolServiceImpl implements IProtocolService {

    private final ProtocolMapper baseMapper;
    private final IDeviceService iDeviceService;
    /**
     * 查询协议信息
     */
    @Override
    public ProtocolVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询协议信息列表
     */
    @Override
    public TableDataInfo<ProtocolVo> queryPageList(ProtocolBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Protocol> lqw = buildQueryWrapper(bo);
        Page<ProtocolVo> result = baseMapper.selectPageList(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询协议信息列表
     */
    @Override
    public List<ProtocolVo> queryList(ProtocolBo bo) {
        LambdaQueryWrapper<Protocol> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Protocol> buildQueryWrapper(ProtocolBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Protocol> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getProductIdentification()), Protocol::getProductIdentification, bo.getProductIdentification());
        lqw.like(StringUtils.isNotBlank(bo.getProtocolName()), Protocol::getProtocolName, bo.getProtocolName());
        lqw.eq(StringUtils.isNotBlank(bo.getProtocolIdentification()), Protocol::getProtocolIdentification, bo.getProtocolIdentification());
        lqw.eq(StringUtils.isNotBlank(bo.getProtocolVersion()), Protocol::getProtocolVersion, bo.getProtocolVersion());
        lqw.eq(StringUtils.isNotBlank(bo.getProtocolType()), Protocol::getProtocolType, bo.getProtocolType());
        lqw.eq(StringUtils.isNotBlank(bo.getProtocolVoice()), Protocol::getProtocolVoice, bo.getProtocolVoice());
        lqw.like(StringUtils.isNotBlank(bo.getClassName()), Protocol::getClassName, bo.getClassName());
        lqw.eq(StringUtils.isNotBlank(bo.getFilePath()), Protocol::getFilePath, bo.getFilePath());
        lqw.eq(StringUtils.isNotBlank(bo.getContent()), Protocol::getContent, bo.getContent());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), Protocol::getStatus, bo.getStatus());
        return lqw;
    }

    /**
     * 新增协议信息
     */
    @Override
    public Long insertByBo(ProtocolBo bo) {
        Protocol add = BeanUtil.toBean(bo, Protocol.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
            return  add.getId();
        }
        return null;
    }

    /**
     * 修改协议信息
     */
    @Override
    public Boolean updateByBo(ProtocolBo bo) {
        Protocol update = BeanUtil.toBean(bo, Protocol.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Protocol entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除协议信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean enable(Long id) {
        ProtocolVo vo = baseMapper.selectVoById(id);
        DeviceBo bo = new DeviceBo();
        bo.setProductIdentification(vo.getProductIdentification());
        List<DeviceVo> deviceVos = iDeviceService.queryList(bo);
        for (DeviceVo deviceVo: deviceVos) {
            Protocols protocols = new Protocols();
            protocols.setId(vo.getId());
            protocols.setProtocolVoice(vo.getProtocolVoice());
            protocols.setContent(vo.getContent());
            RedisUtils.setCacheObject(Constants.DEVICE_DATA_REPORTED_AGREEMENT_SCRIPT + vo.getProtocolType() + deviceVo.getDeviceIdentification(), protocols);
            baseMapper.updateStatusById(Constants.ENABLE, vo.getId());
        }
        return true;
    }

    @Override
    public boolean disable(Long id) {
        ProtocolVo vo = baseMapper.selectVoById(id);
        DeviceBo bo = new DeviceBo();
        bo.setProductIdentification(vo.getProductIdentification());
        List<DeviceVo> deviceVos = iDeviceService.queryList(bo);
        for (DeviceVo deviceVo: deviceVos) {
            RedisUtils.deleteObject(Constants.DEVICE_DATA_REPORTED_AGREEMENT_SCRIPT + vo.getProtocolType() + deviceVo.getDeviceIdentification());
            baseMapper.updateStatusById(Constants.DISABLE, vo.getId());
        }
        return true;
    }
}
