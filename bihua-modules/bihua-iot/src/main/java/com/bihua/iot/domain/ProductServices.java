package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品服务对象 product_services
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("product_services")
public class ProductServices extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 服务id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 服务名称:支持英文大小写、数字、下划线和中划线

     */
    private String serviceName;
    /**
     * 产品模版标识
     */
    private String templateIdentification;
    /**
     * 产品标识
     */
    private String productIdentification;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 服务的描述信息:文本描述，不影响实际功能，可配置为空字符串""。

     */
    private String description;

}
