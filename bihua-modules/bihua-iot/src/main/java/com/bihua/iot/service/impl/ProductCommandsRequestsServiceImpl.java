package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.ProductCommandsRequestsBo;
import com.bihua.iot.domain.vo.ProductCommandsRequestsVo;
import com.bihua.iot.domain.ProductCommandsRequests;
import com.bihua.iot.mapper.ProductCommandsRequestsMapper;
import com.bihua.iot.service.IProductCommandsRequestsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 产品设备下发服务命令属性Service业务层处理
 *
 * @author bihua
 * @date 2023-06-15
 */
@RequiredArgsConstructor
@Service
public class ProductCommandsRequestsServiceImpl implements IProductCommandsRequestsService {

    private final ProductCommandsRequestsMapper baseMapper;

    /**
     * 查询产品设备下发服务命令属性
     */
    @Override
    public ProductCommandsRequestsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询产品设备下发服务命令属性列表
     */
    @Override
    public TableDataInfo<ProductCommandsRequestsVo> queryPageList(ProductCommandsRequestsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ProductCommandsRequests> lqw = buildQueryWrapper(bo);
        Page<ProductCommandsRequestsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询产品设备下发服务命令属性列表
     */
    @Override
    public List<ProductCommandsRequestsVo> queryList(ProductCommandsRequestsBo bo) {
        LambdaQueryWrapper<ProductCommandsRequests> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ProductCommandsRequests> buildQueryWrapper(ProductCommandsRequestsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ProductCommandsRequests> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getServiceId() != null, ProductCommandsRequests::getServiceId, bo.getServiceId());
        lqw.eq(bo.getCommandsId() != null, ProductCommandsRequests::getCommandsId, bo.getCommandsId());
        lqw.eq(StringUtils.isNotBlank(bo.getDatatype()), ProductCommandsRequests::getDatatype, bo.getDatatype());
        lqw.eq(StringUtils.isNotBlank(bo.getEnumlist()), ProductCommandsRequests::getEnumlist, bo.getEnumlist());
        lqw.eq(bo.getMax() != null, ProductCommandsRequests::getMax, bo.getMax());
        lqw.eq(bo.getMaxlength() != null, ProductCommandsRequests::getMaxlength, bo.getMaxlength());
        lqw.eq(bo.getMin() != null, ProductCommandsRequests::getMin, bo.getMin());
        lqw.eq(StringUtils.isNotBlank(bo.getParameterDescription()), ProductCommandsRequests::getParameterDescription, bo.getParameterDescription());
        lqw.like(StringUtils.isNotBlank(bo.getParameterName()), ProductCommandsRequests::getParameterName, bo.getParameterName());
        lqw.eq(bo.getRequired() != null, ProductCommandsRequests::getRequired, bo.getRequired());
        lqw.eq(bo.getStep() != null, ProductCommandsRequests::getStep, bo.getStep());
        lqw.eq(StringUtils.isNotBlank(bo.getUnit()), ProductCommandsRequests::getUnit, bo.getUnit());
        return lqw;
    }

    /**
     * 新增产品设备下发服务命令属性
     */
    @Override
    public Boolean insertByBo(ProductCommandsRequestsBo bo) {
        ProductCommandsRequests add = BeanUtil.toBean(bo, ProductCommandsRequests.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改产品设备下发服务命令属性
     */
    @Override
    public Boolean updateByBo(ProductCommandsRequestsBo bo) {
        ProductCommandsRequests update = BeanUtil.toBean(bo, ProductCommandsRequests.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ProductCommandsRequests entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除产品设备下发服务命令属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
