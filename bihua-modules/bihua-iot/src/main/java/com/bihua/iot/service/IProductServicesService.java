package com.bihua.iot.service;

import com.bihua.iot.domain.ProductServices;
import com.bihua.iot.domain.vo.ProductServicesVo;
import com.bihua.iot.domain.bo.ProductServicesBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 产品服务Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductServicesService {

    /**
     * 查询产品服务
     */
    ProductServicesVo queryById(Long id);

    /**
     * 查询产品服务列表
     */
    TableDataInfo<ProductServicesVo> queryPageList(ProductServicesBo bo, PageQuery pageQuery);

    /**
     * 查询产品服务列表
     */
    List<ProductServicesVo> queryList(ProductServicesBo bo);

    /**
     * 新增产品服务
     */
    Boolean insertByBo(ProductServicesBo bo);

    /**
     * 修改产品服务
     */
    Boolean updateByBo(ProductServicesBo bo);

    /**
     * 校验并批量删除产品服务信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
