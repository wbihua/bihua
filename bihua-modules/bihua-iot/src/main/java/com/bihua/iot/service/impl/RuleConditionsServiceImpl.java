package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.RuleConditionsBo;
import com.bihua.iot.domain.vo.RuleConditionsVo;
import com.bihua.iot.domain.RuleConditions;
import com.bihua.iot.mapper.RuleConditionsMapper;
import com.bihua.iot.service.IRuleConditionsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 规则条件Service业务层处理
 *
 * @author bihua
 * @date 2023-07-02
 */
@RequiredArgsConstructor
@Service
public class RuleConditionsServiceImpl implements IRuleConditionsService {

    private final RuleConditionsMapper baseMapper;

    /**
     * 查询规则条件
     */
    @Override
    public RuleConditionsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询规则条件列表
     */
    @Override
    public TableDataInfo<RuleConditionsVo> queryPageList(RuleConditionsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<RuleConditions> lqw = buildQueryWrapper(bo);
        Page<RuleConditionsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询规则条件列表
     */
    @Override
    public List<RuleConditionsVo> queryList(RuleConditionsBo bo) {
        LambdaQueryWrapper<RuleConditions> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<RuleConditions> buildQueryWrapper(RuleConditionsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<RuleConditions> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getRuleId() != null, RuleConditions::getRuleId, bo.getRuleId());
        lqw.eq(bo.getConditionType() != null, RuleConditions::getConditionType, bo.getConditionType());
        lqw.eq(StringUtils.isNotBlank(bo.getDeviceIdentification()), RuleConditions::getDeviceIdentification, bo.getDeviceIdentification());
        lqw.eq(StringUtils.isNotBlank(bo.getProductIdentification()), RuleConditions::getProductIdentification, bo.getProductIdentification());
        lqw.eq(bo.getServiceId() != null, RuleConditions::getServiceId, bo.getServiceId());
        lqw.eq(bo.getPropertiesId() != null, RuleConditions::getPropertiesId, bo.getPropertiesId());
        lqw.eq(StringUtils.isNotBlank(bo.getComparisonMode()), RuleConditions::getComparisonMode, bo.getComparisonMode());
        lqw.eq(StringUtils.isNotBlank(bo.getComparisonValue()), RuleConditions::getComparisonValue, bo.getComparisonValue());
        return lqw;
    }

    /**
     * 新增规则条件
     */
    @Override
    public Boolean insertByBo(RuleConditionsBo bo) {
        RuleConditions add = BeanUtil.toBean(bo, RuleConditions.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改规则条件
     */
    @Override
    public Boolean updateByBo(RuleConditionsBo bo) {
        RuleConditions update = BeanUtil.toBean(bo, RuleConditions.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(RuleConditions entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除规则条件
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
