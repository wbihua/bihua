package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 产品模型视图对象 product
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@ExcelIgnoreUnannotated
public class ProductVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private String appId;

    /**
     * 产品模版标识
     */
    @ExcelProperty(value = "产品模版标识")
    private String templateIdentification;

    /**
     * 产品名称:自定义，支持中文、英文大小写、数字、下划线和中划线
     */
    @ExcelProperty(value = "产品名称:自定义，支持中文、英文大小写、数字、下划线和中划线")
    private String productName;

    /**
     * 产品标识
     */
    @ExcelProperty(value = "产品标识")
    private String productIdentification;

    /**
     * 支持以下两种产品类型•COMMON：普通产品，需直连设备。
•GATEWAY：网关产品，可挂载子设备。

     */
    @ExcelProperty(value = "产品类型")
    private String productType;

    /**
     * 厂商ID:支持英文大小写，数字，下划线和中划线
     */
    @ExcelProperty(value = "厂商ID:支持英文大小写，数字，下划线和中划线")
    private String manufacturerId;

    /**
     * 厂商名称 :支持中文、英文大小写、数字、下划线和中划线
     */
    @ExcelProperty(value = "厂商名称 :支持中文、英文大小写、数字、下划线和中划线")
    private String manufacturerName;

    /**
     * 产品型号，建议包含字母或数字来保证可扩展性。支持英文大小写、数字、下划线和中划线

     */
    @ExcelProperty(value = "产品型号 ")
    private String model;

    /**
     * 数据格式，默认为JSON无需修改。
     */
    @ExcelProperty(value = "数据格式，默认为JSON无需修改。")
    private String dataFormat;

    /**
     * 设备接入平台的协议类型，默认为MQTT无需修改。
 
     */
    @ExcelProperty(value = "设备接入平台的协议类型")
    private String protocolType;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 产品描述
     */
    @ExcelProperty(value = "产品描述")
    private String remark;


}
