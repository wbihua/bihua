package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 协议信息业务对象 protocol
 *
 * @author bihua
 * @date 2023-06-19
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ProtocolBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 产品标识
     */
    @NotBlank(message = "产品标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productIdentification;

    /**
     * 协议名称
     */
    @NotBlank(message = "协议名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolName;

    /**
     * 协议标识
     */
    @NotBlank(message = "协议标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolIdentification;

    /**
     * 协议版本
     */
    @NotBlank(message = "协议版本不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolVersion;

    /**
     * 协议类型 ：mqtt || coap || modbus || http
     */
    @NotBlank(message = "协议类型 ：mqtt || coap || modbus || http不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolType;

    /**
     * 协议语言
     */
    @NotBlank(message = "协议语言不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolVoice;

    /**
     * 类名
     */
    private String className;

    /**
     * 文件地址
     */
    private String filePath;

    /**
     * 内容
     */
    private String content;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @NotBlank(message = "状态(字典值：0启用  1停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 备注
     */
    private String remark;


}
