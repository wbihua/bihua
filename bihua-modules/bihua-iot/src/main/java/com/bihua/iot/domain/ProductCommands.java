package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品设备服务命令对象 product_commands
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("product_commands")
public class ProductCommands extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 命令id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 服务ID
     */
    private Long serviceId;
    /**
     * 指示命令的名字，如门磁的LOCK命令、摄像头的VIDEO_RECORD命令，命令名与参数共同构成一个完整的命令。
支持英文大小写、数字及下划线，长度[2,50]。

     */
    private String name;
    /**
     * 命令描述。
     */
    private String description;

}
