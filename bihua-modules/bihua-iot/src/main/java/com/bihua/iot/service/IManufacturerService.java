package com.bihua.iot.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.iot.domain.Manufacturer;
import com.bihua.iot.domain.bo.ManufacturerBo;
import com.bihua.iot.domain.vo.ManufacturerVo;

/**
 * 厂商管理Service接口
 *
 * @author wbihua
 * @date 2023-07-28
 */
public interface IManufacturerService {

    /**
     * 查询厂商管理
     */
    ManufacturerVo queryById(Long mfrId);

    /**
     * 查询厂商管理列表
     */
    TableDataInfo<ManufacturerVo> queryPageList(ManufacturerBo bo, PageQuery pageQuery);

    /**
     * 查询厂商管理列表
     */
    List<ManufacturerVo> queryList(ManufacturerBo bo);

    /**
     * 新增厂商管理
     */
    Boolean insertByBo(ManufacturerBo bo);

    /**
     * 修改厂商管理
     */
    Boolean updateByBo(ManufacturerBo bo);

    /**
     * 校验并批量删除厂商管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 校验厂商名称是否唯一
     *
     * @param manufacturer 厂商信息
     * @return 结果
     */
    boolean checkMfrNameUnique(Manufacturer manufacturer);
}
