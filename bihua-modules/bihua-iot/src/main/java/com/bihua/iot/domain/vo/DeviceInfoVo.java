package com.bihua.iot.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 子设备档案视图对象 device_info
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceInfoVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 边设备档案主键
     */
    @ExcelProperty(value = "边设备档案主键")
    private Long did;

    private String edgeDevicesIdentification;

    /**
     * 设备节点ID
     */
    @ExcelProperty(value = "设备节点ID")
    private String nodeId;

    /**
     * 设备名称
     */
    @ExcelProperty(value = "设备名称")
    private String nodeName;

    /**
     * 子设备唯一标识
     */
    @ExcelProperty(value = "子设备唯一标识")
    private String deviceId;

    /**
     * 设备描述
     */
    @ExcelProperty(value = "设备描述")
    private String description;

    /**
     * 设备型号
     */
    @ExcelProperty(value = "设备型号")
    private String model;

    /**
     * 子设备连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    @ExcelProperty(value = "子设备连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT")
    private String connectStatus;

    /**
     * 是否支持设备影子TRUE:1、FALSE :0
     */
    @ExcelProperty(value = "是否支持设备影子TRUE:1、FALSE :0")
    private Boolean shadowEnable;

    /**
     * 设备影子数据表名(多个英文逗号分割)
     */
    @ExcelProperty(value = "设备影子数据表名(多个英文逗号分割)")
    private String shadowTableName;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @ExcelProperty(value = "更新者")
    private String updateBy;

    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间")
    private Date updateTime;
}
