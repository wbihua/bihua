package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 协议信息视图对象 protocol
 *
 * @author bihua
 * @date 2023-06-19
 */
@Data
@ExcelIgnoreUnannotated
public class ProtocolVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 产品标识
     */
    @ExcelProperty(value = "产品标识")
    private String productIdentification;

    /**
     * 协议名称
     */
    @ExcelProperty(value = "协议名称")
    private String protocolName;

    /**
     * 协议标识
     */
    @ExcelProperty(value = "协议标识")
    private String protocolIdentification;

    /**
     * 协议版本
     */
    @ExcelProperty(value = "协议版本")
    private String protocolVersion;

    /**
     * 协议类型 ：mqtt || coap || modbus || http
     */
    @ExcelProperty(value = "协议类型 ：mqtt || coap || modbus || http")
    private String protocolType;

    /**
     * 协议语言
     */
    @ExcelProperty(value = "协议语言")
    private String protocolVoice;

    /**
     * 类名
     */
    @ExcelProperty(value = "类名")
    private String className;

    /**
     * 文件地址
     */
    @ExcelProperty(value = "文件地址")
    private String filePath;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String content;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
