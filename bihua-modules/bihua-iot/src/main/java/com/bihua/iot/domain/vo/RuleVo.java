package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 规则信息视图对象 rule
 *
 * @author bihua
 * @date 2023-07-02
 */
@Data
@ExcelIgnoreUnannotated
public class RuleVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private String appId;

    /**
     * 规则标识
     */
    @ExcelProperty(value = "规则标识")
    private String ruleIdentification;

    /**
     * 规则名称
     */
    @ExcelProperty(value = "规则名称")
    private String ruleName;

    /**
     * 任务标识
     */
    @ExcelProperty(value = "任务标识")
    private String jobIdentification;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 触发机制（0:全部，1:任意一个）
     */
    @ExcelProperty(value = "触发机制", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=:全部，1:任意一个")
    private Integer triggering;

    /**
     * 规则描述，可以为空
     */
    @ExcelProperty(value = "规则描述，可以为空")
    private String remark;


}
