package com.bihua.iot.domain;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;

/**
 * @ClassDescription: 创建超级表需要的入参的实体类
 * @ClassName: SuperTableDto
 * @Author: thinglinks
 * @Date: 2021-12-28 15:03:45
 * @Version 1.0
 */
@Data
public class TdSuperTable extends BaseEntity {
    /**
     * 数据库名称
     */
    @NotBlank(message = "invalid operation: databaseName can not be empty")
    private String dataBaseName;

    /**
     * 超级表名称
     */
    @NotBlank(message = "invalid operation: superTableName can not be empty")
    private String superTableName;
    /**
     * 超级表的表结构（业务相关）
     * 第一个字段的数据类型必须为timestamp
     * 字符相关数据类型必须指定大小
     * 字段名称和字段数据类型不能为空
     */
    @NotEmpty(message = "invalid operation: schemaFields can not be empty")
    private List<TdFields> schemaFields;

    /**
     * 超级表的标签字段，可以作为子表在超级表里的标识
     * 字符相关数据类型必须指定大小
     * 字段名称和字段数据类型不能为空
     */
    @NotEmpty(message = "invalid operation: tagsFields can not be empty")
    private List<TdFields> tagsFields;

    /**
     * 字段信息对象，超级表添加列时使用该属性
     */
    private TdFields fields;
}
