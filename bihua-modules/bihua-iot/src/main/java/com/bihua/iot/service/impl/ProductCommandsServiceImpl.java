package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.ProductCommandsBo;
import com.bihua.iot.domain.vo.ProductCommandsVo;
import com.bihua.iot.domain.ProductCommands;
import com.bihua.iot.mapper.ProductCommandsMapper;
import com.bihua.iot.service.IProductCommandsService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 产品设备服务命令Service业务层处理
 *
 * @author bihua
 * @date 2023-06-15
 */
@RequiredArgsConstructor
@Service
public class ProductCommandsServiceImpl implements IProductCommandsService {

    private final ProductCommandsMapper baseMapper;

    /**
     * 查询产品设备服务命令
     */
    @Override
    public ProductCommandsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询产品设备服务命令列表
     */
    @Override
    public TableDataInfo<ProductCommandsVo> queryPageList(ProductCommandsBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ProductCommands> lqw = buildQueryWrapper(bo);
        Page<ProductCommandsVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询产品设备服务命令列表
     */
    @Override
    public List<ProductCommandsVo> queryList(ProductCommandsBo bo) {
        LambdaQueryWrapper<ProductCommands> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ProductCommands> buildQueryWrapper(ProductCommandsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ProductCommands> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getServiceId() != null, ProductCommands::getServiceId, bo.getServiceId());
        lqw.like(StringUtils.isNotBlank(bo.getName()), ProductCommands::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getDescription()), ProductCommands::getDescription, bo.getDescription());
        return lqw;
    }

    /**
     * 新增产品设备服务命令
     */
    @Override
    public Boolean insertByBo(ProductCommandsBo bo) {
        ProductCommands add = BeanUtil.toBean(bo, ProductCommands.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改产品设备服务命令
     */
    @Override
    public Boolean updateByBo(ProductCommandsBo bo) {
        ProductCommands update = BeanUtil.toBean(bo, ProductCommands.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ProductCommands entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除产品设备服务命令
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
