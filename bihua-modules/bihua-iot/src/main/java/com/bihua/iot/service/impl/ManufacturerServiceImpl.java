package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ObjectUtil;
import com.bihua.common.core.domain.entity.SysUser;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.ListUtils;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.ManufacturerBo;
import com.bihua.iot.domain.vo.ManufacturerVo;
import com.bihua.iot.domain.Manufacturer;
import com.bihua.iot.mapper.ManufacturerMapper;
import com.bihua.iot.service.IManufacturerService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 厂商管理Service业务层处理
 *
 * @author wbihua
 * @date 2023-07-28
 */
@RequiredArgsConstructor
@Service
public class ManufacturerServiceImpl implements IManufacturerService {

    private final ManufacturerMapper baseMapper;

    /**
     * 查询厂商管理
     */
    @Override
    public ManufacturerVo queryById(Long mfrId){
        LambdaQueryWrapper<Manufacturer> lqw = Wrappers.lambdaQuery();
        lqw.eq(mfrId != null, Manufacturer::getMfrId, mfrId);
        List<Manufacturer> voList = baseMapper.queryList(lqw);
        if(CollectionUtil.isEmpty(voList)){
            return null;
        }else {
            return BeanCopyUtils.copy(voList.get(0), ManufacturerVo.class);
        }
    }

    /**
     * 查询厂商管理列表
     */
    @Override
    public TableDataInfo<ManufacturerVo> queryPageList(ManufacturerBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Manufacturer> lqw = buildQueryWrapper(bo);
        Page<ManufacturerVo> result = baseMapper.selectPageList(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询厂商管理列表
     */
    @Override
    public List<ManufacturerVo> queryList(ManufacturerBo bo) {
        LambdaQueryWrapper<Manufacturer> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Manufacturer> buildQueryWrapper(ManufacturerBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Manufacturer> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getDeptId() != null, Manufacturer::getDeptId, bo.getDeptId());
        lqw.like(StringUtils.isNotBlank(bo.getMfrName()), Manufacturer::getMfrName, bo.getMfrName());
        lqw.like(StringUtils.isNotBlank(bo.getMfrShortName()), Manufacturer::getMfrShortName, bo.getMfrShortName());
        lqw.eq(StringUtils.isNotBlank(bo.getEmail()), Manufacturer::getEmail, bo.getEmail());
        lqw.eq(StringUtils.isNotBlank(bo.getPhonenumber()), Manufacturer::getPhonenumber, bo.getPhonenumber());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), Manufacturer::getStatus, bo.getStatus());
        return lqw;
    }

    /**
     * 新增厂商管理
     */
    @Override
    public Boolean insertByBo(ManufacturerBo bo) {
        Manufacturer add = BeanUtil.toBean(bo, Manufacturer.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setMfrId(add.getMfrId());
        }
        return flag;
    }

    /**
     * 修改厂商管理
     */
    @Override
    public Boolean updateByBo(ManufacturerBo bo) {
        Manufacturer update = BeanUtil.toBean(bo, Manufacturer.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Manufacturer entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除厂商管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean checkMfrNameUnique(Manufacturer manufacturer) {
        boolean exist = baseMapper.exists(new LambdaQueryWrapper<Manufacturer>()
                .eq(Manufacturer::getMfrName, manufacturer.getMfrName())
                .ne(ObjectUtil.isNotNull(manufacturer.getMfrId()), Manufacturer::getMfrId, manufacturer.getMfrId()));
        return !exist;
    }
}
