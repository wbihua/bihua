package com.bihua.iot.domain;

import com.bihua.common.core.domain.BaseEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author bihua
 * @date 2023年06月17日 22:12
 */
@Data
public class TdTable extends BaseEntity {
    /**
     * 数据库名称
     */
    @NotBlank(message = "invalid operation: databaseName can not be empty")
    private String dataBaseName;

    /**
     * 超级表名称
     */
    @NotBlank(message = "invalid operation: superTableName can not be empty")
    private String superTableName;
    /**
     * 超级表普通列字段的值
     * 值需要与创建超级表时普通列字段的数据类型对应上
     */
    private List<TdFields> schemaFieldValues;

    /**
     * 超级表标签字段的值
     * 值需要与创建超级表时标签字段的数据类型对应上
     */
    private List<TdFields> tagsFieldValues;

    /**
     * 表名称
     */
    @NotBlank(message = "invalid operation: tableName can not be empty")
    private String tableName;
}
