package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;


/**
 * 设备动作视图对象 device_commands
 *
 * @author bihua
 * @date 2023-07-23
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceCommandsVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 设备标识
     */
    @ExcelProperty(value = "设备标识")
    private String deviceIdentification;

    /**
     * 服务ID
     */
    @ExcelProperty(value = "服务ID")
    private Long serviceId;

    /**
     * 命令ID
     */
    @ExcelProperty(value = "命令ID")
    private Long commandId;

    /**
     * 命令请求内容
     */
    @ExcelProperty(value = "命令请求内容")
    private String commandRequest;

    /**
     * 命令响应内容
     */
    @ExcelProperty(value = "命令响应内容")
    private String commandResponse;

    /**
     * 下达状态
     */
    @ExcelProperty(value = "下达状态")
    private String status;
}
