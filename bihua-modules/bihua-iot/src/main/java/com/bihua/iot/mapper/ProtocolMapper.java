package com.bihua.iot.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.iot.domain.Protocol;
import com.bihua.iot.domain.vo.ProtocolVo;

/**
 * 协议信息Mapper接口
 *
 * @author bihua
 * @date 2023-06-19
 */
public interface ProtocolMapper extends BaseMapperPlus<ProtocolMapper, Protocol, ProtocolVo> {

    int updateStatusById(@Param("updatedStatus")String updatedStatus, @Param("id")Long id);

    @DataPermission({
            @DataColumn(key = "deptName", value = "p.dept_id"),
    })
    Page<ProtocolVo> selectPageList(@Param("page") IPage<Protocol> page, @Param(Constants.WRAPPER) Wrapper<Protocol> wrapper);
}
