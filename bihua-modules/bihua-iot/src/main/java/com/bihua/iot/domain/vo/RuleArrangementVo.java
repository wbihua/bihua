package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 规则编排视图对象 rule_arrangement
 *
 * @author bihua
 * @date 2023-07-03
 */
@Data
@ExcelIgnoreUnannotated
public class RuleArrangementVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 应用ID
     */
    @ExcelProperty(value = "应用ID")
    private String appId;

    /**
     * 规则引擎节点
     */
    @ExcelProperty(value = "规则引擎节点", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "link_rule_node")
    private String ruleNode;

    /**
     * 规则名称
     */
    @ExcelProperty(value = "规则名称")
    private String ruleName;

    /**
     * 规则标识
     */
    @ExcelProperty(value = "规则标识")
    private String ruleIdentification;

    /**
     * 规则内容
     */
    @ExcelProperty(value = "规则内容")
    private String ruleContent;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 规则描述，可以为空
     */
    @ExcelProperty(value = "规则描述，可以为空")
    private String remark;


}
