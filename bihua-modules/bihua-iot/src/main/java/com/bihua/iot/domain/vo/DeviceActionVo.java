package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 设备动作数据视图对象 device_action
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceActionVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 设备标识
     */
    @ExcelProperty(value = "设备标识")
    private String deviceIdentification;

    /**
     * 动作类型
     */
    @ExcelProperty(value = "动作类型")
    private String actionType;

    /**
     * 内容信息
     */
    @ExcelProperty(value = "内容信息")
    private String message;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态")
    private String status;


}
