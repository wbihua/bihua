package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备Topic数据业务对象 device_topic
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceTopicBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 类型(0:基础Topic,1:自定义Topic)
     */
    @NotBlank(message = "类型(0:基础Topic,1:自定义Topic)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * topic
     */
    @NotBlank(message = "topic不能为空", groups = { AddGroup.class, EditGroup.class })
    private String topic;

    /**
     * 发布者
     */
    @NotBlank(message = "发布者不能为空", groups = { AddGroup.class, EditGroup.class })
    private String publisher;

    /**
     * 订阅者
     */
    @NotBlank(message = "订阅者不能为空", groups = { AddGroup.class, EditGroup.class })
    private String subscriber;

    /**
     * 备注
     */
    private String remark;


}
