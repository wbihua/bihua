package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 规则信息业务对象 rule
 *
 * @author bihua
 * @date 2023-07-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class RuleBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appId;

    /**
     * 规则标识
     */
    @NotBlank(message = "规则标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ruleIdentification;

    /**
     * 规则名称
     */
    @NotBlank(message = "规则名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ruleName;

    /**
     * 任务标识
     */
    @NotBlank(message = "任务标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String jobIdentification;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @NotBlank(message = "状态(字典值：0启用  1停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 触发机制（0:全部，1:任意一个）
     */
    @NotNull(message = "触发机制（0:全部，1:任意一个）不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer triggering;

    /**
     * 规则描述，可以为空
     */
    private String remark;


}
