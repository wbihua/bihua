package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.DeviceTopicBo;
import com.bihua.iot.domain.vo.DeviceTopicVo;
import com.bihua.iot.domain.DeviceTopic;
import com.bihua.iot.mapper.DeviceTopicMapper;
import com.bihua.iot.service.IDeviceTopicService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 设备Topic数据Service业务层处理
 *
 * @author bihua
 * @date 2023-06-16
 */
@RequiredArgsConstructor
@Service
public class DeviceTopicServiceImpl implements IDeviceTopicService {

    private final DeviceTopicMapper baseMapper;

    /**
     * 查询设备Topic数据
     */
    @Override
    public DeviceTopicVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询设备Topic数据列表
     */
    @Override
    public TableDataInfo<DeviceTopicVo> queryPageList(DeviceTopicBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DeviceTopic> lqw = buildQueryWrapper(bo);
        Page<DeviceTopicVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询设备Topic数据列表
     */
    @Override
    public List<DeviceTopicVo> queryList(DeviceTopicBo bo) {
        LambdaQueryWrapper<DeviceTopic> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DeviceTopic> buildQueryWrapper(DeviceTopicBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DeviceTopic> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getDeviceIdentification()), DeviceTopic::getDeviceIdentification, bo.getDeviceIdentification());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), DeviceTopic::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getTopic()), DeviceTopic::getTopic, bo.getTopic());
        lqw.eq(StringUtils.isNotBlank(bo.getPublisher()), DeviceTopic::getPublisher, bo.getPublisher());
        lqw.eq(StringUtils.isNotBlank(bo.getSubscriber()), DeviceTopic::getSubscriber, bo.getSubscriber());
        return lqw;
    }

    /**
     * 新增设备Topic数据
     */
    @Override
    public Boolean insertByBo(DeviceTopicBo bo) {
        DeviceTopic add = BeanUtil.toBean(bo, DeviceTopic.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改设备Topic数据
     */
    @Override
    public Boolean updateByBo(DeviceTopicBo bo) {
        DeviceTopic update = BeanUtil.toBean(bo, DeviceTopic.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DeviceTopic entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除设备Topic数据
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean insertBatch(List<DeviceTopic> topics) {
        return baseMapper.insertBatch(topics);
    }
}
