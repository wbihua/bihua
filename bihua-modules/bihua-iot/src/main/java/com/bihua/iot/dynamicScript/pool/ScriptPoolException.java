package com.bihua.iot.dynamicScript.pool;

public class ScriptPoolException extends RuntimeException {

	private static final long serialVersionUID = -7823778953686153874L;

	public ScriptPoolException(String message) {
		super(message);
	}

	public ScriptPoolException(Throwable e) {
		super(e);
	}

	public ScriptPoolException(String message, Throwable cause) {
		super(message, cause);
	}
}
