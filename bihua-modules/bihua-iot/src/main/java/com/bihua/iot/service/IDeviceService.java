package com.bihua.iot.service;

import com.bihua.iot.domain.Device;
import com.bihua.iot.domain.vo.DeviceVo;
import com.bihua.iot.domain.bo.DeviceBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 边设备档案信息Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceService {

    /**
     * 查询边设备档案信息
     */
    DeviceVo queryById(Long id);

    /**
     * 查询边设备档案信息列表
     */
    TableDataInfo<DeviceVo> queryPageList(DeviceBo bo, PageQuery pageQuery);

    /**
     * 查询边设备档案信息列表
     */
    List<DeviceVo> queryList(DeviceBo bo);

    /**
     * 新增边设备档案信息
     */
    Boolean insertByBo(DeviceBo bo);

    /**
     * 修改边设备档案信息
     */
    Boolean updateByBo(DeviceBo bo);

    /**
     * 校验并批量删除边设备档案信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    DeviceVo findOneByClientId(String clientId);

    DeviceVo findOneByDeviceIdentification(String deviceIdentification);

    Map<String, List<Map<String, Object>>> getDeviceShadow(String ids, String startTime, String endTime);

    int updateConnectStatusByClientId(String updatedConnectStatus, String clientId);

    /**
     * 客户端身份认证
     *
     * @param clientIdentifier 客户端
     * @param username         用户名
     * @param password         密码
     * @param deviceStatus     设备状态
     * @param protocolType     协议类型
     * @return
     */
    Boolean clientAuthentication(String clientIdentifier, String username, String password, String deviceStatus, String protocolType);

    Boolean disconnect(Long[] ids);
}
