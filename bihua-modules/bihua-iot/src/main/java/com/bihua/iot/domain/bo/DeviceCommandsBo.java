package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备动作业务对象 device_commands
 *
 * @author bihua
 * @date 2023-07-23
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceCommandsBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 服务ID
     */
    @NotNull(message = "服务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long serviceId;

    /**
     * 命令ID
     */
    @NotNull(message = "命令ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long commandId;

    /**
     * 命令请求内容
     */
    @NotBlank(message = "命令请求内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String commandRequest;

    /**
     * 命令响应内容
     */
    private String commandResponse;

    /**
     * 下达状态
     */
    @NotBlank(message = "下达状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;


}
