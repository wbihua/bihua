package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 子设备档案对象 device_info
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device_info")
public class DeviceInfo extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 边设备档案主键
     */
    @TableField(value = "d_id")
    private Long did;
    @TableField(exist = false)
    private String edgeDevicesIdentification;
    /**
     * 设备节点ID
     */
    private String nodeId;
    /**
     * 设备名称
     */
    private String nodeName;
    /**
     * 子设备唯一标识
     */
    private String deviceId;
    /**
     * 设备描述
     */
    private String description;
    /**
     * 设备型号
     */
    private String model;
    /**
     * 子设备连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    private String connectStatus;
    /**
     * 是否支持设备影子TRUE:1、FALSE :0
     */
    private Boolean shadowEnable;
    /**
     * 设备影子数据表名(多个英文逗号分割)
     */
    private String shadowTableName;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 备注
     */
    private String remark;

}
