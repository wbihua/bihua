package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 边设备档案信息对象 device
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device")
public class Device extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 客户端标识
     */
    private String clientId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 密码
     */
    private String password;
    /**
     * 认证方式
     */
    private String authMode;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 设备名称
     */
    private String deviceName;
    /**
     * 连接实例
     */
    private String connector;
    /**
     * 设备描述
     */
    private String deviceDescription;
    /**
     * 设备状态： 启用 || 禁用
     */
    private String deviceStatus;
    /**
     * 连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    private String connectStatus;
    /**
     * 是否遗言
     */
    private String isWill;
    /**
     * 设备标签
     */
    private String deviceTags;
    /**
     * 产品标识
     */
    private String productIdentification;
    /**
     * 产品协议类型 ：mqtt || coap || modbus || http
     */
    private String protocolType;
    /**
     * 设备类型
     */
    private String deviceType;
    /**
     * 备注
     */
    private String remark;

}
