package com.bihua.iot.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.iot.domain.TdSuperTable;
import com.bihua.iot.domain.bo.ProductBo;
import com.bihua.iot.domain.vo.ProductVo;

/**
 * 产品模型Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductService {

    /**
     * 查询产品模型
     */
    ProductVo queryById(Long id);

    /**
     * 查询产品模型列表
     */
    TableDataInfo<ProductVo> queryPageList(ProductBo bo, PageQuery pageQuery);

    /**
     * 查询产品模型列表
     */
    List<ProductVo> queryList(ProductBo bo);

    /**
     * 新增产品模型
     */
    Boolean insertByBo(ProductBo bo);

    /**
     * 修改产品模型
     */
    Boolean updateByBo(ProductBo bo);

    /**
     * 校验并批量删除产品模型信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    List<TdSuperTable> createSuperTableDataModel(Long[] productIds, Boolean InitializeOrNot);

    ProductVo findOneByProductIdentification(String productIdentification);
}
