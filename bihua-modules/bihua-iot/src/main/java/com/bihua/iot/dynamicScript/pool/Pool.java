package com.bihua.iot.dynamicScript.pool;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

public class Pool<T> extends GenericObjectPool<T> {

	public Pool(final PooledObjectFactory<T> factory, final GenericObjectPoolConfig<T> poolConfig) {
		super(factory, poolConfig);
	}

	@Override
	public void close() {
		destroy();
	}

	public void destroy() {
		try {
			super.close();
		} catch (RuntimeException e) {
			throw new ScriptPoolException("Could not destroy the pool", e);
		}
	}

	public T getResource() {
		try {
			return super.borrowObject();
		} catch (ScriptPoolException se) {
			throw se;
		} catch (Exception e) {
			throw new ScriptPoolException("Could not get a resource from the pool", e);
		}
	}

	public void returnResource(final T resource) {
		if (resource == null) {
			return;
		}
		try {
			super.returnObject(resource);
		} catch (RuntimeException e) {
			throw new ScriptPoolException("Could not return the resource to the pool", e);
		}
	}

	public void returnBrokenResource(final T resource) {
		if (resource == null) {
			return;
		}
		try {
			super.invalidateObject(resource);
		} catch (Exception e) {
			throw new ScriptPoolException("Could not return the broken resource to the pool", e);
		}
	}

	@Override
	public void addObjects(int count) {
		try {
			for (int i = 0; i < count; i++) {
				addObject();
			}
		} catch (Exception e) {
			throw new ScriptPoolException("Error trying to add idle objects", e);
		}
	}
}
