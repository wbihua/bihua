package com.bihua.iot.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.iot.domain.DeviceInfo;
import com.bihua.iot.domain.vo.DeviceInfoVo;

/**
 * 子设备档案Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceInfoMapper extends BaseMapperPlus<DeviceInfoMapper, DeviceInfo, DeviceInfoVo> {

    DeviceInfo findOneByDeviceId(@Param("deviceId")String deviceId);

    @DataPermission({
            @DataColumn(key = "deptName", value = "p.dept_id"),
    })
    Page<DeviceInfoVo> selectPageList(@Param("page") IPage<DeviceInfo> page, @Param(Constants.WRAPPER) Wrapper<DeviceInfo> wrapper);
}
