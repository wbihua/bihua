package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.RuleBo;
import com.bihua.iot.domain.vo.RuleVo;
import com.bihua.iot.domain.Rule;
import com.bihua.iot.mapper.RuleMapper;
import com.bihua.iot.service.IRuleService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 规则信息Service业务层处理
 *
 * @author bihua
 * @date 2023-07-02
 */
@RequiredArgsConstructor
@Service
public class RuleServiceImpl implements IRuleService {

    private final RuleMapper baseMapper;

    /**
     * 查询规则信息
     */
    @Override
    public RuleVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询规则信息列表
     */
    @Override
    public TableDataInfo<RuleVo> queryPageList(RuleBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<Rule> lqw = buildQueryWrapper(bo);
        Page<RuleVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询规则信息列表
     */
    @Override
    public List<RuleVo> queryList(RuleBo bo) {
        LambdaQueryWrapper<Rule> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<Rule> buildQueryWrapper(RuleBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<Rule> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAppId()), Rule::getAppId, bo.getAppId());
        lqw.eq(StringUtils.isNotBlank(bo.getRuleIdentification()), Rule::getRuleIdentification, bo.getRuleIdentification());
        lqw.like(StringUtils.isNotBlank(bo.getRuleName()), Rule::getRuleName, bo.getRuleName());
        lqw.eq(StringUtils.isNotBlank(bo.getJobIdentification()), Rule::getJobIdentification, bo.getJobIdentification());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), Rule::getStatus, bo.getStatus());
        lqw.eq(bo.getTriggering() != null, Rule::getTriggering, bo.getTriggering());
        return lqw;
    }

    /**
     * 新增规则信息
     */
    @Override
    public Boolean insertByBo(RuleBo bo) {
        Rule add = BeanUtil.toBean(bo, Rule.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改规则信息
     */
    @Override
    public Boolean updateByBo(RuleBo bo) {
        Rule update = BeanUtil.toBean(bo, Rule.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(Rule entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除规则信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
