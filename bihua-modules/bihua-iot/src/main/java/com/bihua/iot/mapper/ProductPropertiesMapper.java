package com.bihua.iot.mapper;

import com.bihua.iot.domain.ProductProperties;
import com.bihua.iot.domain.vo.ProductPropertiesVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 产品服务属性Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductPropertiesMapper extends BaseMapperPlus<ProductPropertiesMapper, ProductProperties, ProductPropertiesVo> {

}
