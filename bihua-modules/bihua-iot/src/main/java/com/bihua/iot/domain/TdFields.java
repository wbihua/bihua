package com.bihua.iot.domain;

import com.bihua.iot.enums.DataTypeEnum;

import lombok.Data;

/**
 * @ClassDescription: 建表的字段实体类
 * @ClassName: Fields
 * @Author: thinglinks
 * @Date: 2021-12-28 09:09:04
 * @Version 1.0
 */
@Data
public class TdFields  {
    private static final long serialVersionUID = 1L;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段值
     */
    private Object fieldValue;

    /**
     * 字段数据类型
     */
    private String dataType;

    /**
     * 字段字节大小
     */
    private Integer size;

    public TdFields() {
    }

    public TdFields(String fieldName, String dataType, Integer size) {
        this.fieldName = fieldName;
        //根据规则匹配字段数据类型
        switch (dataType.toLowerCase()) {
            case ("json"):
                this.dataType = DataTypeEnum.JSON.getDataType();
                this.size = size;
                break;
            case ("string"):
                this.dataType = DataTypeEnum.NCHAR.getDataType();
                this.size = size;
                break;
            case ("binary"):
                this.dataType = DataTypeEnum.BINARY.getDataType();
                this.size = size;
                break;
            case ("int"):
                this.dataType = DataTypeEnum.INT.getDataType();
                break;
            case ("bool"):
                this.dataType = DataTypeEnum.BOOL.getDataType();
                break;
            case ("decimal"):
                this.dataType = DataTypeEnum.DOUBLE.getDataType();
                break;
            case ("timestamp"):
                if ("eventTime".equals(fieldName)) {
                    this.fieldName = "eventTime";
                }
                this.dataType = DataTypeEnum.TIMESTAMP.getDataType();
                break;
        }
    }
}
