package com.bihua.iot.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产品服务业务对象 product_services
 *
 * @author bihua
 * @date 2023-06-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductServicesBo extends BaseEntity {

    /**
     * 服务id
     */
    @NotNull(message = "服务id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 服务名称:支持英文大小写、数字、下划线和中划线

     */
    @NotBlank(message = "服务名称", groups = { AddGroup.class, EditGroup.class })
    private String serviceName;

    /**
     * 产品模版标识
     */
    private String templateIdentification;

    /**
     * 产品标识
     */
    @NotBlank(message = "产品标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productIdentification;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @NotBlank(message = "状态(字典值：0启用  1停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 服务的描述信息:文本描述，不影响实际功能，可配置为空字符串""。

     */
    private String description;


}
