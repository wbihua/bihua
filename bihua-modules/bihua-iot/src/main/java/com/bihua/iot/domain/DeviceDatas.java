package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备消息对象 device_datas
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device_datas")
public class DeviceDatas extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 消息ID
     */
    private String messageId;
    /**
     * topic
     */
    private String topic;
    /**
     * 内容信息
     */
    private String message;
    /**
     * 状态
     */
    private String status;

}
