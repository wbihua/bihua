package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 厂商管理对象 manufacturer
 *
 * @author wbihua
 * @date 2023-07-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("manufacturer")
public class Manufacturer extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 厂商ID
     */
    @TableId(value = "mfr_id")
    private Long mfrId;
    /**
     * 部门ID
     */
    private Long deptId;
    /**
     * 厂商名称
     */
    private String mfrName;
    /**
     * 厂商简称
     */
    private String mfrShortName;
    /**
     * 联系邮箱
     */
    private String email;
    /**
     * 手机号码
     */
    private String phonenumber;
    /**
     * 状态
     */
    private String status;
    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 备注
     */
    private String remark;

}
