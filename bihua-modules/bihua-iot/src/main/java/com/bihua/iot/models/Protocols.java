package com.bihua.iot.models;

import lombok.Data;

/**
 * @author bihua
 * @date 2023年06月20日 11:08
 */
@Data
public class Protocols {
    private static final long serialVersionUID = 1L;
    /**
     * id
     */
    private Long id;
    /**
     * 协议语言
     */
    private String protocolVoice;
    /**
     * 内容
     */
    private String content;
}
