package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.ProductCommandsResponseBo;
import com.bihua.iot.domain.vo.ProductCommandsResponseVo;
import com.bihua.iot.domain.ProductCommandsResponse;
import com.bihua.iot.mapper.ProductCommandsResponseMapper;
import com.bihua.iot.service.IProductCommandsResponseService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 产品设备响应服务命令属性Service业务层处理
 *
 * @author bihua
 * @date 2023-06-15
 */
@RequiredArgsConstructor
@Service
public class ProductCommandsResponseServiceImpl implements IProductCommandsResponseService {

    private final ProductCommandsResponseMapper baseMapper;

    /**
     * 查询产品设备响应服务命令属性
     */
    @Override
    public ProductCommandsResponseVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询产品设备响应服务命令属性列表
     */
    @Override
    public TableDataInfo<ProductCommandsResponseVo> queryPageList(ProductCommandsResponseBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ProductCommandsResponse> lqw = buildQueryWrapper(bo);
        Page<ProductCommandsResponseVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询产品设备响应服务命令属性列表
     */
    @Override
    public List<ProductCommandsResponseVo> queryList(ProductCommandsResponseBo bo) {
        LambdaQueryWrapper<ProductCommandsResponse> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ProductCommandsResponse> buildQueryWrapper(ProductCommandsResponseBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ProductCommandsResponse> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCommandsId() != null, ProductCommandsResponse::getCommandsId, bo.getCommandsId());
        lqw.eq(bo.getServiceId() != null, ProductCommandsResponse::getServiceId, bo.getServiceId());
        lqw.eq(StringUtils.isNotBlank(bo.getDatatype()), ProductCommandsResponse::getDatatype, bo.getDatatype());
        lqw.eq(StringUtils.isNotBlank(bo.getEnumlist()), ProductCommandsResponse::getEnumlist, bo.getEnumlist());
        lqw.eq(bo.getMax() != null, ProductCommandsResponse::getMax, bo.getMax());
        lqw.eq(bo.getMaxlength() != null, ProductCommandsResponse::getMaxlength, bo.getMaxlength());
        lqw.eq(bo.getMin() != null, ProductCommandsResponse::getMin, bo.getMin());
        lqw.eq(StringUtils.isNotBlank(bo.getParameterDescription()), ProductCommandsResponse::getParameterDescription, bo.getParameterDescription());
        lqw.like(StringUtils.isNotBlank(bo.getParameterName()), ProductCommandsResponse::getParameterName, bo.getParameterName());
        lqw.eq(bo.getRequired() != null, ProductCommandsResponse::getRequired, bo.getRequired());
        lqw.eq(bo.getStep() != null, ProductCommandsResponse::getStep, bo.getStep());
        lqw.eq(StringUtils.isNotBlank(bo.getUnit()), ProductCommandsResponse::getUnit, bo.getUnit());
        return lqw;
    }

    /**
     * 新增产品设备响应服务命令属性
     */
    @Override
    public Boolean insertByBo(ProductCommandsResponseBo bo) {
        ProductCommandsResponse add = BeanUtil.toBean(bo, ProductCommandsResponse.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改产品设备响应服务命令属性
     */
    @Override
    public Boolean updateByBo(ProductCommandsResponseBo bo) {
        ProductCommandsResponse update = BeanUtil.toBean(bo, ProductCommandsResponse.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ProductCommandsResponse entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除产品设备响应服务命令属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
