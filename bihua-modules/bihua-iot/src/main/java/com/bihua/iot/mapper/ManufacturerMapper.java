package com.bihua.iot.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.iot.domain.Manufacturer;
import com.bihua.iot.domain.vo.ManufacturerVo;

/**
 * 厂商管理Mapper接口
 *
 * @author wbihua
 * @date 2023-07-28
 */
public interface ManufacturerMapper extends BaseMapperPlus<ManufacturerMapper, Manufacturer, ManufacturerVo> {

    @DataPermission({
            @DataColumn(key = "deptName", value = "m.dept_id"),
    })
    Page<ManufacturerVo> selectPageList(@Param("page") IPage<Manufacturer> page, @Param(Constants.WRAPPER) Wrapper<Manufacturer> wrapper);

    @DataPermission({
            @DataColumn(key = "deptName", value = "m.dept_id"),
    })
    List<Manufacturer> queryList(@Param(Constants.WRAPPER) Wrapper<Manufacturer> wrapper);
}
