package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 规则条件业务对象 rule_conditions
 *
 * @author bihua
 * @date 2023-07-02
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class RuleConditionsBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 规则ID
     */
    @NotNull(message = "规则ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long ruleId;

    /**
     * 条件类型(0:匹配设备触发、1:指定设备触发、2:按策略定时触发)
     */
    @NotNull(message = "条件类型(0:匹配设备触发、1:指定设备触发、2:按策略定时触发)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer conditionType;

    /**
     * 设备标识(匹配设备设备类型存储一个产品下所有的设备标识逗号分隔，指定设备触发存储指定的设备标识)
     */
    @NotBlank(message = "设备标识(匹配设备设备类型存储一个产品下所有的设备标识逗号分隔，指定设备触发存储指定的设备标识)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 产品标识
     */
    @NotBlank(message = "产品标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productIdentification;

    /**
     * 服务ID
     */
    @NotNull(message = "服务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long serviceId;

    /**
     * 属性ID
     */
    @NotNull(message = "属性ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long propertiesId;

    /**
     * 比较模式
<
>=
==
!=
in
between
     */
    @NotBlank(message = "比较模式不能为空", groups = { AddGroup.class, EditGroup.class })
    private String comparisonMode;

    /**
     * 比较值

between类型传值例子  [10,15] 必须是两位，且数字不能重复
判断数据是否处于一个离散的取值范围内，例如输入[1,2,3,4]，取值范围是1、2、3、4四个值，如果比较值类型为float(double)，两个float（double）型数值相差在0.000001范围内即为相等
     */
    @NotBlank(message = "比较值不能为空", groups = { AddGroup.class, EditGroup.class })
    private String comparisonValue;


}
