package com.bihua.iot.service;

import com.bihua.iot.domain.ProductCommandsResponse;
import com.bihua.iot.domain.vo.ProductCommandsResponseVo;
import com.bihua.iot.domain.bo.ProductCommandsResponseBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 产品设备响应服务命令属性Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductCommandsResponseService {

    /**
     * 查询产品设备响应服务命令属性
     */
    ProductCommandsResponseVo queryById(Long id);

    /**
     * 查询产品设备响应服务命令属性列表
     */
    TableDataInfo<ProductCommandsResponseVo> queryPageList(ProductCommandsResponseBo bo, PageQuery pageQuery);

    /**
     * 查询产品设备响应服务命令属性列表
     */
    List<ProductCommandsResponseVo> queryList(ProductCommandsResponseBo bo);

    /**
     * 新增产品设备响应服务命令属性
     */
    Boolean insertByBo(ProductCommandsResponseBo bo);

    /**
     * 修改产品设备响应服务命令属性
     */
    Boolean updateByBo(ProductCommandsResponseBo bo);

    /**
     * 校验并批量删除产品设备响应服务命令属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
