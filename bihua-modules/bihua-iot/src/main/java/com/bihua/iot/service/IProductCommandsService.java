package com.bihua.iot.service;

import com.bihua.iot.domain.ProductCommands;
import com.bihua.iot.domain.vo.ProductCommandsVo;
import com.bihua.iot.domain.bo.ProductCommandsBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 产品设备服务命令Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductCommandsService {

    /**
     * 查询产品设备服务命令
     */
    ProductCommandsVo queryById(Long id);

    /**
     * 查询产品设备服务命令列表
     */
    TableDataInfo<ProductCommandsVo> queryPageList(ProductCommandsBo bo, PageQuery pageQuery);

    /**
     * 查询产品设备服务命令列表
     */
    List<ProductCommandsVo> queryList(ProductCommandsBo bo);

    /**
     * 新增产品设备服务命令
     */
    Boolean insertByBo(ProductCommandsBo bo);

    /**
     * 修改产品设备服务命令
     */
    Boolean updateByBo(ProductCommandsBo bo);

    /**
     * 校验并批量删除产品设备服务命令信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
