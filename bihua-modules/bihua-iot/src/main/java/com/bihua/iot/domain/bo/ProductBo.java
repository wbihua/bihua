package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品模型业务对象 product
 *
 * @author bihua
 * @date 2023-06-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 部门ID
     */
    @NotNull(message = "部门ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appId;

    /**
     * 产品模版标识
     */
    private String templateIdentification;

    /**
     * 产品名称:自定义，支持中文、英文大小写、数字、下划线和中划线
     */
    @NotBlank(message = "产品名称:自定义，支持中文、英文大小写、数字、下划线和中划线不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productName;

    /**
     * 产品标识
     */
    @NotBlank(message = "产品标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productIdentification;

    /**
     * 支持以下两种产品类型•COMMON：普通产品，需直连设备。
•GATEWAY：网关产品，可挂载子设备。

     */
    @NotBlank(message = "产品类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productType;

    /**
     * 厂商ID:支持英文大小写，数字，下划线和中划线
     */
    @NotBlank(message = "厂商ID:支持英文大小写，数字，下划线和中划线不能为空", groups = { AddGroup.class, EditGroup.class })
    private String manufacturerId;

    /**
     * 厂商名称 :支持中文、英文大小写、数字、下划线和中划线
     */
    private String manufacturerName;

    /**
     * 产品型号，建议包含字母或数字来保证可扩展性。支持英文大小写、数字、下划线和中划线

     */
    @NotBlank(message = "产品型号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String model;

    /**
     * 数据格式，默认为JSON无需修改。
     */
    @NotBlank(message = "数据格式，默认为JSON无需修改。不能为空", groups = { AddGroup.class, EditGroup.class })
    private String dataFormat;

    /**
     * 设备接入平台的协议类型，默认为MQTT无需修改。
 
     */
    @NotBlank(message = "设备接入平台的协议类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String protocolType;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @NotBlank(message = "状态(字典值：0启用  1停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 产品描述
     */
    private String remark;


}
