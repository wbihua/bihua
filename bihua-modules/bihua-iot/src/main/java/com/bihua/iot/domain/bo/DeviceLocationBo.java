package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.math.BigDecimal;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备位置业务对象 device_location
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceLocationBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 纬度
     */
    private BigDecimal latitude;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 位置名称
     */
    private String fullName;

    /**
     * 省,直辖市编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区县
     */
    private String regionCode;

    /**
     * 备注
     */
    private String remark;


}
