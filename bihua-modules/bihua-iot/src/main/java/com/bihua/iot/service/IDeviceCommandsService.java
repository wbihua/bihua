package com.bihua.iot.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.iot.domain.bo.DeviceCommandsBo;
import com.bihua.iot.domain.vo.DeviceCommandsVo;

/**
 * 设备动作Service接口
 *
 * @author bihua
 * @date 2023-07-23
 */
public interface IDeviceCommandsService {

    /**
     * 查询设备动作
     */
    DeviceCommandsVo queryById(Long id);

    /**
     * 查询设备动作列表
     */
    TableDataInfo<DeviceCommandsVo> queryPageList(DeviceCommandsBo bo, PageQuery pageQuery);

    /**
     * 查询设备动作列表
     */
    List<DeviceCommandsVo> queryList(DeviceCommandsBo bo);

    /**
     * 新增设备动作
     */
    Boolean insertByBo(DeviceCommandsBo bo);

    /**
     * 修改设备动作
     */
    Boolean updateByBo(DeviceCommandsBo bo);

    /**
     * 校验并批量删除设备动作信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean updateStatusById(String status, Long id);

    Boolean updateCommandResponseById(String commandResponse, String status, Long id);
}
