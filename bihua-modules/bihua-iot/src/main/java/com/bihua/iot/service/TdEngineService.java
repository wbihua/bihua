package com.bihua.iot.service;


import java.util.List;
import java.util.Map;

import com.bihua.iot.domain.TdFields;
import com.bihua.iot.domain.TdSelect;
import com.bihua.iot.domain.TdSelectVisual;
import com.bihua.iot.domain.TdTable;
import com.bihua.iot.domain.TdTagsSelect;

/**
 * @InterfaceDescription: TdEngine业务层
 * @InterfaceName: ITdEngineService
 * @Author: thinglinks
 * @Date: 2021-12-27 13:54:58
 * @Version 1.0
 */
public interface TdEngineService {
    void createDateBase(String dataBaseName) throws Exception;

    void createSuperTable(List<TdFields> schemaFields, List<TdFields> tagsFields, String dataBaseName, String superTableName) throws Exception;

    void createTable(TdTable table) throws Exception;

    void insertData(TdTable table) throws Exception;

    List<Map<String, Object>> selectByTimesTamp(TdSelect select);

    void addColumnForSuperTable(String superTableName, TdFields fields) throws Exception;

    void dropColumnForSuperTable(String superTableName, TdFields fields) throws Exception;

    Long getCountByTimesTamp(TdSelect select) throws Exception;

    boolean checkTableExists(String dataBaseName, String tableName);

    void initSTableFrame(String msg) throws Exception;

    List<Map<String, Object>> getLastData(TdSelect select);

    Map<String, Map<String, Object>> getLastDataByTags(TdTagsSelect tagsSelect);
    
    List<Map<String, Object>> getHistoryData(TdSelectVisual selectVisual);

    List<Map<String, Object>> getRealtimeData(TdSelectVisual selectVisual);

    List<Map<String, Object>> getAggregateData(TdSelectVisual selectVisual);
}
