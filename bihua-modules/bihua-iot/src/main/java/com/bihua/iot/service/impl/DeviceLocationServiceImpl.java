package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.DeviceLocationBo;
import com.bihua.iot.domain.vo.DeviceLocationVo;
import com.bihua.iot.domain.DeviceLocation;
import com.bihua.iot.mapper.DeviceLocationMapper;
import com.bihua.iot.service.IDeviceLocationService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 设备位置Service业务层处理
 *
 * @author bihua
 * @date 2023-06-16
 */
@RequiredArgsConstructor
@Service
public class DeviceLocationServiceImpl implements IDeviceLocationService {

    private final DeviceLocationMapper baseMapper;

    /**
     * 查询设备位置
     */
    @Override
    public DeviceLocationVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询设备位置列表
     */
    @Override
    public TableDataInfo<DeviceLocationVo> queryPageList(DeviceLocationBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<DeviceLocation> lqw = buildQueryWrapper(bo);
        Page<DeviceLocationVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询设备位置列表
     */
    @Override
    public List<DeviceLocationVo> queryList(DeviceLocationBo bo) {
        LambdaQueryWrapper<DeviceLocation> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<DeviceLocation> buildQueryWrapper(DeviceLocationBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<DeviceLocation> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getDeviceIdentification()), DeviceLocation::getDeviceIdentification, bo.getDeviceIdentification());
        lqw.eq(bo.getLatitude() != null, DeviceLocation::getLatitude, bo.getLatitude());
        lqw.eq(bo.getLongitude() != null, DeviceLocation::getLongitude, bo.getLongitude());
        lqw.like(StringUtils.isNotBlank(bo.getFullName()), DeviceLocation::getFullName, bo.getFullName());
        lqw.eq(StringUtils.isNotBlank(bo.getProvinceCode()), DeviceLocation::getProvinceCode, bo.getProvinceCode());
        lqw.eq(StringUtils.isNotBlank(bo.getCityCode()), DeviceLocation::getCityCode, bo.getCityCode());
        lqw.eq(StringUtils.isNotBlank(bo.getRegionCode()), DeviceLocation::getRegionCode, bo.getRegionCode());
        return lqw;
    }

    /**
     * 新增设备位置
     */
    @Override
    public Boolean insertByBo(DeviceLocationBo bo) {
        DeviceLocation add = BeanUtil.toBean(bo, DeviceLocation.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改设备位置
     */
    @Override
    public Boolean updateByBo(DeviceLocationBo bo) {
        DeviceLocation update = BeanUtil.toBean(bo, DeviceLocation.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(DeviceLocation entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除设备位置
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
