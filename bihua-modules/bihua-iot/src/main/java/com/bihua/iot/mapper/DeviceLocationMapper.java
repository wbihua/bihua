package com.bihua.iot.mapper;

import com.bihua.iot.domain.DeviceLocation;
import com.bihua.iot.domain.vo.DeviceLocationVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 设备位置Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceLocationMapper extends BaseMapperPlus<DeviceLocationMapper, DeviceLocation, DeviceLocationVo> {

}
