package com.bihua.iot.dynamicScript;

import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.script.ScriptUtil;

/**
 * 脚本单元测试类
 * 
 * @author looly
 *
 */
public class ScriptUtilTest {
    public static void main(String[] args) {
        final Object result = ScriptUtil.invoke("var convertToBody = function convertToBody(msg) {\n" +
            "\t//your js logic...\n" +
            "\tvar now = new Date();\n" +
            "\n" +
            "\tvar year = now.getFullYear();\n" +
            "\tvar month = ('0' + (now.getMonth() + 1)).slice(-2);\n" +
            "\tvar day = ('0' + now.getDate()).slice(-2);\n" +
            "\tvar hours = ('0' + now.getHours()).slice(-2);\n" +
            "\tvar minutes = ('0' + now.getMinutes()).slice(-2);\n" +
            "\tvar seconds = ('0' + now.getSeconds()).slice(-2);\n" +
            "\n" +
            "\tvar formattedTime = year + month + day + hours + minutes + seconds;\t\n" +
            "\t\n" +
            "\tvar msgObj = JSON.parse(msg);\n" +
            "\tvar msgData = msgObj['msg'];\n" +
            "\tvar root = {};\n" +
            "\tvar devices = [];\n" +
            "\tvar device = {};\n" +
            "\tvar services = [];\n" +
            "\tvar service = {};\n" +
            "\tservice['serviceId']= msgData['serviceId'];\n" +
            "\tservice['data']= msgData['data'];\n" +
            "\tservice['eventTime']= formattedTime;\n" +
            "\tservices = services.concat(service);\n" +
            "\tdevice['deviceId']= msgData['devEui'];\n" +
            "\tdevice['services']= services;\n" +
            "\tdevices = devices.concat(device);\n" +
            "\troot['devices']= devices;\n" +
            "\treturn JSON.stringify(root);\n" +
            "}", "convertToBody", "{\n" +
            "\"msg\":{\n" +
            "\"devEui\":\"bihuapuone\",\n" +
            "\"data\":{ \"name\":\"name: bihua\"},\n" +
            "\"serviceId\":\"bihua_common_s1\"\n" +
            "}\n" +
            "}");
        System.out.println(result);
    }
}
