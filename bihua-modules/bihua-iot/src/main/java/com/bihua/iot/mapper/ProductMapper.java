package com.bihua.iot.mapper;

import com.bihua.iot.domain.Device;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.iot.domain.Product;
import com.bihua.iot.domain.vo.ProductVo;

import java.util.List;

/**
 * 产品模型Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductMapper extends BaseMapperPlus<ProductMapper, Product, ProductVo> {

    @DataPermission({
            @DataColumn(key = "deptName", value = "p.dept_id"),
    })
    Page<ProductVo> selectPageList(@Param("page") IPage<Product> page, @Param(Constants.WRAPPER) Wrapper<Product> wrapper);

    @DataPermission({
            @DataColumn(key = "deptName", value = "p.dept_id"),
    })
    List<Product> queryList(@Param(Constants.WRAPPER) Wrapper<Product> wrapper);
}
