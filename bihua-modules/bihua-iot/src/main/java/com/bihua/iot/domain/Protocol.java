package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 协议信息对象 protocol
 *
 * @author bihua
 * @date 2023-06-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("protocol")
public class Protocol extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 产品标识
     */
    private String productIdentification;
    /**
     * 协议名称
     */
    private String protocolName;
    /**
     * 协议标识
     */
    private String protocolIdentification;
    /**
     * 协议版本
     */
    private String protocolVersion;
    /**
     * 协议类型 ：mqtt || coap || modbus || http
     */
    private String protocolType;
    /**
     * 协议语言
     */
    private String protocolVoice;
    /**
     * 类名
     */
    private String className;
    /**
     * 文件地址
     */
    private String filePath;
    /**
     * 内容
     */
    private String content;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 备注
     */
    private String remark;

}
