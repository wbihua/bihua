package com.bihua.iot.service;

import com.bihua.iot.domain.ProductProperties;
import com.bihua.iot.domain.vo.ProductPropertiesVo;
import com.bihua.iot.domain.bo.ProductPropertiesBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 产品服务属性Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductPropertiesService {

    /**
     * 查询产品服务属性
     */
    ProductPropertiesVo queryById(Long id);

    /**
     * 查询产品服务属性列表
     */
    TableDataInfo<ProductPropertiesVo> queryPageList(ProductPropertiesBo bo, PageQuery pageQuery);

    /**
     * 查询产品服务属性列表
     */
    List<ProductPropertiesVo> queryList(ProductPropertiesBo bo);

    /**
     * 新增产品服务属性
     */
    Boolean insertByBo(ProductPropertiesBo bo);

    /**
     * 修改产品服务属性
     */
    Boolean updateByBo(ProductPropertiesBo bo);

    /**
     * 校验并批量删除产品服务属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
