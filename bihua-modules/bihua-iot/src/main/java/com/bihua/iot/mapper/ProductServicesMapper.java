package com.bihua.iot.mapper;

import com.bihua.iot.domain.ProductServices;
import com.bihua.iot.domain.vo.ProductServicesVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 产品服务Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductServicesMapper extends BaseMapperPlus<ProductServicesMapper, ProductServices, ProductServicesVo> {

}
