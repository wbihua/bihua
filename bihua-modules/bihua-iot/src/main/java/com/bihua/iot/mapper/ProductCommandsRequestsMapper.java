package com.bihua.iot.mapper;

import com.bihua.iot.domain.ProductCommandsRequests;
import com.bihua.iot.domain.vo.ProductCommandsRequestsVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 产品设备下发服务命令属性Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductCommandsRequestsMapper extends BaseMapperPlus<ProductCommandsRequestsMapper, ProductCommandsRequests, ProductCommandsRequestsVo> {

}
