package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备动作对象 device_commands
 *
 * @author bihua
 * @date 2023-07-23
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device_commands")
public class DeviceCommands extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id", type=IdType.AUTO)
    private Long id;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 服务ID
     */
    private Long serviceId;
    /**
     * 命令ID
     */
    private Long commandId;
    /**
     * 命令请求内容
     */
    private String commandRequest;
    /**
     * 命令响应内容
     */
    private String commandResponse;
    /**
     * 下达状态
     */
    private String status;

}
