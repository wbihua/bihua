package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 产品设备服务命令视图对象 product_commands
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@ExcelIgnoreUnannotated
public class ProductCommandsVo {

    private static final long serialVersionUID = 1L;

    /**
     * 命令id
     */
    @ExcelProperty(value = "命令id")
    private Long id;

    /**
     * 服务ID
     */
    @ExcelProperty(value = "服务ID")
    private Long serviceId;

    /**
     * 指示命令的名字，如门磁的LOCK命令、摄像头的VIDEO_RECORD命令，命令名与参数共同构成一个完整的命令。
支持英文大小写、数字及下划线，长度[2,50]。

     */
    @ExcelProperty(value = "指示命令的名字")
    private String name;

    /**
     * 命令描述。
     */
    @ExcelProperty(value = "命令描述。")
    private String description;


}
