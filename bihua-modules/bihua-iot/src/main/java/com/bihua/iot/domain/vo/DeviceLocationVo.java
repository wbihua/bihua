package com.bihua.iot.domain.vo;

import java.math.BigDecimal;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 设备位置视图对象 device_location
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceLocationVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 设备标识
     */
    @ExcelProperty(value = "设备标识")
    private String deviceIdentification;

    /**
     * 纬度
     */
    @ExcelProperty(value = "纬度")
    private BigDecimal latitude;

    /**
     * 经度
     */
    @ExcelProperty(value = "经度")
    private BigDecimal longitude;

    /**
     * 位置名称
     */
    @ExcelProperty(value = "位置名称")
    private String fullName;

    /**
     * 省,直辖市编码
     */
    @ExcelProperty(value = "省,直辖市编码")
    private String provinceCode;

    /**
     * 市编码
     */
    @ExcelProperty(value = "市编码")
    private String cityCode;

    /**
     * 区县
     */
    @ExcelProperty(value = "区县")
    private String regionCode;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
