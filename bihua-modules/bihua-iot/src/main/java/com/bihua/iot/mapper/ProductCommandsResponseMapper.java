package com.bihua.iot.mapper;

import com.bihua.iot.domain.ProductCommandsResponse;
import com.bihua.iot.domain.vo.ProductCommandsResponseVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 产品设备响应服务命令属性Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductCommandsResponseMapper extends BaseMapperPlus<ProductCommandsResponseMapper, ProductCommandsResponse, ProductCommandsResponseVo> {

}
