package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品设备下发服务命令属性业务对象 product_commands_requests
 *
 * @author bihua
 * @date 2023-06-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductCommandsRequestsBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 服务ID
     */
    @NotNull(message = "服务ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long serviceId;

    /**
     * 命令ID
     */
    @NotNull(message = "命令ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long commandsId;

    /**
     * 指示数据类型。取值范围：string、int、decimal

     */
    @NotBlank(message = "指示数据类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String datatype;

    /**
     * 指示枚举值。
如开关状态status可有如下取值
"enumList" : ["OPEN","CLOSE"]
目前本字段是非功能性字段，仅起到描述作用。建议准确定义。

     */
    private String enumlist;

    /**
     * 指示最大值。
仅当dataType为int、decimal时生效，逻辑小于等于。
     */
    private Integer max;

    /**
     * 指示字符串长度。
仅当dataType为string时生效。
     */
    private Integer maxlength;

    /**
     * 指示最小值。
仅当dataType为int、decimal时生效，逻辑大于等于。
     */
    private Integer min;

    /**
     * 命令中参数的描述，不影响实际功能，可配置为空字符串""。
     */
    private String parameterDescription;

    /**
     * 命令中参数的名字。
     */
    private String parameterName;

    /**
     * 指示本条属性是否必填，取值为0或1，默认取值1（必填）。
目前本字段是非功能性字段，仅起到描述作用。
     */
    @NotNull(message = "指示本条属性是否必填不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer required;

    /**
     * 指示步长。
     */
    private Integer step;

    /**
     * 指示单位。
取值根据参数确定，如：
•温度单位：“C”或“K”
•百分比单位：“%”
•压强单位：“Pa”或“kPa”

     */
    private String unit;


}
