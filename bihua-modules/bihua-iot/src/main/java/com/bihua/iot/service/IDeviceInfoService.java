package com.bihua.iot.service;

import com.bihua.iot.domain.DeviceInfo;
import com.bihua.iot.domain.vo.DeviceInfoVo;
import com.bihua.iot.domain.bo.DeviceInfoBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 子设备档案Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceInfoService {

    /**
     * 查询子设备档案
     */
    DeviceInfoVo queryById(Long id);

    /**
     * 查询子设备档案列表
     */
    TableDataInfo<DeviceInfoVo> queryPageList(DeviceInfoBo bo, PageQuery pageQuery);

    /**
     * 查询子设备档案列表
     */
    List<DeviceInfoVo> queryList(DeviceInfoBo bo);

    /**
     * 新增子设备档案
     */
    Boolean insertByBo(DeviceInfoBo bo);

    /**
     * 修改子设备档案
     */
    Boolean updateByBo(DeviceInfoBo bo);

    /**
     * 校验并批量删除子设备档案信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean refreshDeviceInfoDataModel(Collection<Long> idCollection);

    /**
     * 查询子设备影子数据
     *
     * @param ids 需要查询的子设备id
     * @param startTime 开始时间 格式：yyyy-MM-dd HH:mm:ss
     * @param endTime 结束时间 格式：yyyy-MM-dd HH:mm:ss
     * @return 子设备影子数据
     */
   Map<String, List<Map<String, Object>>> getDeviceInfoShadow(String ids, String startTime, String endTime);

    int deleteByDeviceIds(List<String> deviceIds);

    DeviceInfoVo findOneByDeviceId(String deviceId);
}
