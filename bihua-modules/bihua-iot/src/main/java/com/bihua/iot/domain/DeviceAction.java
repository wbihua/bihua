package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备动作数据对象 device_action
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode
@TableName("device_action")
public class DeviceAction{

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 动作类型
     */
    private String actionType;
    /**
     * 内容信息
     */
    private String message;
    /**
     * 状态
     */
    private String status;
    /**
     * 创建时间
     */
    private Date createTime;
}
