package com.bihua.iot.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 边设备档案信息业务对象 device
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 客户端标识
     */
    @NotBlank(message = "客户端标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String clientId;

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String userName;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String password;

    /**
     * 认证方式
     */
    @NotBlank(message = "认证方式不能为空", groups = { AddGroup.class, EditGroup.class })
    private String authMode;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 设备名称
     */
    @NotBlank(message = "设备名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceName;

    /**
     * 连接实例
     */
    @NotBlank(message = "连接实例不能为空", groups = { AddGroup.class, EditGroup.class })
    private String connector;

    /**
     * 设备描述
     */
    private String deviceDescription;

    /**
     * 设备状态： 启用 || 禁用
     */
    @NotBlank(message = "设备状态： 启用 || 禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceStatus;

    /**
     * 连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    private String connectStatus;

    /**
     * 是否遗言
     */
    private String isWill;

    /**
     * 设备标签
     */
    private String deviceTags;

    /**
     * 产品标识
     */
    @NotBlank(message = "产品标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String productIdentification;

    /**
     * 产品协议类型 ：mqtt || coap || modbus || http
     */
    private String protocolType;

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 备注
     */
    private String remark;

    private DeviceLocationBo deviceLocation;
}
