package com.bihua.iot.mapper;

import com.bihua.iot.domain.DeviceAction;
import com.bihua.iot.domain.vo.DeviceActionVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 设备动作数据Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceActionMapper extends BaseMapperPlus<DeviceActionMapper, DeviceAction, DeviceActionVo> {

}
