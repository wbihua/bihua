package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品设备响应服务命令属性对象 product_commands_response
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("product_commands_response")
public class ProductCommandsResponse extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 命令ID
     */
    private Long commandsId;
    /**
     * 服务ID
     */
    private Long serviceId;
    /**
     * 指示数据类型。取值范围：string、int、decimal

     */
    private String datatype;
    /**
     * 指示枚举值。
如开关状态status可有如下取值
"enumList" : ["OPEN","CLOSE"]
目前本字段是非功能性字段，仅起到描述作用。建议准确定义。

     */
    private String enumlist;
    /**
     * 指示最大值。
仅当dataType为int、decimal时生效，逻辑小于等于。
     */
    private Integer max;
    /**
     * 指示字符串长度。
仅当dataType为string时生效。
     */
    private Integer maxlength;
    /**
     * 指示最小值。
仅当dataType为int、decimal时生效，逻辑大于等于。
     */
    private Integer min;
    /**
     * 命令中参数的描述，不影响实际功能，可配置为空字符串""。
     */
    private String parameterDescription;
    /**
     * 命令中参数的名字。
     */
    private String parameterName;
    /**
     * 指示本条属性是否必填，取值为0或1，默认取值1（必填）。
目前本字段是非功能性字段，仅起到描述作用。
     */
    private Integer required;
    /**
     * 指示步长。
     */
    private Integer step;
    /**
     * 指示单位。
取值根据参数确定，如：
•温度单位：“C”或“K”
•百分比单位：“%”
•压强单位：“Pa”或“kPa”

     */
    private String unit;

}
