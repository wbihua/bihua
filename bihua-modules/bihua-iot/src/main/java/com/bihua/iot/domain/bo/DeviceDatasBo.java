package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备消息业务对象 device_datas
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceDatasBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 消息ID
     */
    @NotBlank(message = "消息ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String messageId;

    /**
     * topic
     */
    @NotBlank(message = "topic不能为空", groups = { AddGroup.class, EditGroup.class })
    private String topic;

    /**
     * 内容信息
     */
    @NotBlank(message = "内容信息不能为空", groups = { AddGroup.class, EditGroup.class })
    private String message;

    /**
     * 状态
     */
    @NotBlank(message = "状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;


}
