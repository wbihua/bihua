package com.bihua.iot.mapper;

import com.bihua.iot.domain.DeviceCommands;
import com.bihua.iot.domain.vo.DeviceCommandsVo;
import com.bihua.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 设备动作Mapper接口
 *
 * @author bihua
 * @date 2023-07-23
 */
public interface DeviceCommandsMapper extends BaseMapperPlus<DeviceCommandsMapper, DeviceCommands, DeviceCommandsVo> {

    int updateStatusById(@Param("updatedStatus") String updatedStatus, @Param("id") Long id);
    int updateCommandRequestById(@Param("commandRequest") String commandRequest, @Param("id") Long id);
    int updateCommandResponseById(@Param("commandResponse") String commandResponse, @Param("updatedStatus") String updatedStatus, @Param("id") Long id);
}
