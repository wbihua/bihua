package com.bihua.iot.service;

import com.bihua.iot.domain.RuleConditions;
import com.bihua.iot.domain.vo.RuleConditionsVo;
import com.bihua.iot.domain.bo.RuleConditionsBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 规则条件Service接口
 *
 * @author bihua
 * @date 2023-07-02
 */
public interface IRuleConditionsService {

    /**
     * 查询规则条件
     */
    RuleConditionsVo queryById(Long id);

    /**
     * 查询规则条件列表
     */
    TableDataInfo<RuleConditionsVo> queryPageList(RuleConditionsBo bo, PageQuery pageQuery);

    /**
     * 查询规则条件列表
     */
    List<RuleConditionsVo> queryList(RuleConditionsBo bo);

    /**
     * 新增规则条件
     */
    Boolean insertByBo(RuleConditionsBo bo);

    /**
     * 修改规则条件
     */
    Boolean updateByBo(RuleConditionsBo bo);

    /**
     * 校验并批量删除规则条件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
