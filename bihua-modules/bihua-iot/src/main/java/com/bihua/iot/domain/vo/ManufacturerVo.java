package com.bihua.iot.domain.vo;

import java.util.Date;

import com.bihua.common.annotation.Sensitive;
import com.bihua.common.enums.SensitiveStrategy;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 厂商管理视图对象 manufacturer
 *
 * @author wbihua
 * @date 2023-07-28
 */
@Data
@ExcelIgnoreUnannotated
public class ManufacturerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 厂商ID
     */
    @ExcelProperty(value = "厂商ID")
    private Long mfrId;

    /**
     * 部门ID
     */
    @ExcelProperty(value = "部门ID")
    private Long deptId;

    /**
     * 厂商名称
     */
    @ExcelProperty(value = "厂商名称")
    private String mfrName;

    /**
     * 厂商简称
     */
    @ExcelProperty(value = "厂商简称")
    private String mfrShortName;

    /**
     * 联系邮箱
     */
    @Sensitive(strategy = SensitiveStrategy.EMAIL)
    @ExcelProperty(value = "联系邮箱")
    private String email;

    /**
     * 手机号码
     */
    @Sensitive(strategy = SensitiveStrategy.PHONE)
    @ExcelProperty(value = "手机号码")
    private String phonenumber;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(dictType = "sys_common_status")
    private String status;
    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
