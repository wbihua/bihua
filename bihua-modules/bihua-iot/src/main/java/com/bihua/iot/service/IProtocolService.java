package com.bihua.iot.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.iot.domain.bo.ProtocolBo;
import com.bihua.iot.domain.vo.ProtocolVo;

/**
 * 协议信息Service接口
 *
 * @author bihua
 * @date 2023-06-19
 */
public interface IProtocolService {

    /**
     * 查询协议信息
     */
    ProtocolVo queryById(Long id);

    /**
     * 查询协议信息列表
     */
    TableDataInfo<ProtocolVo> queryPageList(ProtocolBo bo, PageQuery pageQuery);

    /**
     * 查询协议信息列表
     */
    List<ProtocolVo> queryList(ProtocolBo bo);

    /**
     * 新增协议信息
     */
    Long insertByBo(ProtocolBo bo);

    /**
     * 修改协议信息
     */
    Boolean updateByBo(ProtocolBo bo);

    /**
     * 校验并批量删除协议信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    boolean enable(Long id);

    boolean disable(Long id);
}
