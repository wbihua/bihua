package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备Topic数据对象 device_topic
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device_topic")
public class DeviceTopic extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 类型(0:基础Topic,1:自定义Topic)
     */
    private String type;
    /**
     * topic
     */
    private String topic;
    /**
     * 发布者
     */
    private String publisher;
    /**
     * 订阅者
     */
    private String subscriber;
    /**
     * 备注
     */
    private String remark;

}
