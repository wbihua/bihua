package com.bihua.iot.dynamicCompilation;

import java.util.Date;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;


public class BihuaProtocol {

    public static void main(String[] args) throws Exception {
        String injson = "";
        if (args != null && args.length != 0) {
            injson = args[0];
        }
        JSONObject jsonObject = JSONUtil.parseObj(injson);
        String data = ((JSONObject) jsonObject.get("msg")).get("data").toString();
        JSONArray services = new JSONArray();
        JSONObject server = new JSONObject();
        server.put("serviceId", ((JSONObject) jsonObject.get("msg")).get("serviceId"));
        server.put("data", data);
        server.put("eventTime", DateUtil.format(new Date(), DatePattern.PURE_DATETIME_FORMAT));
        services.add(server);
        JSONObject device = new JSONObject();
        device.put("deviceId", ((JSONObject) jsonObject.get("msg")).get("devEui"));
        device.put("services", services);
        JSONArray devices = new JSONArray();
        devices.add(device);
        JSONObject root = new JSONObject();
        root.put("devices", devices);
        System.out.println(root.toString());
    }
}
