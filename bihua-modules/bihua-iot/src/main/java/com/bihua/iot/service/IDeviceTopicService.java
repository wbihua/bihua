package com.bihua.iot.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.iot.domain.DeviceTopic;
import com.bihua.iot.domain.bo.DeviceTopicBo;
import com.bihua.iot.domain.vo.DeviceTopicVo;

/**
 * 设备Topic数据Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceTopicService {

    /**
     * 查询设备Topic数据
     */
    DeviceTopicVo queryById(Long id);

    /**
     * 查询设备Topic数据列表
     */
    TableDataInfo<DeviceTopicVo> queryPageList(DeviceTopicBo bo, PageQuery pageQuery);

    /**
     * 查询设备Topic数据列表
     */
    List<DeviceTopicVo> queryList(DeviceTopicBo bo);

    /**
     * 新增设备Topic数据
     */
    Boolean insertByBo(DeviceTopicBo bo);

    /**
     * 修改设备Topic数据
     */
    Boolean updateByBo(DeviceTopicBo bo);

    /**
     * 校验并批量删除设备Topic数据信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean insertBatch(List<DeviceTopic> topics);
}
