package com.bihua.iot.service;

import com.alibaba.fastjson.JSONObject;
import com.bihua.iot.domain.DeviceDatas;
import com.bihua.iot.domain.vo.DeviceDatasVo;
import com.bihua.iot.domain.bo.DeviceDatasBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 设备消息Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceDatasService {

    /**
     * 查询设备消息
     */
    DeviceDatasVo queryById(Long id);

    /**
     * 查询设备消息列表
     */
    TableDataInfo<DeviceDatasVo> queryPageList(DeviceDatasBo bo, PageQuery pageQuery);

    /**
     * 查询设备消息列表
     */
    List<DeviceDatasVo> queryList(DeviceDatasBo bo);

    /**
     * 新增设备消息
     */
    Boolean insertByBo(DeviceDatasBo bo);

    /**
     * 修改设备消息
     */
    Boolean updateByBo(DeviceDatasBo bo);

    /**
     * 校验并批量删除设备消息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * thinglinks-mqtt基础数据处理
     *
     * @param thinglinksMessage
     */
    void insertBaseDatas(JSONObject thinglinksMessage) throws Exception;
}
