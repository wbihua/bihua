package com.bihua.iot.mapper;

import com.bihua.iot.domain.DeviceDatas;
import com.bihua.iot.domain.vo.DeviceDatasVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 设备消息Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceDatasMapper extends BaseMapperPlus<DeviceDatasMapper, DeviceDatas, DeviceDatasVo> {

}
