package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 规则信息对象 rule
 *
 * @author bihua
 * @date 2023-07-02
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("rule")
public class Rule extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 应用ID
     */
    private String appId;
    /**
     * 规则标识
     */
    private String ruleIdentification;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 任务标识
     */
    private String jobIdentification;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 触发机制（0:全部，1:任意一个）
     */
    private Integer triggering;
    /**
     * 规则描述，可以为空
     */
    private String remark;

}
