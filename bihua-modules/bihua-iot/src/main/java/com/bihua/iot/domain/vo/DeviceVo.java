package com.bihua.iot.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 边设备档案信息视图对象 device
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 客户端标识
     */
    @ExcelProperty(value = "客户端标识")
    private String clientId;

    /**
     * 用户名
     */
    @ExcelProperty(value = "用户名")
    private String userName;

    /**
     * 密码
     */
    @ExcelProperty(value = "密码")
    private String password;

    /**
     * 认证方式
     */
    @ExcelProperty(value = "认证方式")
    private String authMode;

    /**
     * 设备标识
     */
    @ExcelProperty(value = "设备标识")
    private String deviceIdentification;

    /**
     * 设备名称
     */
    @ExcelProperty(value = "设备名称")
    private String deviceName;

    /**
     * 连接实例
     */
    @ExcelProperty(value = "连接实例")
    private String connector;

    /**
     * 设备描述
     */
    @ExcelProperty(value = "设备描述")
    private String deviceDescription;

    /**
     * 设备状态： 启用 || 禁用
     */
    @ExcelProperty(value = "设备状态： 启用 || 禁用")
    private String deviceStatus;

    /**
     * 连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    @ExcelProperty(value = "连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT")
    private String connectStatus;

    /**
     * 是否遗言
     */
    @ExcelProperty(value = "是否遗言")
    private String isWill;

    /**
     * 设备标签
     */
    @ExcelProperty(value = "设备标签")
    private String deviceTags;

    /**
     * 产品标识
     */
    @ExcelProperty(value = "产品标识")
    private String productIdentification;

    /**
     * 产品协议类型 ：mqtt || coap || modbus || http
     */
    @ExcelProperty(value = "产品协议类型 ：mqtt || coap || modbus || http")
    private String protocolType;

    /**
     * 设备类型
     */
    @ExcelProperty(value = "设备类型")
    private String deviceType;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /**
     * 更新者
     */
    @ExcelProperty(value = "更新者")
    private String updateBy;

    /**
     * 更新时间
     */
    @ExcelProperty(value = "更新时间")
    private Date updateTime;

    private DeviceLocationVo location;
}
