package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 产品模型对象 product
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("product")
public class Product extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 部门ID
     */
    private Long deptId;
    /**
     * 应用ID
     */
    private String appId;
    /**
     * 产品模版标识
     */
    private String templateIdentification;
    /**
     * 产品名称:自定义，支持中文、英文大小写、数字、下划线和中划线
     */
    private String productName;
    /**
     * 产品标识
     */
    private String productIdentification;
    /**
     * 支持以下两种产品类型•COMMON：普通产品，需直连设备。
•GATEWAY：网关产品，可挂载子设备。

     */
    private String productType;
    /**
     * 厂商ID:支持英文大小写，数字，下划线和中划线
     */
    private String manufacturerId;
    /**
     * 厂商名称 :支持中文、英文大小写、数字、下划线和中划线
     */
    private String manufacturerName;
    /**
     * 产品型号，建议包含字母或数字来保证可扩展性。支持英文大小写、数字、下划线和中划线

     */
    private String model;
    /**
     * 数据格式，默认为JSON无需修改。
     */
    private String dataFormat;
    /**
     * 设备接入平台的协议类型，默认为MQTT无需修改。
 
     */
    private String protocolType;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 产品描述
     */
    private String remark;

}
