package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 规则编排对象 rule_arrangement
 *
 * @author bihua
 * @date 2023-07-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("rule_arrangement")
public class RuleArrangement extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 应用ID
     */
    private String appId;
    /**
     * 规则引擎节点
     */
    private String ruleNode;
    /**
     * 规则名称
     */
    private String ruleName;
    /**
     * 规则标识
     */
    private String ruleIdentification;
    /**
     * 规则内容
     */
    private String ruleContent;
    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;
    /**
     * 规则描述，可以为空
     */
    private String remark;

}
