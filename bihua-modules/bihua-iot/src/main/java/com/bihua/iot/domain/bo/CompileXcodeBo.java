package com.bihua.iot.domain.bo;

import javax.validation.constraints.NotBlank;

import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author bihua
 * @date 2023年06月19日 15:02
 */
@Data
@EqualsAndHashCode
public class CompileXcodeBo extends BaseEntity {
    @NotBlank(message = "协议ID不能为空")
    private Long id;
    @NotBlank(message = "协议语言不能为空")
    private String protocolVoice;
    @NotBlank(message = "脚本方法块不能为空")
    private String content;
    @NotBlank(message = "脚本方法块入参不能为空")
    private String inparam;

}
