package com.bihua.iot.service;

import com.bihua.iot.domain.RuleArrangement;
import com.bihua.iot.domain.vo.RuleArrangementVo;
import com.bihua.iot.domain.bo.RuleArrangementBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 规则编排Service接口
 *
 * @author bihua
 * @date 2023-07-03
 */
public interface IRuleArrangementService {

    /**
     * 查询规则编排
     */
    RuleArrangementVo queryById(Long id);

    /**
     * 查询规则编排列表
     */
    TableDataInfo<RuleArrangementVo> queryPageList(RuleArrangementBo bo, PageQuery pageQuery);

    /**
     * 查询规则编排列表
     */
    List<RuleArrangementVo> queryList(RuleArrangementBo bo);

    /**
     * 新增规则编排
     */
    Boolean insertByBo(RuleArrangementBo bo);

    /**
     * 修改规则编排
     */
    Boolean updateByBo(RuleArrangementBo bo);
    /**
     * 修改规则编排
     */
    Boolean updateRuleContent(String ruleIdentification);

    /**
     * 校验并批量删除规则编排信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
