package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 产品服务视图对象 product_services
 *
 * @author bihua
 * @date 2023-06-15
 */
@Data
@ExcelIgnoreUnannotated
public class ProductServicesVo {

    private static final long serialVersionUID = 1L;

    /**
     * 服务id
     */
    @ExcelProperty(value = "服务id")
    private Long id;

    /**
     * 服务名称:
     */
    @ExcelProperty(value = "服务名称")
    private String serviceName;

    /**
     * 产品模版标识
     */
    @ExcelProperty(value = "产品模版标识")
    private String templateIdentification;

    /**
     * 产品标识
     */
    @ExcelProperty(value = "产品标识")
    private String productIdentification;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @ExcelProperty(value = "状态(字典值：0启用  1停用)")
    private String status;

    /**
     * 服务的描述信息
     */
    @ExcelProperty(value = "服务的描述信息")
    private String description;


}
