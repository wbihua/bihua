package com.bihua.iot.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.iot.domain.Device;
import com.bihua.iot.domain.Product;
import com.bihua.iot.domain.vo.DeviceVo;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.iot.domain.vo.ProductVo;
import org.apache.ibatis.annotations.Param;

/**
 * 边设备档案信息Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceMapper extends BaseMapperPlus<DeviceMapper, Device, DeviceVo> {

    @DataPermission({
            @DataColumn(key = "deptName", value = "p.dept_id"),
    })
    Page<DeviceVo> selectPageList(@Param("page") IPage<Device> page, @Param(Constants.WRAPPER) Wrapper<Device> wrapper);

    Device findOneByClientId(@Param("clientId") String clientId);

    Device findOneByDeviceIdentification(@Param("deviceIdentification") String deviceIdentification);

    int updateConnectStatusByClientId(@Param("updatedConnectStatus") String updatedConnectStatus, @Param("clientId") String clientId);

    Device findOneByClientIdAndUserNameAndPasswordAndDeviceStatusAndProtocolType(@Param("clientId") String clientId, @Param("userName") String userName, @Param("password") String password, @Param("deviceStatus") String deviceStatus, @Param("protocolType") String protocolType);
}
