package com.bihua.iot.service;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Map;

/**
 * @author bihua
 * @date 2023年06月19日 10:24
 */
public interface MqttService {

    String sendMessage(@RequestBody Map<String, Object> params);

    String closeConnection(@RequestBody List<String> clientIdentifiers);
}
