package com.bihua.iot.mapper;

import com.bihua.iot.domain.DeviceTopic;
import com.bihua.iot.domain.vo.DeviceTopicVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 设备Topic数据Mapper接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface DeviceTopicMapper extends BaseMapperPlus<DeviceTopicMapper, DeviceTopic, DeviceTopicVo> {

}
