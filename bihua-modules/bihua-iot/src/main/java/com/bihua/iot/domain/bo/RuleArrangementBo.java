package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 规则编排业务对象 rule_arrangement
 *
 * @author bihua
 * @date 2023-07-03
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class RuleArrangementBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 应用ID
     */
    @NotBlank(message = "应用ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String appId;

    /**
     * 规则引擎节点
     */
    @NotBlank(message = "规则引擎节点不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ruleNode;

    /**
     * 规则名称
     */
    @NotBlank(message = "规则名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String ruleName;

    /**
     * 规则标识
     */
    private String ruleIdentification;

    /**
     * 规则内容
     */
    private String ruleContent;

    /**
     * 状态(字典值：0启用  1停用)
     */
    private String status;

    /**
     * 规则描述，可以为空
     */
    private String remark;


}
