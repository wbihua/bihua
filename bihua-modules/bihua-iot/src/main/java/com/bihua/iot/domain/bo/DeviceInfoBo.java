package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 子设备档案业务对象 device_info
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceInfoBo extends BaseEntity {

    /**
     * 主键
     */
    @NotNull(message = "主键不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 边设备档案主键
     */
    @NotNull(message = "边设备档案主键不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long did;

    /**
     * 设备节点ID
     */
    @NotBlank(message = "设备节点ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nodeId;

    /**
     * 设备名称
     */
    @NotBlank(message = "设备名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nodeName;

    /**
     * 子设备唯一标识
     */
    @NotBlank(message = "子设备唯一标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceId;

    /**
     * 设备描述
     */
    private String description;

    /**
     * 设备型号
     */
    private String model;

    /**
     * 子设备连接状态 : 在线：ONLINE || 离线：OFFLINE || 未连接：INIT
     */
    private String connectStatus;

    /**
     * 是否支持设备影子TRUE:1、FALSE :0
     */
    @NotNull(message = "是否支持设备影子TRUE:1、FALSE :0不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean shadowEnable;

    /**
     * 设备影子数据表名(多个英文逗号分割)
     */
    private String shadowTableName;

    /**
     * 状态(字典值：0启用  1停用)
     */
    @NotBlank(message = "状态(字典值：0启用  1停用)不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;

    /**
     * 备注
     */
    private String remark;
}
