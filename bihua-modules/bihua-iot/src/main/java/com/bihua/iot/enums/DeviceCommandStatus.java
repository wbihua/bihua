package com.bihua.iot.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wuzhong
 * @date 2023年07月23日 23:22
 */
@Getter
@AllArgsConstructor
public enum DeviceCommandStatus {

    CREATE("1","创建命令"),
    WAIT("2","等待响应"),
    SUCCESS("3","响应成功"),
    FAILURE("4","响应失败");

    private  String key;
    private  String value;
}
