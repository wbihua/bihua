package com.bihua.iot.mapper;

import com.bihua.iot.domain.RuleConditions;
import com.bihua.iot.domain.vo.RuleConditionsVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 规则条件Mapper接口
 *
 * @author bihua
 * @date 2023-07-02
 */
public interface RuleConditionsMapper extends BaseMapperPlus<RuleConditionsMapper, RuleConditions, RuleConditionsVo> {

}
