package com.bihua.iot.service;

import com.bihua.iot.domain.DeviceLocation;
import com.bihua.iot.domain.vo.DeviceLocationVo;
import com.bihua.iot.domain.bo.DeviceLocationBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 设备位置Service接口
 *
 * @author bihua
 * @date 2023-06-16
 */
public interface IDeviceLocationService {

    /**
     * 查询设备位置
     */
    DeviceLocationVo queryById(Long id);

    /**
     * 查询设备位置列表
     */
    TableDataInfo<DeviceLocationVo> queryPageList(DeviceLocationBo bo, PageQuery pageQuery);

    /**
     * 查询设备位置列表
     */
    List<DeviceLocationVo> queryList(DeviceLocationBo bo);

    /**
     * 新增设备位置
     */
    Boolean insertByBo(DeviceLocationBo bo);

    /**
     * 修改设备位置
     */
    Boolean updateByBo(DeviceLocationBo bo);

    /**
     * 校验并批量删除设备位置信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
