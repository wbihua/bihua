package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 规则条件视图对象 rule_conditions
 *
 * @author bihua
 * @date 2023-07-02
 */
@Data
@ExcelIgnoreUnannotated
public class RuleConditionsVo {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ExcelProperty(value = "主键")
    private Long id;

    /**
     * 规则ID
     */
    @ExcelProperty(value = "规则ID")
    private Long ruleId;

    /**
     * 条件类型(0:匹配设备触发、1:指定设备触发、2:按策略定时触发)
     */
    @ExcelProperty(value = "条件类型(0:匹配设备触发、1:指定设备触发、2:按策略定时触发)")
    private Integer conditionType;

    /**
     * 设备标识(匹配设备设备类型存储一个产品下所有的设备标识逗号分隔，指定设备触发存储指定的设备标识)
     */
    @ExcelProperty(value = "设备标识(匹配设备设备类型存储一个产品下所有的设备标识逗号分隔，指定设备触发存储指定的设备标识)")
    private String deviceIdentification;

    /**
     * 产品标识
     */
    @ExcelProperty(value = "产品标识")
    private String productIdentification;

    /**
     * 服务ID
     */
    @ExcelProperty(value = "服务ID")
    private Long serviceId;

    /**
     * 属性ID
     */
    @ExcelProperty(value = "属性ID")
    private Long propertiesId;

    /**
     * 比较模式
<

>=
==
!=
in
between
     */
    @ExcelProperty(value = "比较模式")
    private String comparisonMode;

    /**
     * 比较值

between类型传值例子  [10,15] 必须是两位，且数字不能重复
判断数据是否处于一个离散的取值范围内，例如输入[1,2,3,4]，取值范围是1、2、3、4四个值，如果比较值类型为float(double)，两个float（double）型数值相差在0.000001范围内即为相等
     */
    @ExcelProperty(value = "比较值", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "d=ouble")
    private String comparisonValue;


}
