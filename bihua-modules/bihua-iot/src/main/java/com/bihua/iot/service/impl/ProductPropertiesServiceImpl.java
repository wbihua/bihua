package com.bihua.iot.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.iot.domain.bo.ProductPropertiesBo;
import com.bihua.iot.domain.vo.ProductPropertiesVo;
import com.bihua.iot.domain.ProductProperties;
import com.bihua.iot.mapper.ProductPropertiesMapper;
import com.bihua.iot.service.IProductPropertiesService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 产品服务属性Service业务层处理
 *
 * @author bihua
 * @date 2023-06-15
 */
@RequiredArgsConstructor
@Service
public class ProductPropertiesServiceImpl implements IProductPropertiesService {

    private final ProductPropertiesMapper baseMapper;

    /**
     * 查询产品服务属性
     */
    @Override
    public ProductPropertiesVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询产品服务属性列表
     */
    @Override
    public TableDataInfo<ProductPropertiesVo> queryPageList(ProductPropertiesBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<ProductProperties> lqw = buildQueryWrapper(bo);
        Page<ProductPropertiesVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询产品服务属性列表
     */
    @Override
    public List<ProductPropertiesVo> queryList(ProductPropertiesBo bo) {
        LambdaQueryWrapper<ProductProperties> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<ProductProperties> buildQueryWrapper(ProductPropertiesBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<ProductProperties> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), ProductProperties::getName, bo.getName());
        lqw.eq(bo.getServiceId() != null, ProductProperties::getServiceId, bo.getServiceId());
        lqw.eq(StringUtils.isNotBlank(bo.getDatatype()), ProductProperties::getDatatype, bo.getDatatype());
        lqw.eq(StringUtils.isNotBlank(bo.getDescription()), ProductProperties::getDescription, bo.getDescription());
        lqw.eq(StringUtils.isNotBlank(bo.getEnumlist()), ProductProperties::getEnumlist, bo.getEnumlist());
        lqw.eq(bo.getMax() != null, ProductProperties::getMax, bo.getMax());
        lqw.eq(bo.getMaxlength() != null, ProductProperties::getMaxlength, bo.getMaxlength());
        lqw.eq(StringUtils.isNotBlank(bo.getMethod()), ProductProperties::getMethod, bo.getMethod());
        lqw.eq(bo.getMin() != null, ProductProperties::getMin, bo.getMin());
        lqw.eq(bo.getRequired() != null, ProductProperties::getRequired, bo.getRequired());
        lqw.eq(bo.getStep() != null, ProductProperties::getStep, bo.getStep());
        lqw.eq(StringUtils.isNotBlank(bo.getUnit()), ProductProperties::getUnit, bo.getUnit());
        return lqw;
    }

    /**
     * 新增产品服务属性
     */
    @Override
    public Boolean insertByBo(ProductPropertiesBo bo) {
        ProductProperties add = BeanUtil.toBean(bo, ProductProperties.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改产品服务属性
     */
    @Override
    public Boolean updateByBo(ProductPropertiesBo bo) {
        ProductProperties update = BeanUtil.toBean(bo, ProductProperties.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(ProductProperties entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除产品服务属性
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
