package com.bihua.iot.mapper;

import com.bihua.iot.domain.ProductCommands;
import com.bihua.iot.domain.vo.ProductCommandsVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 产品设备服务命令Mapper接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface ProductCommandsMapper extends BaseMapperPlus<ProductCommandsMapper, ProductCommands, ProductCommandsVo> {

}
