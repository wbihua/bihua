package com.bihua.iot.service;

import com.bihua.common.utils.redis.RedisUtils;
import com.bihua.iot.constant.Constants;
import com.bihua.iot.dynamicCompilation.service.JavaExecuteService;
import com.bihua.iot.dynamicScript.nashorn.NashornExecuteService;
import com.bihua.iot.enums.ProtocolType;
import com.bihua.iot.models.Protocols;

/**
 * @author bihua
 * @date 2023年06月20日 11:26
 */
public class DynamicExecuteService {

    public static String executeDynamically(String deviceIdentification, String mesBody) {
        Protocols protocols = RedisUtils.getCacheObject(Constants.DEVICE_DATA_REPORTED_AGREEMENT_SCRIPT + ProtocolType.MQTT.getValue() + deviceIdentification);
        switch (protocols.getProtocolVoice()) {
            case "java":
                return JavaExecuteService.executeDynamically(protocols.getContent(), mesBody);
            case "js":
                return NashornExecuteService.executeDynamically(protocols.getId(), protocols.getContent(), mesBody);
            default:
                return null;
        }
    }
}
