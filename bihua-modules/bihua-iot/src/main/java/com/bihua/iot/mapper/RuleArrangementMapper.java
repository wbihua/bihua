package com.bihua.iot.mapper;

import com.bihua.iot.domain.RuleArrangement;
import com.bihua.iot.domain.vo.RuleArrangementVo;
import com.bihua.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

/**
 * 规则编排Mapper接口
 *
 * @author bihua
 * @date 2023-07-03
 */
public interface RuleArrangementMapper extends BaseMapperPlus<RuleArrangementMapper, RuleArrangement, RuleArrangementVo> {

    int updateRuleIdentificationById(@Param("ruleIdentification")String ruleIdentification, @Param("id")Long id);

    int updateRuleContentByRuleIdentification(@Param("ruleContent")String ruleContent, @Param("ruleIdentification")String ruleIdentification);

    RuleArrangement findOneByRuleIdentification(@Param("ruleIdentification")String ruleIdentification);
}
