package com.bihua.iot.domain.bo;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.xss.Xss;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 厂商管理业务对象 manufacturer
 *
 * @author wbihua
 * @date 2023-07-28
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class ManufacturerBo extends BaseEntity {

    /**
     * 厂商ID
     */
    @NotNull(message = "厂商ID不能为空", groups = { EditGroup.class })
    private Long mfrId;

    /**
     * 部门ID
     */
    @NotNull(message = "部门ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long deptId;

    /**
     * 厂商名称
     */
    @Xss(message = "厂商名称不能包含脚本字符")
    @NotBlank(message = "厂商名称不能为空", groups = { AddGroup.class, EditGroup.class })
    @Size(min = 0, max = 50, message = "厂商名称长度不能超过{max}个字符")
    private String mfrName;

    /**
     * 厂商简称
     */
    @Xss(message = "厂商简称不能包含脚本字符")
    @NotBlank(message = "厂商简称不能为空", groups = { AddGroup.class, EditGroup.class })
    @Size(min = 0, max = 50, message = "厂商简称长度不能超过{max}个字符")
    private String mfrShortName;

    /**
     * 联系邮箱
     */
    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过{max}个字符")
    private String email;

    /**
     * 手机号码
     */
    private String phonenumber;

    /**
     * 状态
     */
    @NotBlank(message = "状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;
    /**
     * 备注
     */
    private String remark;


}
