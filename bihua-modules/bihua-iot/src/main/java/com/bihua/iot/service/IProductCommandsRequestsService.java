package com.bihua.iot.service;

import com.bihua.iot.domain.ProductCommandsRequests;
import com.bihua.iot.domain.vo.ProductCommandsRequestsVo;
import com.bihua.iot.domain.bo.ProductCommandsRequestsBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 产品设备下发服务命令属性Service接口
 *
 * @author bihua
 * @date 2023-06-15
 */
public interface IProductCommandsRequestsService {

    /**
     * 查询产品设备下发服务命令属性
     */
    ProductCommandsRequestsVo queryById(Long id);

    /**
     * 查询产品设备下发服务命令属性列表
     */
    TableDataInfo<ProductCommandsRequestsVo> queryPageList(ProductCommandsRequestsBo bo, PageQuery pageQuery);

    /**
     * 查询产品设备下发服务命令属性列表
     */
    List<ProductCommandsRequestsVo> queryList(ProductCommandsRequestsBo bo);

    /**
     * 新增产品设备下发服务命令属性
     */
    Boolean insertByBo(ProductCommandsRequestsBo bo);

    /**
     * 修改产品设备下发服务命令属性
     */
    Boolean updateByBo(ProductCommandsRequestsBo bo);

    /**
     * 校验并批量删除产品设备下发服务命令属性信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
