package com.bihua.iot.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备动作数据业务对象 device_action
 *
 * @author bihua
 * @date 2023-06-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class DeviceActionBo extends BaseEntity {

    /**
     * id
     */
    @NotNull(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 设备标识
     */
    @NotBlank(message = "设备标识不能为空", groups = { AddGroup.class, EditGroup.class })
    private String deviceIdentification;

    /**
     * 动作类型
     */
    @NotBlank(message = "动作类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String actionType;

    /**
     * 内容信息
     */
    @NotBlank(message = "内容信息不能为空", groups = { AddGroup.class, EditGroup.class })
    private String message;

    /**
     * 状态
     */
    @NotBlank(message = "状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private String status;


}
