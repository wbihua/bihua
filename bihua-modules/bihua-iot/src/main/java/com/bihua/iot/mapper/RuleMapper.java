package com.bihua.iot.mapper;

import com.bihua.iot.domain.Rule;
import com.bihua.iot.domain.vo.RuleVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 规则信息Mapper接口
 *
 * @author bihua
 * @date 2023-07-02
 */
public interface RuleMapper extends BaseMapperPlus<RuleMapper, Rule, RuleVo> {

}
