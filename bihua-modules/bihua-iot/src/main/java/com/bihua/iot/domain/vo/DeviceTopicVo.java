package com.bihua.iot.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 设备Topic数据视图对象 device_topic
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@ExcelIgnoreUnannotated
public class DeviceTopicVo {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ExcelProperty(value = "id")
    private Long id;

    /**
     * 设备标识
     */
    @ExcelProperty(value = "设备标识")
    private String deviceIdentification;

    /**
     * 类型(0:基础Topic,1:自定义Topic)
     */
    @ExcelProperty(value = "类型(0:基础Topic,1:自定义Topic)")
    private String type;

    /**
     * topic
     */
    @ExcelProperty(value = "topic")
    private String topic;

    /**
     * 发布者
     */
    @ExcelProperty(value = "发布者")
    private String publisher;

    /**
     * 订阅者
     */
    @ExcelProperty(value = "订阅者")
    private String subscriber;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
