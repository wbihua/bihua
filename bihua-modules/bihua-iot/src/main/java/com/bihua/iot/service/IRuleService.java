package com.bihua.iot.service;

import com.bihua.iot.domain.Rule;
import com.bihua.iot.domain.vo.RuleVo;
import com.bihua.iot.domain.bo.RuleBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 规则信息Service接口
 *
 * @author bihua
 * @date 2023-07-02
 */
public interface IRuleService {

    /**
     * 查询规则信息
     */
    RuleVo queryById(Long id);

    /**
     * 查询规则信息列表
     */
    TableDataInfo<RuleVo> queryPageList(RuleBo bo, PageQuery pageQuery);

    /**
     * 查询规则信息列表
     */
    List<RuleVo> queryList(RuleBo bo);

    /**
     * 新增规则信息
     */
    Boolean insertByBo(RuleBo bo);

    /**
     * 修改规则信息
     */
    Boolean updateByBo(RuleBo bo);

    /**
     * 校验并批量删除规则信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
