package com.bihua.iot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bihua.iot.domain.TdFields;
import com.bihua.iot.domain.TdSelect;
import com.bihua.iot.domain.TdSelectVisual;
import com.bihua.iot.domain.TdTable;
import com.bihua.iot.domain.TdTagsSelect;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author bihua
 * @date 2023年06月17日 22:17
 */
public interface TdEngineMapper extends BaseMapper<TdTable> {

    void createDatabase(@Param("dataBaseName") String dataBaseName);

    void createSuperTable(@Param("schemaFields") List<TdFields> schemaFields,
                          @Param("tagsFields") List<TdFields> tagsFields,
                          @Param("dataBaseName") String dataBaseName,
                          @Param("superTableName") String superTableName);

    void createTable(TdTable tableDto);

    void insertData(TdTable tableDto);

    List<Map<String, Object>> selectByTimestamp(TdSelect select);

    void addColumnForSuperTable(@Param("superTableName") String superTableName,
                                @Param("fields") TdFields fieldsVo);

    void dropColumnForSuperTable(@Param("superTableName") String superTableName,
                                 @Param("fields") TdFields fieldsVo);

    void addTagForSuperTable(@Param("superTableName") String superTableName,
                             @Param("fields") TdFields fieldsVo);

    void dropTagForSuperTable(@Param("superTableName") String superTableName,
                              @Param("fields") TdFields fieldsVo);

    Map<String, Long> getCountByTimestamp(TdSelect select);

    /**
     * 检查表是否存在
     * @param dataBaseName
     * @param tableName 可以为超级表名或普通表名
     * @return
     */
    Integer checkTableExists(@Param("dataBaseName") String dataBaseName, @Param("tableName")String tableName);

    List<Map<String, Object>> getLastData(TdSelect select);

    List<Map<String, Object>> getHistoryData(TdSelectVisual selectVisual);

    List<Map<String, Object>> getRealtimeData(TdSelectVisual selectVisual);

    List<Map<String, Object>> getAggregateData(TdSelectVisual selectVisual);

    List<Map<String, Object>> getLastDataByTags(TdTagsSelect tagsSelect);
}
