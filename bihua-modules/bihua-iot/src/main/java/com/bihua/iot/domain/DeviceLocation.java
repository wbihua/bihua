package com.bihua.iot.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import java.math.BigDecimal;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 设备位置对象 device_location
 *
 * @author bihua
 * @date 2023-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("device_location")
public class DeviceLocation extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 主键
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 设备标识
     */
    private String deviceIdentification;
    /**
     * 纬度
     */
    private BigDecimal latitude;
    /**
     * 经度
     */
    private BigDecimal longitude;
    /**
     * 位置名称
     */
    private String fullName;
    /**
     * 省,直辖市编码
     */
    private String provinceCode;
    /**
     * 市编码
     */
    private String cityCode;
    /**
     * 区县
     */
    private String regionCode;
    /**
     * 备注
     */
    private String remark;

}
