package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.CmsBlogType;
import com.bihua.blog.domain.bo.CmsBlogTypeBo;
import com.bihua.blog.domain.vo.CmsBlogTypeVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * blog分类关联Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsBlogTypeService {

    /**
     * 查询blog分类关联
     */
    CmsBlogTypeVo queryById(Long typeId);

    /**
     * 查询blog分类关联列表
     */
    TableDataInfo<CmsBlogTypeVo> queryPageList(CmsBlogTypeBo bo, PageQuery pageQuery);

    /**
     * 查询blog分类关联列表
     */
    List<CmsBlogTypeVo> queryList(CmsBlogTypeBo bo);

    /**
     * 新增blog分类关联
     */
    Boolean insertByBo(CmsBlogTypeBo bo);

    /**
     * 修改blog分类关联
     */
    Boolean updateByBo(CmsBlogTypeBo bo);

    /**
     * 校验并批量删除blog分类关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean insertBatch(List<CmsBlogType> cmsBlogTypes);
}
