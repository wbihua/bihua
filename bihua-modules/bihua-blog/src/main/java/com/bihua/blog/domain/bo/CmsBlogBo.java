package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 博客信息业务对象 cms_blog
 *
 * @author bihua
 * @date 2023-05-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsBlogBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;

    /**
     * 类型
     */
    @NotBlank(message = "类型不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 内容
     */
    private String content;

    /**
     * 置顶（0否 1是）
     */
    private String top;

    /**
     * 阅读
     */
    private Long views;

    /**
     * 状态（0暂存 1发布）
     */
    private String status;

    /**
     * 首页图片ID
     */
    private String blogPicOssId;
    /**
     * 首页图片URL
     */
    private String blogPicUrl;
    /**
     * 首页图片名称
     */
    private String blogPicName;

    /**
     * 简介
     */
    private String blogDesc;

    /**
     * 附件列表
     */
    private String blogFiles;

    /**
     * 分类
     */
    private Long[] typeIds;

    /**
     * 分类
     */
    private Long[] tagIds;
}
