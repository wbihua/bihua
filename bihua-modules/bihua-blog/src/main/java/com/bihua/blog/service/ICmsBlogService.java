package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsBlogBo;
import com.bihua.blog.domain.vo.CmsBlogAllTagAndAllTypeVo;
import com.bihua.blog.domain.vo.CmsBlogVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 博客信息Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsBlogService {

    /**
     * 查询博客信息
     */
    CmsBlogVo queryById(Long id);

    /**
     * 查询博客信息列表
     */
    TableDataInfo<CmsBlogVo> queryPageList(CmsBlogBo bo, PageQuery pageQuery);

    /**
     * 查询博客信息列表
     */
    List<CmsBlogVo> queryList(CmsBlogBo bo);

    /**
     * 新增博客信息
     */
    Long insertByBo(CmsBlogBo bo);

    /**
     * 修改博客信息
     */
    Boolean updateByBo(CmsBlogBo bo);

    /**
     * 校验并批量删除博客信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    CmsBlogAllTagAndAllTypeVo getAllTagAndAllType();

    /**
     * 按分类查询文章列表
     */
    TableDataInfo<CmsBlogVo> selectCmsBlogListByTypeId(Long typeId, PageQuery pageQuery);
}
