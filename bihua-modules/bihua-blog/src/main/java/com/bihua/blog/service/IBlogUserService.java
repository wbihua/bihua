package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.BlogUser;
import com.bihua.blog.domain.bo.BlogUserBo;
import com.bihua.blog.domain.vo.BlogUserVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 用户信息Service接口
 *
 * @author bihua
 * @date 2023-05-29
 */
public interface IBlogUserService {

    /**
     * 查询用户信息
     */
    BlogUserVo queryById(Long userId);

    /**
     * 查询用户信息列表
     */
    TableDataInfo<BlogUserVo> queryPageList(BlogUserBo bo, PageQuery pageQuery);

    /**
     * 查询用户信息列表
     */
    List<BlogUserVo> queryList(BlogUserBo bo);

    /**
     * 新增用户信息
     */
    Boolean insertByBo(BlogUserBo bo);

    /**
     * 修改用户信息
     */
    Boolean updateByBo(BlogUserBo bo);

    /**
     * 校验并批量删除用户信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    int updateUserStatus(BlogUser user);

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    int resetPwd(BlogUser user);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    int resetUserPwd(String userName, String password);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    boolean updateUserAvatar(String userName, String avatar);
}
