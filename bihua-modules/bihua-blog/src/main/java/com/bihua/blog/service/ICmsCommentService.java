package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsCommentBo;
import com.bihua.blog.domain.vo.CmsCommentVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 评论Service接口
 *
 * @author bihua
 * @date 2023-06-05
 */
public interface ICmsCommentService {

    /**
     * 查询评论
     */
    CmsCommentVo queryById(Long id);

    /**
     * 查询评论列表
     */
    TableDataInfo<CmsCommentVo> queryPageList(CmsCommentBo bo, PageQuery pageQuery);

    /**
     * 查询评论列表
     */
    List<CmsCommentVo> queryList(CmsCommentBo bo);

    /**
     * 新增评论
     */
    Boolean insertByBo(CmsCommentBo bo);

    /**
     * 修改评论
     */
    Boolean updateByBo(CmsCommentBo bo);

    /**
     * 校验并批量删除评论信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
