package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 留言对象 cms_message
 *
 * @author bihua
 * @date 2023-06-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_message")
public class CmsMessage extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 父留言id
     */
    private Long parentId;
    /**
     * 主留言id(第一级留言)
     */
    private Long mainId;
    /**
     * 点赞数量
     */
    private Long likeNum;
    /**
     * 内容
     */
    private String content;
    /**
     * 留言类型：对人评论，对项目评论，对资源评论（0代表留言 1代表回复）
     */
    private String type;
    /**
     * 被留言者id，可以是人、项目、资源
     */
    private Long blogId;
    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 留言者id
     */
    private Long userId;

}
