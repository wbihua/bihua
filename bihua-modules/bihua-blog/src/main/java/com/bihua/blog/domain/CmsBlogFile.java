package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog文件对象 cms_blog_file
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode
@TableName("cms_blog_file")
public class CmsBlogFile{

    private static final long serialVersionUID=1L;

    /**
     * 文件ID
     */
    private Long fileId;
    /**
     * blogID
     */
    private Long blogId;

}
