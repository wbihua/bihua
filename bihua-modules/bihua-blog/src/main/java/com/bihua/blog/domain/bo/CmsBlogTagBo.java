package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog标签关联业务对象 cms_blog_tag
 *
 * @author bihua
 * @date 2023-05-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsBlogTagBo extends BaseEntity {

    /**
     * 标签ID
     */
    @NotNull(message = "标签ID不能为空", groups = { EditGroup.class })
    private Long tagId;

    /**
     * blogID
     */
    @NotNull(message = "blogID不能为空", groups = { EditGroup.class })
    private Long blogId;


}
