package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog分类关联业务对象 cms_blog_type
 *
 * @author bihua
 * @date 2023-05-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsBlogTypeBo extends BaseEntity {

    /**
     * 类型ID
     */
    @NotNull(message = "类型ID不能为空", groups = { EditGroup.class })
    private Long typeId;

    /**
     * blogID
     */
    @NotNull(message = "blogID不能为空", groups = { EditGroup.class })
    private Long blogId;


}
