package com.bihua.blog.domain.vo;

import java.util.List;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author bihua
 * @date 2023年05月31日 16:23
 */
@Data
@AllArgsConstructor
@ExcelIgnoreUnannotated
public class CmsBlogAllTagAndAllTypeVo {
    /** 角色对象 */
    private List<CmsTagVo> tags;

    /** 角色对象 */
    private List<CmsTypeVo> types;

}
