package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsType;
import com.bihua.blog.domain.vo.CmsTypeVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 分类信息Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsTypeMapper extends BaseMapperPlus<CmsTypeMapper, CmsType, CmsTypeVo> {

}
