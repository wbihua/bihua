package com.bihua.blog.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 评论视图对象 cms_comment
 *
 * @author bihua
 * @date 2023-06-05
 */
@Data
@ExcelIgnoreUnannotated
public class CmsCommentVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 父评论id
     */
    @ExcelProperty(value = "父评论id")
    private Long parentId;

    /**
     * 主评论id(第一级评论)
     */
    @ExcelProperty(value = "主评论id(第一级评论)")
    private Long mainId;

    /**
     * 点赞数量
     */
    @ExcelProperty(value = "点赞数量")
    private Long likeNum;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String content;

    /**
     * 评论类型：对人评论，对项目评论，对资源评论
     */
    @ExcelProperty(value = "评论类型：对人评论，对项目评论，对资源评论")
    private String type;

    /**
     * 被评论者id，可以是人、项目、资源
     */
    @ExcelProperty(value = "被评论者id，可以是人、项目、资源")
    private Long blogId;

    /**
     * 评论者id
     */
    @ExcelProperty(value = "评论者id")
    private Long userId;

    /**
     * 用户头像
     */
    @ExcelProperty(value = "用户头像")
    private String avatar;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /** 父留言者 */
    @ExcelProperty(value = "父留言者")
    private String pCreateBy;
}
