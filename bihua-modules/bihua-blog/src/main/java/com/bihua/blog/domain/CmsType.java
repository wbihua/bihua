package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分类信息对象 cms_type
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_type")
public class CmsType extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 分类ID
     */
    @TableId(value = "type_id")
    private Long typeId;
    /**
     * 分类名称
     */
    private String typeName;
    /**
     * 分类图像名称
     */
    private String typePicName;
    /**
     * 分类图像ID
     */
    private String typePicOssId;
    /**
     * 分类图像URL
     */
    private String typePicUrl;
}
