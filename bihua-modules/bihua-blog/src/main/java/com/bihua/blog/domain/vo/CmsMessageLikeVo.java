package com.bihua.blog.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 留言点赞视图对象 cms_message_like
 *
 * @author bihua
 * @date 2023-06-07
 */
@Data
@ExcelIgnoreUnannotated
public class CmsMessageLikeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 点赞留言ID
     */
    @ExcelProperty(value = "点赞留言ID")
    private Long messageId;

    /**
     * 用户ID
     */
    @ExcelProperty(value = "用户ID")
    private Long userId;


}
