package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog文件业务对象 cms_blog_file
 *
 * @author bihua
 * @date 2023-05-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsBlogFileBo extends BaseEntity{

    /**
     * 文件ID
     */
    @NotNull(message = "文件ID不能为空", groups = { EditGroup.class })
    private Long fileId;

    /**
     * blogID
     */
    @NotNull(message = "blogID不能为空", groups = { EditGroup.class })
    private Long blogId;

    private Long[] fileIds;
}
