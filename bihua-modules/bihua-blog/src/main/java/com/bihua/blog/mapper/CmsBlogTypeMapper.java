package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsBlogType;
import com.bihua.blog.domain.vo.CmsBlogTypeVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * blog分类关联Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsBlogTypeMapper extends BaseMapperPlus<CmsBlogTypeMapper, CmsBlogType, CmsBlogTypeVo> {

}
