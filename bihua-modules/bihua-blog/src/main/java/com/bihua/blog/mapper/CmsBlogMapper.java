package com.bihua.blog.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlog;
import com.bihua.blog.domain.vo.CmsBlogVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 博客信息Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsBlogMapper extends BaseMapperPlus<CmsBlogMapper, CmsBlog, CmsBlogVo> {
    /**
     * 按分类查询文章管理列表
     */
    Page<CmsBlog> selectCmsBlogListByTypeId(@Param("page") Page<CmsBlogVo> page, @Param("typeId") Long typeId);
}
