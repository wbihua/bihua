package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 评论对象 cms_comment
 *
 * @author bihua
 * @date 2023-06-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_comment")
public class CmsComment extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 父评论id
     */
    private Long parentId;
    /**
     * 主评论id(第一级评论)
     */
    private Long mainId;
    /**
     * 点赞数量
     */
    private Long likeNum;
    /**
     * 内容
     */
    private String content;
    /**
     * 评论类型：对人评论，对项目评论，对资源评论
     */
    private String type;
    /**
     * 被评论者id，可以是人、项目、资源
     */
    private Long blogId;
    /**
     * 删除标志（0代表存在 1代表删除）
     */
    @TableLogic
    private String delFlag;
    /**
     * 评论者id
     */
    private Long userId;

}
