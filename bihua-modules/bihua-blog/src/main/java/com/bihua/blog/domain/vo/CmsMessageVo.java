package com.bihua.blog.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;

import lombok.Data;



/**
 * 留言视图对象 cms_message
 *
 * @author bihua
 * @date 2023-06-07
 */
@Data
@ExcelIgnoreUnannotated
public class CmsMessageVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 父留言id
     */
    @ExcelProperty(value = "父留言id")
    private Long parentId;

    /**
     * 主留言id(第一级留言)
     */
    @ExcelProperty(value = "主留言id(第一级留言)")
    private Long mainId;

    /**
     * 点赞数量
     */
    @ExcelProperty(value = "点赞数量")
    private Long likeNum;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String content;

    /**
     * 留言类型：对人评论，对项目评论，对资源评论（0代表留言 1代表回复）
     */
    @ExcelProperty(value = "留言类型：对人评论，对项目评论，对资源评论", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=代表留言,1=代表回复")
    private String type;

    /**
     * 被留言者id，可以是人、项目、资源
     */
    @ExcelProperty(value = "被留言者id，可以是人、项目、资源")
    private Long blogId;

    /**
     * 留言者id
     */
    @ExcelProperty(value = "留言者id")
    private Long userId;

    /**
     * 用户头像
     */
    @ExcelProperty(value = "用户头像")
    private String avatar;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;

    /** 父留言者 */
    @ExcelProperty(value = "父留言者")
    private String pCreateBy;
}
