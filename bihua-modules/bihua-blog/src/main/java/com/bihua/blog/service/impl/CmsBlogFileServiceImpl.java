package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlogFile;
import com.bihua.blog.domain.bo.CmsBlogFileBo;
import com.bihua.blog.domain.vo.CmsBlogFileVo;
import com.bihua.blog.mapper.CmsBlogFileMapper;
import com.bihua.blog.service.ICmsBlogFileService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * blog文件Service业务层处理
 *
 * @author bihua
 * @date 2023-05-30
 */
@RequiredArgsConstructor
@Service
public class CmsBlogFileServiceImpl implements ICmsBlogFileService {

    private final CmsBlogFileMapper baseMapper;

    /**
     * 查询blog文件
     */
    @Override
    public CmsBlogFileVo queryById(Long fileId){
        return baseMapper.selectVoById(fileId);
    }

    /**
     * 查询blog文件列表
     */
    @Override
    public TableDataInfo<CmsBlogFileVo> queryPageList(CmsBlogFileBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsBlogFile> lqw = buildQueryWrapper(bo);
        Page<CmsBlogFileVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询blog文件列表
     */
    @Override
    public List<CmsBlogFileVo> queryList(CmsBlogFileBo bo) {
        LambdaQueryWrapper<CmsBlogFile> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsBlogFile> buildQueryWrapper(CmsBlogFileBo bo) {
        LambdaQueryWrapper<CmsBlogFile> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(bo.getBlogId()), CmsBlogFile::getBlogId, bo.getBlogId());
        return lqw;
    }

    /**
     * 新增blog文件
     */
    @Override
    public Boolean insertByBo(CmsBlogFileBo bo) {
        CmsBlogFile add = BeanUtil.toBean(bo, CmsBlogFile.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setFileId(add.getFileId());
        }
        return flag;
    }

    /**
     * 修改blog文件
     */
    @Override
    public Boolean updateByBo(CmsBlogFileBo bo) {
        CmsBlogFile update = BeanUtil.toBean(bo, CmsBlogFile.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsBlogFile entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除blog文件
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean insertBatch(List<CmsBlogFile> cmsBlogFiles) {
        return baseMapper.insertBatch(cmsBlogFiles);
    }
}
