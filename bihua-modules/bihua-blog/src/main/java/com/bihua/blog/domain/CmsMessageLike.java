package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 留言点赞对象 cms_message_like
 *
 * @author bihua
 * @date 2023-06-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_message_like")
public class CmsMessageLike extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 点赞留言ID
     */
    private Long messageId;
    /**
     * 用户ID
     */
    private Long userId;

}
