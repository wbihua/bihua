package com.bihua.blog.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 分类信息视图对象 cms_type
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@ExcelIgnoreUnannotated
public class CmsTypeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 分类ID
     */
    @ExcelProperty(value = "分类ID")
    private Long typeId;

    /**
     * 分类名称
     */
    @ExcelProperty(value = "分类名称")
    private String typeName;

    /**
     * 分类图像名称
     */
    @ExcelProperty(value = "分类图像名称")
    private String typePicName;
    /**
     * 分类图像ID
     */
    @ExcelProperty(value = "分类图像ID")
    private String typePicOssId;
    /**
     * 分类图像URL
     */
    @ExcelProperty(value = "分类图像URL")
    private String typePicUrl;

    /** 博客数量 */
    @ExcelProperty(value = "博客数量")
    private long blogNum;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;
}
