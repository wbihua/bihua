package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 分类信息业务对象 cms_type
 *
 * @author bihua
 * @date 2023-05-30
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsTypeBo extends BaseEntity {

    /**
     * 分类ID
     */
    @NotNull(message = "分类ID不能为空", groups = { EditGroup.class })
    private Long typeId;

    /**
     * 分类名称
     */
    @NotBlank(message = "分类名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String typeName;

    /**
     * 分类图像名称
     */
    private String typePicName;
    /**
     * 分类图像ID
     */
    private String typePicOssId;
    /**
     * 分类图像URL
     */
    private String typePicUrl;

}
