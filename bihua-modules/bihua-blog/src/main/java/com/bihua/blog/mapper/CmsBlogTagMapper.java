package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsBlogTag;
import com.bihua.blog.domain.vo.CmsBlogTagVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * blog标签关联Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsBlogTagMapper extends BaseMapperPlus<CmsBlogTagMapper, CmsBlogTag, CmsBlogTagVo> {

}
