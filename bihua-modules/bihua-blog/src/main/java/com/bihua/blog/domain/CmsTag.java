package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 标签信息对象 cms_tag
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_tag")
public class CmsTag extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 标签ID
     */
    @TableId(value = "tag_id")
    private Long tagId;
    /**
     * 标签名称
     */
    private String tagName;

}
