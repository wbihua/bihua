package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsMessage;
import com.bihua.blog.domain.vo.CmsMessageVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 留言Mapper接口
 *
 * @author bihua
 * @date 2023-06-07
 */
public interface CmsMessageMapper extends BaseMapperPlus<CmsMessageMapper, CmsMessage, CmsMessageVo> {

}
