package com.bihua.blog.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * blog文件视图对象 cms_blog_file
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@ExcelIgnoreUnannotated
public class CmsBlogFileVo {

    private static final long serialVersionUID = 1L;

    /**
     * 文件ID
     */
    @ExcelProperty(value = "文件ID")
    private Long fileId;

    /**
     * blogID
     */
    @ExcelProperty(value = "blogID")
    private Long blogId;


}
