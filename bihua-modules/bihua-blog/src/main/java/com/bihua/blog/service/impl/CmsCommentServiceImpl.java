package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsComment;
import com.bihua.blog.domain.bo.CmsCommentBo;
import com.bihua.blog.domain.vo.CmsCommentVo;
import com.bihua.blog.mapper.CmsCommentMapper;
import com.bihua.blog.service.ICmsCommentService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.entity.SysUser;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;
import com.bihua.system.service.ISysUserService;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 评论Service业务层处理
 *
 * @author bihua
 * @date 2023-06-05
 */
@RequiredArgsConstructor
@Service
public class CmsCommentServiceImpl implements ICmsCommentService {

    private final CmsCommentMapper baseMapper;
    private final ISysUserService iSysUserService;

    /**
     * 查询评论
     */
    @Override
    public CmsCommentVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询评论列表
     */
    @Override
    public TableDataInfo<CmsCommentVo> queryPageList(CmsCommentBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsComment> lqw = buildQueryWrapper(bo);
        Page<CmsCommentVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        if(StringUtils.isNotEmpty(bo.getCreateBy())){
            for (CmsCommentVo vo : result.getRecords()) {
                //查询子评论(回复)
                CmsCommentBo bo1 = new CmsCommentBo();
                bo1.setType("1");
                bo1.setParentId(vo.getId());
                List<CmsCommentVo> childCommentList = baseMapper.selectVoList(buildQueryWrapper(bo1));
                result.getRecords().addAll(childCommentList);
            }
            result.setTotal(result.getRecords().size());
        }
        for (CmsCommentVo vo : result.getRecords()) {
            //添加头像
            Long userId = vo.getUserId();
            if (userId!=null){
                SysUser user = iSysUserService.selectUserById(userId);
                vo.setAvatar(user.getAvatar());
            }
            //添加父评论信息
            Long parentId = vo.getParentId();
            if (parentId!=null){
                CmsComment parentComment = baseMapper.selectById(parentId);
                vo.setPCreateBy(parentComment.getCreateBy());
            }
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询评论列表
     */
    @Override
    public List<CmsCommentVo> queryList(CmsCommentBo bo) {
        LambdaQueryWrapper<CmsComment> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsComment> buildQueryWrapper(CmsCommentBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsComment> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getParentId() != null, CmsComment::getParentId, bo.getParentId());
        lqw.eq(bo.getMainId() != null, CmsComment::getMainId, bo.getMainId());
        lqw.eq(StringUtils.isNotBlank(bo.getContent()), CmsComment::getContent, bo.getContent());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), CmsComment::getType, bo.getType());
        lqw.eq(bo.getBlogId() != null, CmsComment::getBlogId, bo.getBlogId());
        lqw.eq(StringUtils.isNotBlank(bo.getCreateBy()), CmsComment::getCreateBy, bo.getCreateBy());
        lqw.eq(bo.getUserId() != null, CmsComment::getUserId, bo.getUserId());
        return lqw;
    }

    /**
     * 新增评论
     */
    @Override
    public Boolean insertByBo(CmsCommentBo bo) {
        CmsComment add = BeanUtil.toBean(bo, CmsComment.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改评论
     */
    @Override
    public Boolean updateByBo(CmsCommentBo bo) {
        CmsComment update = BeanUtil.toBean(bo, CmsComment.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsComment entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除评论
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
