package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsTagBo;
import com.bihua.blog.domain.vo.CmsTagVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 标签信息Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsTagService {

    /**
     * 查询标签信息
     */
    CmsTagVo queryById(Long tagId);

    /**
     * 查询标签信息列表
     */
    TableDataInfo<CmsTagVo> queryPageList(CmsTagBo bo, PageQuery pageQuery);

    /**
     * 查询标签信息列表
     */
    List<CmsTagVo> queryList(CmsTagBo bo);

    /**
     * 新增标签信息
     */
    Boolean insertByBo(CmsTagBo bo);

    /**
     * 修改标签信息
     */
    Boolean updateByBo(CmsTagBo bo);

    /**
     * 校验并批量删除标签信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 查询标签信息列表
     */
    List<CmsTagVo> queryListByIds(Collection<Long> ids);
}
