package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsBlogFile;
import com.bihua.blog.domain.vo.CmsBlogFileVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * blog文件Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsBlogFileMapper extends BaseMapperPlus<CmsBlogFileMapper, CmsBlogFile, CmsBlogFileVo> {

}
