package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsTypeBo;
import com.bihua.blog.domain.vo.CmsTypeVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 分类信息Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsTypeService {

    /**
     * 查询分类信息
     */
    CmsTypeVo queryById(Long typeId);

    /**
     * 查询分类信息列表
     */
    TableDataInfo<CmsTypeVo> queryPageList(CmsTypeBo bo, PageQuery pageQuery);

    /**
     * 查询分类信息列表
     */
    List<CmsTypeVo> queryList(CmsTypeBo bo);

    /**
     * 新增分类信息
     */
    Boolean insertByBo(CmsTypeBo bo);

    /**
     * 修改分类信息
     */
    Boolean updateByBo(CmsTypeBo bo);

    /**
     * 校验并批量删除分类信息信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    /**
     * 查询标签信息列表
     */
    List<CmsTypeVo> queryListByIds(Collection<Long> ids);
}
