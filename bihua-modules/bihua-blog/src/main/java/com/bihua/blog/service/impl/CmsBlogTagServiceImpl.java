package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlogTag;
import com.bihua.blog.domain.bo.CmsBlogTagBo;
import com.bihua.blog.domain.vo.CmsBlogTagVo;
import com.bihua.blog.mapper.CmsBlogTagMapper;
import com.bihua.blog.service.ICmsBlogTagService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;

/**
 * blog标签关联Service业务层处理
 *
 * @author bihua
 * @date 2023-05-30
 */
@RequiredArgsConstructor
@Service
public class CmsBlogTagServiceImpl implements ICmsBlogTagService {

    private final CmsBlogTagMapper baseMapper;

    /**
     * 查询blog标签关联
     */
    @Override
    public CmsBlogTagVo queryById(Long tagId){
        return baseMapper.selectVoById(tagId);
    }

    /**
     * 查询blog标签关联列表
     */
    @Override
    public TableDataInfo<CmsBlogTagVo> queryPageList(CmsBlogTagBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsBlogTag> lqw = buildQueryWrapper(bo);
        Page<CmsBlogTagVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询blog标签关联列表
     */
    @Override
    public List<CmsBlogTagVo> queryList(CmsBlogTagBo bo) {
        LambdaQueryWrapper<CmsBlogTag> lqw = buildQueryWrapper(bo);
        lqw.eq(ObjectUtil.isNotEmpty(bo.getBlogId()) && bo.getBlogId() > 0L, CmsBlogTag::getBlogId, bo.getBlogId());
        lqw.eq(ObjectUtil.isNotEmpty(bo.getTagId()) && bo.getTagId() > 0L, CmsBlogTag::getTagId, bo.getTagId());
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsBlogTag> buildQueryWrapper(CmsBlogTagBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsBlogTag> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增blog标签关联
     */
    @Override
    public Boolean insertByBo(CmsBlogTagBo bo) {
        CmsBlogTag add = BeanUtil.toBean(bo, CmsBlogTag.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTagId(add.getTagId());
        }
        return flag;
    }

    /**
     * 修改blog标签关联
     */
    @Override
    public Boolean updateByBo(CmsBlogTagBo bo) {
        CmsBlogTag update = BeanUtil.toBean(bo, CmsBlogTag.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsBlogTag entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除blog标签关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean insertBatch(List<CmsBlogTag> cmsBlogTags) {
        return baseMapper.insertBatch(cmsBlogTags);
    }
}
