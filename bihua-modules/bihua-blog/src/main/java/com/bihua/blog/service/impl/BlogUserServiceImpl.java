package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.BlogUser;
import com.bihua.blog.domain.bo.BlogUserBo;
import com.bihua.blog.domain.vo.BlogUserVo;
import com.bihua.blog.mapper.BlogUserMapper;
import com.bihua.blog.service.IBlogUserService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 用户信息Service业务层处理
 *
 * @author bihua
 * @date 2023-05-29
 */
@RequiredArgsConstructor
@Service
public class BlogUserServiceImpl implements IBlogUserService {

    private final BlogUserMapper baseMapper;

    /**
     * 查询用户信息
     */
    @Override
    public BlogUserVo queryById(Long userId){
        return baseMapper.selectVoById(userId);
    }

    /**
     * 查询用户信息列表
     */
    @Override
    public TableDataInfo<BlogUserVo> queryPageList(BlogUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<BlogUser> lqw = buildQueryWrapper(bo);
        Page<BlogUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询用户信息列表
     */
    @Override
    public List<BlogUserVo> queryList(BlogUserBo bo) {
        LambdaQueryWrapper<BlogUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<BlogUser> buildQueryWrapper(BlogUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<BlogUser> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getUserName()), BlogUser::getUserName, bo.getUserName());
        lqw.like(StringUtils.isNotBlank(bo.getPhonenumber()), BlogUser::getPhonenumber, bo.getPhonenumber());
        lqw.eq(StringUtils.isNotBlank(bo.getStatus()), BlogUser::getStatus, bo.getStatus());
        lqw.between(params.get("beginTime") != null && params.get("endTime") != null,
            BlogUser::getCreateTime, params.get("beginTime"), params.get("endTime"));
        return lqw;
    }

    /**
     * 新增用户信息
     */
    @Override
    public Boolean insertByBo(BlogUserBo bo) {
        BlogUser add = BeanUtil.toBean(bo, BlogUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setUserId(add.getUserId());
        }
        return flag;
    }

    /**
     * 修改用户信息
     */
    @Override
    public Boolean updateByBo(BlogUserBo bo) {
        BlogUser update = BeanUtil.toBean(bo, BlogUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(BlogUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除用户信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserStatus(BlogUser user) {
        return baseMapper.updateById(user);
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(BlogUser user) {
        return baseMapper.updateById(user);
    }

    @Override
    public int resetUserPwd(String userName, String password) {
        return baseMapper.update(null,
            new LambdaUpdateWrapper<BlogUser>()
                .set(BlogUser::getPassword, password)
                .eq(BlogUser::getUserName, userName));
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    @Override
    public boolean updateUserAvatar(String userName, String avatar) {
        return baseMapper.update(null,
            new LambdaUpdateWrapper<BlogUser>()
                .set(BlogUser::getAvatar, avatar)
                .eq(BlogUser::getUserName, userName)) > 0;
    }
}
