package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 博客信息对象 cms_blog
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_blog")
public class CmsBlog extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * ID
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * 类型
     */
    private String type;
    /**
     * 内容
     */
    private String content;
    /**
     * 置顶（0否 1是）
     */
    private String top;
    /**
     * 阅读
     */
    private Long views;
    /**
     * 状态（0暂存 1发布）
     */
    private String status;
    /**
     * 首页图片ID
     */
    private String blogPicOssId;
    /**
     * 首页图片URL
     */
    private String blogPicUrl;
    /**
     * 首页图片名称
     */
    private String blogPicName;
    /**
     * 简介
     */
    private String blogDesc;
    /**
     * 附件列表
     */
    private String blogFiles;

}
