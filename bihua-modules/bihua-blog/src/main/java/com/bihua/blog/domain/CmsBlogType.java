package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog分类关联对象 cms_blog_type
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode
@TableName("cms_blog_type")
public class CmsBlogType{

    private static final long serialVersionUID=1L;

    /**
     * 类型ID
     */
    private Long typeId;
    /**
     * blogID
     */
    private Long blogId;

}
