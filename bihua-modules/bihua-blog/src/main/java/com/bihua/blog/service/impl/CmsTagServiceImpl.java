package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlogTag;
import com.bihua.blog.domain.CmsTag;
import com.bihua.blog.domain.bo.CmsTagBo;
import com.bihua.blog.domain.vo.CmsTagVo;
import com.bihua.blog.mapper.CmsBlogTagMapper;
import com.bihua.blog.mapper.CmsTagMapper;
import com.bihua.blog.service.ICmsTagService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import lombok.RequiredArgsConstructor;

/**
 * 标签信息Service业务层处理
 *
 * @author bihua
 * @date 2023-05-30
 */
@RequiredArgsConstructor
@Service
public class CmsTagServiceImpl implements ICmsTagService {

    private final CmsTagMapper baseMapper;
    private final CmsBlogTagMapper cmsBlogTagMapper;

    /**
     * 查询标签信息
     */
    @Override
    public CmsTagVo queryById(Long tagId){
        return baseMapper.selectVoById(tagId);
    }

    /**
     * 查询标签信息列表
     */
    @Override
    public TableDataInfo<CmsTagVo> queryPageList(CmsTagBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsTag> lqw = buildQueryWrapper(bo);
        Page<CmsTagVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (CmsTagVo vo: result.getRecords()) {
            long count = cmsBlogTagMapper.selectCount(new LambdaQueryWrapper<CmsBlogTag>().eq(CmsBlogTag::getTagId, vo.getTagId()));
            vo.setBlogNum(count);
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询标签信息列表
     */
    @Override
    public List<CmsTagVo> queryList(CmsTagBo bo) {
        LambdaQueryWrapper<CmsTag> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsTag> buildQueryWrapper(CmsTagBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsTag> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getTagName()), CmsTag::getTagName, bo.getTagName());
        return lqw;
    }

    /**
     * 新增标签信息
     */
    @Override
    public Boolean insertByBo(CmsTagBo bo) {
        CmsTag add = BeanUtil.toBean(bo, CmsTag.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTagId(add.getTagId());
        }
        return flag;
    }

    /**
     * 修改标签信息
     */
    @Override
    public Boolean updateByBo(CmsTagBo bo) {
        CmsTag update = BeanUtil.toBean(bo, CmsTag.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsTag entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除标签信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<CmsTagVo> queryListByIds(Collection<Long> ids) {
        if(CollectionUtil.isNotEmpty(ids)){
            return baseMapper.selectVoBatchIds(ids);
        }else{
            return ListUtil.empty();
        }
    }
}
