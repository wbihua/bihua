package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsCommentLike;
import com.bihua.blog.domain.bo.CmsCommentLikeBo;
import com.bihua.blog.domain.vo.CmsCommentLikeVo;
import com.bihua.blog.mapper.CmsCommentLikeMapper;
import com.bihua.blog.service.ICmsCommentLikeService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 留言点赞Service业务层处理
 *
 * @author bihua
 * @date 2023-06-05
 */
@RequiredArgsConstructor
@Service
public class CmsCommentLikeServiceImpl implements ICmsCommentLikeService {

    private final CmsCommentLikeMapper baseMapper;

    /**
     * 查询留言点赞
     */
    @Override
    public CmsCommentLikeVo queryById(Long commentId){
        return baseMapper.selectVoById(commentId);
    }

    /**
     * 查询留言点赞列表
     */
    @Override
    public TableDataInfo<CmsCommentLikeVo> queryPageList(CmsCommentLikeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsCommentLike> lqw = buildQueryWrapper(bo);
        Page<CmsCommentLikeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询留言点赞列表
     */
    @Override
    public List<CmsCommentLikeVo> queryList(CmsCommentLikeBo bo) {
        LambdaQueryWrapper<CmsCommentLike> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsCommentLike> buildQueryWrapper(CmsCommentLikeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsCommentLike> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增留言点赞
     */
    @Override
    public Boolean insertByBo(CmsCommentLikeBo bo) {
        CmsCommentLike add = BeanUtil.toBean(bo, CmsCommentLike.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setCommentId(add.getCommentId());
        }
        return flag;
    }

    /**
     * 修改留言点赞
     */
    @Override
    public Boolean updateByBo(CmsCommentLikeBo bo) {
        CmsCommentLike update = BeanUtil.toBean(bo, CmsCommentLike.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsCommentLike entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除留言点赞
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
