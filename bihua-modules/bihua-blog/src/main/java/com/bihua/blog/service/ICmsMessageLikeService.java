package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsMessageLikeBo;
import com.bihua.blog.domain.vo.CmsMessageLikeVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 留言点赞Service接口
 *
 * @author bihua
 * @date 2023-06-07
 */
public interface ICmsMessageLikeService {

    /**
     * 查询留言点赞
     */
    CmsMessageLikeVo queryById(Long messageId);

    /**
     * 查询留言点赞列表
     */
    TableDataInfo<CmsMessageLikeVo> queryPageList(CmsMessageLikeBo bo, PageQuery pageQuery);

    /**
     * 查询留言点赞列表
     */
    List<CmsMessageLikeVo> queryList(CmsMessageLikeBo bo);

    /**
     * 新增留言点赞
     */
    Boolean insertByBo(CmsMessageLikeBo bo);

    /**
     * 修改留言点赞
     */
    Boolean updateByBo(CmsMessageLikeBo bo);

    /**
     * 校验并批量删除留言点赞信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
