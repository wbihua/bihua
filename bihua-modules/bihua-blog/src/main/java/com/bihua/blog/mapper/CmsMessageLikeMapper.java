package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsMessageLike;
import com.bihua.blog.domain.vo.CmsMessageLikeVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 留言点赞Mapper接口
 *
 * @author bihua
 * @date 2023-06-07
 */
public interface CmsMessageLikeMapper extends BaseMapperPlus<CmsMessageLikeMapper, CmsMessageLike, CmsMessageLikeVo> {

}
