package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsTag;
import com.bihua.blog.domain.vo.CmsTagVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 标签信息Mapper接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface CmsTagMapper extends BaseMapperPlus<CmsTagMapper, CmsTag, CmsTagVo> {

}
