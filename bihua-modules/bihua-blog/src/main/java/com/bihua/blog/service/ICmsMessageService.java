package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsMessageBo;
import com.bihua.blog.domain.vo.CmsMessageVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 留言Service接口
 *
 * @author bihua
 * @date 2023-06-07
 */
public interface ICmsMessageService {

    /**
     * 查询留言
     */
    CmsMessageVo queryById(Long id);

    /**
     * 查询留言列表
     */
    TableDataInfo<CmsMessageVo> queryPageList(CmsMessageBo bo, PageQuery pageQuery);

    /**
     * 查询留言列表
     */
    List<CmsMessageVo> queryList(CmsMessageBo bo);

    /**
     * 新增留言
     */
    Boolean insertByBo(CmsMessageBo bo);

    /**
     * 修改留言
     */
    Boolean updateByBo(CmsMessageBo bo);

    /**
     * 校验并批量删除留言信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
