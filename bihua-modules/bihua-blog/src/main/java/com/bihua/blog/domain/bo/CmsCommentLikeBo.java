package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 留言点赞业务对象 cms_comment_like
 *
 * @author bihua
 * @date 2023-06-05
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsCommentLikeBo extends BaseEntity {

    /**
     * 点赞评论ID
     */
    @NotNull(message = "点赞评论ID不能为空", groups = { EditGroup.class })
    private Long commentId;

    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空", groups = { EditGroup.class })
    private Long userId;


}
