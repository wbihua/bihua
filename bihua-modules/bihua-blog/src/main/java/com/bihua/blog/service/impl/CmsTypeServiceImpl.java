package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlogType;
import com.bihua.blog.domain.CmsType;
import com.bihua.blog.domain.bo.CmsTypeBo;
import com.bihua.blog.domain.vo.CmsTypeVo;
import com.bihua.blog.mapper.CmsBlogTypeMapper;
import com.bihua.blog.mapper.CmsTypeMapper;
import com.bihua.blog.service.ICmsTypeService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import lombok.RequiredArgsConstructor;

/**
 * 分类信息Service业务层处理
 *
 * @author bihua
 * @date 2023-05-30
 */
@RequiredArgsConstructor
@Service
public class CmsTypeServiceImpl implements ICmsTypeService {

    private final CmsTypeMapper baseMapper;
    private final CmsBlogTypeMapper cmsBlogTypeMapper;
    /**
     * 查询分类信息
     */
    @Override
    public CmsTypeVo queryById(Long typeId){
        return baseMapper.selectVoById(typeId);
    }

    /**
     * 查询分类信息列表
     */
    @Override
    public TableDataInfo<CmsTypeVo> queryPageList(CmsTypeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsType> lqw = buildQueryWrapper(bo);
        Page<CmsTypeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        for (CmsTypeVo vo: result.getRecords()) {
            long count = cmsBlogTypeMapper.selectCount(new LambdaQueryWrapper<CmsBlogType>().eq(CmsBlogType::getTypeId, vo.getTypeId()));
            vo.setBlogNum(count);
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询分类信息列表
     */
    @Override
    public List<CmsTypeVo> queryList(CmsTypeBo bo) {
        LambdaQueryWrapper<CmsType> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsType> buildQueryWrapper(CmsTypeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsType> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getTypeName()), CmsType::getTypeName, bo.getTypeName());
        return lqw;
    }

    /**
     * 新增分类信息
     */
    @Override
    public Boolean insertByBo(CmsTypeBo bo) {
        CmsType add = BeanUtil.toBean(bo, CmsType.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTypeId(add.getTypeId());
        }
        return flag;
    }

    /**
     * 修改分类信息
     */
    @Override
    public Boolean updateByBo(CmsTypeBo bo) {
        CmsType update = BeanUtil.toBean(bo, CmsType.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsType entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除分类信息
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<CmsTypeVo> queryListByIds(Collection<Long> ids) {
        if(CollectionUtil.isNotEmpty(ids)){
            return baseMapper.selectVoBatchIds(ids);
        }else{
            return ListUtil.empty();
        }
    }
}
