package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 留言业务对象 cms_message
 *
 * @author bihua
 * @date 2023-06-07
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsMessageBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 父留言id
     */
    @NotNull(message = "父留言id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parentId;

    /**
     * 主留言id(第一级留言)
     */
    @NotNull(message = "主留言id(第一级留言)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long mainId;

    /**
     * 点赞数量
     */
    private Long likeNum;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 留言类型：对人评论，对项目评论，对资源评论（0代表留言 1代表回复）
     */
    @NotBlank(message = "留言类型：对人评论，对项目评论，对资源评论（0代表留言 1代表回复）不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 被留言者id，可以是人、项目、资源
     */
    private Long blogId;

    /**
     * 留言者id
     */
    private Long userId;


}
