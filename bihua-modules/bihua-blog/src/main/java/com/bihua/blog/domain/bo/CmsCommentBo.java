package com.bihua.blog.domain.bo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 评论业务对象 cms_comment
 *
 * @author bihua
 * @date 2023-06-05
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class CmsCommentBo extends BaseEntity {

    /**
     * ID
     */
    @NotNull(message = "ID不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 父评论id
     */
    @NotNull(message = "父评论id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long parentId;

    /**
     * 主评论id(第一级评论)
     */
    @NotNull(message = "主评论id(第一级评论)不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long mainId;

    /**
     * 点赞数量
     */
    private Long likeNum;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空", groups = { AddGroup.class, EditGroup.class })
    private String content;

    /**
     * 评论类型：对人评论，对项目评论，对资源评论
     */
    @NotBlank(message = "评论类型：对人评论，对项目评论，对资源评论不能为空", groups = { AddGroup.class, EditGroup.class })
    private String type;

    /**
     * 被评论者id，可以是人、项目、资源
     */
    @NotNull(message = "被评论者id，可以是人、项目、资源不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long blogId;

    /**
     * 评论者id
     */
    private Long userId;


}
