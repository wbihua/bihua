package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsCommentLike;
import com.bihua.blog.domain.vo.CmsCommentLikeVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 留言点赞Mapper接口
 *
 * @author bihua
 * @date 2023-06-05
 */
public interface CmsCommentLikeMapper extends BaseMapperPlus<CmsCommentLikeMapper, CmsCommentLike, CmsCommentLikeVo> {

}
