package com.bihua.blog.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 留言点赞视图对象 cms_comment_like
 *
 * @author bihua
 * @date 2023-06-05
 */
@Data
@ExcelIgnoreUnannotated
public class CmsCommentLikeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 点赞评论ID
     */
    @ExcelProperty(value = "点赞评论ID")
    private Long commentId;

    /**
     * 用户ID
     */
    @ExcelProperty(value = "用户ID")
    private Long userId;


}
