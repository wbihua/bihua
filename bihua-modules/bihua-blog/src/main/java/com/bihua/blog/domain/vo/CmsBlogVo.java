package com.bihua.blog.domain.vo;

import java.util.Date;
import java.util.List;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;

import lombok.Data;


/**
 * 博客信息视图对象 cms_blog
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@ExcelIgnoreUnannotated
public class CmsBlogVo {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ExcelProperty(value = "ID")
    private Long id;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 类型
     */
    @ExcelProperty(value = "类型")
    private String type;

    /**
     * 内容
     */
    @ExcelProperty(value = "内容")
    private String content;

    /**
     * 置顶（0否 1是）
     */
    @ExcelProperty(value = "置顶", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=否,1=是")
    private String top;

    /**
     * 阅读
     */
    @ExcelProperty(value = "阅读")
    private Long views;

    /**
     * 状态（0暂存 1发布）
     */
    @ExcelProperty(value = "状态", converter = ExcelDictConvert.class)
    @ExcelDictFormat(readConverterExp = "0=暂存,1=发布")
    private String status;

    /**
     * 首页图片ID
     */
    @ExcelProperty(value = "首页图片ID")
    private String blogPicOssId;
    /**
     * 首页图片URL
     */
    @ExcelProperty(value = "首页图片URL")
    private String blogPicUrl;
    /**
     * 首页图片名称
     */
    @ExcelProperty(value = "首页图片名称")
    private String blogPicName;

    /**
     * 简介
     */
    @ExcelProperty(value = "简介")
    private String blogDesc;

    /**
     * 附件列表
     */
    @ExcelProperty(value = "附件列表")
    private String blogFiles;

    /**
     * 创建者
     */
    @ExcelProperty(value = "创建者")
    private String createBy;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 分类
     */
    private Long[] typeIds;
    /**
     * 分类
     */
    private Long[] tagIds;
    /** 角色对象 */
    private List<CmsTagVo> tags;

    /** 角色对象 */
    private List<CmsTypeVo> types;
}
