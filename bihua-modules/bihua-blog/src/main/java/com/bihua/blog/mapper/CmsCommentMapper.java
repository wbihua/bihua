package com.bihua.blog.mapper;

import com.bihua.blog.domain.CmsComment;
import com.bihua.blog.domain.vo.CmsCommentVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 评论Mapper接口
 *
 * @author bihua
 * @date 2023-06-05
 */
public interface CmsCommentMapper extends BaseMapperPlus<CmsCommentMapper, CmsComment, CmsCommentVo> {

}
