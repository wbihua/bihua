package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsMessage;
import com.bihua.blog.domain.bo.CmsMessageBo;
import com.bihua.blog.domain.vo.BlogUserVo;
import com.bihua.blog.domain.vo.CmsMessageVo;
import com.bihua.blog.mapper.CmsMessageMapper;
import com.bihua.blog.service.IBlogUserService;
import com.bihua.blog.service.ICmsMessageService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;

/**
 * 留言Service业务层处理
 *
 * @author bihua
 * @date 2023-06-07
 */
@RequiredArgsConstructor
@Service
public class CmsMessageServiceImpl implements ICmsMessageService {

    private final CmsMessageMapper baseMapper;
    private final IBlogUserService iBlogUserService;

    /**
     * 查询留言
     */
    @Override
    public CmsMessageVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询留言列表
     */
    @Override
    public TableDataInfo<CmsMessageVo> queryPageList(CmsMessageBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsMessage> lqw = buildQueryWrapper(bo);
        Page<CmsMessageVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        if(StringUtils.isNotEmpty(bo.getCreateBy())){
            for (CmsMessageVo vo : result.getRecords()) {
                //查询子评论(回复)
                CmsMessageBo bo1 = new CmsMessageBo();
                bo1.setType("1");
                bo1.setParentId(vo.getId());
                List<CmsMessageVo> childCommentList = baseMapper.selectVoList(buildQueryWrapper(bo1));
                result.getRecords().addAll(childCommentList);
            }
            result.setTotal(result.getRecords().size());
        }
        for (CmsMessageVo vo : result.getRecords()) {
            //添加头像
            Long userId = vo.getUserId();
            if (userId!=null){
                BlogUserVo user = iBlogUserService.queryById(userId);
                if(ObjectUtil.isNotEmpty(user)){
                    vo.setAvatar(user.getAvatar());
                }
            }
            //添加父评论信息
            Long parentId = vo.getParentId();
            if (parentId!=null){
                CmsMessage parentComment = baseMapper.selectById(parentId);
                vo.setPCreateBy(parentComment.getCreateBy());
            }
        }
        return TableDataInfo.build(result);
    }

    /**
     * 查询留言列表
     */
    @Override
    public List<CmsMessageVo> queryList(CmsMessageBo bo) {
        LambdaQueryWrapper<CmsMessage> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsMessage> buildQueryWrapper(CmsMessageBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsMessage> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getParentId() != null, CmsMessage::getParentId, bo.getParentId());
        lqw.eq(bo.getMainId() != null, CmsMessage::getMainId, bo.getMainId());
        lqw.eq(StringUtils.isNotBlank(bo.getContent()), CmsMessage::getContent, bo.getContent());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), CmsMessage::getType, bo.getType());
        lqw.eq(bo.getBlogId() != null, CmsMessage::getBlogId, bo.getBlogId());
        lqw.eq(StringUtils.isNotBlank(bo.getCreateBy()), CmsMessage::getCreateBy, bo.getCreateBy());
        lqw.eq(bo.getUserId() != null, CmsMessage::getUserId, bo.getUserId());
        return lqw;
    }

    /**
     * 新增留言
     */
    @Override
    public Boolean insertByBo(CmsMessageBo bo) {
        CmsMessage add = BeanUtil.toBean(bo, CmsMessage.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改留言
     */
    @Override
    public Boolean updateByBo(CmsMessageBo bo) {
        CmsMessage update = BeanUtil.toBean(bo, CmsMessage.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsMessage entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除留言
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
