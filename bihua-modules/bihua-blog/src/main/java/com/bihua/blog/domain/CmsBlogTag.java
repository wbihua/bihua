package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * blog标签关联对象 cms_blog_tag
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@EqualsAndHashCode
@TableName("cms_blog_tag")
public class CmsBlogTag  {

    private static final long serialVersionUID=1L;

    /**
     * 标签ID
     */
    private Long tagId;
    /**
     * blogID
     */
    private Long blogId;

}
