package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.CmsBlogTag;
import com.bihua.blog.domain.bo.CmsBlogTagBo;
import com.bihua.blog.domain.vo.CmsBlogTagVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * blog标签关联Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsBlogTagService {

    /**
     * 查询blog标签关联
     */
    CmsBlogTagVo queryById(Long tagId);

    /**
     * 查询blog标签关联列表
     */
    TableDataInfo<CmsBlogTagVo> queryPageList(CmsBlogTagBo bo, PageQuery pageQuery);

    /**
     * 查询blog标签关联列表
     */
    List<CmsBlogTagVo> queryList(CmsBlogTagBo bo);

    /**
     * 新增blog标签关联
     */
    Boolean insertByBo(CmsBlogTagBo bo);

    /**
     * 修改blog标签关联
     */
    Boolean updateByBo(CmsBlogTagBo bo);

    /**
     * 校验并批量删除blog标签关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean insertBatch(List<CmsBlogTag> cmsBlogTags);
}
