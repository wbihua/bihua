package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsMessageLike;
import com.bihua.blog.domain.bo.CmsMessageLikeBo;
import com.bihua.blog.domain.vo.CmsMessageLikeVo;
import com.bihua.blog.mapper.CmsMessageLikeMapper;
import com.bihua.blog.service.ICmsMessageLikeService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 留言点赞Service业务层处理
 *
 * @author bihua
 * @date 2023-06-07
 */
@RequiredArgsConstructor
@Service
public class CmsMessageLikeServiceImpl implements ICmsMessageLikeService {

    private final CmsMessageLikeMapper baseMapper;

    /**
     * 查询留言点赞
     */
    @Override
    public CmsMessageLikeVo queryById(Long messageId){
        return baseMapper.selectVoById(messageId);
    }

    /**
     * 查询留言点赞列表
     */
    @Override
    public TableDataInfo<CmsMessageLikeVo> queryPageList(CmsMessageLikeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsMessageLike> lqw = buildQueryWrapper(bo);
        Page<CmsMessageLikeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询留言点赞列表
     */
    @Override
    public List<CmsMessageLikeVo> queryList(CmsMessageLikeBo bo) {
        LambdaQueryWrapper<CmsMessageLike> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsMessageLike> buildQueryWrapper(CmsMessageLikeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsMessageLike> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增留言点赞
     */
    @Override
    public Boolean insertByBo(CmsMessageLikeBo bo) {
        CmsMessageLike add = BeanUtil.toBean(bo, CmsMessageLike.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setMessageId(add.getMessageId());
        }
        return flag;
    }

    /**
     * 修改留言点赞
     */
    @Override
    public Boolean updateByBo(CmsMessageLikeBo bo) {
        CmsMessageLike update = BeanUtil.toBean(bo, CmsMessageLike.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsMessageLike entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除留言点赞
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
