package com.bihua.blog.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.blog.domain.CmsBlogType;
import com.bihua.blog.domain.bo.CmsBlogTypeBo;
import com.bihua.blog.domain.vo.CmsBlogTypeVo;
import com.bihua.blog.mapper.CmsBlogTypeMapper;
import com.bihua.blog.service.ICmsBlogTypeService;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.RequiredArgsConstructor;

/**
 * blog分类关联Service业务层处理
 *
 * @author bihua
 * @date 2023-05-30
 */
@RequiredArgsConstructor
@Service
public class CmsBlogTypeServiceImpl implements ICmsBlogTypeService {

    private final CmsBlogTypeMapper baseMapper;

    /**
     * 查询blog分类关联
     */
    @Override
    public CmsBlogTypeVo queryById(Long typeId){
        return baseMapper.selectVoById(typeId);
    }

    /**
     * 查询blog分类关联列表
     */
    @Override
    public TableDataInfo<CmsBlogTypeVo> queryPageList(CmsBlogTypeBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<CmsBlogType> lqw = buildQueryWrapper(bo);
        Page<CmsBlogTypeVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询blog分类关联列表
     */
    @Override
    public List<CmsBlogTypeVo> queryList(CmsBlogTypeBo bo) {
        LambdaQueryWrapper<CmsBlogType> lqw = buildQueryWrapper(bo);
        lqw.eq(ObjectUtil.isNotEmpty(bo.getBlogId()) && bo.getBlogId() > 0L, CmsBlogType::getBlogId, bo.getBlogId());
        lqw.eq(ObjectUtil.isNotEmpty(bo.getTypeId()) && bo.getTypeId() > 0L, CmsBlogType::getTypeId, bo.getTypeId());
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<CmsBlogType> buildQueryWrapper(CmsBlogTypeBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<CmsBlogType> lqw = Wrappers.lambdaQuery();
        return lqw;
    }

    /**
     * 新增blog分类关联
     */
    @Override
    public Boolean insertByBo(CmsBlogTypeBo bo) {
        CmsBlogType add = BeanUtil.toBean(bo, CmsBlogType.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setTypeId(add.getTypeId());
        }
        return flag;
    }

    /**
     * 修改blog分类关联
     */
    @Override
    public Boolean updateByBo(CmsBlogTypeBo bo) {
        CmsBlogType update = BeanUtil.toBean(bo, CmsBlogType.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(CmsBlogType entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除blog分类关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public Boolean insertBatch(List<CmsBlogType> cmsBlogTypes) {
        return baseMapper.insertBatch(cmsBlogTypes);
    }
}
