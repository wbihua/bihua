package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.bo.CmsCommentLikeBo;
import com.bihua.blog.domain.vo.CmsCommentLikeVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 留言点赞Service接口
 *
 * @author bihua
 * @date 2023-06-05
 */
public interface ICmsCommentLikeService {

    /**
     * 查询留言点赞
     */
    CmsCommentLikeVo queryById(Long commentId);

    /**
     * 查询留言点赞列表
     */
    TableDataInfo<CmsCommentLikeVo> queryPageList(CmsCommentLikeBo bo, PageQuery pageQuery);

    /**
     * 查询留言点赞列表
     */
    List<CmsCommentLikeVo> queryList(CmsCommentLikeBo bo);

    /**
     * 新增留言点赞
     */
    Boolean insertByBo(CmsCommentLikeBo bo);

    /**
     * 修改留言点赞
     */
    Boolean updateByBo(CmsCommentLikeBo bo);

    /**
     * 校验并批量删除留言点赞信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
