package com.bihua.blog.mapper;

import com.bihua.blog.domain.BlogUser;
import com.bihua.blog.domain.vo.BlogUserVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 用户信息Mapper接口
 *
 * @author bihua
 * @date 2023-05-29
 */
public interface BlogUserMapper extends BaseMapperPlus<BlogUserMapper, BlogUser, BlogUserVo> {

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    BlogUser selectUserByUserName(String userName);

    /**
     * 通过手机号查询用户
     *
     * @param phonenumber 手机号
     * @return 用户对象信息
     */
    BlogUser selectUserByPhonenumber(String phonenumber);
}
