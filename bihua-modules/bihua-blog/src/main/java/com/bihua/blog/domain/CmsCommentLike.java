package com.bihua.blog.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.bihua.common.core.domain.BaseEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 留言点赞对象 cms_comment_like
 *
 * @author bihua
 * @date 2023-06-05
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("cms_comment_like")
public class CmsCommentLike extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 点赞评论ID
     */
    private Long commentId;
    /**
     * 用户ID
     */
    private Long userId;

}
