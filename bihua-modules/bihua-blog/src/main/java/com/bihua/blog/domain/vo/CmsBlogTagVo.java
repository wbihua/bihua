package com.bihua.blog.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * blog标签关联视图对象 cms_blog_tag
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@ExcelIgnoreUnannotated
public class CmsBlogTagVo {

    private static final long serialVersionUID = 1L;

    /**
     * 标签ID
     */
    @ExcelProperty(value = "标签ID")
    private Long tagId;

    /**
     * blogID
     */
    @ExcelProperty(value = "blogID")
    private Long blogId;


}
