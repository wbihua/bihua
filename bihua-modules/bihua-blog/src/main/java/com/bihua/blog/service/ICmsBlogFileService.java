package com.bihua.blog.service;

import java.util.Collection;
import java.util.List;

import com.bihua.blog.domain.CmsBlogFile;
import com.bihua.blog.domain.bo.CmsBlogFileBo;
import com.bihua.blog.domain.vo.CmsBlogFileVo;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

/**
 * blog文件Service接口
 *
 * @author bihua
 * @date 2023-05-30
 */
public interface ICmsBlogFileService {

    /**
     * 查询blog文件
     */
    CmsBlogFileVo queryById(Long fileId);

    /**
     * 查询blog文件列表
     */
    TableDataInfo<CmsBlogFileVo> queryPageList(CmsBlogFileBo bo, PageQuery pageQuery);

    /**
     * 查询blog文件列表
     */
    List<CmsBlogFileVo> queryList(CmsBlogFileBo bo);

    /**
     * 新增blog文件
     */
    Boolean insertByBo(CmsBlogFileBo bo);

    /**
     * 修改blog文件
     */
    Boolean updateByBo(CmsBlogFileBo bo);

    /**
     * 校验并批量删除blog文件信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    Boolean insertBatch(List<CmsBlogFile> cmsBlogFiles);
}
