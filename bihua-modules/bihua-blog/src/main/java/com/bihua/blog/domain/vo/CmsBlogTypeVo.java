package com.bihua.blog.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * blog分类关联视图对象 cms_blog_type
 *
 * @author bihua
 * @date 2023-05-30
 */
@Data
@ExcelIgnoreUnannotated
public class CmsBlogTypeVo {

    private static final long serialVersionUID = 1L;

    /**
     * 类型ID
     */
    @ExcelProperty(value = "类型ID")
    private Long typeId;

    /**
     * blogID
     */
    @ExcelProperty(value = "blogID")
    private Long blogId;


}
