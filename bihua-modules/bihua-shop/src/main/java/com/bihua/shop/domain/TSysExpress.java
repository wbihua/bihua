package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 物流公司对象 t_sys_express
 *
 * @author bihua
 * @date 2023-09-18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_sys_express")
public class TSysExpress extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 公司编码
     */
    private String code;
    /**
     * 公司名称
     */
    private String name;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 是否禁用
     */
    private Boolean disabled;

}
