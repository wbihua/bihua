package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品属性值对象 t_shop_attr_val
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_attr_val")
public class TShopAttrVal extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 属性值
     */
    private String attrVal;
    /**
     * 属性id
     */
    private Long idAttrKey;

}
