package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 类别banner关联视图对象 t_shop_category_banner_rel
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@ExcelIgnoreUnannotated
public class TShopCategoryBannerRelVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * banner id
     */
    @ExcelProperty(value = "banner id")
    private Long idBanner;

    /**
     * 类别id
     */
    @ExcelProperty(value = "类别id")
    private Long idCategory;


}
