package com.bihua.shop.service;

import com.bihua.shop.domain.TShopOrderLog;
import com.bihua.shop.domain.vo.TShopOrderLogVo;
import com.bihua.shop.domain.bo.TShopOrderLogBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 订单日志Service接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface ITShopOrderLogService {

    /**
     * 查询订单日志
     */
    TShopOrderLogVo queryById(Long id);

    /**
     * 查询订单日志列表
     */
    TableDataInfo<TShopOrderLogVo> queryPageList(TShopOrderLogBo bo, PageQuery pageQuery);

    /**
     * 查询订单日志列表
     */
    List<TShopOrderLogVo> queryList(TShopOrderLogBo bo);

    /**
     * 新增订单日志
     */
    Boolean insertByBo(TShopOrderLogBo bo);

    /**
     * 修改订单日志
     */
    Boolean updateByBo(TShopOrderLogBo bo);

    /**
     * 校验并批量删除订单日志信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
