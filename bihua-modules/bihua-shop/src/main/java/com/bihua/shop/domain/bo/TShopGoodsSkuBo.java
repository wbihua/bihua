package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品SKU业务对象 t_shop_goods_sku
 *
 * @author bihua
 * @date 2023-09-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopGoodsSkuBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * sku编码,格式:逗号分割的属性值id
     */
    @NotBlank(message = "sku编码,格式:逗号分割的属性值id不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * sku名称,格式:逗号分割的属性值
     */
    @NotBlank(message = "sku名称,格式:逗号分割的属性值不能为空", groups = { AddGroup.class, EditGroup.class })
    private String codeName;

    /**
     * 商品id
     */
    @NotNull(message = "商品id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idGoods;

    /**
     * 是否删除1:是,0:否
     */
    private Boolean isDeleted;

    /**
     * 市场价,原价
     */
    @NotBlank(message = "市场价,原价不能为空", groups = { AddGroup.class, EditGroup.class })
    private String marketingPrice;

    /**
     * 价格
     */
    @NotBlank(message = "价格不能为空", groups = { AddGroup.class, EditGroup.class })
    private String price;

    /**
     * 库存
     */
    @NotNull(message = "库存不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long stock;


}
