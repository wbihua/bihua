package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 商品属性值视图对象 t_shop_attr_val
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@ExcelIgnoreUnannotated
public class TShopAttrValVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 属性值
     */
    @ExcelProperty(value = "属性值")
    private String attrVal;

    /**
     * 属性id
     */
    @ExcelProperty(value = "属性id")
    private Long idAttrKey;


}
