package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 物流公司视图对象 t_sys_express
 *
 * @author bihua
 * @date 2023-09-18
 */
@Data
@ExcelIgnoreUnannotated
public class TSysExpressVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 公司编码
     */
    @ExcelProperty(value = "公司编码")
    private String code;

    /**
     * 公司名称
     */
    @ExcelProperty(value = "公司名称")
    private String name;

    /**
     * 排序
     */
    @ExcelProperty(value = "排序")
    private Integer sort;

    /**
     * 是否禁用
     */
    @ExcelProperty(value = "是否禁用")
    private Boolean disabled;


}
