package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 订单日志业务对象 t_shop_order_log
 *
 * @author bihua
 * @date 2023-09-17
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopOrderLogBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 所属订单id
     */
    @NotNull(message = "所属订单id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idOrder;

    /**
     * 日志详情
     */
    @NotBlank(message = "日志详情不能为空", groups = { AddGroup.class, EditGroup.class })
    private String remark;


}
