package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopOrderLogVo;
import com.bihua.shop.domain.bo.TShopOrderLogBo;
import com.bihua.shop.service.ITShopOrderLogService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 订单日志
 *
 * @author bihua
 * @date 2023-09-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopOrderLog")
public class TShopOrderLogController extends BaseController {

    private final ITShopOrderLogService iTShopOrderLogService;

    /**
     * 查询订单日志列表
     */
    @SaCheckPermission("shop:shopOrderLog:list")
    @GetMapping("/list")
    public TableDataInfo<TShopOrderLogVo> list(TShopOrderLogBo bo, PageQuery pageQuery) {
        return iTShopOrderLogService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出订单日志列表
     */
    @SaCheckPermission("shop:shopOrderLog:export")
    @Log(title = "订单日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopOrderLogBo bo, HttpServletResponse response) {
        List<TShopOrderLogVo> list = iTShopOrderLogService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单日志", TShopOrderLogVo.class, response);
    }

    /**
     * 获取订单日志详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopOrderLog:query")
    @GetMapping("/{id}")
    public R<TShopOrderLogVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopOrderLogService.queryById(id));
    }

    /**
     * 新增订单日志
     */
    @SaCheckPermission("shop:shopOrderLog:add")
    @Log(title = "订单日志", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopOrderLogBo bo) {
        return toAjax(iTShopOrderLogService.insertByBo(bo));
    }

    /**
     * 修改订单日志
     */
    @SaCheckPermission("shop:shopOrderLog:edit")
    @Log(title = "订单日志", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopOrderLogBo bo) {
        return toAjax(iTShopOrderLogService.updateByBo(bo));
    }

    /**
     * 删除订单日志
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopOrderLog:remove")
    @Log(title = "订单日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopOrderLogService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
