package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 类别banner关联业务对象 t_shop_category_banner_rel
 *
 * @author name: bihua
 * @date 2023-09-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopCategoryBannerRelBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * banner id
     */
    @NotNull(message = "banner id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idBanner;

    /**
     * 类别id
     */
    @NotNull(message = "类别id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idCategory;


}
