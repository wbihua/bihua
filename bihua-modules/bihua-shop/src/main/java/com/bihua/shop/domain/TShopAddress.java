package com.bihua.shop.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 收货地址对象 t_shop_address
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@EqualsAndHashCode
@TableName("t_shop_address")
public class TShopAddress {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 用户id
     */
    private Long idUser;
    /**
     * 收件人
     */
    private String name;
    /**
     * 联系电话
     */
    private String tel;
    /**
     * 省
     */
    private String province;
    /**
     * 市
     */
    private String city;
    /**
     * 区县
     */
    private String district;
    /**
     * 地区编码
     */
    private String areaCode;
    /**
     * 邮政编码
     */
    private String postCode;
    /**
     * 详细地址
     */
    private String addressDetail;
    /**
     * 是否默认
     */
    private Boolean isDefault;
    /**
     * 是否删除
     */
    private Boolean isDelete;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}
