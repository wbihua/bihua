package com.bihua.shop.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.shop.domain.TShopGoods;
import com.bihua.shop.domain.vo.TShopGoodsVo;
import com.bihua.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;

/**
 * 商品管理Mapper接口
 *
 * @author bihua
 * @date 2023-09-15
 */
public interface TShopGoodsMapper extends BaseMapperPlus<TShopGoodsMapper, TShopGoods, TShopGoodsVo> {

    Page<TShopGoodsVo> selectPageList(@Param("page") Page<TShopGoods> page, @Param("ew") Wrapper<TShopGoods> wrapper);

    int updateOnSaleById(@Param("id") Long id, @Param("isOnSale") Boolean isOnSale);
}
