package com.bihua.shop.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;
import com.bihua.shop.domain.TShopGoods;
import com.bihua.shop.domain.bo.TShopGoodsBo;
import com.bihua.shop.domain.vo.TShopGoodsVo;
import com.bihua.shop.mapper.TShopGoodsMapper;
import com.bihua.shop.service.ITShopGoodsService;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 商品管理Service业务层处理
 *
 * @author bihua
 * @date 2023-09-15
 */
@RequiredArgsConstructor
@Service
public class TShopGoodsServiceImpl implements ITShopGoodsService {

    private final TShopGoodsMapper baseMapper;

    /**
     * 查询商品管理
     */
    @Override
    public TShopGoodsVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品管理列表
     */
    @Override
    public TableDataInfo<TShopGoodsVo> queryPageList(TShopGoodsBo bo, PageQuery pageQuery) {
        QueryWrapper<TShopGoods> lqw = Wrappers.query();
        lqw.like(StringUtils.isNotBlank(bo.getName()), "tg.name", bo.getName());
        Page<TShopGoodsVo> result = baseMapper.selectPageList(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品管理列表
     */
    @Override
    public List<TShopGoodsVo> queryList(TShopGoodsBo bo) {
        LambdaQueryWrapper<TShopGoods> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopGoods> buildQueryWrapper(TShopGoodsBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopGoods> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getName()), TShopGoods::getName , bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getPrice()), TShopGoods::getPrice, bo.getPrice());
        lqw.eq(bo.getStock() != null, TShopGoods::getStock, bo.getStock());
        lqw.eq(bo.getIdCategory() != null, TShopGoods::getIdCategory, bo.getIdCategory());
        lqw.eq(StringUtils.isNotBlank(bo.getDetail()), TShopGoods::getDetail, bo.getDetail());
        lqw.eq(StringUtils.isNotBlank(bo.getGallery()), TShopGoods::getGallery, bo.getGallery());
        lqw.eq(bo.getIsDelete() != null, TShopGoods::getIsDelete, bo.getIsDelete());
        lqw.eq(bo.getIsHot() != null, TShopGoods::getIsHot, bo.getIsHot());
        lqw.eq(bo.getIsNew() != null, TShopGoods::getIsNew, bo.getIsNew());
        lqw.eq(bo.getIsOnSale() != null, TShopGoods::getIsOnSale, bo.getIsOnSale());
        lqw.eq(bo.getLikeNum() != null, TShopGoods::getLikeNum, bo.getLikeNum());
        return lqw;
    }

    /**
     * 新增商品管理
     */
    @Override
    public Boolean insertByBo(TShopGoodsBo bo) {
        TShopGoods add = BeanUtil.toBean(bo, TShopGoods.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品管理
     */
    @Override
    public Boolean updateByBo(TShopGoodsBo bo) {
        TShopGoods update = BeanUtil.toBean(bo, TShopGoods.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopGoods entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public boolean changeIsOnSale(Long id, Boolean isOnSale) {
        return baseMapper.updateOnSaleById(id, isOnSale) > 0;
    }
}
