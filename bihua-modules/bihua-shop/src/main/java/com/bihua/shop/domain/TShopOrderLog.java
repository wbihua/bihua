package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 订单日志对象 t_shop_order_log
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_order_log")
public class TShopOrderLog extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 所属订单id
     */
    private Long idOrder;
    /**
     * 日志详情
     */
    private String remark;

}
