package com.bihua.shop.service;

import com.bihua.shop.domain.TShopCart;
import com.bihua.shop.domain.vo.TShopCartVo;
import com.bihua.shop.domain.bo.TShopCartBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 购物车Service接口
 *
 * @author bihua
 * @date 2023-09-16
 */
public interface ITShopCartService {

    /**
     * 查询购物车
     */
    TShopCartVo queryById(Long id);

    /**
     * 查询购物车列表
     */
    TableDataInfo<TShopCartVo> queryPageList(TShopCartBo bo, PageQuery pageQuery);

    /**
     * 查询购物车列表
     */
    List<TShopCartVo> queryList(TShopCartBo bo);

    /**
     * 新增购物车
     */
    Boolean insertByBo(TShopCartBo bo);

    /**
     * 修改购物车
     */
    Boolean updateByBo(TShopCartBo bo);

    /**
     * 校验并批量删除购物车信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
