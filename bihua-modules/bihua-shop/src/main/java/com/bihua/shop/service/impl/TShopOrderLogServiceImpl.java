package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopOrderLogBo;
import com.bihua.shop.domain.vo.TShopOrderLogVo;
import com.bihua.shop.domain.TShopOrderLog;
import com.bihua.shop.mapper.TShopOrderLogMapper;
import com.bihua.shop.service.ITShopOrderLogService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 订单日志Service业务层处理
 *
 * @author bihua
 * @date 2023-09-17
 */
@RequiredArgsConstructor
@Service
public class TShopOrderLogServiceImpl implements ITShopOrderLogService {

    private final TShopOrderLogMapper baseMapper;

    /**
     * 查询订单日志
     */
    @Override
    public TShopOrderLogVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询订单日志列表
     */
    @Override
    public TableDataInfo<TShopOrderLogVo> queryPageList(TShopOrderLogBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopOrderLog> lqw = buildQueryWrapper(bo);
        Page<TShopOrderLogVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询订单日志列表
     */
    @Override
    public List<TShopOrderLogVo> queryList(TShopOrderLogBo bo) {
        LambdaQueryWrapper<TShopOrderLog> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopOrderLog> buildQueryWrapper(TShopOrderLogBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopOrderLog> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getIdOrder() != null, TShopOrderLog::getIdOrder, bo.getIdOrder());
        return lqw;
    }

    /**
     * 新增订单日志
     */
    @Override
    public Boolean insertByBo(TShopOrderLogBo bo) {
        TShopOrderLog add = BeanUtil.toBean(bo, TShopOrderLog.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改订单日志
     */
    @Override
    public Boolean updateByBo(TShopOrderLogBo bo) {
        TShopOrderLog update = BeanUtil.toBean(bo, TShopOrderLog.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopOrderLog entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除订单日志
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
