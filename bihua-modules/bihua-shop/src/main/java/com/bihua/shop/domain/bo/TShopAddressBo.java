package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 收货地址业务对象 t_shop_address
 *
 * @author bihua
 * @date 2023-09-17
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopAddressBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idUser;

    /**
     * 收件人
     */
    @NotBlank(message = "收件人不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 联系电话
     */
    @NotBlank(message = "联系电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String tel;

    /**
     * 省
     */
    @NotBlank(message = "省不能为空", groups = { AddGroup.class, EditGroup.class })
    private String province;

    /**
     * 市
     */
    @NotBlank(message = "市不能为空", groups = { AddGroup.class, EditGroup.class })
    private String city;

    /**
     * 区县
     */
    @NotBlank(message = "区县不能为空", groups = { AddGroup.class, EditGroup.class })
    private String district;

    /**
     * 地区编码
     */
    @NotBlank(message = "地区编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String areaCode;

    /**
     * 邮政编码
     */
    @NotBlank(message = "邮政编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String postCode;

    /**
     * 详细地址
     */
    @NotBlank(message = "详细地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String addressDetail;

    /**
     * 是否默认
     */
    @NotNull(message = "是否默认不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean isDefault;

    /**
     * 是否删除
     */
    @NotNull(message = "是否删除不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean isDelete;


}
