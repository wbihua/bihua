package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品类别对象 t_shop_category
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_category")
public class TShopCategory extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 父类别
     */
    private Long pid;
    /**
     * 名称
     */
    private String name;
    /**
     * 顺序
     */
    private Long sort;
    /**
     * 图标
     */
    private String iconOssId;
    /**
     * 图标
     */
    private String iconOssUrl;
    /**
     * 链接地址
     */
    private String url;
    /**
     * 是否删除
     */
    private Boolean isDelete;
    /**
     * 是否显示在首页
     */
    private Boolean showIndex;
    /**
     * 备注
     */
    private String remark;

}
