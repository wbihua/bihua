package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopCartVo;
import com.bihua.shop.domain.bo.TShopCartBo;
import com.bihua.shop.service.ITShopCartService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 购物车
 *
 * @author bihua
 * @date 2023-09-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopCart")
public class TShopCartController extends BaseController {

    private final ITShopCartService iTShopCartService;

    /**
     * 查询购物车列表
     */
    @SaCheckPermission("shop:shopCart:list")
    @GetMapping("/list")
    public TableDataInfo<TShopCartVo> list(TShopCartBo bo, PageQuery pageQuery) {
        return iTShopCartService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出购物车列表
     */
    @SaCheckPermission("shop:shopCart:export")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopCartBo bo, HttpServletResponse response) {
        List<TShopCartVo> list = iTShopCartService.queryList(bo);
        ExcelUtil.exportExcel(list, "购物车", TShopCartVo.class, response);
    }

    /**
     * 获取购物车详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopCart:query")
    @GetMapping("/{id}")
    public R<TShopCartVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopCartService.queryById(id));
    }

    /**
     * 新增购物车
     */
    @SaCheckPermission("shop:shopCart:add")
    @Log(title = "购物车", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopCartBo bo) {
        return toAjax(iTShopCartService.insertByBo(bo));
    }

    /**
     * 修改购物车
     */
    @SaCheckPermission("shop:shopCart:edit")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopCartBo bo) {
        return toAjax(iTShopCartService.updateByBo(bo));
    }

    /**
     * 删除购物车
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopCart:remove")
    @Log(title = "购物车", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopCartService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
