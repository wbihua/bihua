package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopGoodsSku;
import com.bihua.shop.domain.vo.TShopGoodsSkuVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 商品SKUMapper接口
 *
 * @author bihua
 * @date 2023-09-15
 */
public interface TShopGoodsSkuMapper extends BaseMapperPlus<TShopGoodsSkuMapper, TShopGoodsSku, TShopGoodsSkuVo> {

}
