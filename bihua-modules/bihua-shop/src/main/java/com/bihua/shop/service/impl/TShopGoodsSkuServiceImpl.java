package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopGoodsSkuBo;
import com.bihua.shop.domain.vo.TShopGoodsSkuVo;
import com.bihua.shop.domain.TShopGoodsSku;
import com.bihua.shop.mapper.TShopGoodsSkuMapper;
import com.bihua.shop.service.ITShopGoodsSkuService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品SKUService业务层处理
 *
 * @author bihua
 * @date 2023-09-15
 */
@RequiredArgsConstructor
@Service
public class TShopGoodsSkuServiceImpl implements ITShopGoodsSkuService {

    private final TShopGoodsSkuMapper baseMapper;

    /**
     * 查询商品SKU
     */
    @Override
    public TShopGoodsSkuVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品SKU列表
     */
    @Override
    public TableDataInfo<TShopGoodsSkuVo> queryPageList(TShopGoodsSkuBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopGoodsSku> lqw = buildQueryWrapper(bo);
        Page<TShopGoodsSkuVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品SKU列表
     */
    @Override
    public List<TShopGoodsSkuVo> queryList(TShopGoodsSkuBo bo) {
        LambdaQueryWrapper<TShopGoodsSku> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopGoodsSku> buildQueryWrapper(TShopGoodsSkuBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopGoodsSku> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), TShopGoodsSku::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getCodeName()), TShopGoodsSku::getCodeName, bo.getCodeName());
        lqw.eq(bo.getIdGoods() != null, TShopGoodsSku::getIdGoods, bo.getIdGoods());
        lqw.eq(bo.getIsDeleted() != null, TShopGoodsSku::getIsDeleted, bo.getIsDeleted());
        lqw.eq(StringUtils.isNotBlank(bo.getMarketingPrice()), TShopGoodsSku::getMarketingPrice, bo.getMarketingPrice());
        lqw.eq(StringUtils.isNotBlank(bo.getPrice()), TShopGoodsSku::getPrice, bo.getPrice());
        lqw.eq(bo.getStock() != null, TShopGoodsSku::getStock, bo.getStock());
        return lqw;
    }

    /**
     * 新增商品SKU
     */
    @Override
    public Boolean insertByBo(TShopGoodsSkuBo bo) {
        TShopGoodsSku add = BeanUtil.toBean(bo, TShopGoodsSku.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品SKU
     */
    @Override
    public Boolean updateByBo(TShopGoodsSkuBo bo) {
        TShopGoodsSku update = BeanUtil.toBean(bo, TShopGoodsSku.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopGoodsSku entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品SKU
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
