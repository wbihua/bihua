package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 订单管理业务对象 t_shop_order
 *
 * @author bihua
 * @date 2023-09-17
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopOrderBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idUser;

    /**
     * 订单号
     */
    @NotBlank(message = "订单号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String orderSn;

    /**
     * 状态
     */
    @NotNull(message = "状态不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer status;

    /**
     * 优惠券抵扣金额
     */
    @NotBlank(message = "优惠券抵扣金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal couponPrice;

    /**
     * 交易金额
     */
    @NotBlank(message = "交易金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal tradeAmount;

    /**
     * 实付金额
     */
    @NotBlank(message = "实付金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal realPrice;

    /**
     * 总金额
     */
    @NotBlank(message = "总金额不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal totalPrice;

    /**
     * 支付状态1:未付款;2:已付款,3:支付中
     */
    @NotNull(message = "支付状态1:未付款;2:已付款,3:支付中不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer payStatus;

    /**
     * 支付流水号
     */
    @NotBlank(message = "支付流水号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String payId;

    /**
     * 实付类型:alipay,wechat
     */
    @NotBlank(message = "实付类型:alipay,wechat不能为空", groups = { AddGroup.class, EditGroup.class })
    private String payType;

    /**
     * 支付成功时间
     */
    @NotBlank(message = "支付成功时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private String payTime;

    /**
     * 收件人
     */
    @NotBlank(message = "收件人不能为空", groups = { AddGroup.class, EditGroup.class })
    private String consignee;

    /**
     * 收件人电话
     */
    @NotBlank(message = "收件人电话不能为空", groups = { AddGroup.class, EditGroup.class })
    private String mobile;

    /**
     * 收件地址
     */
    @NotBlank(message = "收件地址不能为空", groups = { AddGroup.class, EditGroup.class })
    private String consigneeAddress;

    /**
     * 收货信息
     */
    @NotNull(message = "收货信息不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idAddress;

    /**
     * 快递公司
     */
    @NotNull(message = "快递公司不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idExpress;

    /**
     * 快递单号
     */
    @NotBlank(message = "快递单号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String shippingSn;

    /**
     * 配送费用
     */
    @NotBlank(message = "配送费用不能为空", groups = { AddGroup.class, EditGroup.class })
    private BigDecimal shippingAmount;

    /**
     * 出库时间
     */
    @NotNull(message = "出库时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date shippingTime;

    /**
     * 确认收货时间
     */
    @NotNull(message = "确认收货时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date confirmReceivingTime;

    /**
     * 订单备注
     */
    @NotBlank(message = "订单备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String message;

    /**
     * 管理员备注
     */
    @NotBlank(message = "管理员备注不能为空", groups = { AddGroup.class, EditGroup.class })
    private String adminMessage;


}
