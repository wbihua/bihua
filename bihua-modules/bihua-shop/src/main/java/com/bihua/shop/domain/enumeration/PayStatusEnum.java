package com.bihua.shop.domain.enumeration;

/**
 * @author wuzhong
 * @date 2023年09月17日 21:14
 */
public enum PayStatusEnum {
    UN_PAY(1, "未付款"),
    UN_SEND(2, "已付款"),
    PAYING(3, "支付中");


    private String value;
    private Integer status;

    PayStatusEnum(Integer status, String value) {
        this.status = status;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public Integer getStatus() {
        return status;
    }

    public static PayStatusEnum getPayStatus(Integer status){
        for(PayStatusEnum se : values()){
            if(se.getStatus().intValue() == status.intValue()){
                return se;
            }
        }
        return null;
    }
}
