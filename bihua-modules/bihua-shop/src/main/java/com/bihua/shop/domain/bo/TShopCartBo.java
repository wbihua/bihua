package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 购物车业务对象 t_shop_cart
 *
 * @author bihua
 * @date 2023-09-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopCartBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 数量
     */
    @NotNull(message = "数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long count;

    /**
     * 商品id
     */
    @NotNull(message = "商品id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idGoods;

    /**
     * skuId
     */
    @NotNull(message = "skuId不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idSku;

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idUser;


}
