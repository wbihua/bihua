package com.bihua.shop.service;

import com.bihua.shop.domain.TShopAddress;
import com.bihua.shop.domain.vo.TShopAddressVo;
import com.bihua.shop.domain.bo.TShopAddressBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 收货地址Service接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface ITShopAddressService {

    /**
     * 查询收货地址
     */
    TShopAddressVo queryById(Long id);

    /**
     * 查询收货地址列表
     */
    TableDataInfo<TShopAddressVo> queryPageList(TShopAddressBo bo, PageQuery pageQuery);

    /**
     * 查询收货地址列表
     */
    List<TShopAddressVo> queryList(TShopAddressBo bo);

    /**
     * 新增收货地址
     */
    Boolean insertByBo(TShopAddressBo bo);

    /**
     * 修改收货地址
     */
    Boolean updateByBo(TShopAddressBo bo);

    /**
     * 校验并批量删除收货地址信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
