package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.bihua.common.core.domain.BaseEntity;

/**
 * 会员管理业务对象 t_shop_user
 *
 * @author bihua
 * @date 2023-09-16
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopUserBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 头像
     */
    @NotBlank(message = "头像不能为空", groups = { AddGroup.class, EditGroup.class })
    private String avatar;

    /**
     * 昵称
     */
    @NotBlank(message = "昵称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String nickName;

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String password;

    /**
     * 性别:male;female
     */
    @NotBlank(message = "性别:male;female不能为空", groups = { AddGroup.class, EditGroup.class })
    private String gender;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空", groups = { AddGroup.class, EditGroup.class })
    private String mobile;

    /**
     * 微信头像
     */
    @NotBlank(message = "微信头像不能为空", groups = { AddGroup.class, EditGroup.class })
    private String wechatHeadImgUrl;

    /**
     * 微信昵称
     */
    @NotBlank(message = "微信昵称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String wechatNickName;

    /**
     * 微信OpenID
     */
    @NotBlank(message = "微信OpenID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String wechatOpenId;

    /**
     * 最后登陆时间
     */
    @NotNull(message = "最后登陆时间不能为空", groups = { AddGroup.class, EditGroup.class })
    private Date lastLoginTime;

    /**
     * 最后登录IP
     */
    private String loginIp;
}
