package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopOrderBo;
import com.bihua.shop.domain.bo.TShopOrderItemBo;
import com.bihua.shop.domain.vo.TShopOrderVo;
import com.bihua.shop.service.ITShopOrderItemService;
import com.bihua.shop.service.ITShopOrderService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 订单管理
 *
 * @author bihua
 * @date 2023-09-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopOrder")
public class TShopOrderController extends BaseController {

    private final ITShopOrderService iTShopOrderService;
    private final ITShopOrderItemService iTShopOrderItemService;

    /**
     * 查询订单管理列表
     */
    @SaCheckPermission("shop:shopOrder:list")
    @GetMapping("/list")
    public TableDataInfo<TShopOrderVo> list(TShopOrderBo bo, PageQuery pageQuery) {
        return iTShopOrderService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出订单管理列表
     */
    @SaCheckPermission("shop:shopOrder:export")
    @Log(title = "订单管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopOrderBo bo, HttpServletResponse response) {
        List<TShopOrderVo> list = iTShopOrderService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单管理", TShopOrderVo.class, response);
    }

    /**
     * 获取订单管理详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopOrder:query")
    @GetMapping("/{id}")
    public R<TShopOrderVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopOrderService.queryById(id));
    }
    @SaCheckPermission("shop:shopOrder:query")
    @GetMapping("/getInfoByOrderSn/{orderSn}")
    public R<TShopOrderVo> getInfoByOrderSn(@NotNull(message = "订单编号不能为空") @PathVariable("orderSn") String orderSn) {
        TShopOrderVo vo = iTShopOrderService.queryByOrderSn(orderSn);
        TShopOrderItemBo bo = new TShopOrderItemBo();
        bo.setIdOrder(vo.getId());
        vo.setItems(iTShopOrderItemService.queryList(bo));
        return R.ok(vo);
    }
    /**
     * 新增订单管理
     */
    @SaCheckPermission("shop:shopOrder:add")
    @Log(title = "订单管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopOrderBo bo) {
        return toAjax(iTShopOrderService.insertByBo(bo));
    }

    /**
     * 修改订单管理
     */
    @SaCheckPermission("shop:shopOrder:edit")
    @Log(title = "订单管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopOrderBo bo) {
        return toAjax(iTShopOrderService.updateByBo(bo));
    }

    /**
     * 删除订单管理
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopOrder:remove")
    @Log(title = "订单管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopOrderService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
    @SaCheckPermission("shop:shopOrder:sendOut")
    @RequestMapping(value = "/sendOut/{id}", method = RequestMethod.POST)
    @Log(title = "订单管理--发货", businessType = BusinessType.UPDATE)
    public R<Void> sendOut(@PathVariable("id") Long id,
                          @RequestParam("idExpress") Long idExpress,
                          @RequestParam("shippingSn") String shippingSn,
                          @RequestParam(value = "shippingAmount",defaultValue = "0",required = false) String shippingAmount) {

        return toAjax(iTShopOrderService.sendOut(id, idExpress, shippingSn, shippingAmount));
    }
}
