package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 订单明细业务对象 t_shop_order_item
 *
 * @author bihua
 * @date 2023-09-17
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopOrderItemBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 所属订单id
     */
    @NotNull(message = "所属订单id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idOrder;

    /**
     * 商品id
     */
    @NotNull(message = "商品id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idGoods;

    /**
     * skuId
     */
    @NotNull(message = "skuId不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idSku;

    /**
     * 数量
     */
    @NotBlank(message = "数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private String count;

    /**
     * 单价
     */
    @NotBlank(message = "单价不能为空", groups = { AddGroup.class, EditGroup.class })
    private String price;

    /**
     * 合计
     */
    @NotBlank(message = "合计不能为空", groups = { AddGroup.class, EditGroup.class })
    private String totalPrice;


}
