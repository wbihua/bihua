package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品类别业务对象 t_shop_category
 *
 * @author name: bihua
 * @date 2023-09-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopCategoryBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 父类别
     */
    private Long pid;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 顺序
     */
    @NotNull(message = "顺序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long sort;

    /**
     * 图标
     */
    @NotBlank(message = "图标ID不能为空", groups = { AddGroup.class, EditGroup.class })
    private String iconOssId;
    /**
     * 图标
     */
    @NotBlank(message = "图标URL不能为空", groups = { AddGroup.class, EditGroup.class })
    private String iconOssUrl;

    /**
     * 链接地址
     */
    private String url;

    /**
     * 是否删除
     */
    @NotNull(message = "是否删除不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean isDelete;

    /**
     * 是否显示在首页
     */
    @NotNull(message = "是否显示在首页不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean showIndex;

    /**
     * 备注
     */
    private String remark;


}
