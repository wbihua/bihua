package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 广告banner对象 t_shop_banner
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_banner")
public class TShopBanner extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * banner图id
     */
    private String ossId;
    /**
     * banner图URL
     */
    private String ossUrl;
    /**
     * 界面
     */
    private String page;
    /**
     * 参数
     */
    private String param;
    /**
     * 标题
     */
    private String title;
    /**
     * 类型
     */
    private String type;
    /**
     * 点击banner跳转到url
     */
    private String url;

}
