package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.bihua.shop.domain.enumeration.OrderStatusEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopOrderBo;
import com.bihua.shop.domain.vo.TShopOrderVo;
import com.bihua.shop.domain.TShopOrder;
import com.bihua.shop.mapper.TShopOrderMapper;
import com.bihua.shop.service.ITShopOrderService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 订单管理Service业务层处理
 *
 * @author bihua
 * @date 2023-09-17
 */
@RequiredArgsConstructor
@Service
public class TShopOrderServiceImpl implements ITShopOrderService {

    private final TShopOrderMapper baseMapper;

    /**
     * 查询订单管理
     */
    @Override
    public TShopOrderVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询订单管理列表
     */
    @Override
    public TableDataInfo<TShopOrderVo> queryPageList(TShopOrderBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopOrder> lqw = buildQueryWrapper(bo);
        Page<TShopOrderVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询订单管理列表
     */
    @Override
    public List<TShopOrderVo> queryList(TShopOrderBo bo) {
        LambdaQueryWrapper<TShopOrder> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopOrder> buildQueryWrapper(TShopOrderBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getIdUser() != null, TShopOrder::getIdUser, bo.getIdUser());
        lqw.eq(StringUtils.isNotBlank(bo.getOrderSn()), TShopOrder::getOrderSn, bo.getOrderSn());
        lqw.eq(bo.getStatus() != null, TShopOrder::getStatus, bo.getStatus());
        lqw.eq(bo.getPayStatus() != null, TShopOrder::getPayStatus, bo.getPayStatus());
        lqw.eq(StringUtils.isNotBlank(bo.getPayId()), TShopOrder::getPayId, bo.getPayId());
        lqw.eq(StringUtils.isNotBlank(bo.getPayType()), TShopOrder::getPayType, bo.getPayType());
        lqw.eq(StringUtils.isNotBlank(bo.getPayTime()), TShopOrder::getPayTime, bo.getPayTime());
        lqw.eq(StringUtils.isNotBlank(bo.getConsignee()), TShopOrder::getConsignee, bo.getConsignee());
        lqw.eq(StringUtils.isNotBlank(bo.getMobile()), TShopOrder::getMobile, bo.getMobile());
        lqw.eq(StringUtils.isNotBlank(bo.getConsigneeAddress()), TShopOrder::getConsigneeAddress, bo.getConsigneeAddress());
        lqw.eq(bo.getIdAddress() != null, TShopOrder::getIdAddress, bo.getIdAddress());
        lqw.eq(bo.getIdExpress() != null, TShopOrder::getIdExpress, bo.getIdExpress());
        lqw.eq(StringUtils.isNotBlank(bo.getShippingSn()), TShopOrder::getShippingSn, bo.getShippingSn());
        lqw.eq(bo.getShippingTime() != null, TShopOrder::getShippingTime, bo.getShippingTime());
        lqw.eq(bo.getConfirmReceivingTime() != null, TShopOrder::getConfirmReceivingTime, bo.getConfirmReceivingTime());
        lqw.eq(StringUtils.isNotBlank(bo.getMessage()), TShopOrder::getMessage, bo.getMessage());
        lqw.eq(StringUtils.isNotBlank(bo.getAdminMessage()), TShopOrder::getAdminMessage, bo.getAdminMessage());
        lqw.between(params.get("beginCreateTime") != null && params.get("endCreateTime") != null,
                TShopOrder::getCreateTime, params.get("beginCreateTime"), params.get("endCreateTime"));
        return lqw;
    }

    /**
     * 新增订单管理
     */
    @Override
    public Boolean insertByBo(TShopOrderBo bo) {
        TShopOrder add = BeanUtil.toBean(bo, TShopOrder.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改订单管理
     */
    @Override
    public Boolean updateByBo(TShopOrderBo bo) {
        TShopOrder update = BeanUtil.toBean(bo, TShopOrder.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除订单管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public TShopOrderVo queryByOrderSn(String orderSn) {
        return baseMapper.queryByOrderSn(orderSn);
    }

    @Override
    public Boolean sendOut(Long id, Long idExpress, String shippingSn, String shippingAmount) {
        return baseMapper.updateExpressById(id, idExpress, shippingSn, OrderStatusEnum.SENDED.getStatus(), new BigDecimal(shippingAmount)) > 0;
    }
}
