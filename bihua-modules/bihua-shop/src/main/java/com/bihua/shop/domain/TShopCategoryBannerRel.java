package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 类别banner关联对象 t_shop_category_banner_rel
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_category_banner_rel")
public class TShopCategoryBannerRel extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * banner id
     */
    private Long idBanner;
    /**
     * 类别id
     */
    private Long idCategory;

}
