package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TSysExpressVo;
import com.bihua.shop.domain.bo.TSysExpressBo;
import com.bihua.shop.service.ITSysExpressService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 物流公司
 *
 * @author bihua
 * @date 2023-09-18
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopExpress")
public class TSysExpressController extends BaseController {

    private final ITSysExpressService iTSysExpressService;

    /**
     * 查询物流公司列表
     */
    @SaCheckPermission("shop:shopExpress:list")
    @GetMapping("/list")
    public TableDataInfo<TSysExpressVo> list(TSysExpressBo bo, PageQuery pageQuery) {
        return iTSysExpressService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出物流公司列表
     */
    @SaCheckPermission("shop:shopExpress:export")
    @Log(title = "物流公司", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TSysExpressBo bo, HttpServletResponse response) {
        List<TSysExpressVo> list = iTSysExpressService.queryList(bo);
        ExcelUtil.exportExcel(list, "物流公司", TSysExpressVo.class, response);
    }

    /**
     * 获取物流公司详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopExpress:query")
    @GetMapping("/{id}")
    public R<TSysExpressVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTSysExpressService.queryById(id));
    }

    /**
     * 新增物流公司
     */
    @SaCheckPermission("shop:shopExpress:add")
    @Log(title = "物流公司", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TSysExpressBo bo) {
        return toAjax(iTSysExpressService.insertByBo(bo));
    }

    /**
     * 修改物流公司
     */
    @SaCheckPermission("shop:shopExpress:edit")
    @Log(title = "物流公司", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TSysExpressBo bo) {
        return toAjax(iTSysExpressService.updateByBo(bo));
    }

    /**
     * 删除物流公司
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopExpress:remove")
    @Log(title = "物流公司", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTSysExpressService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
