package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopAddressVo;
import com.bihua.shop.domain.bo.TShopAddressBo;
import com.bihua.shop.service.ITShopAddressService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 收货地址
 *
 * @author bihua
 * @date 2023-09-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopAddress")
public class TShopAddressController extends BaseController {

    private final ITShopAddressService iTShopAddressService;

    /**
     * 查询收货地址列表
     */
    @SaCheckPermission("shop:shopAddress:list")
    @GetMapping("/list")
    public TableDataInfo<TShopAddressVo> list(TShopAddressBo bo, PageQuery pageQuery) {
        return iTShopAddressService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出收货地址列表
     */
    @SaCheckPermission("shop:shopAddress:export")
    @Log(title = "收货地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopAddressBo bo, HttpServletResponse response) {
        List<TShopAddressVo> list = iTShopAddressService.queryList(bo);
        ExcelUtil.exportExcel(list, "收货地址", TShopAddressVo.class, response);
    }

    /**
     * 获取收货地址详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopAddress:query")
    @GetMapping("/{id}")
    public R<TShopAddressVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopAddressService.queryById(id));
    }

    /**
     * 新增收货地址
     */
    @SaCheckPermission("shop:shopAddress:add")
    @Log(title = "收货地址", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopAddressBo bo) {
        return toAjax(iTShopAddressService.insertByBo(bo));
    }

    /**
     * 修改收货地址
     */
    @SaCheckPermission("shop:shopAddress:edit")
    @Log(title = "收货地址", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopAddressBo bo) {
        return toAjax(iTShopAddressService.updateByBo(bo));
    }

    /**
     * 删除收货地址
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopAddress:remove")
    @Log(title = "收货地址", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopAddressService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
