package com.bihua.shop.domain.enumeration;

/**
 * @author wuzhong
 * @date 2023年09月17日 17:38
 */
public enum OrderStatusEnum {
    UN_PAY(1, "待付款"),
    UN_SEND(2, "待发货"),
    SENDED(3, "已发货"),
    FINISHED(4, "已完成"),
    CANCEL(5,"已取消"),
    REFUND_ING(6,"退款中"),
    REFUND(7,"已退款");


    private String value;
    private Integer status;

    OrderStatusEnum(Integer status, String value) {
        this.status = status;
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public Integer getStatus() {
        return status;
    }

    public static  OrderStatusEnum get(Integer status){
        for(OrderStatusEnum se : values()){
            if(se.getStatus().intValue() == status.intValue()){
                return se;
            }
        }
        return null;
    }
}
