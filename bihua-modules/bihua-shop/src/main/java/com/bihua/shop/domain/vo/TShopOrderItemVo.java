package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 订单明细视图对象 t_shop_order_item
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@ExcelIgnoreUnannotated
public class TShopOrderItemVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 所属订单id
     */
    @ExcelProperty(value = "所属订单id")
    private Long idOrder;

    /**
     * 商品id
     */
    @ExcelProperty(value = "商品id")
    private Long idGoods;

    /**
     * skuId
     */
    @ExcelProperty(value = "skuId")
    private Long idSku;

    /**
     * 数量
     */
    @ExcelProperty(value = "数量")
    private String count;

    /**
     * 单价
     */
    @ExcelProperty(value = "单价")
    private String price;

    /**
     * 合计
     */
    @ExcelProperty(value = "合计")
    private String totalPrice;

    /**
     * 商品名称
     */
    private String goodName;
    /**
     * 商品价格
     */
    private String goodPrice;
    /**
     * 小图
     */
    private String picOssUrl;
    /**
     * sku名称,格式:逗号分割的属性值
     */
    private String codeName;
    /**
     * SKU价格
     */
    private String skuPrice;

    private String title;

    public String getTitle() {
        return idSku != null
                ? (goodName + " " + codeName)
                : codeName;
    }
}
