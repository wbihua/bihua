package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopCategoryBannerRelBo;
import com.bihua.shop.domain.vo.TShopCategoryBannerRelVo;
import com.bihua.shop.service.ITShopCategoryBannerRelService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 类别banner关联
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopCategoryBannerRel")
public class TShopCategoryBannerRelController extends BaseController {

    private final ITShopCategoryBannerRelService iTShopCategoryBannerRelService;

    /**
     * 查询类别banner关联列表
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:list")
    @GetMapping("/list")
    public TableDataInfo<TShopCategoryBannerRelVo> list(TShopCategoryBannerRelBo bo, PageQuery pageQuery) {
        return iTShopCategoryBannerRelService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出类别banner关联列表
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:export")
    @Log(title = "类别banner关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopCategoryBannerRelBo bo, HttpServletResponse response) {
        List<TShopCategoryBannerRelVo> list = iTShopCategoryBannerRelService.queryList(bo);
        ExcelUtil.exportExcel(list, "类别banner关联", TShopCategoryBannerRelVo.class, response);
    }

    /**
     * 获取类别banner关联详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:query")
    @GetMapping("/{id}")
    public R<TShopCategoryBannerRelVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopCategoryBannerRelService.queryById(id));
    }

    /**
     * 新增类别banner关联
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:add")
    @Log(title = "类别banner关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopCategoryBannerRelBo bo) {
        return toAjax(iTShopCategoryBannerRelService.insertByBo(bo));
    }

    /**
     * 修改类别banner关联
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:edit")
    @Log(title = "类别banner关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopCategoryBannerRelBo bo) {
        return toAjax(iTShopCategoryBannerRelService.updateByBo(bo));
    }

    /**
     * 删除类别banner关联
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopCategoryBannerRel:remove")
    @Log(title = "类别banner关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopCategoryBannerRelService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
