package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 商品SKU视图对象 t_shop_goods_sku
 *
 * @author bihua
 * @date 2023-09-15
 */
@Data
@ExcelIgnoreUnannotated
public class TShopGoodsSkuVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * sku编码,格式:逗号分割的属性值id
     */
    @ExcelProperty(value = "sku编码,格式:逗号分割的属性值id")
    private String code;

    /**
     * sku名称,格式:逗号分割的属性值
     */
    @ExcelProperty(value = "sku名称,格式:逗号分割的属性值")
    private String codeName;

    /**
     * 商品id
     */
    @ExcelProperty(value = "商品id")
    private Long idGoods;

    /**
     * 是否删除1:是,0:否
     */
    @ExcelProperty(value = "是否删除1:是,0:否")
    private Boolean isDeleted;

    /**
     * 市场价,原价
     */
    @ExcelProperty(value = "市场价,原价")
    private String marketingPrice;

    /**
     * 价格
     */
    @ExcelProperty(value = "价格")
    private String price;

    /**
     * 库存
     */
    @ExcelProperty(value = "库存")
    private Long stock;


}
