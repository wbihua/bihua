package com.bihua.shop.service;

import com.bihua.shop.domain.TShopOrderItem;
import com.bihua.shop.domain.vo.TShopOrderItemVo;
import com.bihua.shop.domain.bo.TShopOrderItemBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 订单明细Service接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface ITShopOrderItemService {

    /**
     * 查询订单明细
     */
    TShopOrderItemVo queryById(Long id);

    /**
     * 查询订单明细列表
     */
    TableDataInfo<TShopOrderItemVo> queryPageList(TShopOrderItemBo bo, PageQuery pageQuery);

    /**
     * 查询订单明细列表
     */
    List<TShopOrderItemVo> queryList(TShopOrderItemBo bo);

    /**
     * 新增订单明细
     */
    Boolean insertByBo(TShopOrderItemBo bo);

    /**
     * 修改订单明细
     */
    Boolean updateByBo(TShopOrderItemBo bo);

    /**
     * 校验并批量删除订单明细信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
