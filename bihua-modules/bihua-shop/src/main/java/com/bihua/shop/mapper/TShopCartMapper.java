package com.bihua.shop.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.shop.domain.TShopCart;
import com.bihua.shop.domain.vo.TShopCartVo;

/**
 * 购物车Mapper接口
 *
 * @author bihua
 * @date 2023-09-16
 */
public interface TShopCartMapper extends BaseMapperPlus<TShopCartMapper, TShopCart, TShopCartVo> {

    Page<TShopCartVo> selectPageList(@Param("page") Page<TShopCart> page, @Param("ew") Wrapper<TShopCart> wrapper);
}
