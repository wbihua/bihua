package com.bihua.shop.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.shop.domain.bo.TShopUserBo;
import com.bihua.shop.domain.vo.TShopUserVo;

/**
 * 会员管理Service接口
 *
 * @author bihua
 * @date 2023-09-16
 */
public interface ITShopUserService {

    /**
     * 查询会员管理
     */
    TShopUserVo queryById(Long id);

    /**
     * 查询会员管理列表
     */
    TableDataInfo<TShopUserVo> queryPageList(TShopUserBo bo, PageQuery pageQuery);

    /**
     * 查询会员管理列表
     */
    List<TShopUserVo> queryList(TShopUserBo bo);

    /**
     * 新增会员管理
     */
    Boolean insertByBo(TShopUserBo bo);

    /**
     * 修改会员管理
     */
    Boolean updateByBo(TShopUserBo bo);

    /**
     * 校验并批量删除会员管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
    /**
     * 重置用户密码
     *
     * @param id 用户名
     * @param password 密码
     * @return 结果
     */
    int resetUserPwd(Long id, String password);

    /**
     * 修改用户头像
     *
     * @param id 用户名
     * @param avatar   头像地址
     * @return 结果
     */
    boolean updateUserAvatar(Long id, String avatar);

    TShopUserVo findByWechatOpenId(String openId);

}
