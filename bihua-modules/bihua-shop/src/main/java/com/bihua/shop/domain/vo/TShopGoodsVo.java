package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;
import java.util.List;


/**
 * 商品管理视图对象 t_shop_goods
 *
 * @author bihua
 * @date 2023-09-15
 */
@Data
@ExcelIgnoreUnannotated
public class TShopGoodsVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 价格
     */
    @ExcelProperty(value = "价格")
    private String price;

    /**
     * 库存数量
     */
    @ExcelProperty(value = "库存数量")
    private Long stock;

    /**
     * 类别id
     */
    @ExcelProperty(value = "类别id")
    private Long idCategory;
    @ExcelProperty(value = "类别id")
    private String nameCategory;

    /**
     * 小图
     */
    @ExcelProperty(value = "小图")
    private String picOssId;
    @ExcelProperty(value = "小图")
    private String picOssUrl;
    /**
     * 产品详情
     */
    @ExcelProperty(value = "产品详情")
    private String detail;

    /**
     * 大图相册列表,以逗号分隔
     */
    @ExcelProperty(value = "大图相册列表,以逗号分隔")
    private String gallery;

    /**
     * 是否删除
     */
    @ExcelProperty(value = "是否删除")
    private Boolean isDelete;

    /**
     * 是否人气商品
     */
    @ExcelProperty(value = "是否人气商品")
    private Boolean isHot;

    /**
     * 是否新品推荐
     */
    @ExcelProperty(value = "是否新品推荐")
    private Boolean isNew;

    /**
     * 是否上架
     */
    @ExcelProperty(value = "是否上架")
    private Boolean isOnSale;

    /**
     * 收藏数
     */
    @ExcelProperty(value = "收藏数")
    private Long likeNum;

    /**
     * 产品简介
     */
    @ExcelProperty(value = "产品简介")
    private String remark;

    private List<TShopGoodsSkuVo> skuVos;
}
