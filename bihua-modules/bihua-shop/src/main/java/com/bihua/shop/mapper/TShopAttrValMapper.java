package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopAttrVal;
import com.bihua.shop.domain.vo.TShopAttrValVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 商品属性值Mapper接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface TShopAttrValMapper extends BaseMapperPlus<TShopAttrValMapper, TShopAttrVal, TShopAttrValVo> {

}
