package com.bihua.shop.service;

import com.bihua.shop.domain.TShopGoodsSku;
import com.bihua.shop.domain.vo.TShopGoodsSkuVo;
import com.bihua.shop.domain.bo.TShopGoodsSkuBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品SKUService接口
 *
 * @author bihua
 * @date 2023-09-15
 */
public interface ITShopGoodsSkuService {

    /**
     * 查询商品SKU
     */
    TShopGoodsSkuVo queryById(Long id);

    /**
     * 查询商品SKU列表
     */
    TableDataInfo<TShopGoodsSkuVo> queryPageList(TShopGoodsSkuBo bo, PageQuery pageQuery);

    /**
     * 查询商品SKU列表
     */
    List<TShopGoodsSkuVo> queryList(TShopGoodsSkuBo bo);

    /**
     * 新增商品SKU
     */
    Boolean insertByBo(TShopGoodsSkuBo bo);

    /**
     * 修改商品SKU
     */
    Boolean updateByBo(TShopGoodsSkuBo bo);

    /**
     * 校验并批量删除商品SKU信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
