package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopUserVo;
import com.bihua.shop.domain.bo.TShopUserBo;
import com.bihua.shop.service.ITShopUserService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 会员管理
 *
 * @author bihua
 * @date 2023-09-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopUser")
public class TShopUserController extends BaseController {

    private final ITShopUserService iTShopUserService;

    /**
     * 查询会员管理列表
     */
    @SaCheckPermission("shop:shopUser:list")
    @GetMapping("/list")
    public TableDataInfo<TShopUserVo> list(TShopUserBo bo, PageQuery pageQuery) {
        return iTShopUserService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出会员管理列表
     */
    @SaCheckPermission("shop:shopUser:export")
    @Log(title = "会员管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopUserBo bo, HttpServletResponse response) {
        List<TShopUserVo> list = iTShopUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "会员管理", TShopUserVo.class, response);
    }

    /**
     * 获取会员管理详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopUser:query")
    @GetMapping("/{id}")
    public R<TShopUserVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopUserService.queryById(id));
    }

    /**
     * 新增会员管理
     */
    @SaCheckPermission("shop:shopUser:add")
    @Log(title = "会员管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopUserBo bo) {
        return toAjax(iTShopUserService.insertByBo(bo));
    }

    /**
     * 修改会员管理
     */
    @SaCheckPermission("shop:shopUser:edit")
    @Log(title = "会员管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopUserBo bo) {
        return toAjax(iTShopUserService.updateByBo(bo));
    }

    /**
     * 删除会员管理
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopUser:remove")
    @Log(title = "会员管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopUserService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
