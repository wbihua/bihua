package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 收货地址视图对象 t_shop_address
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@ExcelIgnoreUnannotated
public class TShopAddressVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long idUser;

    /**
     * 收件人
     */
    @ExcelProperty(value = "收件人")
    private String name;

    /**
     * 联系电话
     */
    @ExcelProperty(value = "联系电话")
    private String tel;

    /**
     * 省
     */
    @ExcelProperty(value = "省")
    private String province;

    /**
     * 市
     */
    @ExcelProperty(value = "市")
    private String city;

    /**
     * 区县
     */
    @ExcelProperty(value = "区县")
    private String district;

    /**
     * 地区编码
     */
    @ExcelProperty(value = "地区编码")
    private String areaCode;

    /**
     * 邮政编码
     */
    @ExcelProperty(value = "邮政编码")
    private String postCode;

    /**
     * 详细地址
     */
    @ExcelProperty(value = "详细地址")
    private String addressDetail;

    /**
     * 是否默认
     */
    @ExcelProperty(value = "是否默认")
    private Boolean isDefault;

    /**
     * 是否删除
     */
    @ExcelProperty(value = "是否删除")
    private Boolean isDelete;


}
