package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 购物车视图对象 t_shop_cart
 *
 * @author bihua
 * @date 2023-09-16
 */
@Data
@ExcelIgnoreUnannotated
public class TShopCartVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 数量
     */
    @ExcelProperty(value = "数量")
    private Long count;

    /**
     * 商品id
     */
    @ExcelProperty(value = "商品id")
    private Long idGoods;

    /**
     * skuId
     */
    @ExcelProperty(value = "skuId")
    private Long idSku;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long idUser;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 商品名称
     */
    private String goodName;
    /**
     * 商品价格
     */
    private String goodPrice;
    /**
     * sku名称,格式:逗号分割的属性值
     */
    private String codeName;
    /**
     * SKU价格
     */
    private String skuPrice;

    private String price;


    public String getPrice() {
        if(skuPrice!=null){
            return skuPrice;
        }
        return goodPrice;
    }

}
