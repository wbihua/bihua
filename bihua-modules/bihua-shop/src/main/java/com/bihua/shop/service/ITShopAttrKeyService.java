package com.bihua.shop.service;

import com.bihua.shop.domain.TShopAttrKey;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.shop.domain.bo.TShopAttrKeyBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性名Service接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface ITShopAttrKeyService {

    /**
     * 查询商品属性名
     */
    TShopAttrKeyVo queryById(Long id);

    /**
     * 查询商品属性名列表
     */
    TableDataInfo<TShopAttrKeyVo> queryPageList(TShopAttrKeyBo bo, PageQuery pageQuery);

    /**
     * 查询商品属性名列表
     */
    List<TShopAttrKeyVo> queryList(TShopAttrKeyBo bo);

    /**
     * 新增商品属性名
     */
    Boolean insertByBo(TShopAttrKeyBo bo);

    /**
     * 修改商品属性名
     */
    Boolean updateByBo(TShopAttrKeyBo bo);

    /**
     * 校验并批量删除商品属性名信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
