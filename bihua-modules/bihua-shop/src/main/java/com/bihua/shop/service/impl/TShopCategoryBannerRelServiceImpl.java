package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopCategoryBannerRelBo;
import com.bihua.shop.domain.vo.TShopCategoryBannerRelVo;
import com.bihua.shop.domain.TShopCategoryBannerRel;
import com.bihua.shop.mapper.TShopCategoryBannerRelMapper;
import com.bihua.shop.service.ITShopCategoryBannerRelService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 类别banner关联Service业务层处理
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@RequiredArgsConstructor
@Service
public class TShopCategoryBannerRelServiceImpl implements ITShopCategoryBannerRelService {

    private final TShopCategoryBannerRelMapper baseMapper;

    /**
     * 查询类别banner关联
     */
    @Override
    public TShopCategoryBannerRelVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询类别banner关联列表
     */
    @Override
    public TableDataInfo<TShopCategoryBannerRelVo> queryPageList(TShopCategoryBannerRelBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopCategoryBannerRel> lqw = buildQueryWrapper(bo);
        Page<TShopCategoryBannerRelVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询类别banner关联列表
     */
    @Override
    public List<TShopCategoryBannerRelVo> queryList(TShopCategoryBannerRelBo bo) {
        LambdaQueryWrapper<TShopCategoryBannerRel> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopCategoryBannerRel> buildQueryWrapper(TShopCategoryBannerRelBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopCategoryBannerRel> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getIdBanner() != null, TShopCategoryBannerRel::getIdBanner, bo.getIdBanner());
        lqw.eq(bo.getIdCategory() != null, TShopCategoryBannerRel::getIdCategory, bo.getIdCategory());
        return lqw;
    }

    /**
     * 新增类别banner关联
     */
    @Override
    public Boolean insertByBo(TShopCategoryBannerRelBo bo) {
        TShopCategoryBannerRel add = BeanUtil.toBean(bo, TShopCategoryBannerRel.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改类别banner关联
     */
    @Override
    public Boolean updateByBo(TShopCategoryBannerRelBo bo) {
        TShopCategoryBannerRel update = BeanUtil.toBean(bo, TShopCategoryBannerRel.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopCategoryBannerRel entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除类别banner关联
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
