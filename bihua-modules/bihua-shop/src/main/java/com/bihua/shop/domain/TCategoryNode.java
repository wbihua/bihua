package com.bihua.shop.domain;

import java.util.List;

import lombok.Data;

/**
 * @author wuzhong
 * @date 2023年09月13日 21:00
 */
@Data
public class TCategoryNode extends TShopCategory{
    private List<TCategoryNode> children= null;
    public String getLabel(){
        return getName();
    }
}
