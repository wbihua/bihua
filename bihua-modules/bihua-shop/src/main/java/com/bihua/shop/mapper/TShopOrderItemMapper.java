package com.bihua.shop.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.shop.domain.TShopOrderItem;
import com.bihua.shop.domain.vo.TShopOrderItemVo;

/**
 * 订单明细Mapper接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface TShopOrderItemMapper extends BaseMapperPlus<TShopOrderItemMapper, TShopOrderItem, TShopOrderItemVo> {

    List<TShopOrderItemVo> selectVoList(@Param("ew") Wrapper<TShopOrderItem> wrapper);

}
