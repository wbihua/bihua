package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopOrder;
import com.bihua.shop.domain.vo.TShopOrderVo;
import com.bihua.common.core.mapper.BaseMapperPlus;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 * 订单管理Mapper接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface TShopOrderMapper extends BaseMapperPlus<TShopOrderMapper, TShopOrder, TShopOrderVo> {

    TShopOrderVo queryByOrderSn(@Param("orderSn") String orderSn);

    int updateExpressById(@Param("id") Long id, @Param("idExpress") Long idExpress, @Param("shippingSn") String shippingSn, @Param("status") Integer status, @Param("shippingAmount") BigDecimal shippingAmount);
}
