package com.bihua.shop.job;

import com.bihua.shop.service.WeixinService;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author wuzhong
 * @date 2023年09月21日 16:19
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class UpdateWeixinTokenJob {
    private final WeixinService weixinService;
    /**
     * 1、简单任务示例（Bean模式）
     */
    @XxlJob("UpdateWeixinTokenJob")
    public void updateWeixinToken() throws Exception {
        XxlJobHelper.log("XXL-JOB, before update weixin token.");
        weixinService.updateWeixinToken();
        XxlJobHelper.log("XXL-JOB, after update weixin token.");
    }
}
