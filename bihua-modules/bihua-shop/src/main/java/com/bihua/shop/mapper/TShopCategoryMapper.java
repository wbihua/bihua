package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopCategory;
import com.bihua.shop.domain.vo.TShopCategoryVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 商品类别Mapper接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface TShopCategoryMapper extends BaseMapperPlus<TShopCategoryMapper, TShopCategory, TShopCategoryVo> {

}
