package com.bihua.shop.domain.vo;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;

import lombok.Data;



/**
 * 会员管理视图对象 t_shop_user
 *
 * @author bihua
 * @date 2023-09-16
 */
@Data
@ExcelIgnoreUnannotated
public class TShopUserVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 头像
     */
    @ExcelProperty(value = "头像")
    private String avatar;

    /**
     * 昵称
     */
    @ExcelProperty(value = "昵称")
    private String nickName;

    /**
     * 密码
     */
    @ExcelProperty(value = "密码")
    private String password;

    /**
     * 性别:male;female
     */
    @ExcelProperty(value = "性别:male;female")
    private String gender;

    /**
     * 手机号
     */
    @ExcelProperty(value = "手机号")
    private String mobile;

    /**
     * 微信头像
     */
    @ExcelProperty(value = "微信头像")
    private String wechatHeadImgUrl;

    /**
     * 微信昵称
     */
    @ExcelProperty(value = "微信昵称")
    private String wechatNickName;

    /**
     * 微信OpenID
     */
    @ExcelProperty(value = "微信OpenID")
    private String wechatOpenId;
    /**
     * 注册时间
     */
    @ExcelProperty(value = "注册时间")
    private Date createTime;
    /**
     * 最后登陆时间
     */
    @ExcelProperty(value = "最后登陆时间")
    private Date lastLoginTime;


}
