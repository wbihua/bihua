package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopOrderLog;
import com.bihua.shop.domain.vo.TShopOrderLogVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 订单日志Mapper接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface TShopOrderLogMapper extends BaseMapperPlus<TShopOrderLogMapper, TShopOrderLog, TShopOrderLogVo> {

}
