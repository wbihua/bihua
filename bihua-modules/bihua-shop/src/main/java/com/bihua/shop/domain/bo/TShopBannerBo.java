package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 广告banner业务对象 t_shop_banner
 *
 * @author name: bihua
 * @date 2023-09-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopBannerBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * banner图id
     */
    private String ossId;

    /**
     * banner图URL
     */
    private String ossUrl;

    /**
     * 界面
     */
    private String page;

    /**
     * 参数
     */
    private String param;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String title;

    /**
     * 类型
     */
    private String type;

    /**
     * 点击banner跳转到url
     */
    private String url;


}
