package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 商品类别视图对象 t_shop_category
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@ExcelIgnoreUnannotated
public class TShopCategoryVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 父类别
     */
    @ExcelProperty(value = "父类别")
    private Long pid;

    /**
     * 名称
     */
    @ExcelProperty(value = "名称")
    private String name;

    /**
     * 顺序
     */
    @ExcelProperty(value = "顺序")
    private Long sort;

    /**
     * 图标
     */
    @ExcelProperty(value = "图标ID")
    private String iconOssId;
    /**
     * 图标
     */
    @ExcelProperty(value = "图标URL")
    private String iconOssUrl;
    /**
     * 链接地址
     */
    @ExcelProperty(value = "链接地址")
    private String url;

    /**
     * 是否删除
     */
    @ExcelProperty(value = "是否删除")
    private Boolean isDelete;

    /**
     * 是否显示在首页
     */
    @ExcelProperty(value = "是否显示在首页")
    private Boolean showIndex;

    /**
     * 备注
     */
    @ExcelProperty(value = "备注")
    private String remark;


}
