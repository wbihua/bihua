package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopGoodsBo;
import com.bihua.shop.domain.bo.TShopGoodsSkuBo;
import com.bihua.shop.domain.vo.TShopGoodsSkuVo;
import com.bihua.shop.domain.vo.TShopGoodsVo;
import com.bihua.shop.service.ITShopGoodsService;
import com.bihua.shop.service.ITShopGoodsSkuService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 商品管理
 *
 * @author bihua
 * @date 2023-09-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopGoods")
public class TShopGoodsController extends BaseController {

    private final ITShopGoodsService iTShopGoodsService;
    private final ITShopGoodsSkuService iTShopGoodsSkuService;
    /**
     * 查询商品管理列表
     */
    @SaCheckPermission("shop:shopGoods:list")
    @GetMapping("/list")
    public TableDataInfo<TShopGoodsVo> list(TShopGoodsBo bo, PageQuery pageQuery) {
        return iTShopGoodsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品管理列表
     */
    @SaCheckPermission("shop:shopGoods:export")
    @Log(title = "商品管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopGoodsBo bo, HttpServletResponse response) {
        List<TShopGoodsVo> list = iTShopGoodsService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品管理", TShopGoodsVo.class, response);
    }

    /**
     * 获取商品管理详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopGoods:query")
    @GetMapping("/{id}")
    public R<TShopGoodsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        TShopGoodsVo vo = iTShopGoodsService.queryById(id);
        TShopGoodsSkuBo bo = new TShopGoodsSkuBo();
        bo.setIdGoods(id);
        bo.setIsDeleted(false);
        List<TShopGoodsSkuVo> skuVos = iTShopGoodsSkuService.queryList(bo);
        vo.setSkuVos(skuVos);
        return R.ok(vo);
    }

    /**
     * 新增商品管理
     */
    @SaCheckPermission("shop:shopGoods:add")
    @Log(title = "商品管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Long> add(@Validated(AddGroup.class) @RequestBody TShopGoodsBo bo) {
        boolean flag =  iTShopGoodsService.insertByBo(bo);
        if(flag){
            return R.ok(bo.getId());
        }else{
            return R.fail();
        }
    }

    /**
     * 修改商品管理
     */
    @SaCheckPermission("shop:shopGoods:edit")
    @Log(title = "商品管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopGoodsBo bo) {
        return toAjax(iTShopGoodsService.updateByBo(bo));
    }

    /**
     * 删除商品管理
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopGoods:remove")
    @Log(title = "商品管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopGoodsService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    @RequestMapping(value="/changeIsOnSale",method = RequestMethod.POST)
    @SaCheckPermission("shop:shopGoods:edit")
    @Log(title = "商品管理-是否上架", businessType = BusinessType.UPDATE)
    public R<Void> changeIsOnSale(@NotNull(message = "主键不能为空") @RequestParam("id")  Long id, @RequestParam("isOnSale") Boolean isOnSale){
        boolean flag = iTShopGoodsService.changeIsOnSale(id,isOnSale);
        return toAjax(flag);
    }
}
