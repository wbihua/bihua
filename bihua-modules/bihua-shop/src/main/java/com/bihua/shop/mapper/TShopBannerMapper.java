package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopBanner;
import com.bihua.shop.domain.vo.TShopBannerVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 广告bannerMapper接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface TShopBannerMapper extends BaseMapperPlus<TShopBannerMapper, TShopBanner, TShopBannerVo> {

}
