package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品属性名对象 t_shop_attr_key
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_attr_key")
public class TShopAttrKey extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 属性名
     */
    private String attrName;
    /**
     * 商品类别id
     */
    private Long idCategory;

}
