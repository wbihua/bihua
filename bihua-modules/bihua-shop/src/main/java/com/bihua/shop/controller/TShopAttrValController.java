package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.ArrayUtil;
import org.apache.commons.compress.utils.Lists;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopAttrKeyBo;
import com.bihua.shop.domain.bo.TShopAttrValBo;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.shop.domain.vo.TShopAttrValVo;
import com.bihua.shop.service.ITShopAttrKeyService;
import com.bihua.shop.service.ITShopAttrValService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 商品属性值
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopAttrVal")
public class TShopAttrValController extends BaseController {

    private final ITShopAttrValService iTShopAttrValService;
    private final ITShopAttrKeyService iTShopAttrKeyService;
    /**
     * 查询商品属性值列表
     */
    @SaCheckPermission("shop:shopAttrVal:list")
    @GetMapping("/list")
    public TableDataInfo<TShopAttrValVo> list(TShopAttrValBo bo, PageQuery pageQuery) {
        return iTShopAttrValService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品属性值列表
     */
    @SaCheckPermission("shop:shopAttrVal:export")
    @Log(title = "商品属性值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopAttrValBo bo, HttpServletResponse response) {
        List<TShopAttrValVo> list = iTShopAttrValService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品属性值", TShopAttrValVo.class, response);
    }

    /**
     * 获取商品属性值详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopAttrVal:query")
    @GetMapping("/{id}")
    public R<TShopAttrValVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopAttrValService.queryById(id));
    }
    @SaCheckPermission("shop:shopAttrVal:query")
    @RequestMapping(value = "/getAttrByCategoryAndGoods/{idCategory}",method = RequestMethod.GET)
    public R<Map<String, List>> getAttrByCategoryAndGoods(@PathVariable("idCategory") Long idCategory) {
        TShopAttrKeyBo tShopAttrKeyBo = new TShopAttrKeyBo();
        tShopAttrKeyBo.setIdCategory(idCategory);
        List<TShopAttrKeyVo> keyList = iTShopAttrKeyService.queryList(tShopAttrKeyBo);
        List<Long> idAttrKeyList = Lists.newArrayList();
        for(TShopAttrKeyVo attrKey:keyList){
            idAttrKeyList.add(attrKey.getId());
        }
        List<TShopAttrValVo> valList = Lists.newArrayList();
        if(!idAttrKeyList.isEmpty()) {
            valList = iTShopAttrValService.queryListByKeys(idAttrKeyList);
        }
        Map<String , List> retMap = new HashMap<>();
        retMap.put("keyList", keyList);
        retMap.put("valList", valList);
        return R.ok(retMap);
    }
    @SaCheckPermission("shop:shopAttrVal:query")
    @RequestMapping(value = "getAttrVals", method = RequestMethod.GET)
    public R<List<TShopAttrValVo>> getAttrVals(@RequestParam("idAttrKey")Long idAttrKey) {
        TShopAttrValBo bo = new TShopAttrValBo();
        bo.setIdAttrKey(idAttrKey);
        List<TShopAttrValVo> list  = iTShopAttrValService.queryList(bo);
        return R.ok(list);
    }
    /**
     * 新增商品属性值
     */
    @SaCheckPermission("shop:shopAttrVal:add")
    @Log(title = "商品属性值", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopAttrValBo bo) {
        return toAjax(iTShopAttrValService.insertByBo(bo));
    }

    /**
     * 修改商品属性值
     */
    @SaCheckPermission("shop:shopAttrVal:edit")
    @Log(title = "商品属性值", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopAttrValBo bo) {
        return toAjax(iTShopAttrValService.updateByBo(bo));
    }

    /**
     * 删除商品属性值
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopAttrVal:remove")
    @Log(title = "商品属性值", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopAttrValService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    @RequestMapping(value="updateAttrVal",method = RequestMethod.POST)
    @Log(title = "商品属性值", businessType = BusinessType.UPDATE)
    public R<Void> updateAttrName(@NotEmpty(message = "主键不能为空")@RequestParam("id") Long id,@RequestParam("attrVal") String attrVal){
        TShopAttrValVo attrValEntity = iTShopAttrValService.queryById(id);
        attrValEntity.setAttrVal(attrVal);
        return toAjax(iTShopAttrValService.updateByBo(BeanCopyUtils.copy(attrValEntity, TShopAttrValBo.class)));
    }
}
