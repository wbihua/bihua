package com.bihua.shop.domain.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 登录用户身份权限
 *
 * @author Lion Li
 */

@Data
@NoArgsConstructor
public class ShopLoginUser implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 性别:male;female
     */
    private String gender;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 微信头像
     */
    private String wechatHeadImgUrl;
    /**
     * 微信昵称
     */
    private String wechatNickName;
    /**
     * 微信OpenID
     */
    private String wechatOpenId;
    private Boolean refreshWechatInfo = true;
    /**
     * 注册时间
     */
    private Date createTime;
    /**
     * 最后登陆时间
     */
    private Date lastLoginTime;
    /**
     * 最后登录IP
     */
    private String loginIp;
    /**
     * 获取登录id
     */
    public String getLoginId() {
        if (userType == null) {
            throw new IllegalArgumentException("用户类型不能为空");
        }
        if (id == null) {
            throw new IllegalArgumentException("用户ID不能为空");
        }
        return userType + ":" + id;
    }
}
