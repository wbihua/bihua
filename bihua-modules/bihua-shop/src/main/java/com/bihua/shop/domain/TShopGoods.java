package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品管理对象 t_shop_goods
 *
 * @author bihua
 * @date 2023-09-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_goods")
public class TShopGoods extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 价格
     */
    private String price;
    /**
     * 库存数量
     */
    private Long stock;
    /**
     * 类别id
     */
    private Long idCategory;
    /**
     * 小图
     */
    private String picOssId;
    private String picOssUrl;
    /**
     * 产品详情
     */
    private String detail;
    /**
     * 大图相册列表,以逗号分隔
     */
    private String gallery;
    /**
     * 是否删除
     */
    private Boolean isDelete;
    /**
     * 是否人气商品
     */
    private Boolean isHot;
    /**
     * 是否新品推荐
     */
    private Boolean isNew;
    /**
     * 是否上架
     */
    private Boolean isOnSale;
    /**
     * 收藏数
     */
    private Long likeNum;
    /**
     * 产品简介
     */
    private String remark;

}
