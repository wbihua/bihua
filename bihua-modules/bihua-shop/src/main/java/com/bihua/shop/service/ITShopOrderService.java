package com.bihua.shop.service;

import java.util.Collection;
import java.util.List;

import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.shop.domain.bo.TShopOrderBo;
import com.bihua.shop.domain.vo.TShopOrderVo;

/**
 * 订单管理Service接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface ITShopOrderService {

    /**
     * 查询订单管理
     */
    TShopOrderVo queryById(Long id);

    /**
     * 查询订单管理列表
     */
    TableDataInfo<TShopOrderVo> queryPageList(TShopOrderBo bo, PageQuery pageQuery);

    /**
     * 查询订单管理列表
     */
    List<TShopOrderVo> queryList(TShopOrderBo bo);

    /**
     * 新增订单管理
     */
    Boolean insertByBo(TShopOrderBo bo);

    /**
     * 修改订单管理
     */
    Boolean updateByBo(TShopOrderBo bo);

    /**
     * 校验并批量删除订单管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    TShopOrderVo queryByOrderSn(String orderSn);

    Boolean sendOut(Long id, Long idExpress, String shippingSn, String shippingAmount);
}
