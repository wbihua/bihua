package com.bihua.shop.service;

import com.bihua.shop.domain.TSysExpress;
import com.bihua.shop.domain.vo.TSysExpressVo;
import com.bihua.shop.domain.bo.TSysExpressBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 物流公司Service接口
 *
 * @author bihua
 * @date 2023-09-18
 */
public interface ITSysExpressService {

    /**
     * 查询物流公司
     */
    TSysExpressVo queryById(Long id);

    /**
     * 查询物流公司列表
     */
    TableDataInfo<TSysExpressVo> queryPageList(TSysExpressBo bo, PageQuery pageQuery);

    /**
     * 查询物流公司列表
     */
    List<TSysExpressVo> queryList(TSysExpressBo bo);

    /**
     * 新增物流公司
     */
    Boolean insertByBo(TSysExpressBo bo);

    /**
     * 修改物流公司
     */
    Boolean updateByBo(TSysExpressBo bo);

    /**
     * 校验并批量删除物流公司信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
