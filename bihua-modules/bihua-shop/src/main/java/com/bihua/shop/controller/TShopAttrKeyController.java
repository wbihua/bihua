package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopAttrKeyBo;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.shop.service.ITShopAttrKeyService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 商品属性名
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopAttrKey")
public class TShopAttrKeyController extends BaseController {

    private final ITShopAttrKeyService iTShopAttrKeyService;

    /**
     * 查询商品属性名列表
     */
    @SaCheckPermission("shop:shopAttrKey:list")
    @GetMapping("/list")
    public TableDataInfo<TShopAttrKeyVo> list(TShopAttrKeyBo bo, PageQuery pageQuery) {
        return iTShopAttrKeyService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出商品属性名列表
     */
    @SaCheckPermission("shop:shopAttrKey:export")
    @Log(title = "商品属性名", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopAttrKeyBo bo, HttpServletResponse response) {
        List<TShopAttrKeyVo> list = iTShopAttrKeyService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品属性名", TShopAttrKeyVo.class, response);
    }

    /**
     * 获取商品属性名详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopAttrKey:query")
    @GetMapping("/{id}")
    public R<TShopAttrKeyVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopAttrKeyService.queryById(id));
    }

    /**
     * 新增商品属性名
     */
    @SaCheckPermission("shop:shopAttrKey:add")
    @Log(title = "商品属性名", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopAttrKeyBo bo) {
        return toAjax(iTShopAttrKeyService.insertByBo(bo));
    }

    /**
     * 修改商品属性名
     */
    @SaCheckPermission("shop:shopAttrKey:edit")
    @Log(title = "商品属性名", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopAttrKeyBo bo) {
        return toAjax(iTShopAttrKeyService.updateByBo(bo));
    }

    /**
     * 删除商品属性名
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopAttrKey:remove")
    @Log(title = "商品属性名", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopAttrKeyService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    @SaCheckPermission("shop:shopAttrKey:updateAttrName")
    @RequestMapping(value="updateAttrName",method = RequestMethod.POST)
    @Log(title = "商品属性名", businessType = BusinessType.UPDATE)
    public R<Void> updateAttrName(@NotEmpty(message = "主键不能为空") @RequestParam("id") Long id,@RequestParam("attrName") String attrName){
        TShopAttrKeyVo attrKey = iTShopAttrKeyService.queryById(id);
        attrKey.setAttrName(attrName);
        return toAjax(iTShopAttrKeyService.updateByBo(BeanCopyUtils.copy(attrKey, TShopAttrKeyBo.class)));
    }
}
