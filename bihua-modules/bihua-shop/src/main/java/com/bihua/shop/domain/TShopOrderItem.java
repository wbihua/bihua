package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 订单明细对象 t_shop_order_item
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_order_item")
public class TShopOrderItem extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 所属订单id
     */
    private Long idOrder;
    /**
     * 商品id
     */
    private Long idGoods;
    /**
     * skuId
     */
    private Long idSku;
    /**
     * 数量
     */
    private String count;
    /**
     * 单价
     */
    private String price;
    /**
     * 合计
     */
    private String totalPrice;

}
