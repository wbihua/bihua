package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopAddress;
import com.bihua.shop.domain.vo.TShopAddressVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 收货地址Mapper接口
 *
 * @author bihua
 * @date 2023-09-17
 */
public interface TShopAddressMapper extends BaseMapperPlus<TShopAddressMapper, TShopAddress, TShopAddressVo> {

}
