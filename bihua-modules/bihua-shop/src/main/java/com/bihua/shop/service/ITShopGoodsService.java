package com.bihua.shop.service;

import com.bihua.shop.domain.TShopGoods;
import com.bihua.shop.domain.vo.TShopGoodsVo;
import com.bihua.shop.domain.bo.TShopGoodsBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品管理Service接口
 *
 * @author bihua
 * @date 2023-09-15
 */
public interface ITShopGoodsService {

    /**
     * 查询商品管理
     */
    TShopGoodsVo queryById(Long id);

    /**
     * 查询商品管理列表
     */
    TableDataInfo<TShopGoodsVo> queryPageList(TShopGoodsBo bo, PageQuery pageQuery);

    /**
     * 查询商品管理列表
     */
    List<TShopGoodsVo> queryList(TShopGoodsBo bo);

    /**
     * 新增商品管理
     */
    Boolean insertByBo(TShopGoodsBo bo);

    /**
     * 修改商品管理
     */
    Boolean updateByBo(TShopGoodsBo bo);

    /**
     * 校验并批量删除商品管理信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

    boolean changeIsOnSale(Long id, Boolean isOnSale);
}
