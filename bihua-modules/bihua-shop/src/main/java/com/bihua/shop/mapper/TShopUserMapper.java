package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopUser;
import com.bihua.shop.domain.vo.TShopUserVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 会员管理Mapper接口
 *
 * @author bihua
 * @date 2023-09-16
 */
public interface TShopUserMapper extends BaseMapperPlus<TShopUserMapper, TShopUser, TShopUserVo> {

}
