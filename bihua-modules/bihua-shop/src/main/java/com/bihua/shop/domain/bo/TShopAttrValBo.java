package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品属性值业务对象 t_shop_attr_val
 *
 * @author name: bihua
 * @date 2023-09-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopAttrValBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 属性值
     */
    @NotBlank(message = "属性值不能为空", groups = { AddGroup.class, EditGroup.class })
    private String attrVal;

    /**
     * 属性id
     */
    @NotNull(message = "属性id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idAttrKey;


}
