package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopCartBo;
import com.bihua.shop.domain.vo.TShopCartVo;
import com.bihua.shop.domain.TShopCart;
import com.bihua.shop.mapper.TShopCartMapper;
import com.bihua.shop.service.ITShopCartService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 购物车Service业务层处理
 *
 * @author bihua
 * @date 2023-09-16
 */
@RequiredArgsConstructor
@Service
public class TShopCartServiceImpl implements ITShopCartService {

    private final TShopCartMapper baseMapper;

    /**
     * 查询购物车
     */
    @Override
    public TShopCartVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询购物车列表
     */
    @Override
    public TableDataInfo<TShopCartVo> queryPageList(TShopCartBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopCart> lqw = buildQueryWrapper(bo);
        Page<TShopCartVo> result = baseMapper.selectPageList(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询购物车列表
     */
    @Override
    public List<TShopCartVo> queryList(TShopCartBo bo) {
        LambdaQueryWrapper<TShopCart> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopCart> buildQueryWrapper(TShopCartBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopCart> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getCount() != null, TShopCart::getCount, bo.getCount());
        lqw.eq(bo.getIdGoods() != null, TShopCart::getIdGoods, bo.getIdGoods());
        lqw.eq(bo.getIdSku() != null, TShopCart::getIdSku, bo.getIdSku());
        lqw.eq(bo.getIdUser() != null, TShopCart::getIdUser, bo.getIdUser());
        return lqw;
    }

    /**
     * 新增购物车
     */
    @Override
    public Boolean insertByBo(TShopCartBo bo) {
        TShopCart add = BeanUtil.toBean(bo, TShopCart.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改购物车
     */
    @Override
    public Boolean updateByBo(TShopCartBo bo) {
        TShopCart update = BeanUtil.toBean(bo, TShopCart.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopCart entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除购物车
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
