package com.bihua.shop.domain;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 订单管理对象 t_shop_order
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@EqualsAndHashCode
@TableName("t_shop_order")
public class TShopOrder {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 用户id
     */
    private Long idUser;
    /**
     * 订单号
     */
    private String orderSn;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 优惠券抵扣金额
     */
    private BigDecimal couponPrice;
    /**
     * 交易金额
     */
    private BigDecimal tradeAmount;
    /**
     * 实付金额
     */
    private BigDecimal realPrice;
    /**
     * 总金额
     */
    private BigDecimal totalPrice;
    /**
     * 支付状态1:未付款;2:已付款,3:支付中
     */
    private Integer payStatus;
    /**
     * 支付流水号
     */
    private String payId;
    /**
     * 实付类型:alipay,wechat
     */
    private String payType;
    /**
     * 支付成功时间
     */
    private String payTime;
    /**
     * 收件人
     */
    private String consignee;
    /**
     * 收件人电话
     */
    private String mobile;
    /**
     * 收件地址
     */
    private String consigneeAddress;
    /**
     * 收货信息
     */
    private Long idAddress;
    /**
     * 快递公司
     */
    private Long idExpress;
    /**
     * 快递单号
     */
    private String shippingSn;
    /**
     * 配送费用
     */
    private BigDecimal shippingAmount;
    /**
     * 出库时间
     */
    private Date shippingTime;
    /**
     * 确认收货时间
     */
    private Date confirmReceivingTime;
    /**
     * 订单备注
     */
    private String message;
    /**
     * 管理员备注
     */
    private String adminMessage;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}
