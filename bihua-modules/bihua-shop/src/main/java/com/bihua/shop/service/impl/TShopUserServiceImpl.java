package com.bihua.shop.service.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.exception.ServiceException;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.utils.redis.CacheUtils;
import com.bihua.common.utils.spring.SpringUtils;
import com.bihua.shop.domain.TShopUser;
import com.bihua.shop.domain.bo.TShopUserBo;
import com.bihua.shop.domain.vo.TShopUserVo;
import com.bihua.shop.mapper.TShopUserMapper;
import com.bihua.shop.service.ITShopUserService;
import com.bihua.sms.core.SmsTemplate;
import com.bihua.sms.entity.SmsResult;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.RandomUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 会员管理Service业务层处理
 *
 * @author bihua
 * @date 2023-09-16
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class TShopUserServiceImpl implements ITShopUserService {

    private final TShopUserMapper baseMapper;
    /**
     * 查询会员管理
     */
    @Override
    public TShopUserVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询会员管理列表
     */
    @Override
    public TableDataInfo<TShopUserVo> queryPageList(TShopUserBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopUser> lqw = buildQueryWrapper(bo);
        Page<TShopUserVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询会员管理列表
     */
    @Override
    public List<TShopUserVo> queryList(TShopUserBo bo) {
        LambdaQueryWrapper<TShopUser> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopUser> buildQueryWrapper(TShopUserBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAvatar()), TShopUser::getAvatar, bo.getAvatar());
        lqw.like(StringUtils.isNotBlank(bo.getNickName()), TShopUser::getNickName, bo.getNickName());
        lqw.eq(StringUtils.isNotBlank(bo.getPassword()), TShopUser::getPassword, bo.getPassword());
        lqw.eq(StringUtils.isNotBlank(bo.getGender()), TShopUser::getGender, bo.getGender());
        lqw.eq(StringUtils.isNotBlank(bo.getMobile()), TShopUser::getMobile, bo.getMobile());
        lqw.eq(StringUtils.isNotBlank(bo.getWechatHeadImgUrl()), TShopUser::getWechatHeadImgUrl, bo.getWechatHeadImgUrl());
        lqw.like(StringUtils.isNotBlank(bo.getWechatNickName()), TShopUser::getWechatNickName, bo.getWechatNickName());
        lqw.eq(StringUtils.isNotBlank(bo.getWechatOpenId()), TShopUser::getWechatOpenId, bo.getWechatOpenId());
        lqw.eq(bo.getLastLoginTime() != null, TShopUser::getLastLoginTime, bo.getLastLoginTime());
        lqw.between(params.get("beginCreateTime") != null && params.get("endCreateTime") != null,
                TShopUser::getCreateTime, params.get("beginCreateTime"), params.get("endCreateTime"));
        lqw.between(params.get("beginLastLoginTime") != null && params.get("endLastLoginTime") != null,
                TShopUser::getLastLoginTime, params.get("beginLastLoginTime"), params.get("endLastLoginTime"));
        return lqw;
    }

    /**
     * 新增会员管理
     */
    @Override
    public Boolean insertByBo(TShopUserBo bo) {
        TShopUser add = BeanUtil.toBean(bo, TShopUser.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改会员管理
     */
    @Override
    public Boolean updateByBo(TShopUserBo bo) {
        TShopUser update = BeanUtil.toBean(bo, TShopUser.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除会员管理
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public int resetUserPwd(Long id, String password) {
        return baseMapper.update(null,
                new LambdaUpdateWrapper<TShopUser>()
                        .set(TShopUser::getPassword, password)
                        .eq(TShopUser::getId, id));
    }

    @Override
    public boolean updateUserAvatar(Long id, String avatar) {
        return baseMapper.update(null,
                new LambdaUpdateWrapper<TShopUser>()
                        .set(TShopUser::getAvatar, avatar)
                        .eq(TShopUser::getId, id)) > 0;
    }

    @Override
    public TShopUserVo findByWechatOpenId(String openId) {
        LambdaQueryWrapper<TShopUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(openId), TShopUser::getWechatOpenId, openId);
        return baseMapper.selectVoOne(lqw);
    }
}
