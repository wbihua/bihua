package com.bihua.shop.domain;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 会员管理对象 t_shop_user
 *
 * @author bihua
 * @date 2023-09-16
 */
@Data
@EqualsAndHashCode
@TableName("t_shop_user")
public class TShopUser {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别:male;female
     */
    private String gender;
    /**
     * 手机号
     */
    private String mobile;
    /**
     * 微信头像
     */
    private String wechatHeadImgUrl;
    /**
     * 微信昵称
     */
    private String wechatNickName;
    /**
     * 微信OpenID
     */
    private String wechatOpenId;
    /**
     * 注册时间
     */
    private Date createTime;
    /**
     * 最后登陆时间
     */
    private Date lastLoginTime;
    /**
     * 最后登录IP
     */
    private String loginIp;

}
