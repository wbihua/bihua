package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 物流公司业务对象 t_sys_express
 *
 * @author bihua
 * @date 2023-09-18
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TSysExpressBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 公司编码
     */
    @NotBlank(message = "公司编码不能为空", groups = { AddGroup.class, EditGroup.class })
    private String code;

    /**
     * 公司名称
     */
    @NotBlank(message = "公司名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String name;

    /**
     * 排序
     */
    @NotNull(message = "排序不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer sort;

    /**
     * 是否禁用
     */
    @NotNull(message = "是否禁用不能为空", groups = { AddGroup.class, EditGroup.class })
    private Boolean disabled;


}
