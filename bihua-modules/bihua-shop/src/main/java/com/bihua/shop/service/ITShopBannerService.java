package com.bihua.shop.service;

import com.bihua.shop.domain.TShopBanner;
import com.bihua.shop.domain.vo.TShopBannerVo;
import com.bihua.shop.domain.bo.TShopBannerBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 广告bannerService接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface ITShopBannerService {

    /**
     * 查询广告banner
     */
    TShopBannerVo queryById(Long id);

    List<TShopBannerVo> queryByIds(List<Long> ids);
    /**
     * 查询广告banner列表
     */
    TableDataInfo<TShopBannerVo> queryPageList(TShopBannerBo bo, PageQuery pageQuery);

    /**
     * 查询广告banner列表
     */
    List<TShopBannerVo> queryList(TShopBannerBo bo);

    /**
     * 新增广告banner
     */
    Boolean insertByBo(TShopBannerBo bo);

    /**
     * 修改广告banner
     */
    Boolean updateByBo(TShopBannerBo bo);

    /**
     * 校验并批量删除广告banner信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
