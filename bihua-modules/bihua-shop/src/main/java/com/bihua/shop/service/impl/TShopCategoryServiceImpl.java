package com.bihua.shop.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.utils.StringUtils;
import com.bihua.shop.domain.TCategoryNode;
import com.bihua.shop.domain.TShopCategory;
import com.bihua.shop.domain.bo.TShopCategoryBo;
import com.bihua.shop.domain.vo.TShopCategoryVo;
import com.bihua.shop.mapper.TShopCategoryMapper;
import com.bihua.shop.service.ITShopCategoryService;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 商品类别Service业务层处理
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@RequiredArgsConstructor
@Service
public class TShopCategoryServiceImpl implements ITShopCategoryService {

    private final TShopCategoryMapper baseMapper;

    /**
     * 查询商品类别
     */
    @Override
    public TShopCategoryVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品类别列表
     */
    @Override
    public TableDataInfo<TShopCategoryVo> queryPageList(TShopCategoryBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopCategory> lqw = buildQueryWrapper(bo);
        Page<TShopCategoryVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    @Override
    public List<TCategoryNode> getCategories() {
        QueryWrapper<TShopCategory> wrapper = Wrappers.query();
        wrapper.orderByAsc("sort");
        List<TShopCategoryVo> list = baseMapper.selectVoList(wrapper);
        List<TCategoryNode> nodes = Lists.newArrayList();
        for(TShopCategoryVo category:list){
            if(category.getPid()==null) {
                TCategoryNode node = new TCategoryNode();
                BeanUtils.copyProperties(category, node);
                nodes.add(node);
            }

        }
        for(TCategoryNode node:nodes){
            for(TShopCategoryVo category:list){
                if(category.getPid()!=null&&category.getPid().intValue() == node.getId().intValue()){
                    TCategoryNode child = new TCategoryNode();
                    BeanUtils.copyProperties(category,child);
                    if(node.getChildren()==null){
                        node.setChildren(Lists.newArrayList());
                    }
                    node.getChildren().add(child);
                }
            }
        }
        return nodes;
    }

    /**
     * 查询商品类别列表
     */
    @Override
    public List<TShopCategoryVo> queryList(TShopCategoryBo bo) {
        LambdaQueryWrapper<TShopCategory> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopCategory> buildQueryWrapper(TShopCategoryBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopCategory> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getPid() != null, TShopCategory::getPid, bo.getPid());
        lqw.like(StringUtils.isNotBlank(bo.getName()), TShopCategory::getName, bo.getName());
        lqw.eq(bo.getSort() != null, TShopCategory::getSort, bo.getSort());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), TShopCategory::getUrl, bo.getUrl());
        lqw.eq(bo.getIsDelete() != null, TShopCategory::getIsDelete, bo.getIsDelete());
        lqw.eq(bo.getShowIndex() != null, TShopCategory::getShowIndex, bo.getShowIndex());
        return lqw;
    }

    /**
     * 新增商品类别
     */
    @Override
    public Boolean insertByBo(TShopCategoryBo bo) {
        TShopCategory add = BeanUtil.toBean(bo, TShopCategory.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品类别
     */
    @Override
    public Boolean updateByBo(TShopCategoryBo bo) {
        TShopCategory update = BeanUtil.toBean(bo, TShopCategory.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopCategory entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品类别
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
