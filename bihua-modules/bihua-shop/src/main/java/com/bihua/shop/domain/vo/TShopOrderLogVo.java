package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 订单日志视图对象 t_shop_order_log
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@ExcelIgnoreUnannotated
public class TShopOrderLogVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 所属订单id
     */
    @ExcelProperty(value = "所属订单id")
    private Long idOrder;

    /**
     * 日志详情
     */
    @ExcelProperty(value = "日志详情")
    private String remark;


}
