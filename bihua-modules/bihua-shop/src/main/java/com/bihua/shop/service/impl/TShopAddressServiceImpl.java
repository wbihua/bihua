package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopAddressBo;
import com.bihua.shop.domain.vo.TShopAddressVo;
import com.bihua.shop.domain.TShopAddress;
import com.bihua.shop.mapper.TShopAddressMapper;
import com.bihua.shop.service.ITShopAddressService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 收货地址Service业务层处理
 *
 * @author bihua
 * @date 2023-09-17
 */
@RequiredArgsConstructor
@Service
public class TShopAddressServiceImpl implements ITShopAddressService {

    private final TShopAddressMapper baseMapper;

    /**
     * 查询收货地址
     */
    @Override
    public TShopAddressVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询收货地址列表
     */
    @Override
    public TableDataInfo<TShopAddressVo> queryPageList(TShopAddressBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopAddress> lqw = buildQueryWrapper(bo);
        Page<TShopAddressVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询收货地址列表
     */
    @Override
    public List<TShopAddressVo> queryList(TShopAddressBo bo) {
        LambdaQueryWrapper<TShopAddress> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopAddress> buildQueryWrapper(TShopAddressBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopAddress> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getIdUser() != null, TShopAddress::getIdUser, bo.getIdUser());
        lqw.like(StringUtils.isNotBlank(bo.getName()), TShopAddress::getName, bo.getName());
        lqw.eq(StringUtils.isNotBlank(bo.getTel()), TShopAddress::getTel, bo.getTel());
        lqw.eq(StringUtils.isNotBlank(bo.getProvince()), TShopAddress::getProvince, bo.getProvince());
        lqw.eq(StringUtils.isNotBlank(bo.getCity()), TShopAddress::getCity, bo.getCity());
        lqw.eq(StringUtils.isNotBlank(bo.getDistrict()), TShopAddress::getDistrict, bo.getDistrict());
        lqw.eq(StringUtils.isNotBlank(bo.getAreaCode()), TShopAddress::getAreaCode, bo.getAreaCode());
        lqw.eq(StringUtils.isNotBlank(bo.getPostCode()), TShopAddress::getPostCode, bo.getPostCode());
        lqw.eq(StringUtils.isNotBlank(bo.getAddressDetail()), TShopAddress::getAddressDetail, bo.getAddressDetail());
        lqw.eq(bo.getIsDefault() != null, TShopAddress::getIsDefault, bo.getIsDefault());
        lqw.eq(bo.getIsDelete() != null, TShopAddress::getIsDelete, bo.getIsDelete());
        return lqw;
    }

    /**
     * 新增收货地址
     */
    @Override
    public Boolean insertByBo(TShopAddressBo bo) {
        TShopAddress add = BeanUtil.toBean(bo, TShopAddress.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改收货地址
     */
    @Override
    public Boolean updateByBo(TShopAddressBo bo) {
        TShopAddress update = BeanUtil.toBean(bo, TShopAddress.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopAddress entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除收货地址
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
