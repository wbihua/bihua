package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopGoodsSkuVo;
import com.bihua.shop.domain.bo.TShopGoodsSkuBo;
import com.bihua.shop.service.ITShopGoodsSkuService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 商品SKU
 *
 * @author bihua
 * @date 2023-09-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopGoodsSku")
public class TShopGoodsSkuController extends BaseController {

    private final ITShopGoodsSkuService iTShopGoodsSkuService;

    /**
     * 查询商品SKU列表
     */
    @SaCheckPermission("shop:shopGoodsSku:list")
    @GetMapping("/list")
    public R<List<TShopGoodsSkuVo>> list(TShopGoodsSkuBo bo) {
        return R.ok(iTShopGoodsSkuService.queryList(bo));
    }

    /**
     * 导出商品SKU列表
     */
    @SaCheckPermission("shop:shopGoodsSku:export")
    @Log(title = "商品SKU", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopGoodsSkuBo bo, HttpServletResponse response) {
        List<TShopGoodsSkuVo> list = iTShopGoodsSkuService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品SKU", TShopGoodsSkuVo.class, response);
    }

    /**
     * 获取商品SKU详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopGoodsSku:query")
    @GetMapping("/{id}")
    public R<TShopGoodsSkuVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopGoodsSkuService.queryById(id));
    }

    /**
     * 新增商品SKU
     */
    @SaCheckPermission("shop:shopGoodsSku:add")
    @Log(title = "商品SKU", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopGoodsSkuBo bo) {
        return toAjax(iTShopGoodsSkuService.insertByBo(bo));
    }

    /**
     * 修改商品SKU
     */
    @SaCheckPermission("shop:shopGoodsSku:edit")
    @Log(title = "商品SKU", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopGoodsSkuBo bo) {
        return toAjax(iTShopGoodsSkuService.updateByBo(bo));
    }

    /**
     * 删除商品SKU
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopGoodsSku:remove")
    @Log(title = "商品SKU", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopGoodsSkuService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
