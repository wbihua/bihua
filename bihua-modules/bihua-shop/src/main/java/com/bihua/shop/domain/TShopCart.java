package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 购物车对象 t_shop_cart
 *
 * @author bihua
 * @date 2023-09-16
 */
@Data
@EqualsAndHashCode
@TableName("t_shop_cart")
public class TShopCart{

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 数量
     */
    private Long count;
    /**
     * 商品id
     */
    private Long idGoods;
    /**
     * skuId
     */
    private Long idSku;
    /**
     * 用户id
     */
    private Long idUser;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新时间
     */
    private Date updateTime;
}
