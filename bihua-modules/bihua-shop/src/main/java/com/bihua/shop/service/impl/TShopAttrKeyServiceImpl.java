package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopAttrKeyBo;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.shop.domain.TShopAttrKey;
import com.bihua.shop.mapper.TShopAttrKeyMapper;
import com.bihua.shop.service.ITShopAttrKeyService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品属性名Service业务层处理
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@RequiredArgsConstructor
@Service
public class TShopAttrKeyServiceImpl implements ITShopAttrKeyService {

    private final TShopAttrKeyMapper baseMapper;

    /**
     * 查询商品属性名
     */
    @Override
    public TShopAttrKeyVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品属性名列表
     */
    @Override
    public TableDataInfo<TShopAttrKeyVo> queryPageList(TShopAttrKeyBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopAttrKey> lqw = buildQueryWrapper(bo);
        Page<TShopAttrKeyVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品属性名列表
     */
    @Override
    public List<TShopAttrKeyVo> queryList(TShopAttrKeyBo bo) {
        LambdaQueryWrapper<TShopAttrKey> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopAttrKey> buildQueryWrapper(TShopAttrKeyBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopAttrKey> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(bo.getAttrName()), TShopAttrKey::getAttrName, bo.getAttrName());
        lqw.eq(bo.getIdCategory() != null, TShopAttrKey::getIdCategory, bo.getIdCategory());
        return lqw;
    }

    /**
     * 新增商品属性名
     */
    @Override
    public Boolean insertByBo(TShopAttrKeyBo bo) {
        TShopAttrKey add = BeanUtil.toBean(bo, TShopAttrKey.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品属性名
     */
    @Override
    public Boolean updateByBo(TShopAttrKeyBo bo) {
        TShopAttrKey update = BeanUtil.toBean(bo, TShopAttrKey.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopAttrKey entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品属性名
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
