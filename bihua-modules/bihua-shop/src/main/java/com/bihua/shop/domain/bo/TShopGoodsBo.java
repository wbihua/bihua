package com.bihua.shop.domain.bo;

import javax.validation.constraints.NotNull;

import com.bihua.common.core.domain.BaseEntity;
import com.bihua.common.core.validate.EditGroup;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 商品管理业务对象 t_shop_goods
 *
 * @author bihua
 * @date 2023-09-15
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopGoodsBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 价格
     */
    private String price;

    /**
     * 库存数量
     */
    private Long stock;

    /**
     * 类别id
     */
    private Long idCategory;

    /**
     * 小图
     */
    private String picOssId;
    private String picOssUrl;

    /**
     * 产品详情
     */
    private String detail;

    /**
     * 大图相册列表,以逗号分隔
     */
    private String gallery;

    /**
     * 是否删除
     */
    private Boolean isDelete;

    /**
     * 是否人气商品
     */
    private Boolean isHot;

    /**
     * 是否新品推荐
     */
    private Boolean isNew;

    /**
     * 是否上架
     */
    private Boolean isOnSale;

    /**
     * 收藏数
     */
    private Long likeNum;

    /**
     * 产品简介
     */
    private String remark;


}
