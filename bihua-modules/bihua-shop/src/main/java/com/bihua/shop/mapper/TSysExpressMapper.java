package com.bihua.shop.mapper;

import com.bihua.shop.domain.TSysExpress;
import com.bihua.shop.domain.vo.TSysExpressVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 物流公司Mapper接口
 *
 * @author bihua
 * @date 2023-09-18
 */
public interface TSysExpressMapper extends BaseMapperPlus<TSysExpressMapper, TSysExpress, TSysExpressVo> {

}
