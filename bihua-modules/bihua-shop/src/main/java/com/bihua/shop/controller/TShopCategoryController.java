package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.bihua.shop.domain.TCategoryNode;
import org.apache.commons.compress.utils.Lists;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopAttrKeyBo;
import com.bihua.shop.domain.bo.TShopCategoryBannerRelBo;
import com.bihua.shop.domain.bo.TShopCategoryBo;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.shop.domain.vo.TShopBannerVo;
import com.bihua.shop.domain.vo.TShopCategoryBannerRelVo;
import com.bihua.shop.domain.vo.TShopCategoryVo;
import com.bihua.shop.service.ITShopAttrKeyService;
import com.bihua.shop.service.ITShopBannerService;
import com.bihua.shop.service.ITShopCategoryBannerRelService;
import com.bihua.shop.service.ITShopCategoryService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.collection.CollectionUtil;
import lombok.RequiredArgsConstructor;

/**
 * 商品类别
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopCategory")
public class TShopCategoryController extends BaseController {

    private final ITShopCategoryService iTShopCategoryService;
    private final ITShopAttrKeyService iTShopAttrKeyService;
    private final ITShopCategoryBannerRelService iTShopCategoryBannerRelService;
    private final ITShopBannerService iTShopBannerService;
    /**
     * 查询商品类别列表
     */
    @SaCheckPermission("shop:shopCategory:list")
    @GetMapping("/list")
    public R<List<TCategoryNode>> list() {
        List<TCategoryNode> list = iTShopCategoryService.getCategories();
        return R.ok(list);
    }
    //public TableDataInfo<TShopCategoryVo> list(TShopCategoryBo bo, PageQuery pageQuery) {
    //    return iTShopCategoryService.queryPageList(bo, pageQuery);
    //}

    /**
     * 导出商品类别列表
     */
    @SaCheckPermission("shop:shopCategory:export")
    @Log(title = "商品类别", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopCategoryBo bo, HttpServletResponse response) {
        List<TShopCategoryVo> list = iTShopCategoryService.queryList(bo);
        ExcelUtil.exportExcel(list, "商品类别", TShopCategoryVo.class, response);
    }

    /**
     * 获取商品类别详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopCategory:query")
    @GetMapping("/{id}")
    public R<TShopCategoryVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopCategoryService.queryById(id));
    }
    @SaCheckPermission("shop:shopCategory:query")
    @RequestMapping(value ="getAttrKeys/{idCategory}",method = RequestMethod.GET)
    public R<List<TShopAttrKeyVo>> getAttrKeys(@NotNull(message = "主键不能为空") @PathVariable("idCategory") Long idCategory){
        TShopAttrKeyBo bo = new TShopAttrKeyBo();
        bo.setIdCategory(idCategory);
        List<TShopAttrKeyVo> list = iTShopAttrKeyService.queryList(bo);
        return R.ok(list);
    }
    @RequestMapping(value="/getBanners/{idCategory}",method = RequestMethod.GET)
    public R<List<TShopBannerVo>> getBanners(@NotNull(message = "主键不能为空") @PathVariable("idCategory") Long idCategory){
        TShopCategoryBannerRelBo bo = new TShopCategoryBannerRelBo();
        bo.setIdCategory(idCategory);
        List<TShopCategoryBannerRelVo> relList = iTShopCategoryBannerRelService.queryList(bo);
        List<Long> bannerIdList = Lists.newArrayList();
        relList.forEach( item->{
            bannerIdList.add(item.getIdBanner());
        });
        if(CollectionUtil.isNotEmpty(bannerIdList)){
            return R.ok(iTShopBannerService.queryByIds(bannerIdList));
        }else{
            return R.ok();
        }
    }
    /**
     * 新增商品类别
     */
    @SaCheckPermission("shop:shopCategory:add")
    @Log(title = "商品类别", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopCategoryBo bo) {
        return toAjax(iTShopCategoryService.insertByBo(bo));
    }

    /**
     * 修改商品类别
     */
    @SaCheckPermission("shop:shopCategory:edit")
    @Log(title = "商品类别", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopCategoryBo bo) {
        return toAjax(iTShopCategoryService.updateByBo(bo));
    }

    /**
     * 删除商品类别
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopCategory:remove")
    @Log(title = "商品类别", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopCategoryService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
    @RequestMapping(value="/removeBanner/{idCategory}/{idBanner}",method = RequestMethod.DELETE)
    @SaCheckPermission("shop:shopCategory:removeBanner")
    public R<Void> removeBanner(@NotNull(message = "主键不能为空") @PathVariable("idCategory") Long idCategory,
                               @PathVariable("idBanner") Long idBanner){
        TShopCategoryBannerRelBo bo = new TShopCategoryBannerRelBo();
        bo.setIdCategory(idCategory);
        bo.setIdBanner(idBanner);
        List<TShopCategoryBannerRelVo> rels = iTShopCategoryBannerRelService.queryList(bo);
        if(CollectionUtil.isNotEmpty(rels)){
            List<Long> idList = Lists.newArrayList();
            rels.forEach( item->{
                idList.add(item.getId());
            });
            iTShopCategoryBannerRelService.deleteWithValidByIds(idList, false);
        }
        return R.ok();
    }
    @RequestMapping(value="/setBanner/{idCategory}/{idBanner}",method = RequestMethod.POST)
    @SaCheckPermission("shop:shopCategory:setBanner")
    public R<Void> setBanner(@NotNull(message = "主键不能为空") @PathVariable("idCategory") Long idCategory,
                            @PathVariable("idBanner") Long idBanner){
        TShopCategoryBannerRelBo bo = new TShopCategoryBannerRelBo();
        bo.setIdCategory(idCategory);
        bo.setIdBanner(idBanner);
        List<TShopCategoryBannerRelVo> rels = iTShopCategoryBannerRelService.queryList(bo);
        if(CollectionUtil.isNotEmpty(rels)){
            return R.ok();
        }
        TShopCategoryBannerRelBo relBo = new TShopCategoryBannerRelBo();
        relBo.setIdCategory(idCategory);
        relBo.setIdBanner(idBanner);
        iTShopCategoryBannerRelService.insertByBo(relBo);
        return R.ok();
    }
    @PostMapping(value="/changeShowIndex/{idCategory}/{showIndex}")
    @SaCheckPermission("shop:shopCategory:changeShowIndex")
    public R<Void> changeShowIndex(@NotNull(message = "主键不能为空") @PathVariable("idCategory") Long idCategory,
                                  @PathVariable("showIndex") Boolean showIndex){
        TShopCategoryVo category = iTShopCategoryService.queryById(idCategory);
        category.setShowIndex(showIndex);
        iTShopCategoryService.updateByBo(BeanCopyUtils.copy(category, TShopCategoryBo.class));
        return R.ok();
    }
}
