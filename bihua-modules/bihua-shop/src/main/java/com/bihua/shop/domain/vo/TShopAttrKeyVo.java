package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 商品属性名视图对象 t_shop_attr_key
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@ExcelIgnoreUnannotated
public class TShopAttrKeyVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 属性名
     */
    @ExcelProperty(value = "属性名")
    private String attrName;

    /**
     * 商品类别id
     */
    @ExcelProperty(value = "商品类别id")
    private Long idCategory;


}
