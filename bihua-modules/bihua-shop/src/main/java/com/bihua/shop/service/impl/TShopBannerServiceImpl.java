package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopBannerBo;
import com.bihua.shop.domain.vo.TShopBannerVo;
import com.bihua.shop.domain.TShopBanner;
import com.bihua.shop.mapper.TShopBannerMapper;
import com.bihua.shop.service.ITShopBannerService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 广告bannerService业务层处理
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@RequiredArgsConstructor
@Service
public class TShopBannerServiceImpl implements ITShopBannerService {

    private final TShopBannerMapper baseMapper;

    /**
     * 查询广告banner
     */
    @Override
    public TShopBannerVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    @Override
    public List<TShopBannerVo> queryByIds(List<Long> ids) {
        return baseMapper.selectVoBatchIds(ids);
    }

    /**
     * 查询广告banner列表
     */
    @Override
    public TableDataInfo<TShopBannerVo> queryPageList(TShopBannerBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopBanner> lqw = buildQueryWrapper(bo);
        Page<TShopBannerVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询广告banner列表
     */
    @Override
    public List<TShopBannerVo> queryList(TShopBannerBo bo) {
        LambdaQueryWrapper<TShopBanner> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopBanner> buildQueryWrapper(TShopBannerBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopBanner> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getPage()), TShopBanner::getPage, bo.getPage());
        lqw.eq(StringUtils.isNotBlank(bo.getParam()), TShopBanner::getParam, bo.getParam());
        lqw.eq(StringUtils.isNotBlank(bo.getTitle()), TShopBanner::getTitle, bo.getTitle());
        lqw.eq(StringUtils.isNotBlank(bo.getType()), TShopBanner::getType, bo.getType());
        lqw.eq(StringUtils.isNotBlank(bo.getUrl()), TShopBanner::getUrl, bo.getUrl());
        return lqw;
    }

    /**
     * 新增广告banner
     */
    @Override
    public Boolean insertByBo(TShopBannerBo bo) {
        TShopBanner add = BeanUtil.toBean(bo, TShopBanner.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改广告banner
     */
    @Override
    public Boolean updateByBo(TShopBannerBo bo) {
        TShopBanner update = BeanUtil.toBean(bo, TShopBanner.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopBanner entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除广告banner
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
