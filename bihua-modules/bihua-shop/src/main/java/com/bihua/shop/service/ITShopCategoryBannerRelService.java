package com.bihua.shop.service;

import com.bihua.shop.domain.TShopCategoryBannerRel;
import com.bihua.shop.domain.vo.TShopCategoryBannerRelVo;
import com.bihua.shop.domain.bo.TShopCategoryBannerRelBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 类别banner关联Service接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface ITShopCategoryBannerRelService {

    /**
     * 查询类别banner关联
     */
    TShopCategoryBannerRelVo queryById(Long id);

    /**
     * 查询类别banner关联列表
     */
    TableDataInfo<TShopCategoryBannerRelVo> queryPageList(TShopCategoryBannerRelBo bo, PageQuery pageQuery);

    /**
     * 查询类别banner关联列表
     */
    List<TShopCategoryBannerRelVo> queryList(TShopCategoryBannerRelBo bo);

    /**
     * 新增类别banner关联
     */
    Boolean insertByBo(TShopCategoryBannerRelBo bo);

    /**
     * 修改类别banner关联
     */
    Boolean updateByBo(TShopCategoryBannerRelBo bo);

    /**
     * 校验并批量删除类别banner关联信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
