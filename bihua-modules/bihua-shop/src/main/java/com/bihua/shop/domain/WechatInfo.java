package com.bihua.shop.domain;

import java.io.Serializable;

import lombok.Data;

@Data
public class WechatInfo implements Serializable {
    private String nickName;
    private String headUrl;
    private String openId;
}
