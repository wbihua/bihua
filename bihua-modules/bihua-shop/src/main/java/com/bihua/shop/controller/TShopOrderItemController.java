package com.bihua.shop.controller;

import java.util.List;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import lombok.RequiredArgsConstructor;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.*;
import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.web.bind.annotation.*;
import org.springframework.validation.annotation.Validated;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.core.validate.QueryGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.vo.TShopOrderItemVo;
import com.bihua.shop.domain.bo.TShopOrderItemBo;
import com.bihua.shop.service.ITShopOrderItemService;
import com.bihua.common.core.page.TableDataInfo;

/**
 * 订单明细
 *
 * @author bihua
 * @date 2023-09-17
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopOrderItem")
public class TShopOrderItemController extends BaseController {

    private final ITShopOrderItemService iTShopOrderItemService;

    /**
     * 查询订单明细列表
     */
    @SaCheckPermission("shop:shopOrderItem:list")
    @GetMapping("/list")
    public TableDataInfo<TShopOrderItemVo> list(TShopOrderItemBo bo, PageQuery pageQuery) {
        return iTShopOrderItemService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出订单明细列表
     */
    @SaCheckPermission("shop:shopOrderItem:export")
    @Log(title = "订单明细", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopOrderItemBo bo, HttpServletResponse response) {
        List<TShopOrderItemVo> list = iTShopOrderItemService.queryList(bo);
        ExcelUtil.exportExcel(list, "订单明细", TShopOrderItemVo.class, response);
    }

    /**
     * 获取订单明细详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopOrderItem:query")
    @GetMapping("/{id}")
    public R<TShopOrderItemVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopOrderItemService.queryById(id));
    }

    /**
     * 新增订单明细
     */
    @SaCheckPermission("shop:shopOrderItem:add")
    @Log(title = "订单明细", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopOrderItemBo bo) {
        return toAjax(iTShopOrderItemService.insertByBo(bo));
    }

    /**
     * 修改订单明细
     */
    @SaCheckPermission("shop:shopOrderItem:edit")
    @Log(title = "订单明细", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopOrderItemBo bo) {
        return toAjax(iTShopOrderItemService.updateByBo(bo));
    }

    /**
     * 删除订单明细
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopOrderItem:remove")
    @Log(title = "订单明细", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopOrderItemService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
