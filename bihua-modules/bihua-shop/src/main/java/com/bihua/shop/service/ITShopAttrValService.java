package com.bihua.shop.service;

import com.bihua.shop.domain.TShopAttrVal;
import com.bihua.shop.domain.vo.TShopAttrValVo;
import com.bihua.shop.domain.bo.TShopAttrValBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品属性值Service接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface ITShopAttrValService {

    /**
     * 查询商品属性值
     */
    TShopAttrValVo queryById(Long id);

    /**
     * 查询商品属性值列表
     */
    TableDataInfo<TShopAttrValVo> queryPageList(TShopAttrValBo bo, PageQuery pageQuery);

    /**
     * 查询商品属性值列表
     */
    List<TShopAttrValVo> queryList(TShopAttrValBo bo);

    /**
     * 查询商品属性值列表
     */
    List<TShopAttrValVo> queryListByKeys(List<Long> keyIds);

    /**
     * 新增商品属性值
     */
    Boolean insertByBo(TShopAttrValBo bo);

    /**
     * 修改商品属性值
     */
    Boolean updateByBo(TShopAttrValBo bo);

    /**
     * 校验并批量删除商品属性值信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
