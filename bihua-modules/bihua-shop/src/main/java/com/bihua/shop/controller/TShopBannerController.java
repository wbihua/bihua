package com.bihua.shop.controller;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.shop.domain.bo.TShopBannerBo;
import com.bihua.shop.domain.vo.TShopBannerVo;
import com.bihua.shop.service.ITShopBannerService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 广告banner
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/shop/shopBanner")
public class TShopBannerController extends BaseController {

    private final ITShopBannerService iTShopBannerService;

    /**
     * 查询广告banner列表
     */
    @SaCheckPermission("shop:shopBanner:list")
    @GetMapping("/list")
    public TableDataInfo<TShopBannerVo> list(TShopBannerBo bo, PageQuery pageQuery) {
        return iTShopBannerService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出广告banner列表
     */
    @SaCheckPermission("shop:shopBanner:export")
    @Log(title = "广告banner", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(TShopBannerBo bo, HttpServletResponse response) {
        List<TShopBannerVo> list = iTShopBannerService.queryList(bo);
        ExcelUtil.exportExcel(list, "广告banner", TShopBannerVo.class, response);
    }

    /**
     * 获取广告banner详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("shop:shopBanner:query")
    @GetMapping("/{id}")
    public R<TShopBannerVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iTShopBannerService.queryById(id));
    }

    /**
     * 新增广告banner
     */
    @SaCheckPermission("shop:shopBanner:add")
    @Log(title = "广告banner", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody TShopBannerBo bo) {
        return toAjax(iTShopBannerService.insertByBo(bo));
    }

    /**
     * 修改广告banner
     */
    @SaCheckPermission("shop:shopBanner:edit")
    @Log(title = "广告banner", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody TShopBannerBo bo) {
        return toAjax(iTShopBannerService.updateByBo(bo));
    }

    /**
     * 删除广告banner
     *
     * @param ids 主键串
     */
    @SaCheckPermission("shop:shopBanner:remove")
    @Log(title = "广告banner", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iTShopBannerService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
