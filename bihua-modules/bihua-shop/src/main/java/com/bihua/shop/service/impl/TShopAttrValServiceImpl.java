package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopAttrValBo;
import com.bihua.shop.domain.vo.TShopAttrValVo;
import com.bihua.shop.domain.TShopAttrVal;
import com.bihua.shop.mapper.TShopAttrValMapper;
import com.bihua.shop.service.ITShopAttrValService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品属性值Service业务层处理
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@RequiredArgsConstructor
@Service
public class TShopAttrValServiceImpl implements ITShopAttrValService {

    private final TShopAttrValMapper baseMapper;

    /**
     * 查询商品属性值
     */
    @Override
    public TShopAttrValVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询商品属性值列表
     */
    @Override
    public TableDataInfo<TShopAttrValVo> queryPageList(TShopAttrValBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopAttrVal> lqw = buildQueryWrapper(bo);
        Page<TShopAttrValVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询商品属性值列表
     */
    @Override
    public List<TShopAttrValVo> queryList(TShopAttrValBo bo) {
        LambdaQueryWrapper<TShopAttrVal> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    @Override
    public List<TShopAttrValVo> queryListByKeys(List<Long> keyIds) {
        LambdaQueryWrapper<TShopAttrVal> lqw = Wrappers.lambdaQuery();
        lqw.in(TShopAttrVal::getIdAttrKey, keyIds);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopAttrVal> buildQueryWrapper(TShopAttrValBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopAttrVal> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getAttrVal()), TShopAttrVal::getAttrVal, bo.getAttrVal());
        lqw.eq(bo.getIdAttrKey() != null, TShopAttrVal::getIdAttrKey, bo.getIdAttrKey());
        if(ObjectUtil.isNotEmpty(params.get("idAttrKey"))){
            lqw.in(TShopAttrVal::getIdAttrKey, (List)params.get("idAttrKey"));
        }
        return lqw;
    }

    /**
     * 新增商品属性值
     */
    @Override
    public Boolean insertByBo(TShopAttrValBo bo) {
        TShopAttrVal add = BeanUtil.toBean(bo, TShopAttrVal.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改商品属性值
     */
    @Override
    public Boolean updateByBo(TShopAttrValBo bo) {
        TShopAttrVal update = BeanUtil.toBean(bo, TShopAttrVal.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopAttrVal entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除商品属性值
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
