package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TShopOrderItemBo;
import com.bihua.shop.domain.vo.TShopOrderItemVo;
import com.bihua.shop.domain.TShopOrderItem;
import com.bihua.shop.mapper.TShopOrderItemMapper;
import com.bihua.shop.service.ITShopOrderItemService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 订单明细Service业务层处理
 *
 * @author bihua
 * @date 2023-09-17
 */
@RequiredArgsConstructor
@Service
public class TShopOrderItemServiceImpl implements ITShopOrderItemService {

    private final TShopOrderItemMapper baseMapper;

    /**
     * 查询订单明细
     */
    @Override
    public TShopOrderItemVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询订单明细列表
     */
    @Override
    public TableDataInfo<TShopOrderItemVo> queryPageList(TShopOrderItemBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TShopOrderItem> lqw = buildQueryWrapper(bo);
        Page<TShopOrderItemVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询订单明细列表
     */
    @Override
    public List<TShopOrderItemVo> queryList(TShopOrderItemBo bo) {
        LambdaQueryWrapper<TShopOrderItem> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TShopOrderItem> buildQueryWrapper(TShopOrderItemBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TShopOrderItem> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getIdOrder() != null, TShopOrderItem::getIdOrder, bo.getIdOrder());
        lqw.eq(bo.getIdGoods() != null, TShopOrderItem::getIdGoods, bo.getIdGoods());
        lqw.eq(bo.getIdSku() != null, TShopOrderItem::getIdSku, bo.getIdSku());
        lqw.eq(StringUtils.isNotBlank(bo.getCount()), TShopOrderItem::getCount, bo.getCount());
        lqw.eq(StringUtils.isNotBlank(bo.getPrice()), TShopOrderItem::getPrice, bo.getPrice());
        lqw.eq(StringUtils.isNotBlank(bo.getTotalPrice()), TShopOrderItem::getTotalPrice, bo.getTotalPrice());
        return lqw;
    }

    /**
     * 新增订单明细
     */
    @Override
    public Boolean insertByBo(TShopOrderItemBo bo) {
        TShopOrderItem add = BeanUtil.toBean(bo, TShopOrderItem.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改订单明细
     */
    @Override
    public Boolean updateByBo(TShopOrderItemBo bo) {
        TShopOrderItem update = BeanUtil.toBean(bo, TShopOrderItem.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TShopOrderItem entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除订单明细
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
