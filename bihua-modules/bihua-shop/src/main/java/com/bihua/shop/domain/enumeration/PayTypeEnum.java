package com.bihua.shop.domain.enumeration;

/**
 * @author wuzhong
 * @date 2023年09月17日 21:18
 */
public enum PayTypeEnum {
    UN_PAY("alipay", "支付宝"),
    UN_SEND("wxpay", "微信支付");


    private String value;
    private String key;

    PayTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }

    public static PayTypeEnum get(String key){
        for(PayTypeEnum payTypeEnum : values()){
            if(payTypeEnum.getKey().equals(key)){
                return payTypeEnum;
            }
        }
        return null;
    }
}
