package com.bihua.shop.domain.bo;

import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.util.Date;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品属性名业务对象 t_shop_attr_key
 *
 * @author name: bihua
 * @date 2023-09-06
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class TShopAttrKeyBo extends BaseEntity {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 属性名
     */
    @NotBlank(message = "属性名不能为空", groups = { AddGroup.class, EditGroup.class })
    private String attrName;

    /**
     * 商品类别id
     */
    @NotNull(message = "商品类别id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long idCategory;


}
