package com.bihua.shop.domain.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.common.annotation.ExcelDictFormat;
import com.bihua.common.convert.ExcelDictConvert;
import lombok.Data;
import java.util.Date;



/**
 * 广告banner视图对象 t_shop_banner
 *
 * @author name: bihua
 * @date 2023-09-06
 */
@Data
@ExcelIgnoreUnannotated
public class TShopBannerVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * banner图id
     */
    /**
     * banner图id
     */
    @ExcelProperty(value = "banner图id")
    private String ossId;
    /**
     * banner图URL
     */
    @ExcelProperty(value = "banner图URL")
    private String ossUrl;

    /**
     * 界面
     */
    @ExcelProperty(value = "界面")
    private String page;

    /**
     * 参数
     */
    @ExcelProperty(value = "参数")
    private String param;

    /**
     * 标题
     */
    @ExcelProperty(value = "标题")
    private String title;

    /**
     * 类型
     */
    @ExcelProperty(value = "类型")
    private String type;

    /**
     * 点击banner跳转到url
     */
    @ExcelProperty(value = "点击banner跳转到url")
    private String url;


}
