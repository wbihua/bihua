package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopCategoryBannerRel;
import com.bihua.shop.domain.vo.TShopCategoryBannerRelVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 类别banner关联Mapper接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface TShopCategoryBannerRelMapper extends BaseMapperPlus<TShopCategoryBannerRelMapper, TShopCategoryBannerRel, TShopCategoryBannerRelVo> {

}
