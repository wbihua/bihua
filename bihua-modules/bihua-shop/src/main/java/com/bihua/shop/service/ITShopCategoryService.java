package com.bihua.shop.service;

import com.bihua.shop.domain.TCategoryNode;
import com.bihua.shop.domain.TShopCategory;
import com.bihua.shop.domain.vo.TShopCategoryVo;
import com.bihua.shop.domain.bo.TShopCategoryBo;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;

import java.util.Collection;
import java.util.List;

/**
 * 商品类别Service接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface ITShopCategoryService {

    /**
     * 查询商品类别
     */
    TShopCategoryVo queryById(Long id);

    /**
     * 查询商品类别列表
     */
    TableDataInfo<TShopCategoryVo> queryPageList(TShopCategoryBo bo, PageQuery pageQuery);

    List<TCategoryNode> getCategories();

    /**
     * 查询商品类别列表
     */
    List<TShopCategoryVo> queryList(TShopCategoryBo bo);

    /**
     * 新增商品类别
     */
    Boolean insertByBo(TShopCategoryBo bo);

    /**
     * 修改商品类别
     */
    Boolean updateByBo(TShopCategoryBo bo);

    /**
     * 校验并批量删除商品类别信息
     */
    Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
