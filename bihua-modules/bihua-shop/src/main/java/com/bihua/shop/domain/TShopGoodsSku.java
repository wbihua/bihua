package com.bihua.shop.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

import com.bihua.common.core.domain.BaseEntity;

/**
 * 商品SKU对象 t_shop_goods_sku
 *
 * @author bihua
 * @date 2023-09-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("t_shop_goods_sku")
public class TShopGoodsSku extends BaseEntity {

    private static final long serialVersionUID=1L;

    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * sku编码,格式:逗号分割的属性值id
     */
    private String code;
    /**
     * sku名称,格式:逗号分割的属性值
     */
    private String codeName;
    /**
     * 商品id
     */
    private Long idGoods;
    /**
     * 是否删除1:是,0:否
     */
    private Boolean isDeleted;
    /**
     * 市场价,原价
     */
    private String marketingPrice;
    /**
     * 价格
     */
    private String price;
    /**
     * 库存
     */
    private Long stock;

}
