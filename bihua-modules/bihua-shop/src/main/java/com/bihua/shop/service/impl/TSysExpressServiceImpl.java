package com.bihua.shop.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.domain.PageQuery;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.bihua.shop.domain.bo.TSysExpressBo;
import com.bihua.shop.domain.vo.TSysExpressVo;
import com.bihua.shop.domain.TSysExpress;
import com.bihua.shop.mapper.TSysExpressMapper;
import com.bihua.shop.service.ITSysExpressService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 物流公司Service业务层处理
 *
 * @author bihua
 * @date 2023-09-18
 */
@RequiredArgsConstructor
@Service
public class TSysExpressServiceImpl implements ITSysExpressService {

    private final TSysExpressMapper baseMapper;

    /**
     * 查询物流公司
     */
    @Override
    public TSysExpressVo queryById(Long id){
        return baseMapper.selectVoById(id);
    }

    /**
     * 查询物流公司列表
     */
    @Override
    public TableDataInfo<TSysExpressVo> queryPageList(TSysExpressBo bo, PageQuery pageQuery) {
        LambdaQueryWrapper<TSysExpress> lqw = buildQueryWrapper(bo);
        Page<TSysExpressVo> result = baseMapper.selectVoPage(pageQuery.build(), lqw);
        return TableDataInfo.build(result);
    }

    /**
     * 查询物流公司列表
     */
    @Override
    public List<TSysExpressVo> queryList(TSysExpressBo bo) {
        LambdaQueryWrapper<TSysExpress> lqw = buildQueryWrapper(bo);
        return baseMapper.selectVoList(lqw);
    }

    private LambdaQueryWrapper<TSysExpress> buildQueryWrapper(TSysExpressBo bo) {
        Map<String, Object> params = bo.getParams();
        LambdaQueryWrapper<TSysExpress> lqw = Wrappers.lambdaQuery();
        lqw.eq(StringUtils.isNotBlank(bo.getCode()), TSysExpress::getCode, bo.getCode());
        lqw.like(StringUtils.isNotBlank(bo.getName()), TSysExpress::getName, bo.getName());
        lqw.eq(bo.getSort() != null, TSysExpress::getSort, bo.getSort());
        lqw.eq(bo.getDisabled() != null, TSysExpress::getDisabled, bo.getDisabled());
        return lqw;
    }

    /**
     * 新增物流公司
     */
    @Override
    public Boolean insertByBo(TSysExpressBo bo) {
        TSysExpress add = BeanUtil.toBean(bo, TSysExpress.class);
        validEntityBeforeSave(add);
        boolean flag = baseMapper.insert(add) > 0;
        if (flag) {
            bo.setId(add.getId());
        }
        return flag;
    }

    /**
     * 修改物流公司
     */
    @Override
    public Boolean updateByBo(TSysExpressBo bo) {
        TSysExpress update = BeanUtil.toBean(bo, TSysExpress.class);
        validEntityBeforeSave(update);
        return baseMapper.updateById(update) > 0;
    }

    /**
     * 保存前的数据校验
     */
    private void validEntityBeforeSave(TSysExpress entity){
        //TODO 做一些数据校验,如唯一约束
    }

    /**
     * 批量删除物流公司
     */
    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return baseMapper.deleteBatchIds(ids) > 0;
    }
}
