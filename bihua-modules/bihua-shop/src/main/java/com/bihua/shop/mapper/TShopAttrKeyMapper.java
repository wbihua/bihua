package com.bihua.shop.mapper;

import com.bihua.shop.domain.TShopAttrKey;
import com.bihua.shop.domain.vo.TShopAttrKeyVo;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 商品属性名Mapper接口
 *
 * @author name: bihua
 * @date 2023-09-06
 */
public interface TShopAttrKeyMapper extends BaseMapperPlus<TShopAttrKeyMapper, TShopAttrKey, TShopAttrKeyVo> {

}
