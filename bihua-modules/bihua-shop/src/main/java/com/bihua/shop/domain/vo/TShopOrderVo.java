package com.bihua.shop.domain.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.bihua.shop.domain.enumeration.OrderStatusEnum;
import com.bihua.shop.domain.enumeration.PayStatusEnum;
import com.bihua.shop.domain.enumeration.PayTypeEnum;
import lombok.Data;



/**
 * 订单管理视图对象 t_shop_order
 *
 * @author bihua
 * @date 2023-09-17
 */
@Data
@ExcelIgnoreUnannotated
public class TShopOrderVo {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    @ExcelProperty(value = "")
    private Long id;

    /**
     * 用户id
     */
    @ExcelProperty(value = "用户id")
    private Long idUser;

    /**
     * 订单号
     */
    @ExcelProperty(value = "订单号")
    private String orderSn;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态")
    private Integer status;
    private String statusName;

    /**
     * 优惠券抵扣金额
     */
    @ExcelProperty(value = "优惠券抵扣金额")
    private BigDecimal couponPrice;

    /**
     * 交易金额
     */
    @ExcelProperty(value = "交易金额")
    private BigDecimal tradeAmount;

    /**
     * 实付金额
     */
    @ExcelProperty(value = "实付金额")
    private BigDecimal realPrice;

    /**
     * 总金额
     */
    @ExcelProperty(value = "总金额")
    private BigDecimal totalPrice;

    /**
     * 支付状态1:未付款;2:已付款,3:支付中
     */
    @ExcelProperty(value = "支付状态1:未付款;2:已付款,3:支付中")
    private Integer payStatus;
    private String payStatusName;
    /**
     * 支付流水号
     */
    @ExcelProperty(value = "支付流水号")
    private String payId;

    /**
     * 实付类型:alipay,wechat
     */
    @ExcelProperty(value = "实付类型:alipay,wechat")
    private String payType;
    private String payTypeName;

    /**
     * 支付成功时间
     */
    @ExcelProperty(value = "支付成功时间")
    private String payTime;

    /**
     * 收件人
     */
    @ExcelProperty(value = "收件人")
    private String consignee;

    /**
     * 收件人电话
     */
    @ExcelProperty(value = "收件人电话")
    private String mobile;

    /**
     * 收件地址
     */
    @ExcelProperty(value = "收件地址")
    private String consigneeAddress;

    /**
     * 收货信息
     */
    @ExcelProperty(value = "收货信息")
    private Long idAddress;

    /**
     * 快递公司
     */
    @ExcelProperty(value = "快递公司")
    private Long idExpress;

    /**
     * 快递单号
     */
    @ExcelProperty(value = "快递单号")
    private String shippingSn;

    /**
     * 配送费用
     */
    @ExcelProperty(value = "配送费用")
    private BigDecimal shippingAmount;

    /**
     * 出库时间
     */
    @ExcelProperty(value = "出库时间")
    private Date shippingTime;

    /**
     * 确认收货时间
     */
    @ExcelProperty(value = "确认收货时间")
    private Date confirmReceivingTime;

    /**
     * 订单备注
     */
    @ExcelProperty(value = "订单备注")
    private String message;

    /**
     * 管理员备注
     */
    @ExcelProperty(value = "管理员备注")
    private String adminMessage;

    /**
     * 创建时间
     */
    @ExcelProperty(value = "管理员备注")
    private Date createTime;

    private List<TShopOrderItemVo> items;

    public String getStatusName(){
        if(status!=null) {
            return OrderStatusEnum.get(status).getValue();
        }
        return null;
    }

    public String getPayStatusName(){
        if(payStatus!=null) {
            return PayStatusEnum.getPayStatus(payStatus).getValue();
        }
        return null;
    }

    public String getPayTypeName(){
        if(payType!=null) {
            return PayTypeEnum.get(payType).getValue();
        }
        return null;
    }
}
