package com.bihua.system.mapper;

import com.bihua.common.core.domain.entity.SysDictType;
import com.bihua.common.core.mapper.BaseMapperPlus;

/**
 * 字典表 数据层
 *
 * @author Lion Li
 */
public interface SysDictTypeMapper extends BaseMapperPlus<SysDictTypeMapper, SysDictType, SysDictType> {

}
