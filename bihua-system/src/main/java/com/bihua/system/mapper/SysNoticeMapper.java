package com.bihua.system.mapper;

import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.system.domain.SysNotice;

/**
 * 通知公告表 数据层
 *
 * @author Lion Li
 */
public interface SysNoticeMapper extends BaseMapperPlus<SysNoticeMapper, SysNotice, SysNotice> {

}
