package com.bihua.system.mapper;

import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.system.domain.SysRoleMenu;

/**
 * 角色与菜单关联表 数据层
 *
 * @author Lion Li
 */
public interface SysRoleMenuMapper extends BaseMapperPlus<SysRoleMenuMapper, SysRoleMenu, SysRoleMenu> {

}
