package com.bihua.system.mapper;

import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.system.domain.SysOss;
import com.bihua.system.domain.vo.SysOssVo;

/**
 * 文件上传 数据层
 *
 * @author Lion Li
 */
public interface SysOssMapper extends BaseMapperPlus<SysOssMapper, SysOss, SysOssVo> {
}
