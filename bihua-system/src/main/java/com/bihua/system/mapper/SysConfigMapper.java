package com.bihua.system.mapper;

import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.system.domain.SysConfig;

/**
 * 参数配置 数据层
 *
 * @author Lion Li
 */
public interface SysConfigMapper extends BaseMapperPlus<SysConfigMapper, SysConfig, SysConfig> {

}
