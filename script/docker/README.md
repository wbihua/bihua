### 碧华后台管理系统前端【bihua-ui】安装
1. 安装Web工程，生成node_modules文件<br/>
npm install
2. 打包Web工程，生成dist文件。<br/>
npm run build:prod
3. nginx部署Web工程<br/>
复制dist文件夹到nginx的html目录下（bihua\script\docker\nginx\html）
4. docker部署nginx应用<br/>
docker-compose up -d nginx-web

### 碧华后台管理系统后端【bihua-admin】安装
1. 执行打包命令<br/>
mvn clean package -P docker -Dmaven.test.skip=true
2. 执行jar文件已经生成到bihua\script\docker下相关目录
3. bihua\script\docker文件夹同步到部署服务器<br/>
scp -r 省略\bihua\script\docker 用户名@部署服务器IP:部署目录<br/>
Redis数据目录请执行 `chmod 777 [部署脚本目录]/docker/redis/data` 赋予读写权限 否则将无法写入数据
4. 在docker文件夹包含docker-compose.yml，执行部署命令：<br/>
docker-compose up -d bihua-mysql bihua-redis bihua-monitor-admin bihua-xxl-job-admin bihua-rule-engine nginx-web bihua-server1 bihua-server2 bihua-blog-api bihua-blog-uniapp bihua-iot-api
5. 查看容器的状态和日志信息 <br/>
docker-compose ps -a
docker logs -f -t --tail 1000 容器名称

### 相关系统访问地址
1. 碧华后台管理系统前端访问地址  <br/>
https://192.168.2.88:14443/index  <br/>
http://192.168.2.88:14080/index  <br/>
用户名和密码：admin/admin123
2. 碧华服务监控中心访问地址  <br/>
http://192.168.2.88:9090/admin  <br/>
bihua/123456
3. 碧华任务调度中心  <br/>
http://192.168.2.88:9100/xxl-job-admin/  <br/>
admin/123456
4. 碧华文件存储中心 <br/>
http://192.168.2.88:9000/minio/login <br/>
admin/12345678

##生产环境Docker部署相关命令
```
mvn clean package -P docker -Dmaven.test.skip=true
npm run build:prod
chmod 777 /data/bihua-work/docker/redis/data

docker-compose up -d bihua-mysql bihua-redis bihua-monitor-admin bihua-xxl-job-admin bihua-rule-engine nginx-web bihua-server1 bihua-server2 bihua-blog-api bihua-blog-uniapp bihua-iot-api

docker-compose stop bihua-mysql bihua-redis bihua-monitor-admin bihua-xxl-job-admin bihua-rule-engine nginx-web bihua-server1 bihua-server2 bihua-blog-api bihua-blog-uniapp bihua-iot-api
docker-compose rm bihua-mysql bihua-redis bihua-monitor-admin bihua-xxl-job-admin bihua-rule-engine nginx-web bihua-server1 bihua-server2 bihua-blog-api bihua-blog-uniapp bihua-iot-api
docker rmi bihua-mysql bihua-redis bihua-monitor-admin bihua-xxl-job-admin bihua-rule-engine nginx-web bihua-server1 bihua-server2 bihua-blog-api bihua-blog-uniapp bihua-iot-api
docker-compose ps -a
docker-compose  start bihua-redis

docker-compose up -d bihua-rule-engine
docker-compose rmi bihua-rule-engine
docker logs -f -t --tail 1000 18b6a5474288
```
### 相关问题
问题：<br/>
2023/04/28 20:05:01 [emerg] 1#1: cannot load certificate key "/etc/nginx/cert/server.key": PEM_read_bio_PrivateKey() failed (SSL: error:2807106B:UI routines:UI_process:processing error:while r
eading strings error:0906406D:PEM routines:PEM_def_callback:problems getting password error:0906A068:PEM routines:PEM_do_header:bad password read)<br/>
解决方法：<br/>
openssl rsa -in server.key -out server-unsecure.key


