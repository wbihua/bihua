package com.bihua.web.controller.blog;

import cn.dev33.satoken.annotation.SaIgnore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsMessageBo;
import com.bihua.blog.domain.vo.CmsMessageVo;
import com.bihua.blog.service.ICmsMessageService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.enums.BusinessType;

import lombok.RequiredArgsConstructor;

/**
 * 留言
 *
 * @author bihua
 * @date 2023-06-07
 */
@SaIgnore
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/message")
public class CmsMessageController extends BaseController {

    private final ICmsMessageService iCmsMessageService;

    /**
     * 查询留言列表
     */
    @GetMapping("/list")
    public TableDataInfo<CmsMessageVo> list(CmsMessageBo bo, PageQuery pageQuery) {
        return iCmsMessageService.queryPageList(bo, pageQuery);
    }
    /**
     * 新增留言
     */
    @Log(title = "留言", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsMessageBo bo) {
        bo.setUserId(getUserId());
        return toAjax(iCmsMessageService.insertByBo(bo));
    }
}
