package com.bihua.web.controller.blog;

import cn.dev33.satoken.annotation.SaIgnore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsBlogBo;
import com.bihua.blog.domain.vo.CmsBlogAllTagAndAllTypeVo;
import com.bihua.blog.domain.vo.CmsBlogVo;
import com.bihua.blog.service.ICmsBlogService;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;

import cn.hutool.core.bean.BeanUtil;
import lombok.RequiredArgsConstructor;

/**
 * 博客信息
 *
 * @author bihua
 * @date 2023-05-30
 */
@SaIgnore
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blog")
public class CmsBlogController extends BaseController {

    private final ICmsBlogService iCmsBlogService;
    /**
     * 首页查询文章列表
     */
    @GetMapping("/list")
    public TableDataInfo<CmsBlogVo> list(CmsBlogBo bo, PageQuery pageQuery) {
        return iCmsBlogService.queryPageList(bo, pageQuery);
    }
    /**
     * 首页获取文章详细信息
     */
    @GetMapping("/{id}")
    public R<CmsBlogVo> getInfoDetail(@PathVariable(value = "id", required = false) Long id)
    {
        return R.ok(iCmsBlogService.queryById(id));
    }
    /**
     * 首页按分类查询文章列表
     */
    @GetMapping("/cmsListByType")
    public TableDataInfo cmsListByTypeId(Long typeId, PageQuery pageQuery)
    {
        return iCmsBlogService.selectCmsBlogListByTypeId(typeId, pageQuery);
    }
    @GetMapping("/getAllTagAndAllType")
    public R<CmsBlogAllTagAndAllTypeVo> getAllTagAndAllType() {
        return R.ok(iCmsBlogService.getAllTagAndAllType());
    }
    /**
     * 首页增加阅读量
     */
    @GetMapping("/addBlogViews/{id}")
    public R<Long> addBlogViews(@PathVariable(value = "id", required = false) Long id)
    {
        CmsBlogVo vo = iCmsBlogService.queryById(id);
        Long views = vo.getViews();
        views += Long.parseLong("1");
        vo.setViews(views);
        CmsBlogBo bo = BeanUtil.toBean(vo, CmsBlogBo.class);
        iCmsBlogService.updateByBo(bo);
        return R.ok(id);
    }
}
