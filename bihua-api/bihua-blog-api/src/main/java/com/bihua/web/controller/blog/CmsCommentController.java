package com.bihua.web.controller.blog;

import cn.dev33.satoken.annotation.SaIgnore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsCommentBo;
import com.bihua.blog.domain.vo.CmsCommentVo;
import com.bihua.blog.service.ICmsCommentService;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.page.TableDataInfo;

import lombok.RequiredArgsConstructor;

/**
 * 评论
 *
 * @author bihua
 * @date 2023-06-05
 */
@SaIgnore
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/comment")
public class CmsCommentController extends BaseController {

    private final ICmsCommentService iCmsCommentService;

    /**
     * 查询评论列表
     */
    @GetMapping("/list")
    public TableDataInfo<CmsCommentVo> list(CmsCommentBo bo, PageQuery pageQuery) {
        return iCmsCommentService.queryPageList(bo, pageQuery);
    }
}
