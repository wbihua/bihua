package com.bihua.smqttx;

import io.github.quickmsg.common.context.ContextHolder;
import io.github.quickmsg.common.context.ReceiveContext;
import io.github.quickmsg.common.message.Message;
import io.github.quickmsg.common.protocol.Protocol;
import io.github.quickmsg.common.protocol.ProtocolAdaptor;
import io.github.quickmsg.common.spi.loader.DynamicLoader;
import io.github.quickmsg.common.utils.RetryFailureHandler;
import io.github.quickmsg.core.mqtt.AbstractReceiveContext;
import io.github.quickmsg.core.protocol.ConnectProtocol;
import io.github.quickmsg.dsl.RuleDslExecutor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;

import java.util.function.Predicate;

/**
 * @author wuzhong
 * @date 2023年07月20日 22:57
 */
@Slf4j
public class BihuaProtocolAdaptor implements ProtocolAdaptor {

    private final Sinks.Many<Message> acceptor;

    @SuppressWarnings("unchecked")
    public BihuaProtocolAdaptor() {
        Integer businessQueueSize = 100000;
        Integer threadSize = 32;
        this.acceptor = Sinks.many().multicast().onBackpressureBuffer(businessQueueSize);
        DynamicLoader.findAll(Protocol.class).filter(new Predicate<Protocol>() {
            @Override
            public boolean test(Protocol protocol) {
                log.warn("BihuaProtocolAdaptor protocol:{},{}", protocol.getClass().toString(), ConnectProtocol.class.isInstance(protocol));
                return !(ConnectProtocol.class.isInstance(protocol));
            }
        }).forEach(protocol ->
                acceptor.asFlux()
                        .doOnError(throwable -> log.error("BihuaProtocolAdaptor consumer", throwable))
                        .onErrorResume(throwable -> Mono.empty())
                        .ofType(protocol.getClassType())
                        .subscribeOn(Schedulers.newParallel("message-acceptor", threadSize))
                        .subscribe(msg -> {
                            Message message = (Message) msg;
                            Protocol<Message> messageProtocol = (Protocol<Message>) protocol;
                            ReceiveContext<?> receiveContext = ContextHolder.getReceiveContext();
                            messageProtocol.doParseProtocol(message, message.getMqttChannel())
                                    .contextWrite(context -> context.putNonNull(ReceiveContext.class, ContextHolder.getReceiveContext()))
                                    .onErrorContinue((throwable, obj) -> {
                                        log.error("BihuaProtocolAdaptor", throwable);
                                    })
                                    .subscribe();
                            log.warn("BihuaProtocolAdaptor message:{}", message);
                            RuleDslExecutor executor = ((AbstractReceiveContext<?>) receiveContext).getRuleDslExecutor();
                            executor.executeRule( message);
                        }));
    }

    @Override
    public void chooseProtocol(Message message) {
        try {
            acceptor.emitNext(message, RetryFailureHandler.RETRY_NON_SERIALIZED);
        } catch (Exception e) {
            log.error("protocol emitNext error", e);
        }
    }
}
