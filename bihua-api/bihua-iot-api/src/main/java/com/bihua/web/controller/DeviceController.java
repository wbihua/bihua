package com.bihua.web.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.R;
import com.bihua.iot.service.IDeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author bihua
 * @date 2023年07月10日 21:35
 */
@SaIgnore
@Slf4j
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/device")
public class DeviceController  extends BaseController {

    private final IDeviceService iDeviceService;

    /**
     * 客户端身份认证
     *
     * @param params
     * @return
     */
    @PostMapping("/clientAuthentication")
    public R<Void> clientAuthentication(@RequestBody Map<String, Object> params) {
        final Object clientIdentifier = params.get("clientIdentifier");
        final Object username = params.get("username");
        final Object password = params.get("password");
        final Object deviceStatus = params.get("deviceStatus");
        final Object protocolType = params.get("protocolType");
        Boolean certificationStatus = iDeviceService.clientAuthentication(clientIdentifier.toString(), username.toString(), password.toString(), deviceStatus.toString(), protocolType.toString());
        log.info("{} 协议设备正在进行身份认证,客户端ID:{},用户名:{},密码:{},认证结果:{}", protocolType, clientIdentifier, username, password, certificationStatus ? "成功" : "失败");
        return certificationStatus ? R.ok("认证成功") : R.fail(403, "认证失败");
    }
}
