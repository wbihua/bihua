package com.bihua.rockermq.consumer;

import java.util.Objects;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.bihua.framework.rocketmq.constant.ConsumerGroupConstant;
import com.bihua.framework.rocketmq.constant.ConsumerTopicConstant;
import com.bihua.iot.enums.MqttEvent;
import com.bihua.iot.service.IDeviceActionService;
import com.bihua.iot.service.IDeviceDatasService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description: Mqtt动作消息消费（Rocketmq模式）
 * @Author: ShiHuan Sun
 * @E-mail: 13733918655@163.com
 * @Website: http://thinglinks.mqttsnet.com
 * @CreateDate: 2021/11/22$ 16:11$
 * @UpdateUser: ShiHuan Sun
 * @UpdateDate: 2021/11/22$ 16:11$
 * @UpdateRemark: 修改内容
 * @Version: 1.0
 */
@RequiredArgsConstructor
@Slf4j
@Component
@RocketMQMessageListener(consumerGroup = "bihua", topic = "bihua-iot-mqttMsg", messageModel = MessageModel.CLUSTERING)
public class DeviceActionMessageConsumer implements RocketMQListener {

    private final IDeviceActionService iDeviceActionService;
    private final IDeviceDatasService iDeviceDatasService;

    @Async("linkAsync")
    @Override
    public void onMessage(Object message) {
        if (null == message){
            log.warn("message cannot be empty {}",message);
            return;
        }
        log.info("ThingLinks物联网平台数据消费-->Received message={}", message);
        JSONObject thinglinksMessage = JSON.parseObject(String.valueOf(message));
        try {
            MqttEvent event = MqttEvent.getMqttEventEnum(thinglinksMessage.get("event").toString());
            switch (Objects.requireNonNull(event)) {
                case CONNECT:
                    iDeviceActionService.insertEvent(thinglinksMessage);
                    iDeviceActionService.connectEvent(thinglinksMessage);
                    break;
                case CLOSE:
                case DISCONNECT:
                    iDeviceActionService.insertEvent(thinglinksMessage);
                    iDeviceActionService.closeEvent(thinglinksMessage);
                    break;
                case PUBLISH:
                    iDeviceDatasService.insertBaseDatas(thinglinksMessage);
                    break;
                case SUBSCRIBE:
                    iDeviceActionService.insertEvent(thinglinksMessage);
                    break;
                case UNSUBSCRIBE:
                    iDeviceActionService.insertEvent(thinglinksMessage);
                    break;
                case PING:
                    iDeviceActionService.refreshDeviceCache(thinglinksMessage);
                    break;
                default:
            }
        } catch (Exception e) {
            log.error("ThingLinks物联网平台数据消费-->消费失败，失败原因：{}", e.getMessage());
        }
    }
}
