package com.bihua.common;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotBlank;

import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.constant.CacheConstants;
import com.bihua.common.constant.Constants;
import com.bihua.common.core.domain.R;
import com.bihua.common.enums.CaptchaType;
import com.bihua.common.utils.StringUtils;
import com.bihua.common.utils.redis.RedisUtils;
import com.bihua.common.utils.reflect.ReflectUtils;
import com.bihua.common.utils.spring.SpringUtils;
import com.bihua.framework.config.properties.CaptchaProperties;
import com.bihua.sms.config.properties.SmsProperties;
import com.bihua.sms.core.SmsTemplate;
import com.bihua.sms.entity.SmsResult;
import com.bihua.system.service.ISysConfigService;

import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.captcha.AbstractCaptcha;
import cn.hutool.captcha.generator.CodeGenerator;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 验证码操作处理
 *
 * @author Lion Li
 */
@SaIgnore
@Slf4j
@Validated
@RequiredArgsConstructor
@RestController
public class CaptchaController {

    private final CaptchaProperties captchaProperties;
    private final SmsProperties smsProperties;
    private final ISysConfigService configService;

    /**
     * 短信验证码
     *
     * @param phoneNumber 用户手机号
     */
    @GetMapping("/captchaSms")
    public R<String> smsCaptcha(@NotBlank(message = "{user.phoneNumber.not.blank}")
                              String phoneNumber) {
        String key = CacheConstants.CAPTCHA_CODE_KEY + phoneNumber;
        String oldSmsCode = RedisUtils.getCacheObject(key);
        if(StringUtils.isNotEmpty(oldSmsCode)){
            log.info("{}:{}分钟内已经发送过短信验证码，不再重复发送",oldSmsCode, Constants.CAPTCHA_EXPIRATION);
        }
        String code;
        if(StringUtils.isNotEmpty(oldSmsCode)){
            code = oldSmsCode;
        }else{
            code = RandomUtil.randomNumbers(4);
            RedisUtils.setCacheObject(key, code, Duration.ofMinutes(Constants.CAPTCHA_EXPIRATION));
        }
        R<String> ret = R.ok();
        if("17712858268".equals(phoneNumber)){
            ret.setData(code);
            return ret;
        }
        if (!("prod".equals(configService.selectConfigByKey("sys.env")))){
            ret.setData(code);
            return ret;
        }
        if (!smsProperties.getEnabled()) {
            return R.fail("当前系统没有开启短信功能！");
        }
        // 验证码模板id 自行处理 (查数据库或写死均可)
        String templateId = "";
        Map<String, String> map = new HashMap<>(1);
        map.put("code", code);
        SmsTemplate smsTemplate = SpringUtils.getBean(SmsTemplate.class);
        SmsResult result = smsTemplate.send(phoneNumber, templateId, map);
        if (!result.isSuccess()) {
            log.error("验证码短信发送异常 => {}", result);
            return R.fail(result.getMessage());
        }
        ret.setData(Boolean.TRUE.toString());
        return ret;
    }

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public R<Map<String, Object>> getCode() {
        Map<String, Object> ajax = new HashMap<>();
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        ajax.put("captchaEnabled", captchaEnabled);
        if (!captchaEnabled) {
            return R.ok(ajax);
        }
        // 保存验证码信息
        String uuid = IdUtil.simpleUUID();
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;
        // 生成验证码
        CaptchaType captchaType = captchaProperties.getType();
        boolean isMath = CaptchaType.MATH == captchaType;
        Integer length = isMath ? captchaProperties.getNumberLength() : captchaProperties.getCharLength();
        CodeGenerator codeGenerator = ReflectUtils.newInstance(captchaType.getClazz(), length);
        AbstractCaptcha captcha = SpringUtils.getBean(captchaProperties.getCategory().getClazz());
        captcha.setGenerator(codeGenerator);
        captcha.createCode();
        String code = captcha.getCode();
        if (isMath) {
            ExpressionParser parser = new SpelExpressionParser();
            Expression exp = parser.parseExpression(StringUtils.remove(code, "="));
            code = exp.getValue(String.class);
        }
        RedisUtils.setCacheObject(verifyKey, code, Duration.ofMinutes(Constants.CAPTCHA_EXPIRATION));
        ajax.put("uuid", uuid);
        ajax.put("img", captcha.getImageBase64());
        return R.ok(ajax);
    }

}
