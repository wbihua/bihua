package com.bihua.web.controller.shop;

import javax.servlet.http.HttpServletRequest;

import cn.dev33.satoken.annotation.SaIgnore;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.R;
import com.bihua.shop.service.ShopLoginService;
import com.bihua.system.service.ISysConfigService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 */
@Validated
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/wechat")
public class WeChatController extends BaseController {
    private final ShopLoginService loginService;
    private final ISysConfigService configService;

    @PostMapping("/bindOpenid")
    public R<Void> bind(String code, HttpServletRequest request) {
        return  toAjax(loginService.bind(code));
    }
    @SaIgnore
    @GetMapping("/getWxAppId")
    public R<String> getWxAppId() {
        String appId = configService.selectConfigByKey("weixin.app.id");
        return R.ok("", appId);
    }
}
