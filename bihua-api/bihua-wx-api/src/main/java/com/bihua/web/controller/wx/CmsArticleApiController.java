package com.bihua.web.controller.wx;

import javax.validation.constraints.NotNull;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.core.domain.R;
import com.bihua.wx.domain.vo.CmsArticleVo;
import com.bihua.wx.service.ICmsArticleService;

import cn.dev33.satoken.annotation.SaIgnore;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author bihua
 * @date 2023年05月27日 21:37
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/article")
@Slf4j
public class CmsArticleApiController {

    private final ICmsArticleService iCmsArticleService;
    /**
     * 获取CMS文章中心详细信息
     * @param id 主键
     */
    @SaIgnore
    @GetMapping("/{id}")
    public R<CmsArticleVo> getInfo(@NotNull(message = "主键不能为空")
                                   @PathVariable Long id) {
        return R.ok(iCmsArticleService.queryById(id));
    }
}
