import request from '@/utils/request'

// 查询子设备档案列表
export function listDeviceInfo(query) {
  return request({
    url: '/iot/info/list',
    method: 'get',
    params: query
  })
}

// 查询子设备档案详细
export function getDeviceInfo(id) {
  return request({
    url: '/iot/info/' + id,
    method: 'get'
  })
}

// 新增子设备档案
export function addDeviceInfo(data) {
  return request({
    url: '/iot/info',
    method: 'post',
    data: data
  })
}

// 修改子设备档案
export function updateDeviceInfo(data) {
  return request({
    url: '/iot/info',
    method: 'put',
    data: data
  })
}

// 删除子设备档案
export function delDeviceInfo(id) {
  return request({
    url: '/iot/info/' + id,
    method: 'delete'
  })
}
// 初始化子设备影子数据
export function refreshDeviceInfoDataModel(id) {
  return request({
    url: '/iot/info/refreshDeviceInfoDataModel/' + id,
    method: 'get'
  })
}
// 查询子设备影子数据
export function getDeviceInfoShadow(data) {
  return request({
    url: '/iot/shadow/getDeviceInfoShadow',
    method: 'post',
    data: data
  })
}
