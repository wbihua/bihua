import request from '@/utils/request'

// 查询厂商管理列表
export function listManufacturer(query) {
  return request({
    url: '/iot/manufacturer/list',
    method: 'get',
    params: query
  })
}

// 查询厂商管理详细
export function getManufacturer(mfrId) {
  return request({
    url: '/iot/manufacturer/' + mfrId,
    method: 'get'
  })
}

// 新增厂商管理
export function addManufacturer(data) {
  return request({
    url: '/iot/manufacturer',
    method: 'post',
    data: data
  })
}

// 修改厂商管理
export function updateManufacturer(data) {
  return request({
    url: '/iot/manufacturer',
    method: 'put',
    data: data
  })
}

// 删除厂商管理
export function delManufacturer(mfrId) {
  return request({
    url: '/iot/manufacturer/' + mfrId,
    method: 'delete'
  })
}
