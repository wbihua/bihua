import request from '@/utils/request'

// 查询规则条件列表
export function listConditions(query) {
  return request({
    url: '/iot/conditions/list',
    method: 'get',
    params: query
  })
}

// 查询规则条件详细
export function getConditions(id) {
  return request({
    url: '/iot/conditions/' + id,
    method: 'get'
  })
}

// 新增规则条件
export function addConditions(data) {
  return request({
    url: '/iot/conditions',
    method: 'post',
    data: data
  })
}

// 修改规则条件
export function updateConditions(data) {
  return request({
    url: '/iot/conditions',
    method: 'put',
    data: data
  })
}

// 删除规则条件
export function delConditions(id) {
  return request({
    url: '/iot/conditions/' + id,
    method: 'delete'
  })
}
