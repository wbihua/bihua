import request from '@/utils/request'

// 查询产品服务列表
export function listServices(query) {
  return request({
    url: '/iot/services/list',
    method: 'get',
    params: query
  })
}

// 查询产品服务列表
export function queryProductServices(deviceIdentification) {
  return request({
    url: '/iot/services/queryProductServices/'+deviceIdentification,
    method: 'get'
  })
}

// 查询产品服务详细
export function getServices(id) {
  return request({
    url: '/iot/services/' + id,
    method: 'get'
  })
}

// 新增产品服务
export function addServices(data) {
  return request({
    url: '/iot/services',
    method: 'post',
    data: data
  })
}

// 修改产品服务
export function updateServices(data) {
  return request({
    url: '/iot/services',
    method: 'put',
    data: data
  })
}

// 删除产品服务
export function delServices(id) {
  return request({
    url: '/iot/services/' + id,
    method: 'delete'
  })
}
