import request from '@/utils/request'

// 查询产品设备响应服务命令属性列表
export function listResponse(query) {
  return request({
    url: '/iot/commandsResponse/list',
    method: 'get',
    params: query
  })
}

// 查询产品设备响应服务命令属性详细
export function getResponse(id) {
  return request({
    url: '/iot/commandsResponse/' + id,
    method: 'get'
  })
}

// 新增产品设备响应服务命令属性
export function addResponse(data) {
  return request({
    url: '/iot/commandsResponse',
    method: 'post',
    data: data
  })
}

// 修改产品设备响应服务命令属性
export function updateResponse(data) {
  return request({
    url: '/iot/commandsResponse',
    method: 'put',
    data: data
  })
}

// 删除产品设备响应服务命令属性
export function delResponse(id) {
  return request({
    url: '/iot/commandsResponse/' + id,
    method: 'delete'
  })
}
