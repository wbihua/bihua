import request from '@/utils/request'

// 查询产品设备下发服务命令属性列表
export function listRequests(query) {
  return request({
    url: '/iot/commandsRequests/list',
    method: 'get',
    params: query
  })
}

// 查询产品设备下发服务命令属性详细
export function getRequests(id) {
  return request({
    url: '/iot/commandsRequests/' + id,
    method: 'get'
  })
}

// 新增产品设备下发服务命令属性
export function addRequests(data) {
  return request({
    url: '/iot/commandsRequests',
    method: 'post',
    data: data
  })
}

// 修改产品设备下发服务命令属性
export function updateRequests(data) {
  return request({
    url: '/iot/commandsRequests',
    method: 'put',
    data: data
  })
}

// 删除产品设备下发服务命令属性
export function delRequests(id) {
  return request({
    url: '/iot/commandsRequests/' + id,
    method: 'delete'
  })
}
