import request from '@/utils/request'

// 查询设备位置列表
export function listLocation(query) {
  return request({
    url: '/iot/location/list',
    method: 'get',
    params: query
  })
}

// 查询设备位置详细
export function getLocation(id) {
  return request({
    url: '/iot/location/' + id,
    method: 'get'
  })
}

// 新增设备位置
export function addLocation(data) {
  return request({
    url: '/iot/location',
    method: 'post',
    data: data
  })
}

// 修改设备位置
export function updateLocation(data) {
  return request({
    url: '/iot/location',
    method: 'put',
    data: data
  })
}

// 删除设备位置
export function delLocation(id) {
  return request({
    url: '/iot/location/' + id,
    method: 'delete'
  })
}
