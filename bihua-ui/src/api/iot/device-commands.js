import request from '@/utils/request'

// 查询设备动作列表
export function listCommands(query) {
  return request({
    url: '/iot/devicecommands/list',
    method: 'get',
    params: query
  })
}

// 查询设备动作详细
export function getCommands(id) {
  return request({
    url: '/iot/devicecommands/' + id,
    method: 'get'
  })
}

// 新增设备动作
export function addCommands(data) {
  return request({
    url: '/iot/devicecommands',
    method: 'post',
    data: data
  })
}

// 修改设备动作
export function updateCommands(data) {
  return request({
    url: '/iot/devicecommands',
    method: 'put',
    data: data
  })
}

// 删除设备动作
export function delCommands(id) {
  return request({
    url: '/iot/devicecommands/' + id,
    method: 'delete'
  })
}

// 下发设备动作数据
export function distributeCommand(data) {
  return request({
    url: '/iot/devicecommands/distributeCommand',
    method: 'post',
    data: data
  })
}
