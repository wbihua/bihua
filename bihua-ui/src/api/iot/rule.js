import request from '@/utils/request'

// 查询规则信息列表
export function listRule(query) {
  return request({
    url: '/iot/rule/list',
    method: 'get',
    params: query
  })
}

// 查询规则信息详细
export function getRule(id) {
  return request({
    url: '/iot/rule/' + id,
    method: 'get'
  })
}

// 新增规则信息
export function addRule(data) {
  return request({
    url: '/iot/rule',
    method: 'post',
    data: data
  })
}

// 修改规则信息
export function updateRule(data) {
  return request({
    url: '/iot/rule',
    method: 'put',
    data: data
  })
}

// 删除规则信息
export function delRule(id) {
  return request({
    url: '/iot/rule/' + id,
    method: 'delete'
  })
}
