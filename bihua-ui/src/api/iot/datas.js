import request from '@/utils/request'

// 查询设备消息列表
export function listDatas(query) {
  return request({
    url: '/iot/datas/list',
    method: 'get',
    params: query
  })
}

// 查询设备消息详细
export function getDatas(id) {
  return request({
    url: '/iot/datas/' + id,
    method: 'get'
  })
}

// 新增设备消息
export function addDatas(data) {
  return request({
    url: '/iot/datas',
    method: 'post',
    data: data
  })
}

// 修改设备消息
export function updateDatas(data) {
  return request({
    url: '/iot/datas',
    method: 'put',
    data: data
  })
}

// 删除设备消息
export function delDatas(id) {
  return request({
    url: '/iot/datas/' + id,
    method: 'delete'
  })
}
