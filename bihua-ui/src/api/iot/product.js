import request from '@/utils/request'

// 查询产品模型列表
export function listProduct(query) {
  return request({
    url: '/iot/product/list',
    method: 'get',
    params: query
  })
}

// 查询产品模型详细
export function getProduct(id) {
  return request({
    url: '/iot/product/' + id,
    method: 'get'
  })
}

// 新增产品模型
export function addProduct(data) {
  return request({
    url: '/iot/product',
    method: 'post',
    data: data
  })
}

// 修改产品模型
export function updateProduct(data) {
  return request({
    url: '/iot/product',
    method: 'put',
    data: data
  })
}

// 删除产品模型
export function delProduct(id) {
  return request({
    url: '/iot/product/' + id,
    method: 'delete'
  })
}
/**
 * 初始化数据模型
 * @param productIds 产品ID集合
 * @param initializeOrNot  是否初始化
 */
export function initializeDataModel(productIds, initializeOrNot) {
  return request({
    url: '/iot/product/initializeDataModel/' + productIds + "/" + initializeOrNot,
    method: 'get'
  })
}
