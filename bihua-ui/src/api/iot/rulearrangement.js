import request from '@/utils/request'

// 查询规则编排列表
export function listRulearrangement(query) {
  return request({
    url: '/iot/rulearrangement/list',
    method: 'get',
    params: query
  })
}

// 查询规则编排详细
export function getRulearrangement(id) {
  return request({
    url: '/iot/rulearrangement/' + id,
    method: 'get'
  })
}

// 新增规则编排
export function addRulearrangement(data) {
  return request({
    url: '/iot/rulearrangement',
    method: 'post',
    data: data
  })
}

// 修改规则编排
export function updateRulearrangement(data) {
  return request({
    url: '/iot/rulearrangement',
    method: 'put',
    data: data
  })
}

// 删除规则编排
export function delRulearrangement(id) {
  return request({
    url: '/iot/rulearrangement/' + id,
    method: 'delete'
  })
}
