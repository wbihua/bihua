import request from '@/utils/request'
import {
  praseStrEmpty
} from "@/utils/ruoyi";

// 查询边设备档案信息列表
export function listDevice(query) {
  return request({
    url: '/iot/device/list',
    method: 'get',
    params: query
  })
}

// 查询边设备档案信息详细
export function getDevice(id) {
  return request({
    url: '/iot/device/' + id,
    method: 'get'
  })
}

// 新增边设备档案信息
export function addDevice(data) {
  return request({
    url: '/iot/device',
    method: 'post',
    data: data
  })
}

// 修改边设备档案信息
export function updateDevice(data) {
  return request({
    url: '/iot/device',
    method: 'put',
    data: data
  })
}

// 删除边设备档案信息
export function delDevice(id) {
  return request({
    url: '/iot/device/' + id,
    method: 'delete'
  })
}
// 查询设备统计接口
export function listStatusCount(query) {
  return request({
    url: '/iot/device/listStatusCount',
    method: 'get',
    params: query
  })
}
// 校验clientId是否存在
export function validationDeviceIdentification_clientId(clientId) {
  return request({
    url: '/iot/device/validationFindOneByClientId/' + praseStrEmpty(clientId),
    method: 'get'
  })
}

// 校验设备标识是否存在
export function validationDeviceIdentification_deviceIdentification(deviceIdentification) {
  return request({
    url: '/iot/device/validationFindOneByDeviceIdentification/' + praseStrEmpty(deviceIdentification),
    method: 'get'
  })
}
// 查询普通设备影子数据
export function getDeviceShadow(data) {
  return request({
    url: '/iot/shadow/getDeviceShadow',
    method: 'post',
    data: data
  })
}
// 断开设备连接
export function disconnectDevice(id) {
  return request({
    url: '/iot/device/disconnect/' + id,
    method: 'post'
  })
}
