import request from '@/utils/request'

// 查询协议信息列表
export function listProtocol(query) {
  return request({
    url: '/iot/protocol/list',
    method: 'get',
    params: query
  })
}

// 查询协议信息详细
export function getProtocol(id) {
  return request({
    url: '/iot/protocol/' + id,
    method: 'get'
  })
}

// 新增协议信息
export function addProtocol(data) {
  return request({
    url: '/iot/protocol',
    method: 'post',
    data: data
  })
}

// 修改协议信息
export function updateProtocol(data) {
  return request({
    url: '/iot/protocol',
    method: 'put',
    data: data
  })
}

// 删除协议信息
export function delProtocol(id) {
  return request({
    url: '/iot/protocol/' + id,
    method: 'delete'
  })
}
// 动态编译代码
export function dynamicallyXcode(data) {
  return request({
    url: '/iot/protocolCompileXcode/dynamicallyXcode',
    method: 'post',
    data: data
  })
}
// 启用协议管理
export function enable(id) {
  return request({
    url: '/iot/protocol/enable/' + id,
    method: 'get'
  })
}

// 停用协议管理
export function disable(id) {
  return request({
    url: '/iot/protocol/disable/' + id,
    method: 'get'
  })
}
