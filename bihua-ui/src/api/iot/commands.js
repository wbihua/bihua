import request from '@/utils/request'

// 查询产品设备服务命令列表
export function listCommands(query) {
  return request({
    url: '/iot/commands/list',
    method: 'get',
    params: query
  })
}

// 查询产品设备服务命令列表
export function queryProductCommands(serviceId) {
  return request({
    url: '/iot/commands/queryProductCommands/'+serviceId,
    method: 'get'
  })
}

// 查询产品设备服务命令详细
export function getCommands(id) {
  return request({
    url: '/iot/commands/' + id,
    method: 'get'
  })
}

// 新增产品设备服务命令
export function addCommands(data) {
  return request({
    url: '/iot/commands',
    method: 'post',
    data: data
  })
}

// 修改产品设备服务命令
export function updateCommands(data) {
  return request({
    url: '/iot/commands',
    method: 'put',
    data: data
  })
}

// 删除产品设备服务命令
export function delCommands(id) {
  return request({
    url: '/iot/commands/' + id,
    method: 'delete'
  })
}
