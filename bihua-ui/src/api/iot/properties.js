import request from '@/utils/request'

// 查询产品服务属性列表
export function listProperties(query) {
  return request({
    url: '/iot/properties/list',
    method: 'get',
    params: query
  })
}

// 查询产品服务属性详细
export function getProperties(id) {
  return request({
    url: '/iot/properties/' + id,
    method: 'get'
  })
}

// 新增产品服务属性
export function addProperties(data) {
  return request({
    url: '/iot/properties',
    method: 'post',
    data: data
  })
}

// 修改产品服务属性
export function updateProperties(data) {
  return request({
    url: '/iot/properties',
    method: 'put',
    data: data
  })
}

// 删除产品服务属性
export function delProperties(id) {
  return request({
    url: '/iot/properties/' + id,
    method: 'delete'
  })
}
