import request from '@/utils/request'

export function getMenu() {
  return request({
    url: '/wx//wxMenu/getMenu',
    method: 'get'
  })
}

export function updateMenu(data) {
  return request({
    url: '/wx//wxMenu/updateMenu',
    method: 'post',
    data: data
  })
}
