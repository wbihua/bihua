import request from '@/utils/request'

// 查询微信模版消息发送记录列表
export function listTemplateMsgLog(query) {
  return request({
    url: '/wx/templateMsgLog/list',
    method: 'get',
    params: query
  })
}

// 查询微信模版消息发送记录详细
export function getTemplateMsgLog(logId) {
  return request({
    url: '/wx/templateMsgLog/' + logId,
    method: 'get'
  })
}

// 删除微信模版消息发送记录
export function delTemplateMsgLog(logId) {
  return request({
    url: '/wx/templateMsgLog/' + logId,
    method: 'delete'
  })
}
