import request from '@/utils/request'

// 查询CMS文章中心列表
export function listArticle(query) {
  return request({
    url: '/wx/article/list',
    method: 'get',
    params: query
  })
}

// 查询CMS文章中心详细
export function getArticle(id) {
  return request({
    url: '/wx/article/' + id,
    method: 'get'
  })
}

// 新增CMS文章中心
export function addArticle(data) {
  return request({
    url: '/wx/article',
    method: 'post',
    data: data
  })
}

// 修改CMS文章中心
export function updateArticle(data) {
  return request({
    url: '/wx/article',
    method: 'put',
    data: data
  })
}

// 删除CMS文章中心
export function delArticle(id) {
  return request({
    url: '/wx/article/' + id,
    method: 'delete'
  })
}
