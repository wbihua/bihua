import request from '@/utils/request'

// 查询公众号账号列表
export function listAccount(query) {
  return request({
    url: '/wx/account/list',
    method: 'get',
    params: query
  })
}

// 查询公众号账号详细
export function getAccount(appid) {
  return request({
    url: '/wx/account/' + appid,
    method: 'get'
  })
}

// 新增公众号账号
export function addAccount(data) {
  return request({
    url: '/wx/account',
    method: 'post',
    data: data
  })
}

// 修改公众号账号
export function updateAccount(data) {
  return request({
    url: '/wx/account',
    method: 'put',
    data: data
  })
}

// 删除公众号账号
export function delAccount(appid) {
  return request({
    url: '/wx/account/' + appid,
    method: 'delete'
  })
}
