import request from '@/utils/request'

// 查询自动回复规则列表
export function listMsgReplyRule(query) {
  return request({
    url: '/wx/msgReplyRule/list',
    method: 'get',
    params: query
  })
}

// 查询自动回复规则详细
export function getMsgReplyRule(ruleId) {
  return request({
    url: '/wx/msgReplyRule/' + ruleId,
    method: 'get'
  })
}

// 新增自动回复规则
export function addMsgReplyRule(data) {
  return request({
    url: '/wx/msgReplyRule',
    method: 'post',
    data: data
  })
}

// 修改自动回复规则
export function updateMsgReplyRule(data) {
  return request({
    url: '/wx/msgReplyRule',
    method: 'put',
    data: data
  })
}

// 删除自动回复规则
export function delMsgReplyRule(ruleId) {
  return request({
    url: '/wx/msgReplyRule/' + ruleId,
    method: 'delete'
  })
}
