import request from '@/utils/request'

// 查询公众号账号列表
export function materialCount() {
  return request({
    url: '/wx/wxAssets/materialCount',
    method: 'get'
  })
}

export function materialFileBatchGet(query) {
  return request({
    url: '/wx/wxAssets/materialFileBatchGet',
    method: 'get',
    params: query
  })
}

export function materialDelete(data) {
  return request({
    url: '/wx/wxAssets/materialDelete',
    method: 'post',
    data: data
  })
}
export function materialFileUpload(data) {
  return request({
    url: '/wx/wxAssets/materialFileUpload',
    method: 'post',
    data: data
  })
}
export function materialNewsBatchGet(query) {
  return request({
    url: '/wx/wxAssets/materialNewsBatchGet',
    method: 'get',
    params: query
  })
}

export function materialNewsUpload(data) {
  return request({
    url: '/wx/wxAssets/materialNewsUpload',
    method: 'post',
    data: data
  })
}

export function materialArticleUpdate(data) {
  return request({
    url: '/wx/wxAssets/materialArticleUpdate',
    method: 'post',
    data: data
  })
}

