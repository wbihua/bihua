import request from '@/utils/request'

// 查询消息模板列表
export function listMsgTemplate(query) {
  return request({
    url: '/wx/msgTemplate/list',
    method: 'get',
    params: query
  })
}

// 查询消息模板详细
export function getMsgTemplate(id) {
  return request({
    url: '/wx/msgTemplate/' + id,
    method: 'get'
  })
}

// 新增消息模板
export function addMsgTemplate(data) {
  return request({
    url: '/wx/msgTemplate',
    method: 'post',
    data: data
  })
}

// 修改消息模板
export function updateMsgTemplate(data) {
  return request({
    url: '/wx/msgTemplate',
    method: 'put',
    data: data
  })
}

// 删除消息模板
export function delMsgTemplate(id) {
  return request({
    url: '/wx/msgTemplate/' + id,
    method: 'delete'
  })
}

export function syncWxTemplate() {
  return request({
    url: '/wx/msgTemplate/syncWxTemplate',
    method: 'post'
  })
}

export function sendMsgBatch(data) {
  return request({
    url: '/wx/msgTemplate/sendMsgBatch',
    method: 'post',
    data: data
  })
}
