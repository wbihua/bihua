import request from '@/utils/request'

// 查询用户列表
export function listUser(query) {
  return request({
    url: '/wx/user/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getUser(openid) {
  return request({
    url: '/wx/user/' + openid,
    method: 'get'
  })
}
// 批量查询用户详细
export function listByIds(data) {
  return request({
    url: '/wx/user/listByIds',
    method: 'post',
    data: data
  })
}
// 删除用户
export function delUser(openid) {
  return request({
    url: '/wx/user/' + openid,
    method: 'delete'
  })
}

// 新增用户
export function syncWxUsers() {
  return request({
    url: '/wx/user/syncWxUsers',
    method: 'post'
  })
}
// 查询用户标签
export function getWxTags() {
  return request({
    url: '/wx/user/getWxTags',
    method: 'get'
  })
}
// 修改或新增标签
export function saveTag(data) {
  return request({
    url: '/wx/user/saveTag',
    method: 'post',
    data: data
  })
}
// 删除标签
export function deleteTag(tagid) {
  return request({
    url: '/wx/user/deleteTag/' + tagid,
    method: 'delete'
  })
}

// 批量给用户打标签
export function batchTagging(data) {
  return request({
    url: '/wx/user/batchTagging',
    method: 'post',
    data: data
  })
}
// 批量移除用户标签
export function batchUnTagging(data) {
  return request({
    url: '/wx/user/batchUnTagging',
    method: 'post',
    data: data
  })
}

