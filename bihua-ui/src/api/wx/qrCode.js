import request from '@/utils/request'

// 查询公众号带参二维码列表
export function listQrCode(query) {
  return request({
    url: '/wx/qrCode/list',
    method: 'get',
    params: query
  })
}

// 查询公众号带参二维码详细
export function getQrCode(id) {
  return request({
    url: '/wx/qrCode/' + id,
    method: 'get'
  })
}

// 新增公众号带参二维码
export function addQrCode(data) {
  return request({
    url: '/wx/qrCode',
    method: 'post',
    data: data
  })
}

// 修改公众号带参二维码
export function updateQrCode(data) {
  return request({
    url: '/wx/qrCode',
    method: 'put',
    data: data
  })
}

// 删除公众号带参二维码
export function delQrCode(id) {
  return request({
    url: '/wx/qrCode/' + id,
    method: 'delete'
  })
}
// 创建带参二维码ticket
export function createTicket(data) {
  return request({
    url: '/wx/qrCode/createTicket',
    method: 'post',
    data: data
  })
}
