import request from '@/utils/request'

// 查询微信消息列表
export function listMsg(query) {
  return request({
    url: '/wx/msg/list',
    method: 'get',
    params: query
  })
}

// 查询微信消息详细
export function getMsg(id) {
  return request({
    url: '/wx/msg/' + id,
    method: 'get'
  })
}

// 新增微信消息
export function addMsg(data) {
  return request({
    url: '/wx/msg',
    method: 'post',
    data: data
  })
}

// 修改微信消息
export function updateMsg(data) {
  return request({
    url: '/wx/msg',
    method: 'put',
    data: data
  })
}

// 删除微信消息
export function delMsg(id) {
  return request({
    url: '/wx/msg/' + id,
    method: 'delete'
  })
}

// 修改微信消息
export function reply(data) {
  return request({
    url: '/wx/msg/reply',
    method: 'post',
    data: data
  })
}
