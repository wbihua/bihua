import request from '@/utils/request'

// 查询blog文件列表
export function listBlogFile(query) {
  return request({
    url: '/blog/blogFile/list',
    method: 'get',
    params: query
  })
}

// 查询blog文件详细
export function getBlogFile(fileId) {
  return request({
    url: '/blog/blogFile/' + fileId,
    method: 'get'
  })
}

// 新增blog文件
export function addBlogFile(data) {
  return request({
    url: '/blog/blogFile',
    method: 'post',
    data: data
  })
}

// 批量新增blog文件
export function addBatch(data) {
  return request({
    url: '/blog/blogFile/addBatch',
    method: 'post',
    data: data
  })
}
// 查询文件列表
export function getFileList(blogId) {
  return request({
    url: '/blog/blogFile/getFileListByBlogId/' + blogId,
    method: 'get'
  })
}
// 修改blog文件
export function updateBlogFile(data) {
  return request({
    url: '/blog/blogFile',
    method: 'put',
    data: data
  })
}

// 删除blog文件
export function delBlogFile(fileId) {
  return request({
    url: '/blog/blogFile/' + fileId,
    method: 'delete'
  })
}
