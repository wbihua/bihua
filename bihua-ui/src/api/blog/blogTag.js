import request from '@/utils/request'

// 查询blog标签关联列表
export function listBlogTag(query) {
  return request({
    url: '/blog/blogTag/list',
    method: 'get',
    params: query
  })
}

// 查询blog标签关联详细
export function getBlogTag(tagId) {
  return request({
    url: '/blog/blogTag/' + tagId,
    method: 'get'
  })
}

// 新增blog标签关联
export function addBlogTag(data) {
  return request({
    url: '/blog/blogTag',
    method: 'post',
    data: data
  })
}

// 修改blog标签关联
export function updateBlogTag(data) {
  return request({
    url: '/blog/blogTag',
    method: 'put',
    data: data
  })
}

// 删除blog标签关联
export function delBlogTag(tagId) {
  return request({
    url: '/blog/blogTag/' + tagId,
    method: 'delete'
  })
}
