import request from '@/utils/request'

// 查询用户信息列表
export function listBlogUser(query) {
  return request({
    url: '/blog/blogUser/list',
    method: 'get',
    params: query
  })
}

// 查询用户信息详细
export function getBlogUser(userId) {
  return request({
    url: '/blog/blogUser/' + userId,
    method: 'get'
  })
}

// 新增用户信息
export function addBlogUser(data) {
  return request({
    url: '/blog/blogUser',
    method: 'post',
    data: data
  })
}

// 修改用户信息
export function updateBlogUser(data) {
  return request({
    url: '/blog/blogUser',
    method: 'put',
    data: data
  })
}

// 删除用户信息
export function delBlogUser(userId) {
  return request({
    url: '/blog/blogUser/' + userId,
    method: 'delete'
  })
}
// 用户状态修改
export function changeUserStatus(userId, status) {
  const data = {
    userId,
    status
  }
  return request({
    url: '/blog/blogUser/changeStatus',
    method: 'put',
    data: data
  })
}
// 用户密码重置
export function resetUserPwd(userId, password) {
  const data = {
    userId,
    password
  }
  return request({
    url: '/blog/blogUser/resetPwd',
    method: 'put',
    data: data
  })
}
