import request from '@/utils/request'

// 查询博客信息列表
export function listBlog(query) {
  return request({
    url: '/blog/blog/list',
    method: 'get',
    params: query
  })
}

// 查询博客信息详细
export function getBlog(id) {
  return request({
    url: '/blog/blog/' + id,
    method: 'get'
  })
}

// 查询博客信息详细
export function getAllTagAndAllType() {
  return request({
    url: '/blog/blog/getAllTagAndAllType',
    method: 'get'
  })
}

// 新增博客信息
export function addBlog(data) {
  return request({
    url: '/blog/blog',
    method: 'post',
    data: data
  })
}

// 修改博客信息
export function updateBlog(data) {
  return request({
    url: '/blog/blog',
    method: 'put',
    data: data
  })
}

// 删除博客信息
export function delBlog(id) {
  return request({
    url: '/blog/blog/' + id,
    method: 'delete'
  })
}
