import request from '@/utils/request'

// 查询blog分类关联列表
export function listBlogType(query) {
  return request({
    url: '/blog/blogType/list',
    method: 'get',
    params: query
  })
}

// 查询blog分类关联详细
export function getBlogType(typeId) {
  return request({
    url: '/blog/blogType/' + typeId,
    method: 'get'
  })
}

// 新增blog分类关联
export function addBlogType(data) {
  return request({
    url: '/blog/blogType',
    method: 'post',
    data: data
  })
}

// 修改blog分类关联
export function updateBlogType(data) {
  return request({
    url: '/blog/blogType',
    method: 'put',
    data: data
  })
}

// 删除blog分类关联
export function delBlogType(typeId) {
  return request({
    url: '/blog/blogType/' + typeId,
    method: 'delete'
  })
}
