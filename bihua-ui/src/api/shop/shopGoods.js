import request from '@/utils/request'

// 查询商品管理列表
export function listShopGoods(query) {
  return request({
    url: '/shop/shopGoods/list',
    method: 'get',
    params: query
  })
}

// 查询商品管理详细
export function getShopGoods(id) {
  return request({
    url: '/shop/shopGoods/' + id,
    method: 'get'
  })
}

// 新增商品管理
export function addShopGoods(data) {
  return request({
    url: '/shop/shopGoods',
    method: 'post',
    data: data
  })
}

// 修改商品管理
export function updateShopGoods(data) {
  return request({
    url: '/shop/shopGoods',
    method: 'put',
    data: data
  })
}

// 删除商品管理
export function delShopGoods(id) {
  return request({
    url: '/shop/shopGoods/' + id,
    method: 'delete'
  })
}

export function changeIsOnSale(id, isOnSale) {
  return request({
    url: '/shop/shopGoods/changeIsOnSale',
    method: 'post',
    params: {
      id: id,
      isOnSale: isOnSale
    }
  })
}
