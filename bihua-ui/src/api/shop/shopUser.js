import request from '@/utils/request'

// 查询会员管理列表
export function listShopUser(query) {
  return request({
    url: '/shop/shopUser/list',
    method: 'get',
    params: query
  })
}

// 查询会员管理详细
export function getShopUser(id) {
  return request({
    url: '/shop/shopUser/' + id,
    method: 'get'
  })
}

// 新增会员管理
export function addShopUser(data) {
  return request({
    url: '/shop/shopUser',
    method: 'post',
    data: data
  })
}

// 修改会员管理
export function updateShopUser(data) {
  return request({
    url: '/shop/shopUser',
    method: 'put',
    data: data
  })
}

// 删除会员管理
export function delShopUser(id) {
  return request({
    url: '/shop/shopUser/' + id,
    method: 'delete'
  })
}
