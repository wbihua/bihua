import request from '@/utils/request'

// 查询商品类别列表
export function listShopCategory(query) {
  return request({
    url: '/shop/shopCategory/list',
    method: 'get',
    params: query
  })
}

// 查询商品类别详细
export function getShopCategory(id) {
  return request({
    url: '/shop/shopCategory/' + id,
    method: 'get'
  })
}

// 新增商品类别
export function addShopCategory(data) {
  return request({
    url: '/shop/shopCategory',
    method: 'post',
    data: data
  })
}

// 修改商品类别
export function updateShopCategory(data) {
  return request({
    url: '/shop/shopCategory',
    method: 'put',
    data: data
  })
}

// 删除商品类别
export function delShopCategory(id) {
  return request({
    url: '/shop/shopCategory/' + id,
    method: 'delete'
  })
}
export function changeShowIndex(id,showIndex){
  return request({
    url: '/shop/shopCategory/changeShowIndex/' + id + '/' + showIndex,
    method: 'post'
  })
}

export function getBanners(id) {
  return request({
    url: '/shop/shopCategory/getBanners/' + id,
    method: 'get'
  })
}

export function removeBanner(id, idBanner) {
  return request({
    url: '/shop/shopCategory/removeBanner/' + id + '/' + idBanner,
    method: 'delete'
  })
}

export function setCategoryBanner(id, idBanner) {
  return request({
    url: '/shop/shopCategory/setBanner/' + id + '/' + idBanner,
    method: 'post'
  })
}

export function getAttrKeys(id) {
  return request({
    url: '/shop/shopCategory/getAttrKeys/' + id,
    method: 'get'
  })
}
