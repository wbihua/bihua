import request from '@/utils/request'

// 查询商品属性值列表
export function listShopAttrVal(query) {
  return request({
    url: '/shop/shopAttrVal/list',
    method: 'get',
    params: query
  })
}

// 查询商品属性值详细
export function getShopAttrVal(id) {
  return request({
    url: '/shop/shopAttrVal/' + id,
    method: 'get'
  })
}

// 新增商品属性值
export function addShopAttrVal(data) {
  return request({
    url: '/shop/shopAttrVal',
    method: 'post',
    data: data
  })
}

// 修改商品属性值
export function updateShopAttrVal(data) {
  return request({
    url: '/shop/shopAttrVal',
    method: 'put',
    data: data
  })
}

// 删除商品属性值
export function delShopAttrVal(id) {
  return request({
    url: '/shop/shopAttrVal/' + id,
    method: 'delete'
  })
}

export function getAttrVals(idAttrKey) {
  return request({
    url: '/shop/shopAttrVal/getAttrVals',
    method: 'get',
    params: {
      idAttrKey: idAttrKey
    }
  })
}

export function getAttrBy(idCategory) {
  return request({
    url: '/shop/shopAttrVal/getAttrByCategoryAndGoods/'+idCategory,
    method: 'get'
  })
}
