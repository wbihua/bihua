import request from '@/utils/request'

// 查询物流公司列表
export function listShopExpress(query) {
  return request({
    url: '/shop/shopExpress/list',
    method: 'get',
    params: query
  })
}

// 查询物流公司详细
export function getShopExpress(id) {
  return request({
    url: '/shop/shopExpress/' + id,
    method: 'get'
  })
}

// 新增物流公司
export function addShopExpress(data) {
  return request({
    url: '/shop/shopExpress',
    method: 'post',
    data: data
  })
}

// 修改物流公司
export function updateShopExpress(data) {
  return request({
    url: '/shop/shopExpress',
    method: 'put',
    data: data
  })
}

// 删除物流公司
export function delShopExpress(id) {
  return request({
    url: '/shop/shopExpress/' + id,
    method: 'delete'
  })
}
