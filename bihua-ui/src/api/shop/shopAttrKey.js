import request from '@/utils/request'

// 查询商品属性名列表
export function listShopAttrKey(query) {
  return request({
    url: '/shop/shopAttrKey/list',
    method: 'get',
    params: query
  })
}

// 查询商品属性名详细
export function getShopAttrKey(id) {
  return request({
    url: '/shop/shopAttrKey/' + id,
    method: 'get'
  })
}

// 新增商品属性名
export function addShopAttrKey(data) {
  return request({
    url: '/shop/shopAttrKey',
    method: 'post',
    data: data
  })
}

// 修改商品属性名
export function updateShopAttrKey(data) {
  return request({
    url: '/shop/shopAttrKey',
    method: 'put',
    data: data
  })
}

// 删除商品属性名
export function delShopAttrKey(id) {
  return request({
    url: '/shop/shopAttrKey/' + id,
    method: 'delete'
  })
}
