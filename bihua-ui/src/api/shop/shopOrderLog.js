import request from '@/utils/request'

// 查询订单日志列表
export function listShopOrderLog(query) {
  return request({
    url: '/shop/shopOrderLog/list',
    method: 'get',
    params: query
  })
}

// 查询订单日志详细
export function getShopOrderLog(id) {
  return request({
    url: '/shop/shopOrderLog/' + id,
    method: 'get'
  })
}

// 新增订单日志
export function addShopOrderLog(data) {
  return request({
    url: '/shop/shopOrderLog',
    method: 'post',
    data: data
  })
}

// 修改订单日志
export function updateShopOrderLog(data) {
  return request({
    url: '/shop/shopOrderLog',
    method: 'put',
    data: data
  })
}

// 删除订单日志
export function delShopOrderLog(id) {
  return request({
    url: '/shop/shopOrderLog/' + id,
    method: 'delete'
  })
}
