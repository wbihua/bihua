import request from '@/utils/request'

// 查询收货地址列表
export function listShopAddress(query) {
  return request({
    url: '/shop/shopAddress/list',
    method: 'get',
    params: query
  })
}

// 查询收货地址详细
export function getShopAddress(id) {
  return request({
    url: '/shop/shopAddress/' + id,
    method: 'get'
  })
}

// 新增收货地址
export function addShopAddress(data) {
  return request({
    url: '/shop/shopAddress',
    method: 'post',
    data: data
  })
}

// 修改收货地址
export function updateShopAddress(data) {
  return request({
    url: '/shop/shopAddress',
    method: 'put',
    data: data
  })
}

// 删除收货地址
export function delShopAddress(id) {
  return request({
    url: '/shop/shopAddress/' + id,
    method: 'delete'
  })
}
