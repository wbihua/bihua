import request from '@/utils/request'

// 查询广告banner列表
export function listShopBanner(query) {
  return request({
    url: '/shop/shopBanner/list',
    method: 'get',
    params: query
  })
}

// 查询广告banner详细
export function getShopBanner(id) {
  return request({
    url: '/shop/shopBanner/' + id,
    method: 'get'
  })
}

// 新增广告banner
export function addShopBanner(data) {
  return request({
    url: '/shop/shopBanner',
    method: 'post',
    data: data
  })
}

// 修改广告banner
export function updateShopBanner(data) {
  return request({
    url: '/shop/shopBanner',
    method: 'put',
    data: data
  })
}

// 删除广告banner
export function delShopBanner(id) {
  return request({
    url: '/shop/shopBanner/' + id,
    method: 'delete'
  })
}
