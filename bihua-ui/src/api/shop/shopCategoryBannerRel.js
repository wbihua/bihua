import request from '@/utils/request'

// 查询类别banner关联列表
export function listShopCategoryBannerRel(query) {
  return request({
    url: '/shop/shopCategoryBannerRel/list',
    method: 'get',
    params: query
  })
}

// 查询类别banner关联详细
export function getShopCategoryBannerRel(id) {
  return request({
    url: '/shop/shopCategoryBannerRel/' + id,
    method: 'get'
  })
}

// 新增类别banner关联
export function addShopCategoryBannerRel(data) {
  return request({
    url: '/shop/shopCategoryBannerRel',
    method: 'post',
    data: data
  })
}

// 修改类别banner关联
export function updateShopCategoryBannerRel(data) {
  return request({
    url: '/shop/shopCategoryBannerRel',
    method: 'put',
    data: data
  })
}

// 删除类别banner关联
export function delShopCategoryBannerRel(id) {
  return request({
    url: '/shop/shopCategoryBannerRel/' + id,
    method: 'delete'
  })
}
