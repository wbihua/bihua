import request from '@/utils/request'

// 查询订单管理列表
export function listShopOrder(query) {
  return request({
    url: '/shop/shopOrder/list',
    method: 'get',
    params: query
  })
}

// 查询订单管理详细
export function getShopOrder(id) {
  return request({
    url: '/shop/shopOrder/' + id,
    method: 'get'
  })
}
// 通过订单编号查询订单管理详细
export function getInfoByOrderSn(orderSn) {
  return request({
    url: '/shop/shopOrder/getInfoByOrderSn/' + orderSn,
    method: 'get'
  })
}
// 新增订单管理
export function addShopOrder(data) {
  return request({
    url: '/shop/shopOrder',
    method: 'post',
    data: data
  })
}

// 修改订单管理
export function updateShopOrder(data) {
  return request({
    url: '/shop/shopOrder',
    method: 'put',
    data: data
  })
}

// 删除订单管理
export function delShopOrder(id) {
  return request({
    url: '/shop/shopOrder/' + id,
    method: 'delete'
  })
}

export function sendOut(id,idExpress,shippingSn) {
  return request({
    url: '/shop/shopOrder/sendOut/' + id,
    method: 'post',
    params:{
      idExpress:idExpress,
      shippingSn:shippingSn
    }
  })
}
