import request from '@/utils/request'

// 查询订单明细列表
export function listShopOrderItem(query) {
  return request({
    url: '/shop/shopOrderItem/list',
    method: 'get',
    params: query
  })
}

// 查询订单明细详细
export function getShopOrderItem(id) {
  return request({
    url: '/shop/shopOrderItem/' + id,
    method: 'get'
  })
}

// 新增订单明细
export function addShopOrderItem(data) {
  return request({
    url: '/shop/shopOrderItem',
    method: 'post',
    data: data
  })
}

// 修改订单明细
export function updateShopOrderItem(data) {
  return request({
    url: '/shop/shopOrderItem',
    method: 'put',
    data: data
  })
}

// 删除订单明细
export function delShopOrderItem(id) {
  return request({
    url: '/shop/shopOrderItem/' + id,
    method: 'delete'
  })
}
