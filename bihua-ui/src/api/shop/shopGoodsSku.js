import request from '@/utils/request'

// 查询商品SKU列表
export function listShopGoodsSku(query) {
  return request({
    url: '/shop/shopGoodsSku/list',
    method: 'get',
    params: query
  })
}

// 查询商品SKU详细
export function getShopGoodsSku(id) {
  return request({
    url: '/shop/shopGoodsSku/' + id,
    method: 'get'
  })
}

// 新增商品SKU
export function addShopGoodsSku(data) {
  return request({
    url: '/shop/shopGoodsSku',
    method: 'post',
    data: data
  })
}

// 修改商品SKU
export function updateShopGoodsSku(data) {
  return request({
    url: '/shop/shopGoodsSku',
    method: 'put',
    data: data
  })
}

// 删除商品SKU
export function delShopGoodsSku(id) {
  return request({
    url: '/shop/shopGoodsSku/' + id,
    method: 'delete'
  })
}
