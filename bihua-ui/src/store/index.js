import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import dict from './modules/dict'
import user from './modules/user'
import tagsView from './modules/tagsView'
import permission from './modules/permission'
import settings from './modules/settings'
import wxAccount from './modules/wxAccount'
import wxUserTags from './modules/wxUserTags'
import message from './modules/message'
import article from './modules/article'
import getters from './getters'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    dict,
    user,
    tagsView,
    permission,
    settings,
    wxAccount,
    wxUserTags,
    message,
    article
  },
  getters
})

export default store
