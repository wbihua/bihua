package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.DeviceLocationBo;
import com.bihua.iot.domain.vo.DeviceLocationVo;
import com.bihua.iot.service.IDeviceLocationService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 设备位置
 *
 * @author bihua
 * @date 2023-06-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/location")
public class DeviceLocationController extends BaseController {

    private final IDeviceLocationService iDeviceLocationService;

    /**
     * 查询设备位置列表
     */
    @SaCheckPermission("iot:location:list")
    @GetMapping("/list")
    public TableDataInfo<DeviceLocationVo> list(DeviceLocationBo bo, PageQuery pageQuery) {
        return iDeviceLocationService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出设备位置列表
     */
    @SaCheckPermission("iot:location:export")
    @Log(title = "设备位置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DeviceLocationBo bo, HttpServletResponse response) {
        List<DeviceLocationVo> list = iDeviceLocationService.queryList(bo);
        ExcelUtil.exportExcel(list, "设备位置", DeviceLocationVo.class, response);
    }

    /**
     * 获取设备位置详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:location:query")
    @GetMapping("/{id}")
    public R<DeviceLocationVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDeviceLocationService.queryById(id));
    }

    /**
     * 新增设备位置
     */
    @SaCheckPermission("iot:location:add")
    @Log(title = "设备位置", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DeviceLocationBo bo) {
        return toAjax(iDeviceLocationService.insertByBo(bo));
    }

    /**
     * 修改设备位置
     */
    @SaCheckPermission("iot:location:edit")
    @Log(title = "设备位置", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DeviceLocationBo bo) {
        return toAjax(iDeviceLocationService.updateByBo(bo));
    }

    /**
     * 删除设备位置
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:location:remove")
    @Log(title = "设备位置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDeviceLocationService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
