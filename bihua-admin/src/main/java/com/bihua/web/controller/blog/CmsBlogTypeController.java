package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsBlogTypeBo;
import com.bihua.blog.domain.vo.CmsBlogTypeVo;
import com.bihua.blog.service.ICmsBlogTypeService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * blog分类关联
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blogType")
public class CmsBlogTypeController extends BaseController {

    private final ICmsBlogTypeService iCmsBlogTypeService;

    /**
     * 查询blog分类关联列表
     */
    @SaCheckPermission("blog:blogType:list")
    @GetMapping("/list")
    public TableDataInfo<CmsBlogTypeVo> list(CmsBlogTypeBo bo, PageQuery pageQuery) {
        return iCmsBlogTypeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出blog分类关联列表
     */
    @SaCheckPermission("blog:blogType:export")
    @Log(title = "blog分类关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsBlogTypeBo bo, HttpServletResponse response) {
        List<CmsBlogTypeVo> list = iCmsBlogTypeService.queryList(bo);
        ExcelUtil.exportExcel(list, "blog分类关联", CmsBlogTypeVo.class, response);
    }

    /**
     * 获取blog分类关联详细信息
     *
     * @param typeId 主键
     */
    @SaCheckPermission("blog:blogType:query")
    @GetMapping("/{typeId}")
    public R<CmsBlogTypeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long typeId) {
        return R.ok(iCmsBlogTypeService.queryById(typeId));
    }

    /**
     * 新增blog分类关联
     */
    @SaCheckPermission("blog:blogType:add")
    @Log(title = "blog分类关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsBlogTypeBo bo) {
        return toAjax(iCmsBlogTypeService.insertByBo(bo));
    }

    /**
     * 修改blog分类关联
     */
    @SaCheckPermission("blog:blogType:edit")
    @Log(title = "blog分类关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsBlogTypeBo bo) {
        return toAjax(iCmsBlogTypeService.updateByBo(bo));
    }

    /**
     * 删除blog分类关联
     *
     * @param typeIds 主键串
     */
    @SaCheckPermission("blog:blogType:remove")
    @Log(title = "blog分类关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] typeIds) {
        return toAjax(iCmsBlogTypeService.deleteWithValidByIds(Arrays.asList(typeIds), true));
    }
}
