package com.bihua.web.controller.iot;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.core.domain.R;
import com.bihua.iot.domain.bo.CompileXcodeBo;
import com.bihua.iot.dynamicCompilation.service.JavaExecuteService;
import com.bihua.iot.dynamicScript.nashorn.NashornExecuteService;

import lombok.extern.slf4j.Slf4j;

/**
 * @program: thinglinks
 * @description: 动态编译代码Controller
 * @packagename: com.mqttsnet.thinglinks.link.controller.protocol
 * @author: ShiHuan Sun
 * @e-mainl: 13733918655@163.com
 * @date: 2022-07-01 18:31
 **/
@RestController
@RequestMapping("/iot/protocolCompileXcode")
@Slf4j
public class CompileXcodeController {
    /**
     * 动态编译代码
     * @param xcodeBo
     * @return
     */
    @PostMapping("/dynamicallyXcode")
    public R<String> importProductJson(@RequestBody CompileXcodeBo xcodeBo) {
        try {
            switch (xcodeBo.getProtocolVoice()) {
                case "java":
                    return R.ok(JavaExecuteService.executeDynamically(xcodeBo.getContent(), xcodeBo.getInparam()));
                case "js":
                    return R.ok(NashornExecuteService.executeDynamically(xcodeBo.getId(), xcodeBo.getContent(), xcodeBo.getInparam()));
                default:
                    return R.fail();
            }
        } catch (Throwable e) {
            return R.fail(e.getMessage());
        }
    }
}
