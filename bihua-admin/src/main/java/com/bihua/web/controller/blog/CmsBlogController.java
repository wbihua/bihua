package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsBlogBo;
import com.bihua.blog.domain.vo.CmsBlogAllTagAndAllTypeVo;
import com.bihua.blog.domain.vo.CmsBlogVo;
import com.bihua.blog.service.ICmsBlogService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 博客信息
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blog")
public class CmsBlogController extends BaseController {

    private final ICmsBlogService iCmsBlogService;

    /**
     * 查询博客信息列表
     */
    @SaCheckPermission("blog:blog:list")
    @GetMapping("/list")
    public TableDataInfo<CmsBlogVo> list(CmsBlogBo bo, PageQuery pageQuery) {
        return iCmsBlogService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出博客信息列表
     */
    @SaCheckPermission("blog:blog:export")
    @Log(title = "博客信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsBlogBo bo, HttpServletResponse response) {
        List<CmsBlogVo> list = iCmsBlogService.queryList(bo);
        ExcelUtil.exportExcel(list, "博客信息", CmsBlogVo.class, response);
    }

    /**
     * 获取博客信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("blog:blog:query")
    @GetMapping("/{id}")
    public R<CmsBlogVo> getInfo(@NotNull(message = "主键不能为空")
                                    @PathVariable Long id) {
        return R.ok(iCmsBlogService.queryById(id));
    }

    /**
     * 新增博客信息
     */
    @SaCheckPermission("blog:blog:add")
    @Log(title = "博客信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Long> add(@Validated(AddGroup.class) @RequestBody CmsBlogBo bo) {
        Long result = iCmsBlogService.insertByBo(bo);
        return result == null ? R.fail() : R.ok(result);
    }

    /**
     * 修改博客信息
     */
    @SaCheckPermission("blog:blog:edit")
    @Log(title = "博客信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsBlogBo bo) {
        return toAjax(iCmsBlogService.updateByBo(bo));
    }

    /**
     * 删除博客信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("blog:blog:remove")
    @Log(title = "博客信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCmsBlogService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
    /**
     * 获取博客信息详细信息
     *
     */
    @SaCheckPermission("blog:blog:query")
    @GetMapping("/getAllTagAndAllType")
    public R<CmsBlogAllTagAndAllTypeVo> getAllTagAndAllType() {
        return R.ok(iCmsBlogService.getAllTagAndAllType());
    }
}
