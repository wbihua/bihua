package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsTypeBo;
import com.bihua.blog.domain.vo.CmsTypeVo;
import com.bihua.blog.service.ICmsTypeService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 分类信息
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/type")
public class CmsTypeController extends BaseController {

    private final ICmsTypeService iCmsTypeService;

    /**
     * 查询分类信息列表
     */
    @SaCheckPermission("blog:type:list")
    @GetMapping("/list")
    public TableDataInfo<CmsTypeVo> list(CmsTypeBo bo, PageQuery pageQuery) {
        return iCmsTypeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出分类信息列表
     */
    @SaCheckPermission("blog:type:export")
    @Log(title = "分类信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsTypeBo bo, HttpServletResponse response) {
        List<CmsTypeVo> list = iCmsTypeService.queryList(bo);
        ExcelUtil.exportExcel(list, "分类信息", CmsTypeVo.class, response);
    }

    /**
     * 获取分类信息详细信息
     *
     * @param typeId 主键
     */
    @SaCheckPermission("blog:type:query")
    @GetMapping("/{typeId}")
    public R<CmsTypeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long typeId) {
        return R.ok(iCmsTypeService.queryById(typeId));
    }

    /**
     * 新增分类信息
     */
    @SaCheckPermission("blog:type:add")
    @Log(title = "分类信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsTypeBo bo) {
        return toAjax(iCmsTypeService.insertByBo(bo));
    }

    /**
     * 修改分类信息
     */
    @SaCheckPermission("blog:type:edit")
    @Log(title = "分类信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsTypeBo bo) {
        return toAjax(iCmsTypeService.updateByBo(bo));
    }

    /**
     * 删除分类信息
     *
     * @param typeIds 主键串
     */
    @SaCheckPermission("blog:type:remove")
    @Log(title = "分类信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{typeIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] typeIds) {
        return toAjax(iCmsTypeService.deleteWithValidByIds(Arrays.asList(typeIds), true));
    }
}
