package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.RuleConditionsBo;
import com.bihua.iot.domain.vo.RuleConditionsVo;
import com.bihua.iot.service.IRuleConditionsService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 规则条件
 *
 * @author bihua
 * @date 2023-07-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/conditions")
public class RuleConditionsController extends BaseController {

    private final IRuleConditionsService iRuleConditionsService;

    /**
     * 查询规则条件列表
     */
    @SaCheckPermission("iot:conditions:list")
    @GetMapping("/list")
    public TableDataInfo<RuleConditionsVo> list(RuleConditionsBo bo, PageQuery pageQuery) {
        return iRuleConditionsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出规则条件列表
     */
    @SaCheckPermission("iot:conditions:export")
    @Log(title = "规则条件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(RuleConditionsBo bo, HttpServletResponse response) {
        List<RuleConditionsVo> list = iRuleConditionsService.queryList(bo);
        ExcelUtil.exportExcel(list, "规则条件", RuleConditionsVo.class, response);
    }

    /**
     * 获取规则条件详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:conditions:query")
    @GetMapping("/{id}")
    public R<RuleConditionsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iRuleConditionsService.queryById(id));
    }

    /**
     * 新增规则条件
     */
    @SaCheckPermission("iot:conditions:add")
    @Log(title = "规则条件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody RuleConditionsBo bo) {
        return toAjax(iRuleConditionsService.insertByBo(bo));
    }

    /**
     * 修改规则条件
     */
    @SaCheckPermission("iot:conditions:edit")
    @Log(title = "规则条件", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody RuleConditionsBo bo) {
        return toAjax(iRuleConditionsService.updateByBo(bo));
    }

    /**
     * 删除规则条件
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:conditions:remove")
    @Log(title = "规则条件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iRuleConditionsService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
