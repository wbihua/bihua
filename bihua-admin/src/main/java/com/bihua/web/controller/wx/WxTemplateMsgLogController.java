package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxTemplateMsgLogBo;
import com.bihua.wx.domain.vo.WxTemplateMsgLogVo;
import com.bihua.wx.service.IWxTemplateMsgLogService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 微信模版消息发送记录
 *
 * @author bihua
 * @date 2023-05-22
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/templateMsgLog")
public class WxTemplateMsgLogController extends BaseController {

    private final IWxTemplateMsgLogService iWxTemplateMsgLogService;

    /**
     * 查询微信模版消息发送记录列表
     */
    @SaCheckPermission("wx:templateMsgLog:list")
    @GetMapping("/list")
    public TableDataInfo<WxTemplateMsgLogVo> list(@CookieValue String appid, WxTemplateMsgLogBo bo, PageQuery pageQuery) {
        bo.setAppid(appid);
        return iWxTemplateMsgLogService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出微信模版消息发送记录列表
     */
    @SaCheckPermission("wx:templateMsgLog:export")
    @Log(title = "微信模版消息发送记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxTemplateMsgLogBo bo, HttpServletResponse response) {
        List<WxTemplateMsgLogVo> list = iWxTemplateMsgLogService.queryList(bo);
        ExcelUtil.exportExcel(list, "微信模版消息发送记录", WxTemplateMsgLogVo.class, response);
    }

    /**
     * 获取微信模版消息发送记录详细信息
     *
     * @param logId 主键
     */
    @SaCheckPermission("wx:templateMsgLog:query")
    @GetMapping("/{logId}")
    public R<WxTemplateMsgLogVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long logId) {
        return R.ok(iWxTemplateMsgLogService.queryById(logId));
    }

    /**
     * 删除微信模版消息发送记录
     *
     * @param logIds 主键串
     */
    @SaCheckPermission("wx:templateMsgLog:remove")
    @Log(title = "微信模版消息发送记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{logIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] logIds) {
        return toAjax(iWxTemplateMsgLogService.deleteWithValidByIds(Arrays.asList(logIds), true));
    }
}
