package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxQrCodeBo;
import com.bihua.wx.domain.vo.WxQrCodeVo;
import com.bihua.wx.service.IWxQrCodeService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpQrCodeTicket;

/**
 * 公众号带参二维码
 *
 * @author bihua
 * @date 2023-05-19
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/qrCode")
public class WxQrCodeController extends BaseController {

    private final IWxQrCodeService iWxQrCodeService;

    private final WxMpService wxMpService;

    /**
     * 创建带参二维码ticket
     */
    @PostMapping("/createTicket")
    @SaCheckPermission("wx:qrCode:save")
    public R createTicket(@CookieValue String appid, @RequestBody WxQrCodeBo bo) throws WxErrorException {
        wxMpService.switchoverTo(appid);
        WxMpQrCodeTicket ticket = iWxQrCodeService.createQrCode(appid, bo);
        return R.ok(ticket);
    }
    /**
     * 查询公众号带参二维码列表
     */
    @SaCheckPermission("wx:qrCode:list")
    @GetMapping("/list")
    public TableDataInfo<WxQrCodeVo> list(WxQrCodeBo bo, PageQuery pageQuery) {
        return iWxQrCodeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出公众号带参二维码列表
     */
    @SaCheckPermission("wx:qrCode:export")
    @Log(title = "公众号带参二维码", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxQrCodeBo bo, HttpServletResponse response) {
        List<WxQrCodeVo> list = iWxQrCodeService.queryList(bo);
        ExcelUtil.exportExcel(list, "公众号带参二维码", WxQrCodeVo.class, response);
    }

    /**
     * 获取公众号带参二维码详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wx:qrCode:query")
    @GetMapping("/{id}")
    public R<WxQrCodeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWxQrCodeService.queryById(id));
    }

    /**
     * 新增公众号带参二维码
     */
    @SaCheckPermission("wx:qrCode:add")
    @Log(title = "公众号带参二维码", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WxQrCodeBo bo) {
        return toAjax(iWxQrCodeService.insertByBo(bo));
    }

    /**
     * 修改公众号带参二维码
     */
    @SaCheckPermission("wx:qrCode:edit")
    @Log(title = "公众号带参二维码", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WxQrCodeBo bo) {
        return toAjax(iWxQrCodeService.updateByBo(bo));
    }

    /**
     * 删除公众号带参二维码
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wx:qrCode:remove")
    @Log(title = "公众号带参二维码", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWxQrCodeService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
