package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.TemplateMsgBatchBo;
import com.bihua.wx.domain.bo.WxMsgTemplateBo;
import com.bihua.wx.domain.vo.WxMsgTemplateVo;
import com.bihua.wx.service.IWxMsgTemplateService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;

/**
 * 消息模板
 *
 * @author bihua
 * @date 2023-05-22
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/msgTemplate")
public class WxMsgTemplateController extends BaseController {

    private final IWxMsgTemplateService iWxMsgTemplateService;

    /**
     * 查询消息模板列表
     */
    @SaCheckPermission("wx:msgTemplate:list")
    @GetMapping("/list")
    public TableDataInfo<WxMsgTemplateVo> list(@CookieValue String appid, WxMsgTemplateBo bo, PageQuery pageQuery) {
        bo.setAppid(appid);
        return iWxMsgTemplateService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出消息模板列表
     */
    @SaCheckPermission("wx:msgTemplate:export")
    @Log(title = "消息模板", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxMsgTemplateBo bo, HttpServletResponse response) {
        List<WxMsgTemplateVo> list = iWxMsgTemplateService.queryList(bo);
        ExcelUtil.exportExcel(list, "消息模板", WxMsgTemplateVo.class, response);
    }

    /**
     * 获取消息模板详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wx:msgTemplate:query")
    @GetMapping("/{id}")
    public R<WxMsgTemplateVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWxMsgTemplateService.queryById(id));
    }

    /**
     * 新增消息模板
     */
    @SaCheckPermission("wx:msgTemplate:add")
    @Log(title = "消息模板", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WxMsgTemplateBo bo) {
        return toAjax(iWxMsgTemplateService.insertByBo(bo));
    }

    /**
     * 修改消息模板
     */
    @SaCheckPermission("wx:msgTemplate:edit")
    @Log(title = "消息模板", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WxMsgTemplateBo bo) {
        return toAjax(iWxMsgTemplateService.updateByBo(bo));
    }

    /**
     * 删除消息模板
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wx:msgTemplate:remove")
    @Log(title = "消息模板", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWxMsgTemplateService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 同步公众号模板
     */
    @PostMapping("/syncWxTemplate")
    @SaCheckPermission("wx:msgtemplate:save")
    @Log(title = "同步公众号模板", businessType = BusinessType.INSERT)
    public R syncWxTemplate(@CookieValue String appid) throws WxErrorException {
        iWxMsgTemplateService.syncWxTemplate(appid);
        return R.ok();
    }

    /**
     * 批量向用户发送模板消息
     * 通过用户筛选条件（一般使用标签筛选），将消息发送给数据库中所有符合筛选条件的用户
     */
    @PostMapping("/sendMsgBatch")
    @SaCheckPermission("wx:msgtemplate:save")
    public R sendMsgBatch(@CookieValue String appid,@RequestBody TemplateMsgBatchBo bo) {
        iWxMsgTemplateService.sendMsgBatch(bo, appid);
        return R.ok();
    }
}
