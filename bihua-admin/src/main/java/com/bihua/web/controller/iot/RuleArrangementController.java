package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.RuleArrangementBo;
import com.bihua.iot.domain.vo.RuleArrangementVo;
import com.bihua.iot.service.IRuleArrangementService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 规则编排
 *
 * @author bihua
 * @date 2023-07-03
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/rulearrangement")
public class RuleArrangementController extends BaseController {

    private final IRuleArrangementService iRuleArrangementService;
    /**
     * 查询规则编排列表
     */
    @SaCheckPermission("iot:rulearrangement:list")
    @GetMapping("/tokens")
    public R<Void> tokens() {
        return R.ok();
    }
    /**
     * 查询规则编排列表
     */
    @SaCheckPermission("iot:rulearrangement:list")
    @GetMapping("/list")
    public TableDataInfo<RuleArrangementVo> list(RuleArrangementBo bo, PageQuery pageQuery) {
        return iRuleArrangementService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出规则编排列表
     */
    @SaCheckPermission("iot:rulearrangement:export")
    @Log(title = "规则编排", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(RuleArrangementBo bo, HttpServletResponse response) {
        List<RuleArrangementVo> list = iRuleArrangementService.queryList(bo);
        ExcelUtil.exportExcel(list, "规则编排", RuleArrangementVo.class, response);
    }

    /**
     * 获取规则编排详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:rulearrangement:query")
    @GetMapping("/{id}")
    public R<RuleArrangementVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iRuleArrangementService.queryById(id));
    }

    /**
     * 新增规则编排
     */
    @SaCheckPermission("iot:rulearrangement:add")
    @Log(title = "规则编排", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody RuleArrangementBo bo) {
        return toAjax(iRuleArrangementService.insertByBo(bo));
    }

    /**
     * 修改规则编排
     */
    @SaCheckPermission("iot:rulearrangement:edit")
    @Log(title = "规则编排", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody RuleArrangementBo bo) {
        return toAjax(iRuleArrangementService.updateByBo(bo));
    }

    /**
     * 获取规则编排详细信息
     *
     * @param ruleIdentification 主键
     */
    @SaCheckPermission("iot:rulearrangement:edit")
    @GetMapping("/updateRuleContent/{ruleIdentification}")
    public R<Void> updateRuleContent(@NotNull(message = "规则标识不能为空")
                                        @PathVariable String ruleIdentification) {
        return toAjax(iRuleArrangementService.updateRuleContent(ruleIdentification));
    }

    /**
     * 删除规则编排
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:rulearrangement:remove")
    @Log(title = "规则编排", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iRuleArrangementService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
