package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.RuleBo;
import com.bihua.iot.domain.vo.RuleVo;
import com.bihua.iot.service.IRuleService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 规则信息
 *
 * @author bihua
 * @date 2023-07-02
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/rule")
public class RuleController extends BaseController {

    private final IRuleService iRuleService;

    /**
     * 查询规则信息列表
     */
    @SaCheckPermission("iot:rule:list")
    @GetMapping("/list")
    public TableDataInfo<RuleVo> list(RuleBo bo, PageQuery pageQuery) {
        return iRuleService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出规则信息列表
     */
    @SaCheckPermission("iot:rule:export")
    @Log(title = "规则信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(RuleBo bo, HttpServletResponse response) {
        List<RuleVo> list = iRuleService.queryList(bo);
        ExcelUtil.exportExcel(list, "规则信息", RuleVo.class, response);
    }

    /**
     * 获取规则信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:rule:query")
    @GetMapping("/{id}")
    public R<RuleVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iRuleService.queryById(id));
    }

    /**
     * 新增规则信息
     */
    @SaCheckPermission("iot:rule:add")
    @Log(title = "规则信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody RuleBo bo) {
        return toAjax(iRuleService.insertByBo(bo));
    }

    /**
     * 修改规则信息
     */
    @SaCheckPermission("iot:rule:edit")
    @Log(title = "规则信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody RuleBo bo) {
        return toAjax(iRuleService.updateByBo(bo));
    }

    /**
     * 删除规则信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:rule:remove")
    @Log(title = "规则信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iRuleService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
