package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsTagBo;
import com.bihua.blog.domain.vo.CmsTagVo;
import com.bihua.blog.service.ICmsTagService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 标签信息
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/tag")
public class CmsTagController extends BaseController {

    private final ICmsTagService iCmsTagService;

    /**
     * 查询标签信息列表
     */
    @SaCheckPermission("blog:tag:list")
    @GetMapping("/list")
    public TableDataInfo<CmsTagVo> list(CmsTagBo bo, PageQuery pageQuery) {
        return iCmsTagService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出标签信息列表
     */
    @SaCheckPermission("blog:tag:export")
    @Log(title = "标签信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsTagBo bo, HttpServletResponse response) {
        List<CmsTagVo> list = iCmsTagService.queryList(bo);
        ExcelUtil.exportExcel(list, "标签信息", CmsTagVo.class, response);
    }

    /**
     * 获取标签信息详细信息
     *
     * @param tagId 主键
     */
    @SaCheckPermission("blog:tag:query")
    @GetMapping("/{tagId}")
    public R<CmsTagVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long tagId) {
        return R.ok(iCmsTagService.queryById(tagId));
    }

    /**
     * 新增标签信息
     */
    @SaCheckPermission("blog:tag:add")
    @Log(title = "标签信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsTagBo bo) {
        return toAjax(iCmsTagService.insertByBo(bo));
    }

    /**
     * 修改标签信息
     */
    @SaCheckPermission("blog:tag:edit")
    @Log(title = "标签信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsTagBo bo) {
        return toAjax(iCmsTagService.updateByBo(bo));
    }

    /**
     * 删除标签信息
     *
     * @param tagIds 主键串
     */
    @SaCheckPermission("blog:tag:remove")
    @Log(title = "标签信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tagIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] tagIds) {
        return toAjax(iCmsTagService.deleteWithValidByIds(Arrays.asList(tagIds), true));
    }
}
