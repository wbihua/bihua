package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxUserBatchTaggingBo;
import com.bihua.wx.domain.bo.WxUserBo;
import com.bihua.wx.domain.bo.WxUserTagBo;
import com.bihua.wx.domain.vo.WxUserVo;
import com.bihua.wx.service.IWxUserService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.tag.WxUserTag;

/**
 * 用户
 *
 * @author ruoyi
 * @date 2023-05-21
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/user")
public class WxUserController extends BaseController {

    private final IWxUserService iWxUserService;

    /**
     * 查询用户列表
     */
    @SaCheckPermission("wx:user:list")
    @GetMapping("/list")
    public TableDataInfo<WxUserVo> list(@CookieValue String appid, WxUserBo bo, PageQuery pageQuery) {
        bo.setAppid(appid);
        return iWxUserService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出用户列表
     */
    @SaCheckPermission("wx:user:export")
    @Log(title = "用户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxUserBo bo, HttpServletResponse response) {
        List<WxUserVo> list = iWxUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "用户", WxUserVo.class, response);
    }

    /**
     * 获取用户详细信息
     *
     * @param openid 主键
     */
    @SaCheckPermission("wx:user:query")
    @GetMapping("/{openid}")
    public R<WxUserVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable String openid) {
        return R.ok(iWxUserService.queryById(openid));
    }
    /**
     * 列表
     */
    @PostMapping("/listByIds")
    @SaCheckPermission("wx:user:query")
    public R listByIds(@CookieValue String appid,@RequestBody String[] openids){
        List<WxUserVo> users = iWxUserService.listByIds(Arrays.asList(openids));
        return R.ok(users);
    }
    /**
     * 删除用户
     *
     * @param openids 主键串
     */
    @SaCheckPermission("wx:user:remove")
    @Log(title = "用户", businessType = BusinessType.DELETE)
    @DeleteMapping("/{openids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable String[] openids) {
        return toAjax(iWxUserService.deleteWithValidByIds(Arrays.asList(openids), true));
    }

    /**
     * 同步用户列表
     */
    @PostMapping("/syncWxUsers")
    @SaCheckPermission("wx:user:save")
    public R syncWxUsers(@CookieValue String appid) {
        iWxUserService.syncWxUsers(appid);

        return R.ok("任务已建立");
    }
    /**
     * 查询用户标签
     */
    @GetMapping("/getWxTags")
    @SaCheckPermission("wx:user:getWxTags")
    public R getWxTags(@CookieValue String appid) throws WxErrorException {
        List<WxUserTag> wxUserTags =  iWxUserService.getWxTags(appid);
        return R.ok(wxUserTags);
    }

    /**
     * 修改或新增标签
     */
    @PostMapping("/saveTag")
    @SaCheckPermission("wx:user:saveTag")
    public R saveTag(@CookieValue String appid,@RequestBody WxUserTagBo bo) throws WxErrorException{
        Long tagid = bo.getId();
        if(tagid==null || tagid<=0){
            iWxUserService.creatTag(appid,bo.getName());
        }else {
            iWxUserService.updateTag(appid,tagid,bo.getName());
        }
        return R.ok();
    }
    /**
     * 删除标签
     */
    @DeleteMapping("/deleteTag/{tagid}")
    @SaCheckPermission("wx:user:deleteTag")
    public R deleteTag(@CookieValue String appid,@PathVariable("tagid") Long tagid) throws WxErrorException{
        if(tagid==null || tagid<=0){
            return R.fail("标签ID不得为空");
        }
        iWxUserService.deleteTag(appid,tagid);
        return R.ok();
    }
    /**
     * 批量给用户打标签
     */
    @PostMapping("/batchTagging")
    @SaCheckPermission("wx:user:save")
    public R batchTagging(@CookieValue String appid,@RequestBody WxUserBatchTaggingBo bo) throws WxErrorException{
        iWxUserService.batchTagging(appid, bo.getTagid(), bo.getOpenidList());
        return R.ok();
    }
    /**
     * 批量移除用户标签
     */
    @PostMapping("/batchUnTagging")
    @SaCheckPermission("wx:user:save")
    public R batchUnTagging(@CookieValue String appid,@RequestBody WxUserBatchTaggingBo bo) throws WxErrorException{
        iWxUserService.batchUnTagging(appid, bo.getTagid(), bo.getOpenidList());
        return R.ok();
    }
}
