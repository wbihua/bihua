package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.TdSuperTable;
import com.bihua.iot.domain.bo.ProductBo;
import com.bihua.iot.domain.vo.ProductVo;
import com.bihua.iot.service.IProductService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 产品模型
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/iot/product")
public class ProductController extends BaseController {

    private final IProductService iProductService;

    /**
     * 查询产品模型列表
     */
    @SaCheckPermission("iot:product:list")
    @GetMapping("/list")
    public TableDataInfo<ProductVo> list(ProductBo bo, PageQuery pageQuery) {
        return iProductService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出产品模型列表
     */
    @SaCheckPermission("iot:product:export")
    @Log(title = "产品模型", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductBo bo, HttpServletResponse response) {
        List<ProductVo> list = iProductService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品模型", ProductVo.class, response);
    }

    /**
     * 获取产品模型详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:product:detail")
    @GetMapping("/{id}")
    public R<ProductVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductService.queryById(id));
    }

    /**
     * 新增产品模型
     */
    @SaCheckPermission("iot:product:add")
    @Log(title = "产品模型", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductBo bo) {
        return toAjax(iProductService.insertByBo(bo));
    }

    /**
     * 修改产品模型
     */
    @SaCheckPermission("iot:product:edit")
    @Log(title = "产品模型", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductBo bo) {
        return toAjax(iProductService.updateByBo(bo));
    }

    /**
     * 删除产品模型
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:product:remove")
    @Log(title = "产品模型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 初始化数据模型
     *
     * @param productIds      产品ID集合
     * @param initializeOrNot 是否初始化
     * @return
     * @throws Exception
     */
    @SaCheckPermission("iot:product:initialize")
    @Log(title = "产品管理", businessType = BusinessType.OTHER)
    @GetMapping(value = "/initializeDataModel/{productIds}/{initializeOrNot}")
    public R<List<TdSuperTable>> initializeDataModel(@PathVariable("productIds") Long[] productIds, @PathVariable("initializeOrNot") Boolean initializeOrNot) {
        try {
            return R.ok(iProductService.createSuperTableDataModel(productIds, initializeOrNot));
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return R.fail("产品数据异常,请联系管理员");
    }
}
