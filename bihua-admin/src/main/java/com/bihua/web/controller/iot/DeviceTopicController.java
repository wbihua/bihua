package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.DeviceTopicBo;
import com.bihua.iot.domain.vo.DeviceTopicVo;
import com.bihua.iot.service.IDeviceTopicService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 设备Topic数据
 *
 * @author bihua
 * @date 2023-06-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/topic")
public class DeviceTopicController extends BaseController {

    private final IDeviceTopicService iDeviceTopicService;

    /**
     * 查询设备Topic数据列表
     */
    @SaCheckPermission("iot:topic:list")
    @GetMapping("/list")
    public TableDataInfo<DeviceTopicVo> list(DeviceTopicBo bo, PageQuery pageQuery) {
        return iDeviceTopicService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出设备Topic数据列表
     */
    @SaCheckPermission("iot:topic:export")
    @Log(title = "设备Topic数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DeviceTopicBo bo, HttpServletResponse response) {
        List<DeviceTopicVo> list = iDeviceTopicService.queryList(bo);
        ExcelUtil.exportExcel(list, "设备Topic数据", DeviceTopicVo.class, response);
    }

    /**
     * 获取设备Topic数据详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:topic:query")
    @GetMapping("/{id}")
    public R<DeviceTopicVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDeviceTopicService.queryById(id));
    }

    /**
     * 新增设备Topic数据
     */
    @SaCheckPermission("iot:topic:add")
    @Log(title = "设备Topic数据", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DeviceTopicBo bo) {
        return toAjax(iDeviceTopicService.insertByBo(bo));
    }

    /**
     * 修改设备Topic数据
     */
    @SaCheckPermission("iot:topic:edit")
    @Log(title = "设备Topic数据", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DeviceTopicBo bo) {
        return toAjax(iDeviceTopicService.updateByBo(bo));
    }

    /**
     * 删除设备Topic数据
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:topic:remove")
    @Log(title = "设备Topic数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDeviceTopicService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
