package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.CmsArticleBo;
import com.bihua.wx.domain.vo.CmsArticleVo;
import com.bihua.wx.service.ICmsArticleService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * CMS文章中心
 *
 * @author bihua
 * @date 2023-05-24
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/article")
public class CmsArticleController extends BaseController {

    private final ICmsArticleService iCmsArticleService;

    /**
     * 查询CMS文章中心列表
     */
    @SaCheckPermission("wx:article:list")
    @GetMapping("/list")
    public TableDataInfo<CmsArticleVo> list(CmsArticleBo bo, PageQuery pageQuery) {
        return iCmsArticleService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出CMS文章中心列表
     */
    @SaCheckPermission("wx:article:export")
    @Log(title = "CMS文章中心", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsArticleBo bo, HttpServletResponse response) {
        List<CmsArticleVo> list = iCmsArticleService.queryList(bo);
        ExcelUtil.exportExcel(list, "CMS文章中心", CmsArticleVo.class, response);
    }

    /**
     * 获取CMS文章中心详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wx:article:query")
    @GetMapping("/{id}")
    public R<CmsArticleVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCmsArticleService.queryById(id));
    }

    /**
     * 新增CMS文章中心
     */
    @SaCheckPermission("wx:article:add")
    @Log(title = "CMS文章中心", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsArticleBo bo) {
        return toAjax(iCmsArticleService.insertByBo(bo));
    }

    /**
     * 修改CMS文章中心
     */
    @SaCheckPermission("wx:article:edit")
    @Log(title = "CMS文章中心", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsArticleBo bo) {
        return toAjax(iCmsArticleService.updateByBo(bo));
    }

    /**
     * 删除CMS文章中心
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wx:article:remove")
    @Log(title = "CMS文章中心", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCmsArticleService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
