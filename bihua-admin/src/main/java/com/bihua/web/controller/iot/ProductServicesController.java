package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProductServicesBo;
import com.bihua.iot.domain.vo.DeviceVo;
import com.bihua.iot.domain.vo.ProductServicesVo;
import com.bihua.iot.service.IDeviceService;
import com.bihua.iot.service.IProductServicesService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 产品服务
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/services")
public class ProductServicesController extends BaseController {

    private final IProductServicesService iProductServicesService;
    private final IDeviceService iDeviceService;
    /**
     * 查询产品服务列表
     */
    @SaCheckPermission("iot:services:list")
    @GetMapping("/list")
    public TableDataInfo<ProductServicesVo> list(ProductServicesBo bo, PageQuery pageQuery) {
        return iProductServicesService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询产品服务列表
     */
    @SaCheckPermission("iot:services:list")
    @GetMapping("/queryProductServices/{deviceIdentification}")
    public R<List<ProductServicesVo>> queryProductServices(@NotNull(message = "设备标识不能为空")
                                               @PathVariable String deviceIdentification) {
        DeviceVo deviceVo = iDeviceService.findOneByDeviceIdentification(deviceIdentification);
        ProductServicesBo bo = new ProductServicesBo();
        bo.setProductIdentification(deviceVo.getProductIdentification());
        return R.ok(iProductServicesService.queryList(bo));
    }

    /**
     * 导出产品服务列表
     */
    @SaCheckPermission("iot:services:export")
    @Log(title = "产品服务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductServicesBo bo, HttpServletResponse response) {
        List<ProductServicesVo> list = iProductServicesService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品服务", ProductServicesVo.class, response);
    }

    /**
     * 获取产品服务详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:services:query")
    @GetMapping("/{id}")
    public R<ProductServicesVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductServicesService.queryById(id));
    }

    /**
     * 新增产品服务
     */
    @SaCheckPermission("iot:services:add")
    @Log(title = "产品服务", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductServicesBo bo) {
        return toAjax(iProductServicesService.insertByBo(bo));
    }

    /**
     * 修改产品服务
     */
    @SaCheckPermission("iot:services:edit")
    @Log(title = "产品服务", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductServicesBo bo) {
        return toAjax(iProductServicesService.updateByBo(bo));
    }

    /**
     * 删除产品服务
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:services:remove")
    @Log(title = "产品服务", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductServicesService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
