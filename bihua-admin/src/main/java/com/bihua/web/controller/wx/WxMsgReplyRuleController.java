package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxMsgReplyRuleBo;
import com.bihua.wx.domain.vo.WxMsgReplyRuleVo;
import com.bihua.wx.service.IWxMsgReplyRuleService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 自动回复规则
 *
 * @author ruoyi
 * @date 2023-05-22
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/msgReplyRule")
public class WxMsgReplyRuleController extends BaseController {

    private final IWxMsgReplyRuleService iWxMsgReplyRuleService;

    /**
     * 查询自动回复规则列表
     */
    @SaCheckPermission("wx:msgReplyRule:list")
    @GetMapping("/list")
    public TableDataInfo<WxMsgReplyRuleVo> list(WxMsgReplyRuleBo bo, PageQuery pageQuery) {
        return iWxMsgReplyRuleService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出自动回复规则列表
     */
    @SaCheckPermission("wx:msgReplyRule:export")
    @Log(title = "自动回复规则", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxMsgReplyRuleBo bo, HttpServletResponse response) {
        List<WxMsgReplyRuleVo> list = iWxMsgReplyRuleService.queryList(bo);
        ExcelUtil.exportExcel(list, "自动回复规则", WxMsgReplyRuleVo.class, response);
    }

    /**
     * 获取自动回复规则详细信息
     *
     * @param ruleId 主键
     */
    @SaCheckPermission("wx:msgReplyRule:query")
    @GetMapping("/{ruleId}")
    public R<WxMsgReplyRuleVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long ruleId) {
        return R.ok(iWxMsgReplyRuleService.queryById(ruleId));
    }

    /**
     * 新增自动回复规则
     */
    @SaCheckPermission("wx:msgReplyRule:add")
    @Log(title = "自动回复规则", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WxMsgReplyRuleBo bo) {
        return toAjax(iWxMsgReplyRuleService.insertByBo(bo));
    }

    /**
     * 修改自动回复规则
     */
    @SaCheckPermission("wx:msgReplyRule:edit")
    @Log(title = "自动回复规则", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WxMsgReplyRuleBo bo) {
        return toAjax(iWxMsgReplyRuleService.updateByBo(bo));
    }

    /**
     * 删除自动回复规则
     *
     * @param ruleIds 主键串
     */
    @SaCheckPermission("wx:msgReplyRule:remove")
    @Log(title = "自动回复规则", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ruleIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ruleIds) {
        return toAjax(iWxMsgReplyRuleService.deleteWithValidByIds(Arrays.asList(ruleIds), true));
    }
}
