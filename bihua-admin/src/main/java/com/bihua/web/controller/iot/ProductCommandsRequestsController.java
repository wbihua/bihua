package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProductCommandsRequestsBo;
import com.bihua.iot.domain.vo.ProductCommandsRequestsVo;
import com.bihua.iot.service.IProductCommandsRequestsService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 产品设备下发服务命令属性
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/commandsRequests")
public class ProductCommandsRequestsController extends BaseController {

    private final IProductCommandsRequestsService iProductCommandsRequestsService;

    /**
     * 查询产品设备下发服务命令属性列表
     */
    @SaCheckPermission("iot:commandsRequests:list")
    @GetMapping("/list")
    public TableDataInfo<ProductCommandsRequestsVo> list(ProductCommandsRequestsBo bo, PageQuery pageQuery) {
        return iProductCommandsRequestsService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出产品设备下发服务命令属性列表
     */
    @SaCheckPermission("iot:commandsRequests:export")
    @Log(title = "产品设备下发服务命令属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductCommandsRequestsBo bo, HttpServletResponse response) {
        List<ProductCommandsRequestsVo> list = iProductCommandsRequestsService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品设备下发服务命令属性", ProductCommandsRequestsVo.class, response);
    }

    /**
     * 获取产品设备下发服务命令属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:commandsRequests:query")
    @GetMapping("/{id}")
    public R<ProductCommandsRequestsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductCommandsRequestsService.queryById(id));
    }

    /**
     * 新增产品设备下发服务命令属性
     */
    @SaCheckPermission("iot:commandsRequests:add")
    @Log(title = "产品设备下发服务命令属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductCommandsRequestsBo bo) {
        return toAjax(iProductCommandsRequestsService.insertByBo(bo));
    }

    /**
     * 修改产品设备下发服务命令属性
     */
    @SaCheckPermission("iot:commandsRequests:edit")
    @Log(title = "产品设备下发服务命令属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductCommandsRequestsBo bo) {
        return toAjax(iProductCommandsRequestsService.updateByBo(bo));
    }

    /**
     * 删除产品设备下发服务命令属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:commandsRequests:remove")
    @Log(title = "产品设备下发服务命令属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductCommandsRequestsService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
