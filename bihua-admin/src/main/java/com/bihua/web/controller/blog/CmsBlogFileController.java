package com.bihua.web.controller.blog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.CmsBlogFile;
import com.bihua.blog.domain.bo.CmsBlogFileBo;
import com.bihua.blog.domain.vo.CmsBlogFileVo;
import com.bihua.blog.service.ICmsBlogFileService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.system.domain.vo.SysOssVo;
import com.bihua.system.service.ISysOssService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * blog文件
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blogFile")
public class CmsBlogFileController extends BaseController {

    private final ICmsBlogFileService iCmsBlogFileService;

    private final ISysOssService iSysOssService;
    /**
     * 查询blog文件列表
     */
    @SaCheckPermission("blog:blogFile:list")
    @GetMapping("/list")
    public TableDataInfo<CmsBlogFileVo> list(CmsBlogFileBo bo, PageQuery pageQuery) {
        return iCmsBlogFileService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出blog文件列表
     */
    @SaCheckPermission("blog:blogFile:export")
    @Log(title = "blog文件", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsBlogFileBo bo, HttpServletResponse response) {
        List<CmsBlogFileVo> list = iCmsBlogFileService.queryList(bo);
        ExcelUtil.exportExcel(list, "blog文件", CmsBlogFileVo.class, response);
    }

    /**
     * 获取blog文件详细信息
     *
     * @param fileId 主键
     */
    @SaCheckPermission("blog:blogFile:query")
    @GetMapping("/{fileId}")
    public R<CmsBlogFileVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long fileId) {
        return R.ok(iCmsBlogFileService.queryById(fileId));
    }
    /**
     * 通过blogId获取blog文件列表
     *
     * @param blogId 主键
     */
    @SaCheckPermission("blog:blogFile:query")
    @GetMapping("/getFileListByBlogId/{blogId}")
    public R<List<SysOssVo>> getFileListByBlogId(@NotNull(message = "主键不能为空")
                                    @PathVariable Long blogId) {
        CmsBlogFileBo bo = new CmsBlogFileBo();
        bo.setBlogId(blogId);
        List<CmsBlogFileVo> vos = iCmsBlogFileService.queryList(bo);
        List<Long> fileIds = new ArrayList<>();
        vos.forEach(cmsBlogFileVo -> fileIds.add(cmsBlogFileVo.getFileId()));
        return R.ok(iSysOssService.listByIds(fileIds));
    }
    /**
     * 新增blog文件
     */
    @SaCheckPermission("blog:blogFile:add")
    @Log(title = "blog文件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsBlogFileBo bo) {
        return toAjax(iCmsBlogFileService.insertByBo(bo));
    }
    /**
     * 批量新增blog文件
     */
    @SaCheckPermission("blog:blogFile:add")
    @Log(title = "批量新增blog文件", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping("/addBatch")
    public R<Void> addBatch(@Validated(AddGroup.class) @RequestBody CmsBlogFileBo bo) {
        List<CmsBlogFile> cmsBlogFiles = new ArrayList<>();
        for (Long fileId : bo.getFileIds()) {
            CmsBlogFile cmsBlogFile = new CmsBlogFile();
            cmsBlogFile.setBlogId(bo.getBlogId());
            cmsBlogFile.setFileId(fileId);
            cmsBlogFiles.add(cmsBlogFile);
        }
        return toAjax(iCmsBlogFileService.insertBatch(cmsBlogFiles));
    }
    /**
     * 修改blog文件
     */
    @SaCheckPermission("blog:blogFile:edit")
    @Log(title = "blog文件", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsBlogFileBo bo) {
        return toAjax(iCmsBlogFileService.updateByBo(bo));
    }

    /**
     * 删除blog文件
     *
     * @param fileIds 主键串
     */
    @SaCheckPermission("blog:blogFile:remove")
    @Log(title = "blog文件", businessType = BusinessType.DELETE)
    @DeleteMapping("/{fileIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] fileIds) {
        return toAjax(iCmsBlogFileService.deleteWithValidByIds(Arrays.asList(fileIds), true));
    }
}
