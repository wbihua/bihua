package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsCommentLikeBo;
import com.bihua.blog.domain.vo.CmsCommentLikeVo;
import com.bihua.blog.service.ICmsCommentLikeService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 留言点赞
 *
 * @author bihua
 * @date 2023-06-05
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/commentLike")
public class CmsCommentLikeController extends BaseController {

    private final ICmsCommentLikeService iCmsCommentLikeService;

    /**
     * 查询留言点赞列表
     */
    @SaCheckPermission("blog:commentLike:list")
    @GetMapping("/list")
    public TableDataInfo<CmsCommentLikeVo> list(CmsCommentLikeBo bo, PageQuery pageQuery) {
        return iCmsCommentLikeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出留言点赞列表
     */
    @SaCheckPermission("blog:commentLike:export")
    @Log(title = "留言点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsCommentLikeBo bo, HttpServletResponse response) {
        List<CmsCommentLikeVo> list = iCmsCommentLikeService.queryList(bo);
        ExcelUtil.exportExcel(list, "留言点赞", CmsCommentLikeVo.class, response);
    }

    /**
     * 获取留言点赞详细信息
     *
     * @param commentId 主键
     */
    @SaCheckPermission("blog:commentLike:query")
    @GetMapping("/{commentId}")
    public R<CmsCommentLikeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long commentId) {
        return R.ok(iCmsCommentLikeService.queryById(commentId));
    }

    /**
     * 新增留言点赞
     */
    @SaCheckPermission("blog:commentLike:add")
    @Log(title = "留言点赞", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsCommentLikeBo bo) {
        return toAjax(iCmsCommentLikeService.insertByBo(bo));
    }

    /**
     * 修改留言点赞
     */
    @SaCheckPermission("blog:commentLike:edit")
    @Log(title = "留言点赞", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsCommentLikeBo bo) {
        return toAjax(iCmsCommentLikeService.updateByBo(bo));
    }

    /**
     * 删除留言点赞
     *
     * @param commentIds 主键串
     */
    @SaCheckPermission("blog:commentLike:remove")
    @Log(title = "留言点赞", businessType = BusinessType.DELETE)
    @DeleteMapping("/{commentIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] commentIds) {
        return toAjax(iCmsCommentLikeService.deleteWithValidByIds(Arrays.asList(commentIds), true));
    }
}
