package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsMessageLikeBo;
import com.bihua.blog.domain.vo.CmsMessageLikeVo;
import com.bihua.blog.service.ICmsMessageLikeService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 留言点赞
 *
 * @author bihua
 * @date 2023-06-07
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/messageLike")
public class CmsMessageLikeController extends BaseController {

    private final ICmsMessageLikeService iCmsMessageLikeService;

    /**
     * 查询留言点赞列表
     */
    @SaCheckPermission("blog:messageLike:list")
    @GetMapping("/list")
    public TableDataInfo<CmsMessageLikeVo> list(CmsMessageLikeBo bo, PageQuery pageQuery) {
        return iCmsMessageLikeService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出留言点赞列表
     */
    @SaCheckPermission("blog:messageLike:export")
    @Log(title = "留言点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsMessageLikeBo bo, HttpServletResponse response) {
        List<CmsMessageLikeVo> list = iCmsMessageLikeService.queryList(bo);
        ExcelUtil.exportExcel(list, "留言点赞", CmsMessageLikeVo.class, response);
    }

    /**
     * 获取留言点赞详细信息
     *
     * @param messageId 主键
     */
    @SaCheckPermission("blog:messageLike:query")
    @GetMapping("/{messageId}")
    public R<CmsMessageLikeVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long messageId) {
        return R.ok(iCmsMessageLikeService.queryById(messageId));
    }

    /**
     * 新增留言点赞
     */
    @SaCheckPermission("blog:messageLike:add")
    @Log(title = "留言点赞", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsMessageLikeBo bo) {
        return toAjax(iCmsMessageLikeService.insertByBo(bo));
    }

    /**
     * 修改留言点赞
     */
    @SaCheckPermission("blog:messageLike:edit")
    @Log(title = "留言点赞", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsMessageLikeBo bo) {
        return toAjax(iCmsMessageLikeService.updateByBo(bo));
    }

    /**
     * 删除留言点赞
     *
     * @param messageIds 主键串
     */
    @SaCheckPermission("blog:messageLike:remove")
    @Log(title = "留言点赞", businessType = BusinessType.DELETE)
    @DeleteMapping("/{messageIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] messageIds) {
        return toAjax(iCmsMessageLikeService.deleteWithValidByIds(Arrays.asList(messageIds), true));
    }
}
