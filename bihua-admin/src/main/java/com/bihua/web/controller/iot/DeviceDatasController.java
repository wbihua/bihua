package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.DeviceDatasBo;
import com.bihua.iot.domain.vo.DeviceDatasVo;
import com.bihua.iot.service.IDeviceDatasService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 设备消息
 *
 * @author bihua
 * @date 2023-06-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/datas")
public class DeviceDatasController extends BaseController {

    private final IDeviceDatasService iDeviceDatasService;

    /**
     * 查询设备消息列表
     */
    @SaCheckPermission("iot:datas:list")
    @GetMapping("/list")
    public TableDataInfo<DeviceDatasVo> list(DeviceDatasBo bo, PageQuery pageQuery) {
        return iDeviceDatasService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出设备消息列表
     */
    @SaCheckPermission("iot:datas:export")
    @Log(title = "设备消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DeviceDatasBo bo, HttpServletResponse response) {
        List<DeviceDatasVo> list = iDeviceDatasService.queryList(bo);
        ExcelUtil.exportExcel(list, "设备消息", DeviceDatasVo.class, response);
    }

    /**
     * 获取设备消息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:datas:query")
    @GetMapping("/{id}")
    public R<DeviceDatasVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDeviceDatasService.queryById(id));
    }

    /**
     * 新增设备消息
     */
    @SaCheckPermission("iot:datas:add")
    @Log(title = "设备消息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DeviceDatasBo bo) {
        return toAjax(iDeviceDatasService.insertByBo(bo));
    }

    /**
     * 修改设备消息
     */
    @SaCheckPermission("iot:datas:edit")
    @Log(title = "设备消息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DeviceDatasBo bo) {
        return toAjax(iDeviceDatasService.updateByBo(bo));
    }

    /**
     * 删除设备消息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:datas:remove")
    @Log(title = "设备消息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDeviceDatasService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
