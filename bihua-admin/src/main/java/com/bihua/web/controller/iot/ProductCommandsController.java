package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProductCommandsBo;
import com.bihua.iot.domain.vo.ProductCommandsVo;
import com.bihua.iot.service.IProductCommandsService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 产品设备服务命令
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/commands")
public class ProductCommandsController extends BaseController {

    private final IProductCommandsService iProductCommandsService;

    /**
     * 查询产品设备服务命令列表
     */
    @SaCheckPermission("iot:commands:list")
    @GetMapping("/list")
    public TableDataInfo<ProductCommandsVo> list(ProductCommandsBo bo, PageQuery pageQuery) {
        return iProductCommandsService.queryPageList(bo, pageQuery);
    }

    /**
     * 查询产品设备服务命令列表
     */
    @SaCheckPermission("iot:commands:query")
    @GetMapping("/queryProductCommands/{serviceId}")
    public R<List<ProductCommandsVo>> queryProductCommands(@NotNull(message = "设备标识不能为空")
                                                     @PathVariable Long serviceId) {
        ProductCommandsBo bo = new ProductCommandsBo();
        bo.setServiceId(serviceId);
        return R.ok(iProductCommandsService.queryList(bo));
    }
    /**
     * 导出产品设备服务命令列表
     */
    @SaCheckPermission("iot:commands:export")
    @Log(title = "产品设备服务命令", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductCommandsBo bo, HttpServletResponse response) {
        List<ProductCommandsVo> list = iProductCommandsService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品设备服务命令", ProductCommandsVo.class, response);
    }

    /**
     * 获取产品设备服务命令详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:commands:detail")
    @GetMapping("/{id}")
    public R<ProductCommandsVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductCommandsService.queryById(id));
    }

    /**
     * 新增产品设备服务命令
     */
    @SaCheckPermission("iot:commands:add")
    @Log(title = "产品设备服务命令", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductCommandsBo bo) {
        return toAjax(iProductCommandsService.insertByBo(bo));
    }

    /**
     * 修改产品设备服务命令
     */
    @SaCheckPermission("iot:commands:edit")
    @Log(title = "产品设备服务命令", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductCommandsBo bo) {
        return toAjax(iProductCommandsService.updateByBo(bo));
    }

    /**
     * 删除产品设备服务命令
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:commands:remove")
    @Log(title = "产品设备服务命令", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductCommandsService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
