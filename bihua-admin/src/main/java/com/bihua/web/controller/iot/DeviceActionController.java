package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.DeviceActionBo;
import com.bihua.iot.domain.vo.DeviceActionVo;
import com.bihua.iot.service.IDeviceActionService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 设备动作数据
 *
 * @author bihua
 * @date 2023-06-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/action")
public class DeviceActionController extends BaseController {

    private final IDeviceActionService iDeviceActionService;

    /**
     * 查询设备动作数据列表
     */
    @SaCheckPermission("iot:action:list")
    @GetMapping("/list")
    public TableDataInfo<DeviceActionVo> list(DeviceActionBo bo, PageQuery pageQuery) {
        return iDeviceActionService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出设备动作数据列表
     */
    @SaCheckPermission("iot:action:export")
    @Log(title = "设备动作数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DeviceActionBo bo, HttpServletResponse response) {
        List<DeviceActionVo> list = iDeviceActionService.queryList(bo);
        ExcelUtil.exportExcel(list, "设备动作数据", DeviceActionVo.class, response);
    }

    /**
     * 获取设备动作数据详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:action:query")
    @GetMapping("/{id}")
    public R<DeviceActionVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDeviceActionService.queryById(id));
    }

    /**
     * 新增设备动作数据
     */
    @SaCheckPermission("iot:action:add")
    @Log(title = "设备动作数据", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DeviceActionBo bo) {
        return toAjax(iDeviceActionService.insertByBo(bo));
    }

    /**
     * 修改设备动作数据
     */
    @SaCheckPermission("iot:action:edit")
    @Log(title = "设备动作数据", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DeviceActionBo bo) {
        return toAjax(iDeviceActionService.updateByBo(bo));
    }

    /**
     * 删除设备动作数据
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:action:remove")
    @Log(title = "设备动作数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDeviceActionService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
