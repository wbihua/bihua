package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsBlogTagBo;
import com.bihua.blog.domain.vo.CmsBlogTagVo;
import com.bihua.blog.service.ICmsBlogTagService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * blog标签关联
 *
 * @author bihua
 * @date 2023-05-30
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blogTag")
public class CmsBlogTagController extends BaseController {

    private final ICmsBlogTagService iCmsBlogTagService;

    /**
     * 查询blog标签关联列表
     */
    @SaCheckPermission("blog:blogTag:list")
    @GetMapping("/list")
    public TableDataInfo<CmsBlogTagVo> list(CmsBlogTagBo bo, PageQuery pageQuery) {
        return iCmsBlogTagService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出blog标签关联列表
     */
    @SaCheckPermission("blog:blogTag:export")
    @Log(title = "blog标签关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsBlogTagBo bo, HttpServletResponse response) {
        List<CmsBlogTagVo> list = iCmsBlogTagService.queryList(bo);
        ExcelUtil.exportExcel(list, "blog标签关联", CmsBlogTagVo.class, response);
    }

    /**
     * 获取blog标签关联详细信息
     *
     * @param tagId 主键
     */
    @SaCheckPermission("blog:blogTag:query")
    @GetMapping("/{tagId}")
    public R<CmsBlogTagVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long tagId) {
        return R.ok(iCmsBlogTagService.queryById(tagId));
    }

    /**
     * 新增blog标签关联
     */
    @SaCheckPermission("blog:blogTag:add")
    @Log(title = "blog标签关联", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsBlogTagBo bo) {
        return toAjax(iCmsBlogTagService.insertByBo(bo));
    }

    /**
     * 修改blog标签关联
     */
    @SaCheckPermission("blog:blogTag:edit")
    @Log(title = "blog标签关联", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsBlogTagBo bo) {
        return toAjax(iCmsBlogTagService.updateByBo(bo));
    }

    /**
     * 删除blog标签关联
     *
     * @param tagIds 主键串
     */
    @SaCheckPermission("blog:blogTag:remove")
    @Log(title = "blog标签关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{tagIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] tagIds) {
        return toAjax(iCmsBlogTagService.deleteWithValidByIds(Arrays.asList(tagIds), true));
    }
}
