package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsCommentBo;
import com.bihua.blog.domain.vo.CmsCommentVo;
import com.bihua.blog.service.ICmsCommentService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.constant.UserConstants;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 评论
 *
 * @author bihua
 * @date 2023-06-05
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/comment")
public class CmsCommentController extends BaseController {

    private final ICmsCommentService iCmsCommentService;

    /**
     * 查询评论列表
     */
    @SaCheckPermission("blog:comment:list")
    @GetMapping("/list")
    public TableDataInfo<CmsCommentVo> list(CmsCommentBo bo, PageQuery pageQuery) {
        // 角色集合
        Set<String> roles = getLoginUser().getRolePermission();
        if (!UserConstants.ADMIN_ID.equals(getUserId())
            && !roles.contains("admin")
            && !roles.contains("cms")){
            bo.setCreateBy(getUsername());
        }
        return iCmsCommentService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出评论列表
     */
    @SaCheckPermission("blog:comment:export")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsCommentBo bo, HttpServletResponse response) {
        List<CmsCommentVo> list = iCmsCommentService.queryList(bo);
        ExcelUtil.exportExcel(list, "评论", CmsCommentVo.class, response);
    }

    /**
     * 获取评论详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("blog:comment:query")
    @GetMapping("/{id}")
    public R<CmsCommentVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCmsCommentService.queryById(id));
    }

    /**
     * 新增评论
     */
    @SaCheckPermission("blog:comment:add")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsCommentBo bo) {
        bo.setUserId(getUserId());
        return toAjax(iCmsCommentService.insertByBo(bo));
    }

    /**
     * 修改评论
     */
    @SaCheckPermission("blog:comment:edit")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsCommentBo bo) {
        return toAjax(iCmsCommentService.updateByBo(bo));
    }

    /**
     * 删除评论
     *
     * @param ids 主键串
     */
    @SaCheckPermission("blog:comment:remove")
    @Log(title = "评论", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCmsCommentService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
