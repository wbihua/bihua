package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import cn.dev33.satoken.secure.BCrypt;
import com.bihua.blog.domain.BlogUser;
import com.bihua.common.enums.UserType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.BlogUserBo;
import com.bihua.blog.domain.vo.BlogUserVo;
import com.bihua.blog.service.IBlogUserService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 用户信息
 *
 * @author bihua
 * @date 2023-05-29
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/blogUser")
public class BlogUserController extends BaseController {

    private final IBlogUserService iBlogUserService;

    /**
     * 查询用户信息列表
     */
    @SaCheckPermission("blog:blogUser:list")
    @GetMapping("/list")
    public TableDataInfo<BlogUserVo> list(BlogUserBo bo, PageQuery pageQuery) {
        return iBlogUserService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出用户信息列表
     */
    @SaCheckPermission("blog:blogUser:export")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(BlogUserBo bo, HttpServletResponse response) {
        List<BlogUserVo> list = iBlogUserService.queryList(bo);
        ExcelUtil.exportExcel(list, "用户信息", BlogUserVo.class, response);
    }

    /**
     * 获取用户信息详细信息
     *
     * @param userId 主键
     */
    @SaCheckPermission("blog:blogUser:query")
    @GetMapping("/{userId}")
    public R<BlogUserVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long userId) {
        return R.ok(iBlogUserService.queryById(userId));
    }

    /**
     * 新增用户信息
     */
    @SaCheckPermission("blog:blogUser:add")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody BlogUserBo bo) {
        bo.setUserType(UserType.APP_USER.getUserType());
        bo.setStatus("0");
        return toAjax(iBlogUserService.insertByBo(bo));
    }

    /**
     * 修改用户信息
     */
    @SaCheckPermission("blog:blogUser:edit")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody BlogUserBo bo) {
        return toAjax(iBlogUserService.updateByBo(bo));
    }

    /**
     * 删除用户信息
     *
     * @param userIds 主键串
     */
    @SaCheckPermission("blog:blogUser:remove")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{userIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] userIds) {
        return toAjax(iBlogUserService.deleteWithValidByIds(Arrays.asList(userIds), true));
    }

    /**
     * 状态修改
     */
    @SaCheckPermission("blog:blogUser:changeStatus")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public R<Void> changeStatus(@RequestBody BlogUser user) {
        return toAjax(iBlogUserService.updateUserStatus(user));
    }
    /**
     * 重置密码
     */
    @SaCheckPermission("blog:blogUser:resetPwd")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public R<Void> resetPwd(@RequestBody BlogUser user) {
        user.setPassword(BCrypt.hashpw(user.getPassword()));
        return toAjax(iBlogUserService.resetPwd(user));
    }
}
