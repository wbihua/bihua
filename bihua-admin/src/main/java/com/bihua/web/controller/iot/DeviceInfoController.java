package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.DeviceInfoBo;
import com.bihua.iot.domain.vo.DeviceInfoVo;
import com.bihua.iot.service.IDeviceInfoService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 子设备档案
 *
 * @author bihua
 * @date 2023-06-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/info")
public class DeviceInfoController extends BaseController {

    private final IDeviceInfoService iDeviceInfoService;

    /**
     * 查询子设备档案列表
     */
    @SaCheckPermission("iot:info:list")
    @GetMapping("/list")
    public TableDataInfo<DeviceInfoVo> list(DeviceInfoBo bo, PageQuery pageQuery) {
        return iDeviceInfoService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出子设备档案列表
     */
    @SaCheckPermission("iot:info:export")
    @Log(title = "子设备管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(DeviceInfoBo bo, HttpServletResponse response) {
        List<DeviceInfoVo> list = iDeviceInfoService.queryList(bo);
        ExcelUtil.exportExcel(list, "子设备档案", DeviceInfoVo.class, response);
    }

    /**
     * 获取子设备档案详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:info:query")
    @GetMapping("/{id}")
    public R<DeviceInfoVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iDeviceInfoService.queryById(id));
    }

    /**
     * 新增子设备档案
     */
    @SaCheckPermission("iot:info:add")
    @Log(title = "子设备管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody DeviceInfoBo bo) {
        return toAjax(iDeviceInfoService.insertByBo(bo));
    }

    /**
     * 修改子设备档案
     */
    @SaCheckPermission("iot:info:edit")
    @Log(title = "子设备管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody DeviceInfoBo bo) {
        return toAjax(iDeviceInfoService.updateByBo(bo));
    }

    /**
     * 删除子设备档案
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:info:remove")
    @Log(title = "子设备管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iDeviceInfoService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 刷新子设备数据模型
     * @param ids
     * @return
     */
    @SaCheckPermission("iot:info:initialize")
    @Log(title = "子设备管理", businessType = BusinessType.OTHER)
    @GetMapping("/refreshDeviceInfoDataModel/{ids}")
    public R<Void> refreshDeviceInfoDataModel(@PathVariable("ids") Long[] ids)
    {
        return toAjax(iDeviceInfoService.refreshDeviceInfoDataModel(Arrays.asList(ids)));
    }
}
