package com.bihua.web.controller.blog;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.blog.domain.bo.CmsMessageBo;
import com.bihua.blog.domain.vo.CmsMessageVo;
import com.bihua.blog.service.ICmsMessageService;
import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.constant.UserConstants;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 留言
 *
 * @author bihua
 * @date 2023-06-07
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/blog/message")
public class CmsMessageController extends BaseController {

    private final ICmsMessageService iCmsMessageService;

    /**
     * 查询留言列表
     */
    @SaCheckPermission("blog:message:list")
    @GetMapping("/list")
    public TableDataInfo<CmsMessageVo> list(CmsMessageBo bo, PageQuery pageQuery) {
        // 角色集合
        Set<String> roles = getLoginUser().getRolePermission();
        if (!UserConstants.ADMIN_ID.equals(getUserId())
            && !roles.contains("admin")
            && !roles.contains("cms")){
            bo.setCreateBy(getUsername());
        }
        return iCmsMessageService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出留言列表
     */
    @SaCheckPermission("blog:message:export")
    @Log(title = "留言", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(CmsMessageBo bo, HttpServletResponse response) {
        List<CmsMessageVo> list = iCmsMessageService.queryList(bo);
        ExcelUtil.exportExcel(list, "留言", CmsMessageVo.class, response);
    }

    /**
     * 获取留言详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("blog:message:query")
    @GetMapping("/{id}")
    public R<CmsMessageVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iCmsMessageService.queryById(id));
    }

    /**
     * 新增留言
     */
    @SaCheckPermission("blog:message:add")
    @Log(title = "留言", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody CmsMessageBo bo) {
        bo.setUserId(getUserId());
        return toAjax(iCmsMessageService.insertByBo(bo));
    }

    /**
     * 修改留言
     */
    @SaCheckPermission("blog:message:edit")
    @Log(title = "留言", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody CmsMessageBo bo) {
        return toAjax(iCmsMessageService.updateByBo(bo));
    }

    /**
     * 删除留言
     *
     * @param ids 主键串
     */
    @SaCheckPermission("blog:message:remove")
    @Log(title = "留言", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iCmsMessageService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
