package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProductPropertiesBo;
import com.bihua.iot.domain.vo.ProductPropertiesVo;
import com.bihua.iot.service.IProductPropertiesService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 产品服务属性
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/properties")
public class ProductPropertiesController extends BaseController {

    private final IProductPropertiesService iProductPropertiesService;

    /**
     * 查询产品服务属性列表
     */
    @SaCheckPermission("iot:properties:list")
    @GetMapping("/list")
    public TableDataInfo<ProductPropertiesVo> list(ProductPropertiesBo bo, PageQuery pageQuery) {
        return iProductPropertiesService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出产品服务属性列表
     */
    @SaCheckPermission("iot:properties:export")
    @Log(title = "产品服务属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductPropertiesBo bo, HttpServletResponse response) {
        List<ProductPropertiesVo> list = iProductPropertiesService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品服务属性", ProductPropertiesVo.class, response);
    }

    /**
     * 获取产品服务属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:properties:query")
    @GetMapping("/{id}")
    public R<ProductPropertiesVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductPropertiesService.queryById(id));
    }

    /**
     * 新增产品服务属性
     */
    @SaCheckPermission("iot:properties:add")
    @Log(title = "产品服务属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductPropertiesBo bo) {
        return toAjax(iProductPropertiesService.insertByBo(bo));
    }

    /**
     * 修改产品服务属性
     */
    @SaCheckPermission("iot:properties:edit")
    @Log(title = "产品服务属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductPropertiesBo bo) {
        return toAjax(iProductPropertiesService.updateByBo(bo));
    }

    /**
     * 删除产品服务属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:properties:remove")
    @Log(title = "产品服务属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductPropertiesService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
