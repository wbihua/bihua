package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProductCommandsResponseBo;
import com.bihua.iot.domain.vo.ProductCommandsResponseVo;
import com.bihua.iot.service.IProductCommandsResponseService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 产品设备响应服务命令属性
 *
 * @author bihua
 * @date 2023-06-15
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/commandsResponse")
public class ProductCommandsResponseController extends BaseController {

    private final IProductCommandsResponseService iProductCommandsResponseService;

    /**
     * 查询产品设备响应服务命令属性列表
     */
    @SaCheckPermission("iot:commandsResponse:list")
    @GetMapping("/list")
    public TableDataInfo<ProductCommandsResponseVo> list(ProductCommandsResponseBo bo, PageQuery pageQuery) {
        return iProductCommandsResponseService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出产品设备响应服务命令属性列表
     */
    @SaCheckPermission("iot:commandsResponse:export")
    @Log(title = "产品设备响应服务命令属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProductCommandsResponseBo bo, HttpServletResponse response) {
        List<ProductCommandsResponseVo> list = iProductCommandsResponseService.queryList(bo);
        ExcelUtil.exportExcel(list, "产品设备响应服务命令属性", ProductCommandsResponseVo.class, response);
    }

    /**
     * 获取产品设备响应服务命令属性详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:commandsResponse:query")
    @GetMapping("/{id}")
    public R<ProductCommandsResponseVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProductCommandsResponseService.queryById(id));
    }

    /**
     * 新增产品设备响应服务命令属性
     */
    @SaCheckPermission("iot:commandsResponse:add")
    @Log(title = "产品设备响应服务命令属性", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ProductCommandsResponseBo bo) {
        return toAjax(iProductCommandsResponseService.insertByBo(bo));
    }

    /**
     * 修改产品设备响应服务命令属性
     */
    @SaCheckPermission("iot:commandsResponse:edit")
    @Log(title = "产品设备响应服务命令属性", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProductCommandsResponseBo bo) {
        return toAjax(iProductCommandsResponseService.updateByBo(bo));
    }

    /**
     * 删除产品设备响应服务命令属性
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:commandsResponse:remove")
    @Log(title = "产品设备响应服务命令属性", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProductCommandsResponseService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
}
