package com.bihua.web.controller.iot;

import java.util.List;
import java.util.Map;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.R;
import com.bihua.iot.service.IDeviceInfoService;
import com.bihua.iot.service.IDeviceService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * @author bihua
 * @date 2023年06月18日 11:19
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/shadow")
public class DeviceShadowController extends BaseController {
    private final IDeviceService iDeviceService;
    private final IDeviceInfoService iDeviceInfoService;

    /**
     * 查询普通设备影子数据
     * @param params
     * @return
     */
    @SaCheckPermission("iot:shadow:device")
    @PostMapping(value = "/getDeviceShadow")
    public R<Map<String, List<Map<String, Object>>>> getDeviceShadow(@RequestBody Map<String, Object> params)
    {
        final Object ids = params.get("ids");
        final Object startTime = params.get("startTime");
        final Object endTime = params.get("endTime");
        return R.ok(iDeviceService.getDeviceShadow(ids.toString(), startTime.toString(), endTime.toString()));
    }

    /**
     * 查询子设备影子数据
     * @param params
     * @return
     */
    @SaCheckPermission("iot:shadow:deviceInfo")
    @PostMapping(value = "/getDeviceInfoShadow")
    public R<Map<String, List<Map<String, Object>>>> getDeviceInfoShadow(@RequestBody Map<String, Object> params)
    {
        final Object ids = params.get("ids");
        final Object startTime = params.get("startTime");
        final Object endTime = params.get("endTime");
        return R.ok(iDeviceInfoService.getDeviceInfoShadow(ids.toString(), startTime.toString(), endTime.toString()));
    }
}
