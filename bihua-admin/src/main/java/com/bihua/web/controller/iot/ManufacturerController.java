package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.BeanCopyUtils;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.Manufacturer;
import com.bihua.iot.domain.bo.ManufacturerBo;
import com.bihua.iot.domain.vo.ManufacturerVo;
import com.bihua.iot.service.IManufacturerService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 厂商管理
 *
 * @author wbihua
 * @date 2023-07-28
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/manufacturer")
public class ManufacturerController extends BaseController {

    private final IManufacturerService iManufacturerService;

    /**
     * 查询厂商管理列表
     */
    @SaCheckPermission("iot:manufacturer:list")
    @GetMapping("/list")
    public TableDataInfo<ManufacturerVo> list(ManufacturerBo bo, PageQuery pageQuery) {
        return iManufacturerService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出厂商管理列表
     */
    @SaCheckPermission("iot:manufacturer:export")
    @Log(title = "厂商管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ManufacturerBo bo, HttpServletResponse response) {
        List<ManufacturerVo> list = iManufacturerService.queryList(bo);
        ExcelUtil.exportExcel(list, "厂商管理", ManufacturerVo.class, response);
    }

    /**
     * 获取厂商管理详细信息
     *
     * @param mfrId 主键
     */
    @SaCheckPermission("iot:manufacturer:query")
    @GetMapping("/{mfrId}")
    public R<ManufacturerVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long mfrId) {
        return R.ok(iManufacturerService.queryById(mfrId));
    }

    /**
     * 新增厂商管理
     */
    @SaCheckPermission("iot:manufacturer:add")
    @Log(title = "厂商管理", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody ManufacturerBo bo) {
        if (!iManufacturerService.checkMfrNameUnique(BeanCopyUtils.copy(bo, Manufacturer.class))) {
            return R.fail("新增厂商'" + bo.getMfrName() + "'失败，该厂商已存在");
        }
        return toAjax(iManufacturerService.insertByBo(bo));
    }

    /**
     * 修改厂商管理
     */
    @SaCheckPermission("iot:manufacturer:edit")
    @Log(title = "厂商管理", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ManufacturerBo bo) {
        return toAjax(iManufacturerService.updateByBo(bo));
    }

    /**
     * 删除厂商管理
     *
     * @param mfrIds 主键串
     */
    @SaCheckPermission("iot:manufacturer:remove")
    @Log(title = "厂商管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{mfrIds}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] mfrIds) {
        return toAjax(iManufacturerService.deleteWithValidByIds(Arrays.asList(mfrIds), true));
    }
}
