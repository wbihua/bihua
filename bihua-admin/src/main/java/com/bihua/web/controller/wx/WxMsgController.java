package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxMsgBo;
import com.bihua.wx.domain.bo.WxMsgQueryBo;
import com.bihua.wx.domain.bo.WxMsgReplyBo;
import com.bihua.wx.domain.vo.WxMsgVo;
import com.bihua.wx.service.IWxMsgReplyService;
import com.bihua.wx.service.IWxMsgService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.mp.api.WxMpService;

/**
 * 微信消息
 *
 * @author bihua
 * @date 2023-05-23
 */
@Validated
@RequiredArgsConstructor
@RestController
@Slf4j
@RequestMapping("/wx/msg")
public class WxMsgController extends BaseController {

    private final IWxMsgService iWxMsgService;

    private final IWxMsgReplyService iWxMsgReplyService;

    private final WxMpService wxMpService;

    /**
     * 查询微信消息列表
     */
    @SaCheckPermission("wx:msg:list")
    @GetMapping("/list")
    public TableDataInfo<WxMsgVo> list(@CookieValue String appid, WxMsgQueryBo bo, PageQuery pageQuery) {
        bo.setAppid(appid);
        return iWxMsgService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出微信消息列表
     */
    @SaCheckPermission("wx:msg:export")
    @Log(title = "微信消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxMsgQueryBo bo, HttpServletResponse response) {
        List<WxMsgVo> list = iWxMsgService.queryList(bo);
        ExcelUtil.exportExcel(list, "微信消息", WxMsgVo.class, response);
    }

    /**
     * 获取微信消息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("wx:msg:query")
    @GetMapping("/{id}")
    public R<WxMsgVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iWxMsgService.queryById(id));
    }

    /**
     * 新增微信消息
     */
    @SaCheckPermission("wx:msg:add")
    @Log(title = "微信消息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WxMsgBo bo) {
        return toAjax(iWxMsgService.insertByBo(bo));
    }

    /**
     * 修改微信消息
     */
    @SaCheckPermission("wx:msg:edit")
    @Log(title = "微信消息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WxMsgBo bo) {
        return toAjax(iWxMsgService.updateByBo(bo));
    }

    /**
     * 删除微信消息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("wx:msg:remove")
    @Log(title = "微信消息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iWxMsgService.deleteWithValidByIds(Arrays.asList(ids), true));
    }
    /**
     * 回复
     */
    @PostMapping("/reply")
    @SaCheckPermission("wx:msg:save")
    public R reply(@CookieValue String appid,@RequestBody WxMsgReplyBo bo){
        try {
            wxMpService.switchover(appid);
            iWxMsgReplyService.reply(bo.getOpenid(),bo.getReplyType(),bo.getReplyContent());
        } catch (Exception e) {
            log.error("自动回复出错：", e);
        }
        return R.ok();
    }
}
