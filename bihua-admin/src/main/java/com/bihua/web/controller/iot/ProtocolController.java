package com.bihua.web.controller.iot;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.iot.domain.bo.ProtocolBo;
import com.bihua.iot.domain.vo.ProtocolVo;
import com.bihua.iot.service.IProtocolService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 协议信息
 *
 * @author bihua
 * @date 2023-06-19
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/iot/protocol")
public class ProtocolController extends BaseController {

    private final IProtocolService iProtocolService;

    /**
     * 查询协议信息列表
     */
    @SaCheckPermission("iot:protocol:list")
    @GetMapping("/list")
    public TableDataInfo<ProtocolVo> list(ProtocolBo bo, PageQuery pageQuery) {
        return iProtocolService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出协议信息列表
     */
    @SaCheckPermission("iot:protocol:export")
    @Log(title = "协议信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(ProtocolBo bo, HttpServletResponse response) {
        List<ProtocolVo> list = iProtocolService.queryList(bo);
        ExcelUtil.exportExcel(list, "协议信息", ProtocolVo.class, response);
    }

    /**
     * 获取协议信息详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("iot:protocol:query")
    @GetMapping("/{id}")
    public R<ProtocolVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable Long id) {
        return R.ok(iProtocolService.queryById(id));
    }

    /**
     * 新增协议信息
     */
    @SaCheckPermission("iot:protocol:add")
    @Log(title = "协议信息", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Long> add(@Validated(AddGroup.class) @RequestBody ProtocolBo bo) {
        Long id = iProtocolService.insertByBo(bo);
        return ObjectUtil.isNull(id) ? R.fail(id) : R.ok(id);
    }

    /**
     * 修改协议信息
     */
    @SaCheckPermission("iot:protocol:edit")
    @Log(title = "协议信息", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody ProtocolBo bo) {
        return toAjax(iProtocolService.updateByBo(bo));
    }

    /**
     * 删除协议信息
     *
     * @param ids 主键串
     */
    @SaCheckPermission("iot:protocol:remove")
    @Log(title = "协议信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable Long[] ids) {
        return toAjax(iProtocolService.deleteWithValidByIds(Arrays.asList(ids), true));
    }

    /**
     * 启用协议管理
     * @param id
     * @return
     */
    @RepeatSubmit
    @SaCheckPermission("iot:protocol:enable")
    @Log(title = "协议管理", businessType = BusinessType.GRANT)
    @GetMapping("/enable/{id}")
    public R<Void> enable(@PathVariable Long id)
    {
        return toAjax(iProtocolService.enable(id));
    }

    /**
     * 停用协议管理
     * @param id
     * @return
     */
    @RepeatSubmit
    @SaCheckPermission("iot:protocol:disable")
    @Log(title = "协议管理", businessType = BusinessType.GRANT)
    @GetMapping("/disable/{id}")
    public R<Void> disable(@PathVariable Long id)
    {
        return toAjax(iProtocolService.disable(id));
    }

}
