package com.bihua.web.controller.wx;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.bihua.common.core.domain.R;
import com.bihua.wx.domain.bo.MaterialFileBo;
import com.bihua.wx.service.IWxAssetsService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.draft.WxMpDraftArticles;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialArticleUpdate;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialCountResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialFileBatchGetResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialNews;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialNewsBatchGetResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialUploadResult;

/**
 * 微信公众号素材管理
 * 参考官方文档：https://developers.weixin.qq.com/doc/offiaccount/Asset_Management/New_temporary_materials.html
 * 参考WxJava开发文档：https://github.com/Wechat-Group/WxJava/wiki/MP_永久素材管理
 */
@RestController
@RequestMapping("/wx/wxAssets")
@Slf4j
public class WxAssetsManageController {
    @Autowired
    IWxAssetsService wxAssetsService;
    /**
     * 获取素材总数
     *
     * @return
     * @throws WxErrorException
     */
    @GetMapping("/materialCount")
    public R materialCount(@CookieValue String appid) throws WxErrorException {
        WxMpMaterialCountResult res = wxAssetsService.materialCount(appid);
        return R.ok(res);
    }

    /**
     * 获取素材总数
     *
     * @return
     * @throws WxErrorException
     */
    @GetMapping("/materialNewsInfo")
    public R materialNewsInfo(@CookieValue String appid, String mediaId) throws WxErrorException {
        WxMpMaterialNews res = wxAssetsService.materialNewsInfo(appid,mediaId);
        return R.ok(res);
    }


    /**
     * 根据类别分页获取非图文素材列表
     *
     * @param type
     * @param page
     * @return
     * @throws WxErrorException
     */
    @GetMapping("/materialFileBatchGet")
    @SaCheckPermission("wx:wxassets:list")
    public R materialFileBatchGet(@CookieValue String appid, @RequestParam(defaultValue = "image") String type,
                                  @RequestParam(defaultValue = "1") int page) throws WxErrorException {
        WxMpMaterialFileBatchGetResult res = wxAssetsService.materialFileBatchGet(appid,type,page);
        return R.ok(res);
    }

    /**
     * 分页获取图文素材列表
     *
     * @param page
     * @return
     * @throws WxErrorException
     */
    @GetMapping("/materialNewsBatchGet")
    @SaCheckPermission("wx:wxassets:list")
    public R materialNewsBatchGet(@CookieValue String appid, @RequestParam(defaultValue = "1") int page) throws WxErrorException {
        WxMpMaterialNewsBatchGetResult res = wxAssetsService.materialNewsBatchGet(appid,page);
        return R.ok(res);
    }

    /**
     * 添加图文永久素材
     *
     * @param articles
     * @return
     * @throws WxErrorException
     */
    @PostMapping("/materialNewsUpload")
    @SaCheckPermission("wx:wxassets:save")
    public R materialNewsUpload(@CookieValue String appid, @RequestBody List<WxMpDraftArticles> articles) throws WxErrorException {
        if(articles.isEmpty()) {
            return R.fail("图文列表不得为空");
        }
        String res = wxAssetsService.materialNewsUpload(appid,articles);
        return R.ok(res);
    }

    /**
     * 修改图文素材文章
     *
     * @param form
     * @return
     * @throws WxErrorException
     */
    @PostMapping("/materialArticleUpdate")
    @SaCheckPermission("wx:wxassets:save")
    public R materialArticleUpdate(@CookieValue String appid, @RequestBody WxMpMaterialArticleUpdate form) throws WxErrorException {
        if(form.getArticles()==null) {
            return R.fail("文章不得为空");
        }
        wxAssetsService.materialArticleUpdate(appid,form);
        return R.ok();
    }

    /**
     * 添加多媒体永久素材
     *
     * @param file
     * @param fileName
     * @param mediaType
     * @return
     * @throws WxErrorException
     * @throws IOException
     */
    @PostMapping(value = "/materialFileUpload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @SaCheckPermission("wx:wxassets:save")
    public R materialFileUpload(@CookieValue String appid, @RequestPart("file") MultipartFile file, String fileName, String mediaType) throws WxErrorException, IOException {
        if (file == null) {
            return R.fail("文件不得为空");
        }

        WxMpMaterialUploadResult res = wxAssetsService.materialFileUpload(appid,mediaType,fileName,file);
        return R.ok(res);
    }

    /**
     * 删除素材
     *
     * @param bo
     * @return
     * @throws WxErrorException
     */
    @PostMapping("/materialDelete")
    @SaCheckPermission("wx:wxassets:delete")
    public R materialDelete(@CookieValue String appid, @RequestBody MaterialFileBo bo) throws WxErrorException {
        boolean res = wxAssetsService.materialDelete(appid, bo.getMediaId());
        return R.ok(res);
    }

}
