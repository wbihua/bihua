package com.bihua.web.controller.wx;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bihua.common.annotation.Log;
import com.bihua.common.annotation.RepeatSubmit;
import com.bihua.common.core.controller.BaseController;
import com.bihua.common.core.domain.PageQuery;
import com.bihua.common.core.domain.R;
import com.bihua.common.core.page.TableDataInfo;
import com.bihua.common.core.validate.AddGroup;
import com.bihua.common.core.validate.EditGroup;
import com.bihua.common.enums.BusinessType;
import com.bihua.common.utils.poi.ExcelUtil;
import com.bihua.wx.domain.bo.WxAccountBo;
import com.bihua.wx.domain.vo.WxAccountVo;
import com.bihua.wx.service.IWxAccountService;

import cn.dev33.satoken.annotation.SaCheckPermission;
import lombok.RequiredArgsConstructor;

/**
 * 公众号账号
 *
 * @author ruoyi
 * @date 2023-05-16
 */
@Validated
@RequiredArgsConstructor
@RestController
@RequestMapping("/wx/account")
public class WxAccountController extends BaseController {

    private final IWxAccountService iWxAccountService;

    /**
     * 查询公众号账号列表
     */
    @SaCheckPermission("wx:account:list")
    @GetMapping("/list")
    public TableDataInfo<WxAccountVo> list(WxAccountBo bo, PageQuery pageQuery) {
        return iWxAccountService.queryPageList(bo, pageQuery);
    }

    /**
     * 导出公众号账号列表
     */
    @SaCheckPermission("wx:account:export")
    @Log(title = "公众号账号", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(WxAccountBo bo, HttpServletResponse response) {
        List<WxAccountVo> list = iWxAccountService.queryList(bo);
        ExcelUtil.exportExcel(list, "公众号账号", WxAccountVo.class, response);
    }

    /**
     * 获取公众号账号详细信息
     *
     * @param appid 主键
     */
    @SaCheckPermission("wx:account:query")
    @GetMapping("/{appid}")
    public R<WxAccountVo> getInfo(@NotNull(message = "主键不能为空")
                                     @PathVariable String appid) {
        return R.ok(iWxAccountService.queryById(appid));
    }

    /**
     * 新增公众号账号
     */
    @SaCheckPermission("wx:account:add")
    @Log(title = "公众号账号", businessType = BusinessType.INSERT)
    @RepeatSubmit()
    @PostMapping()
    public R<Void> add(@Validated(AddGroup.class) @RequestBody WxAccountBo bo) {
        return toAjax(iWxAccountService.insertByBo(bo));
    }

    /**
     * 修改公众号账号
     */
    @SaCheckPermission("wx:account:edit")
    @Log(title = "公众号账号", businessType = BusinessType.UPDATE)
    @RepeatSubmit()
    @PutMapping()
    public R<Void> edit(@Validated(EditGroup.class) @RequestBody WxAccountBo bo) {
        return toAjax(iWxAccountService.updateByBo(bo));
    }

    /**
     * 删除公众号账号
     *
     * @param appids 主键串
     */
    @SaCheckPermission("wx:account:remove")
    @Log(title = "公众号账号", businessType = BusinessType.DELETE)
    @DeleteMapping("/{appids}")
    public R<Void> remove(@NotEmpty(message = "主键不能为空")
                          @PathVariable String[] appids) {
        return toAjax(iWxAccountService.deleteWithValidByIds(Arrays.asList(appids), true));
    }
}
