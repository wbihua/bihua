package com.bihua.demo.mapper;

import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.demo.domain.TestDemoEncrypt;

/**
 * 测试加密功能
 *
 * @author Lion Li
 */
public interface TestDemoEncryptMapper extends BaseMapperPlus<TestDemoEncryptMapper, TestDemoEncrypt, TestDemoEncrypt> {

}
