package com.bihua.demo.mapper;

import com.bihua.common.annotation.DataColumn;
import com.bihua.common.annotation.DataPermission;
import com.bihua.common.core.mapper.BaseMapperPlus;
import com.bihua.demo.domain.TestTree;
import com.bihua.demo.domain.vo.TestTreeVo;

/**
 * 测试树表Mapper接口
 *
 * @author Lion Li
 * @date 2021-07-26
 */
@DataPermission({
    @DataColumn(key = "deptName", value = "dept_id"),
    @DataColumn(key = "userName", value = "user_id")
})
public interface TestTreeMapper extends BaseMapperPlus<TestTreeMapper, TestTree, TestTreeVo> {

}
